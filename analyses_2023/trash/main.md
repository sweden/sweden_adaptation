---
title: "Sweden adaptation analyses, reboot August 2023"
output:
 html_document:
  keep_md: true
  fig_caption: yes
  toc: true
  toc_float: true
  number_sections: true
  highlight: tango
  theme: space
---

# Sweden adaptation analyses
## Experimental design


```
## ── Attaching core tidyverse packages ──────────────────────── tidyverse 2.0.0 ──
## ✔ dplyr     1.1.2     ✔ readr     2.1.4
## ✔ forcats   1.0.0     ✔ stringr   1.5.0
## ✔ ggplot2   3.4.3     ✔ tibble    3.2.1
## ✔ lubridate 1.9.2     ✔ tidyr     1.3.0
## ✔ purrr     1.0.2     
## ── Conflicts ────────────────────────────────────────── tidyverse_conflicts() ──
## ✖ dplyr::filter()     masks stats::filter()
## ✖ dplyr::group_rows() masks kableExtra::group_rows()
## ✖ dplyr::lag()        masks stats::lag()
## ℹ Use the conflicted package (<http://conflicted.r-lib.org/>) to force all conflicts to become errors
```


### planting and setup

The list of the 203 accessions/genotypes used is presented in acc\_list.txt.
This list includes 200 re-sequenced Swedish accessions,  Edi-0,
Col-Fri and Col-Fri-FLC. Most Swedish accessions were planted in 8
replicates per block and Edi-0, Col-FRI and Col-FRI-FLC, 6043 Lov-1,
6974 Ull2-5, 7517 Var2-6, 8369 Rev-1, 8240 Kulturen-1, 8262 Bil-5,
8247 San-2, 6918 Fab-4 were planted in 16 replicates per block.

Each experiment is organized in a three complete randomized block
design. Plantings followed the calendar presented in table \@ref(tab:expcalendar).


```r
kable(read_csv("data/exp_install_dates.csv", show_col_types = F, na = "NA"), caption = "Calendar followed for the planting of the common garden experiments")%>%
  kable_classic(full_width = F, html_font = "Cambria")
```

<table class=" lightable-classic" style="font-family: Cambria; width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Calendar followed for the planting of the common garden experiments</caption>
 <thead>
  <tr>
   <th style="text-align:right;"> Year </th>
   <th style="text-align:left;"> Experiment </th>
   <th style="text-align:left;"> Block </th>
   <th style="text-align:left;"> planting date </th>
   <th style="text-align:left;"> field installation date </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:left;"> Adal </td>
   <td style="text-align:left;"> A </td>
   <td style="text-align:left;"> 2011-08-08 </td>
   <td style="text-align:left;"> 2011-08-25 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:left;"> Adal </td>
   <td style="text-align:left;"> B </td>
   <td style="text-align:left;"> 2011-08-10 </td>
   <td style="text-align:left;"> 2011-08-25 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:left;"> Adal </td>
   <td style="text-align:left;"> C </td>
   <td style="text-align:left;"> 2011-08-12 </td>
   <td style="text-align:left;"> 2011-08-25 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:left;"> Ramsta </td>
   <td style="text-align:left;"> A </td>
   <td style="text-align:left;"> 2011-08-07 </td>
   <td style="text-align:left;"> 2011-08-24 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:left;"> Ramsta </td>
   <td style="text-align:left;"> B </td>
   <td style="text-align:left;"> 2011-08-09 </td>
   <td style="text-align:left;"> 2011-08-24 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:left;"> Ramsta </td>
   <td style="text-align:left;"> C </td>
   <td style="text-align:left;"> 2011-08-11 </td>
   <td style="text-align:left;"> 2011-08-24 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:left;"> Ullstorp </td>
   <td style="text-align:left;"> A </td>
   <td style="text-align:left;"> 2011-08-31 </td>
   <td style="text-align:left;"> 2011-09-17 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:left;"> Ullstorp </td>
   <td style="text-align:left;"> B </td>
   <td style="text-align:left;"> 2011-09-02 </td>
   <td style="text-align:left;"> 2011-09-17 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:left;"> Ullstorp </td>
   <td style="text-align:left;"> C </td>
   <td style="text-align:left;"> 2011-09-04 </td>
   <td style="text-align:left;"> 2011-09-17 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:left;"> Ratckegarden </td>
   <td style="text-align:left;"> A </td>
   <td style="text-align:left;"> 2011-09-01 </td>
   <td style="text-align:left;"> 2011-09-18 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:left;"> Ratckegarden </td>
   <td style="text-align:left;"> B </td>
   <td style="text-align:left;"> 2011-09-03 </td>
   <td style="text-align:left;"> 2011-09-18 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:left;"> Ratckegarden </td>
   <td style="text-align:left;"> C </td>
   <td style="text-align:left;"> 2011-09-05 </td>
   <td style="text-align:left;"> 2011-09-18 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:left;"> Adal </td>
   <td style="text-align:left;"> A </td>
   <td style="text-align:left;"> 2012-08-08 </td>
   <td style="text-align:left;"> 2012-08-25 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:left;"> Adal </td>
   <td style="text-align:left;"> B </td>
   <td style="text-align:left;"> 2012-08-10 </td>
   <td style="text-align:left;"> 2012-08-25 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:left;"> Adal </td>
   <td style="text-align:left;"> C </td>
   <td style="text-align:left;"> 2012-08-12 </td>
   <td style="text-align:left;"> 2012-08-25 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:left;"> Ramsta </td>
   <td style="text-align:left;"> A </td>
   <td style="text-align:left;"> 2012-08-07 </td>
   <td style="text-align:left;"> 2012-08-24 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:left;"> Ramsta </td>
   <td style="text-align:left;"> B </td>
   <td style="text-align:left;"> 2012-08-09 </td>
   <td style="text-align:left;"> 2012-08-24 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:left;"> Ramsta </td>
   <td style="text-align:left;"> C </td>
   <td style="text-align:left;"> 2012-08-11 </td>
   <td style="text-align:left;"> 2012-08-24 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:left;"> Ullstorp </td>
   <td style="text-align:left;"> A </td>
   <td style="text-align:left;"> 2012-08-31 </td>
   <td style="text-align:left;"> 2012-09-17 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:left;"> Ullstorp </td>
   <td style="text-align:left;"> B </td>
   <td style="text-align:left;"> 2012-09-02 </td>
   <td style="text-align:left;"> 2012-09-17 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:left;"> Ullstorp </td>
   <td style="text-align:left;"> C </td>
   <td style="text-align:left;"> 2012-09-04 </td>
   <td style="text-align:left;"> 2012-09-17 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:left;"> Ratckegarden </td>
   <td style="text-align:left;"> A </td>
   <td style="text-align:left;"> 2012-09-01 </td>
   <td style="text-align:left;"> 2012-09-18 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:left;"> Ratckegarden </td>
   <td style="text-align:left;"> B </td>
   <td style="text-align:left;"> 2012-09-03 </td>
   <td style="text-align:left;"> 2012-09-18 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:left;"> Ratckegarden </td>
   <td style="text-align:left;"> C </td>
   <td style="text-align:left;"> 2012-09-05 </td>
   <td style="text-align:left;"> 2012-09-18 </td>
  </tr>
</tbody>
</table>



### Overview of the phenotypes collected and organisation.

To designate a particular round of experiment we'll use the year it
was sown (not the year it was harvested).

Table \@ref(tab:phencoll2011) and  Table \@ref(tab:phencoll2012). provide a the list of the
phenotypes we have.


```r
kable(read_csv("data/phenotypes_measured_2011.csv", show_col_types = F, na = "NA"), caption = "Table of the phenotypes we have collected in the common garden experiments from 2011.")%>%
  kable_classic(full_width = F, html_font = "Cambria")
```

<table class=" lightable-classic" style="font-family: Cambria; width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Table of the phenotypes we have collected in the common garden experiments from 2011.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> phenotype </th>
   <th style="text-align:left;"> ULL 2011 </th>
   <th style="text-align:left;"> RAT 2011 </th>
   <th style="text-align:left;"> RAM 2011 </th>
   <th style="text-align:left;"> ADA 2011 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> flowering time before winter </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> flowering time in the spring </td>
   <td style="text-align:left;"> . </td>
   <td style="text-align:left;"> . </td>
   <td style="text-align:left;"> . </td>
   <td style="text-align:left;"> . </td>
  </tr>
  <tr>
   <td style="text-align:left;"> herbivore damage in the fall </td>
   <td style="text-align:left;"> . </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> . </td>
   <td style="text-align:left;"> . </td>
  </tr>
  <tr>
   <td style="text-align:left;"> rosette size </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> . </td>
   <td style="text-align:left;"> . </td>
  </tr>
  <tr>
   <td style="text-align:left;"> overwinter survival </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> survival to seed set (Approx) </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fecundity estimate </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> microbial community </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
  </tr>
</tbody>
</table>


```r
kable(read_csv("data/phenotypes_measured_2012.csv", show_col_types = F, na = "NA"), caption = "Table of the phenotypes we have collected in the common garden experiments from 2012.")%>%
  kable_classic(full_width = F, html_font = "Cambria")
```

<table class=" lightable-classic" style="font-family: Cambria; width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Table of the phenotypes we have collected in the common garden experiments from 2012.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> phenotype </th>
   <th style="text-align:left;"> ULL 2012 </th>
   <th style="text-align:left;"> RAT 2012 </th>
   <th style="text-align:left;"> RAM 2012 </th>
   <th style="text-align:left;"> ADA 2012 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> flowering time before winter </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> flowering time in the spring </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> herbivore damage in the fall </td>
   <td style="text-align:left;"> . </td>
   <td style="text-align:left;"> . </td>
   <td style="text-align:left;"> . </td>
   <td style="text-align:left;"> . </td>
  </tr>
  <tr>
   <td style="text-align:left;"> rosette size </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> overwinter survival </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> survival to seed set (Approx) </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fecundity estimate </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> microbial community </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
   <td style="text-align:left;"> X </td>
  </tr>
</tbody>
</table>


## Merging all phenotypes in one file and description

The script merging\_phen\_files_11202014.R was used in April 2014 to
merge all phenotype files into one. I copied that script and updated
the paths. This scripts uses the results from the treatment of each
trait or set of traits from the data folder.



```r
##reading the randomization file.

acc=read.table("./data/acc_list.txt", sep="\t", h=T)


##reading orginal data files for each phenotype

##rosette data (for bothe years):
size=read.table("./data/rosette_size.txt", sep="\t", h=T)
size$experiment=toupper(size$experiment)
size$combi=paste(size$experiment, size$tray, size$row, size$col, sep="_")
color=read.table("./data/color_rosettes.txt", sep="\t", h=T)
color$exp=toupper(color$exp)
color$combi=paste(color$exp, color$tray, color$row, color$col, sep="_")

##I need to keep only one time point per year per experiment in this file.
size=size[(size$date=="20111119" & size$experiment=="ULL")==F,]
size=size[(size$date=="20121120" & size$experiment=="RAT")==F,]
color=color[(color$date=="20111119" & color$exp=="ULL")==F,]
color=color[(color$date=="20121120" & color$exp=="RAT")==F,]

##compare the area column from the color and size data
size$area_color=color$area[match(size$combi, color$combi)]
#plot(size$area, size$area_color, col=as.factor(size$experiment))
##Here the area in pixel number (in color) and the area in cm^2 (size) doesn't have a correlation of one. it might be related to the difference in height at which the pictures were taken (Svante vs. Ben), the number of pixels on the cameras (Mia's Camera vs Nikon D60 later)...

##the stockiness is not in the size data.
size$stockiness=(4*pi*size$area/(size$perimeter^2))

#################################################
#######   2011 experiments   ####################
#################################################

##read the survival data

surv=read.table("./data/survival_2011.txt", h=T, sep="\t")
surv$id=acc$lines[match(surv$line, acc$tubes)]
surv$name=acc$name[match(surv$line, acc$tubes)]

##clean up surv (typos).

surv$spring[surv$spring==11]=1
surv$spring[surv$spring==10]=NA
surv$spring[surv$spring=="?"]=NA
surv$spring[surv$spring=="-"]=NA
surv=droplevels(surv)

##subset size and color to keep only the 2011 data

size2011=size[size$year==2011,]
color2011=color[color$year==2011,]

##read the herbivory data scored in RAT

herb=read.table("./data/rat.snail.2011.csv", sep=",", h=T)[,1:5]

##read the last fecundity dataset for 2011

fecundity=read.table("./data/fecundity_2011.txt", sep="\t", h=T)
##swopping rows and cols for fecundity to be consistent with the other datasets
#x=colnames(fecundity)[c(1:3, 5, 4, 6:ncol(fecundity))]
#colnames(fecundity)=x

##now merge all this, based on surv.

surv$combi=paste(surv$exp, surv$tray, surv$row, surv$column, sep="_")
fecundity$combi=paste(fecundity$exp, fecundity$tray, fecundity$row, fecundity$col, sep="_")
herb$combi=paste("RAT", herb$tray, herb$row, herb$column, sep="_")

##now built the data table.

data2011=surv

##having the planting date in the data would be nice.

dates=data.frame(expand.grid(c("ADA", "RAM", "ULL", "RAT"), c("A", "B", "C")))
dates=dates[order(dates[,1]),]
colnames(dates)=c("exp", "block")
dates$planting=as.Date(c("2011-08-08","2011-08-10", "2011-08-12", "2011-08-07", "2011-08-09", "2011-08-11", "2011-08-31","2011-09-02", "2011-09-4", "2011-09-01", "2011-09-03", "2011-09-05"))
dates$comb=paste(dates$exp, dates$block, sep="_")
comb=paste(data2011$exp,data2011$block, sep="_")
data2011$planting_date=dates$planting[match(comb, dates$comb)]

##match in the phenotypes from size2011
data2011$rosette_date=size2011[match(data2011$combi, size2011$combi),"date"]
data2011$area=size2011[match(data2011$combi, size2011$combi),"area"]
data2011$perimeter=size2011[match(data2011$combi, size2011$combi),"perimeter"]
data2011$max_diameter=size2011[match(data2011$combi, size2011$combi),"max_diameter"]
data2011$sdR=size2011[match(data2011$combi, size2011$combi),"sdR"]
data2011$circle_area=size2011[match(data2011$combi, size2011$combi),"circle_area"]
data2011$stockiness=size2011[match(data2011$combi, size2011$combi),"stockiness"]
##match in the color phenotype
data2011$color=color2011[match(data2011$combi, color2011$combi),"color"]
##match in the herbivory scores
data2011$herbivory=herb[match(data2011$combi, herb$combi),"slug"]
##match in the fecundity estimates
data2011$fecundity=fecundity$fecundity[match(data2011$combi, fecundity$combi)]
##make the rosette_date a working date column.
x=data2011$rosette_date
y=as.Date(paste(substring(x, 1, 4),substring(x, 5, 6), substring(x, 7,8), sep="-"))
data2011$rosette_date=y
##reorder the columns a little
data2011=data2011[,c("exp", "block", "tray", "row", "column", "line", "id", "name", "planting_date", "errors", "fall", "spring", "sampled", "rosette_date", "area", "perimeter", "max_diameter", "sdR", "circle_area", "stockiness","color", "herbivory", "fecundity")]
##save it!
saveRDS(data2011, file="./data/data2011.rds")
write.table(data2011, "./data/data2011.txt", col.names=T, row.names=F, quote=F, sep="\t")


#################################################
#######   2012 experiments   ####################
#################################################

##read the survival data
surv=read.table("./data/survival_2012.txt", h=T, sep="\t")
surv$id=acc$lines[match(surv$line, acc$tubes)]
surv$name=acc$name[match(surv$line, acc$tubes)]

##put it in better shape
surv$combi=paste(surv$exp, surv$tray, surv$row, surv$column, sep="_")
surv$block[surv$tray<=27]="A"
surv$block[surv$tray>=28 & surv$tray<=54]="B"
surv$block[surv$tray>=55]="C"

##subset size to keep only data for the 2012 experiments
size2012=size[size$year==2012,]
color2012=color[color$year==2012,]

##read some flowering time data, from the North (FTN) and the South (FTS)
FTS=read.table("./data/FT_2012_South_dates.txt", sep="\t", h=T)
FTN=read.table("./data/FT_2012_North_dates.txt", sep="\t", h=T)

##read the 2012 fecundity estimates
fecundity=read.table("./data/fecundity_2012.txt", sep="\t", h=T)
##swopping rows and cols for fecundity.
x=colnames(fecundity)
#[c(1:3, 5, 4, 6:ncol(fecundity))]
#colnames(fecundity)=x
fecundity$combi=paste(fecundity$exp, fecundity$tray, fecundity$row, fecundity$col, sep="_")

##combine all data based on surv.

data2012=surv

##clean up the surv column

data2012$survival_03162013[data2012$survival_03162013==11]=1
data2012$survival_03162013[data2012$survival_03162013==","]=NA
data2012=droplevels(data2012)

##add the fall flowering and survival column from the phenotyping Rod did on the images.

for(e in c("ADA", "RAM", "ULL", "RAT")){
if(e=="ADA"){fbw=cbind(e, read.table(paste("./data/fbw_2012_", e, ".txt", sep=""), sep="\t", h=T))}else{fbw=rbind(fbw, cbind(e, read.table(paste("./data/fbw_2012_", e, ".txt", sep=""), sep="\t", h=T)))}
}

fbw$combi=paste(fbw$e, fbw$tray, fbw$row, fbw$column, sep="_")


##clean it up

fbw[fbw$ft_fall=="9","ft_fall"]=0
fbw[fbw$ft_fall=="no photo","ft_fall"]=NA

##use this flowering before winter as a fall column. It's the same as what was done in 2011 but photos were phenotype the same evening for that.
data2012$fall=fbw[match(data2012$combi, fbw$combi),"ft_fall"]

##add planting date

dates=data.frame(expand.grid(c("ADA", "RAM", "ULL", "RAT"), c("A", "B", "C")))
dates=dates[order(dates[,1]),]
colnames(dates)=c("exp", "block")
dates$planting=as.Date(c("2012-08-08","2012-08-10", "2012-08-12", "2012-08-07", "2012-08-09", "2012-08-11", "2012-08-31","2012-09-02", "2012-09-4", "2012-09-01", "2012-09-03", "2012-09-05"))
dates$comb=paste(dates$exp, dates$block, sep="_")
comb=paste(data2012$exp,data2012$block, sep="_")
data2012$planting_date=dates$planting[match(comb, dates$comb)]

##add the rosette data
data2012$rosette_date=size2012[match(data2012$combi, size2012$combi),"date"]
data2012$area=size2012[match(data2012$combi, size2012$combi),"area"]
data2012$perimeter=size2012[match(data2012$combi, size2012$combi),"perimeter"]
data2012$max_diameter=size2012[match(data2012$combi, size2012$combi),"max_diameter"]
data2012$sdR=size2012[match(data2012$combi, size2012$combi),"sdR"]
data2012$circle_area=size2012[match(data2012$combi, size2012$combi),"circle_area"]
data2012$stockiness=size2012[match(data2012$combi, size2012$combi),"stockiness"]
##add the color data
data2012$color=color2012[match(data2012$combi, color2012$combi),"color"]
##add the fecundity data
data2012$fecundity=fecundity$fit[match(data2012$combi, fecundity$combi)]

##add flowering time in the spring data (not available for 2011)
colnames(FTS)[5]="col"
FT=rbind(FTN, FTS)

FT$combi=paste(FT$exp, FT$tray,FT$row, FT$col, sep="_")
data2012$flowering_date=FT[match(data2012$combi, FT$combi),"flowering_date"]
data2012$FT=FT[match(data2012$combi, FT$combi),"FT"]

##make the rosette_date a working date column.

x=data2012$rosette_date
y=as.Date(paste(substring(x, 1, 4),substring(x, 5, 6), substring(x, 7,8), sep="-"))
data2012$rosette_date=y

##reorder col

data2012=data2012[,c("exp", "block", "tray", "row", "column", "line", "id", "name", "planting_date", "errors", "fall", "survival_03162013", "epi", "PC_sampled", "rosette_date", "area", "perimeter", "max_diameter", "sdR", "circle_area", "stockiness", "color", "flowering_date", "FT", "fecundity")]

colnames(data2012)[match("survival_03162013", colnames(data2012))]="spring"

##save it:
saveRDS(data2012, file="./data/data2012.rds")
write.table(data2012, "./data/data2012.txt", col.names=T, row.names=F, quote=F, sep="\t")
```

This results in two files, one for each year. There are named
data\_2011.txt (or .R for the binary version) and data\_2012.txt (or
.R for the binary version).

The column names for each files are summarized below.

### columns in data2011

- exp: name of the experiment  
- block: experimental block within the experiment  
- tray: tray within the experiments  
row: coordinate 1 of position of the plants on a tray (varies from 1
to 11)  
- column: coordinate 2 of position of the plants on a tray (varies from 1
to 6)  
- line: number from 1 to 203 designating the accessions planted  
- id: accession id of the accession as refered to in the call\_method\_75
of the 250 KSNPs data.  
- name: actual name of the accession (might be messed up by encoding,
use ids!!)  
- planting\_date: the planting date. All plants of the same block within
the same experiment have the same date.  
- errors: errors during planting (only use lines with ".")  
- fall: score of survival,flowering, and pathogene infections the
survival code  
- spring: score of survival,flowering, and pathogene infections the
survival code  
- sampled: TRUE if the sample has been sample for microbial community
analysis in the spring.Otherwise FALSE.  
- rosette\_date: Date at which the photographe that was use to make
rosette measurements was taken.  
- area: rosette area on the photograph in cm2  
- perimeter: rosette perimiter (cm)  
- max\_diameter: maximum diameter of the plant.  
- sdR: standard deviation of the plants radius  
- circle\_area: area of a circle of diameter "max\_diameter"  
- stockiness: measure of plant stockiness: (4*pi*area)/(perimeter)^2  
- color: a measure of color variation from green to purple. lower values
are greener.  
- herbivory: herbivore damage score from 0 to 3, 0: no damage, 3:
extensive damage.  
- fecundity: area of occupied by the mature plant stems (number of
pixels of the image that are the plant, not the area of the bounding
box) as a proportion of the total number of pixel on the image.  


### columns for data2012 (only columns that are different):

- epi: sampled by Fernando and Manu for RNASeq.  
- PC\_sampled: TRUE if the sample has been sample for microbial community
analysis in the spring.Otherwise FALSE.  
- flowering\_date: the date at which the plant was scored as flowered (if
reading from the text file, in R,  as.Date() will turn it to a date
format.  
- FT: time from planting to the date the plant was scored as
  flowered.  


### Add some derived phenotype columns and clean up

This next chunck of script does some cleaning up, and compute derive
phenotypes such as ow, sss, fitness (composite of fecundity and sss).


```r
read_chunk("./scripts/deriv_phen.R")
```
#### rational

In Both 2011 and 2012 we have scored survival in the spring and we
have harvested stems at the end of the plants life cycle.
We also have information about flowering time before winter for both
year.
I'll try to create to survival related columns in the data. The first
is going to be overwinter survival (ows). For ows our data are
relatively precise because we have survival/flowering scores before
and after winter. The second survival phenotype is going to be
survival until seed set (sss). This is will include the plants that
survived over-winter, plus the plants we don't have stem flowering
data and stem photos for. This is likely to be a little
unprecise. First we are missing some stems, because some were
missing, we disgarded some while taking the photos when they were in
such a state a photo wasn't meaningful. However that's probably quite
rare over the number of stems we photographed. Second, some dead
plant might have "flown" away from the experiments before we
harvested them. That again should only be a small
fraction. Nevertheless, we should keep those limitations in mind.

#### Over-winter survival (ows)



```r
df=data.frame(plant_status=c("dead", "alive", "flowering", "dead but flowered before winter","flowered before winter but alive", "flowered before winter and flowering again","death by snow mold " ), scoring=0:6)
kable(df, caption="Description of survival scoring, in particular how the plants that flowered before winter were scored")%>%
  kable_classic(full_width = F, html_font = "Cambria")
```

<table class=" lightable-classic" style="font-family: Cambria; width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Description of survival scoring, in particular how the plants that flowered before winter were scored</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> plant_status </th>
   <th style="text-align:right;"> scoring </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> dead </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> alive </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> flowering </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> dead but flowered before winter </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> flowered before winter but alive </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> flowered before winter and flowering again </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> death by snow mold </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
</tbody>
</table>

To considered a plant has survived winter (ows=1) it needs to have been alive in the last sensus in the fall and be alive in the spring. Following table \@ref{tab:survivalscores},that means it must have a value of 1, 2, 4, or 5 in the fall and spring column. Whether it was sampled or not doesn't matter in 2011
because the only sampling we did was in the spring after the
survival/flowering sensus was made. To be considered having not
survived winter, plants must have been present in the last sensus in
the fall, but be dead or absent in the spring. 
For the 2012, data, it's basically the same except we beed to
take into account the epi genetics sampling (epi column). This
sampling happened in Oct or November.##


#### Survival until seed set (sss).
That I\'ll define as surviving until seed set (sss) as having survived
over winter and having data in the fecundity column. ##A0004##
They must not have been harvested for micorbiom


```r
##add the sss column
##A0004##
d11$sss=NA
d11$sss[d11$fall%in%c(1, 2, 4, 5) & is.na(d11$fecundity)==F & d11$sampled==F]=1
d11$sss[d11$fall%in%c(1, 2, 4, 5) & is.na(d11$fecundity)==T  & d11$sampled==F]=0
d12$sss=NA
d12$sss[d12$fall%in%c(1, 2,  4, 5) & is.na(d12$fecundity)==F & d12$PC_sampled==F & d12$epi==F]=1
d12$sss[d12$fall%in%c(1, 2,  4, 5) & is.na(d12$fecundity)==T  & d12$PC_sampled==F & d12$epi==F]=0
```

#### Checking if it makes sens.
I want to know how many of the plants that are scored as having died,
have produced a stem that we have (Table \ref{counts}). 
Basically there are only a few plants died.


```r
###Do some checking

t1=table(d11[,c("ows", "sss")], exclude=NULL)
t2=table(d12[, c("ows", "sss")], exclude=NULL)

kable(data.frame(t1), caption="Number of data points for each value of ows and sss in the 2011 experiments")
```

<table>
<caption>Number of data points for each value of ows and sss in the 2011 experiments</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> ows </th>
   <th style="text-align:left;"> sss </th>
   <th style="text-align:right;"> Freq </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 1860 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 1588 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 1011 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 166 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 11305 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 33 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> 3276 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> 2144 </td>
  </tr>
</tbody>
</table>


```r
kable(data.frame(t2), caption="Number of data points for each value of ows and sss in the 2012 experiments")
```

<table>
<caption>Number of data points for each value of ows and sss in the 2012 experiments</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> ows </th>
   <th style="text-align:left;"> sss </th>
   <th style="text-align:right;"> Freq </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 245 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 1174 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 20 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 10263 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> 4437 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> 5245 </td>
  </tr>
</tbody>
</table>
There are rosette and fitness data for a few wells that were supposed
to be empty. If there weren't empty in the fall, then a subset of them
got rosette data. It therefore looks like it's a combination of
scoring mistakes and a few contaminations. ##A0006##
Those lines will be removed from both datasets. ##A0007##

```r
##removing theoritically "empty" pots from the datasets ##A0007##

d11=droplevels(d11[d11$line!="empty",])
d12=droplevels(d12[d12$line!="empty",])

t1=data.frame(table(ows=d11$ows, sss=d11$sss))
t2=data.frame(table(ows=d12$ows, sss=d12$sss))

kable(t1,caption="overwinter survival and survival to seed set in 2012") 
```

<table>
<caption>overwinter survival and survival to seed set in 2012</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> ows </th>
   <th style="text-align:left;"> sss </th>
   <th style="text-align:right;"> Freq </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 1854 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 1587 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 166 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 11295 </td>
  </tr>
</tbody>
</table>

```r
kable(t2,caption="overwinter survival and survival to seed set in 2013")
```

<table>
<caption>overwinter survival and survival to seed set in 2013</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> ows </th>
   <th style="text-align:left;"> sss </th>
   <th style="text-align:right;"> Freq </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 202 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 1158 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 19 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 10255 </td>
  </tr>
</tbody>
</table>

```r
d11=d11[(d11$ows==0 & d11$sss==1)==F,]
d12=d12[(d12$ows==0 & d12$sss==1)==F,]

t1=data.frame(table(ows=d11$ows, sss=d11$sss))
t2=data.frame(table(ows=d12$ows, sss=d12$sss))

kable(t1,caption="overwinter survival and survival to seed set in 2012, after clean-up") 
```

<table>
<caption>overwinter survival and survival to seed set in 2012, after clean-up</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> ows </th>
   <th style="text-align:left;"> sss </th>
   <th style="text-align:right;"> Freq </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 1854 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 1587 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 11295 </td>
  </tr>
</tbody>
</table>

```r
kable(t2,caption="overwinter survival and survival to seed set in 2013, after clean-up")
```

<table>
<caption>overwinter survival and survival to seed set in 2013, after clean-up</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> ows </th>
   <th style="text-align:left;"> sss </th>
   <th style="text-align:right;"> Freq </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 202 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:right;"> 1158 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 0 </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 10255 </td>
  </tr>
</tbody>
</table>

#### cleaning up rosette data

The number of data points is presented in table \ref{rosettes1}. ##A0008##


```r
##A0008##
kable(table(d11[is.na(d11$area)==F,"exp"]), caption="number of rosettes measured per experiment in 2011. Keeping in mind that this dataset only includes one date but others may be available (see specific org file for analysis of the rosette data). In 2011 we didn't get rosette data because plants grew too big and were overlapping too much for photo analysis.")
```

<table>
<caption>number of rosettes measured per experiment in 2011. Keeping in mind that this dataset only includes one date but others may be available (see specific org file for analysis of the rosette data). In 2011 we didn't get rosette data because plants grew too big and were overlapping too much for photo analysis.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> Var1 </th>
   <th style="text-align:right;"> Freq </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> RAT </td>
   <td style="text-align:right;"> 3349 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> ULL </td>
   <td style="text-align:right;"> 4695 </td>
  </tr>
</tbody>
</table>

```r
kable(table(d12[is.na(d12$area)==F,"exp"]), caption="number of rosettes measured per experiment in 2012. Keeping in mind that this dataset only includes one date but others may be available (see specific org file for analysis of the rosette data). In 2011 we didn't get rosette data because plants grew too big and were overlapping too much for photo analysis.")
```

<table>
<caption>number of rosettes measured per experiment in 2012. Keeping in mind that this dataset only includes one date but others may be available (see specific org file for analysis of the rosette data). In 2011 we didn't get rosette data because plants grew too big and were overlapping too much for photo analysis.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> Var1 </th>
   <th style="text-align:right;"> Freq </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> ADA </td>
   <td style="text-align:right;"> 3192 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> RAM </td>
   <td style="text-align:right;"> 3936 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> RAT </td>
   <td style="text-align:right;"> 3832 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> ULL </td>
   <td style="text-align:right;"> 3633 </td>
  </tr>
</tbody>
</table>



```r
kable(read_csv("data/rosette_counts2.csv", show_col_types = F), caption="Number of rosettes measured per experiment and per year. Keeping in mind that this dataset only includes one date but others may be available (see specific org file for analysis of the rosette data). In 2011 we didn't get rosette data because plants grew too big and were overlapping too much for photo analysis.")%>%kable_classic(full_width = T, html_font = "Cambria")
```

<table class=" lightable-classic" style="font-family: Cambria; margin-left: auto; margin-right: auto;">
<caption>Number of rosettes measured per experiment and per year. Keeping in mind that this dataset only includes one date but others may be available (see specific org file for analysis of the rosette data). In 2011 we didn't get rosette data because plants grew too big and were overlapping too much for photo analysis.</caption>
 <thead>
  <tr>
   <th style="text-align:right;"> exp </th>
   <th style="text-align:right;"> ADA </th>
   <th style="text-align:right;"> RAM </th>
   <th style="text-align:right;"> RAT </th>
   <th style="text-align:right;"> ULL </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 3424 </td>
   <td style="text-align:right;"> 4709 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:right;"> 3958 </td>
   <td style="text-align:right;"> 4901 </td>
   <td style="text-align:right;"> 3851 </td>
   <td style="text-align:right;"> 3705 </td>
  </tr>
</tbody>
</table>

There shouldn't be rosette data if the fall sensus indicates 0, 3, or
5 (see table \@ref{tab:survivalscores}), however there are a few (table \@ref{tab:rosettescounts2}). These can arise from errors in the sensus, or
errors in the image analysis. The most conservative way to deal with
that is probably to remove those lines.




```r
kable(read_csv("data/rosette_counts.csv", show_col_types = F), caption="Counts of rosettes measured that were not scored as alive in the fall sensus. These data points were removed from the data.")%>%kable_classic(full_width = F, html_font = "Cambria")
```

<table class=" lightable-classic" style="font-family: Cambria; width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Counts of rosettes measured that were not scored as alive in the fall sensus. These data points were removed from the data.</caption>
 <thead>
  <tr>
   <th style="text-align:right;"> year </th>
   <th style="text-align:right;"> ADA </th>
   <th style="text-align:right;"> RAM </th>
   <th style="text-align:right;"> RAT </th>
   <th style="text-align:right;"> ULL </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 9 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 65 </td>
  </tr>
</tbody>
</table>


```r
##A0009##
d11=d11[(is.na(d11$area)==F & d11$fall%in%c(1, 2, 4, 5)==F)==F,]
d12=d12[(is.na(d12$area)==F & d12$fall%in%c(1, 2, 4, 5)==F)==F,]
```

I also added a column giving the age of the rosettes when photographed. 


```r
d11$age_rosettes=as.Date(d11$rosette_date)-as.Date(d11$planting_date)
d12$age_rosettes=as.Date(d12$rosette_date)-as.Date(d12$planting_date)
```

#### adding a "fitness" trait.

This trait is going to correspond to the fecundity estimates, but the
NAs will be replaced by 0s when sss==0.


```r
##setting a "fitness" column
d11$fitness=d11$fecundity
d11$fitness[d11$sss==0 & is.na(d11$sss)==F]=0

d12$fitness=d12$fecundity
d12$fitness[d12$sss==0 & is.na(d12$sss)==F]=0
```

#### dealing with the Controls.


For now I've decided to remove the controls from the dataset. I can
bring them in as needed. I'll save them in a seperate file.

I'll also remove all lines saying "empty" in the column line or
anything else than "." in the error column. 

I'll also remove the none Swedish accessions with line numbers 201
(Edi), 202 (Col-FRI) and 203 (Col-FRI-FLC).


```r
##A0012##
c11=d11[d11$line%in%c("C1", "C2"),]
d11=d11[d11$line!="empty",]
d11=d11[d11$line%in%c("C1", "C2")==F,]
d11=d11[d11$error==".",]

c12=d12[d12$line%in%c("C1", "C2"),]
d12=d12[d12$line!="empty",]
d12=d12[d12$line%in%c("C1", "C2")==F,]
##quick fix of the error column in the 2012 data.
d12$error[d12$error==""]="."
d12$error[d12$error!="."]="error"
##and finish removing lines with errors
d12=droplevels(d12[d12$error==".",])

##removing Edi-0, Col-FRI-FLC, Col-FRI
##A0017##
d11=d11[d11$line%in%c("201", "202", "203")==F,]
d12=d12[d12$line%in%c("201", "202", "203")==F,]
```

### Save the clean datasets


```r
##save a final cleaned up version of the data

saveRDS(d11, file="./data/d11_for_analysis.rds")
saveRDS(d12, file="./data/d12_for_analysis.rds")
```


## Prepare the SNP data

The SNP data used are from a VCF file, generated by F. Rabanal, and used in Brachi et al. 2022.

The following prepares the plink files needed to run GWAs in rMVP.


```sh
sh scripts/prep_SNPs.sh
```

## Generate the kinship matrix


```r
library(rMVP)

prefix=paste("data/snps/bed/sweden_200_MAF10", sep="")
system(paste("mkdir data/snps/rMVP"))
out="data/snps/rMVP/sweden_200_MAF10"

MVP.Data(fileBed=prefix,
         filePhe=NULL,
         fileKin=FALSE,
         filePC=FALSE,
         #priority="speed",
         #maxLine=10000,
         out=out
)

##calculate kinship

MVP.Data.Kin(TRUE, mvp_prefix=out, out=out)
```



















# Compare K from Gemma and from rMVP


```r
Kgemma=read.table(file = "../data.for.paper/K_200_MAF10.cXX.txt", h=F)
acc=read.table(file="../data.for.paper/sweden_200_MAF10.bimbam.acclist")

Kgemma=as.matrix(Kgemma)
row.names(Kgemma)=acc[,1]
colnames(Kgemma)=acc[,1]

Krmvp <- attach.big.matrix("data/snps/rMVP/sweden_200_MAF10.kin.desc")
acc=read.table("data/snps/rMVP/sweden_200_MAF10.geno.ind", h=F)
Krmvp=as.matrix(Krmvp)
row.names(Krmvp)=acc[,1]
colnames(Krmvp)=acc[,1]

#matrices are in the same order. 
all(row.names(Krmvp)==row.names(Kgemma))
```

```
## [1] TRUE
```

Now compare the two matrices.


```r
data.frame(Kgemma=as.vector(Kgemma[upper.tri(Kgemma)]), Krmvp=as.vector(Krmvp[upper.tri(Krmvp)]))%>%ggplot(aes(x=Kgemma, y=Krmvp))+geom_point()
```

![](main_files/figure-html/unnamed-chunk-14-1.png)<!-- -->
The conclusion is that the two matrices are extremely similar. 

I'll use Krmvp for consistency with GWAs. 


```r
K=Krmvp
```

# Cluster accessions into groups. 

Daniele and I have different ways of computing the distances. 


```r
p2=data.frame(BB=as.dist(-1*K), DF=dist(K))%>%ggplot(aes(BB, DF))+geom_hex()
p2
```

```
## Don't know how to automatically pick scale for object of type <dist>.
## Defaulting to continuous.
## Don't know how to automatically pick scale for object of type <dist>.
## Defaulting to continuous.
```

![](main_files/figure-html/unnamed-chunk-16-1.png)<!-- -->
I'll go with mine, but will compare with df's. 


```r
gdm=-1*as.matrix(K)
gd=as.dist(gdm)
k.clust=hclust(gd)
k.clust.df=hclust(dist(Kgemma))
```

## Choosing a number of clusters


```r
#gdm=gdm[k.clust$order,k.clust$order ]
maxclust=50
res=data.frame(nclust=2:maxclust)

mgd=as.matrix(gd); diag(mgd)=NA; mgd[lower.tri(mgd)]=NA
vmgd=na.omit(as.vector(mgd))

res2=data.frame(gd=vmgd)
clusts=data.frame(id=labels(gd))

mingd=min(gd)
for (nc in 2:maxclust){
  #clusters=pam(x=gd, diss=T, k=6)$clustering
   clusters=cutree(k.clust, k=nc) 
   clusters=clusters[colnames(gdm)]
   clusts[, paste("K_",sprintf("%02d",nc),sep="")]=paste(clusters[labels(gd)])
  #as.integer(hcut(dist(K), k=nc, method="hclust")$cluster)
  mb=matrix(NA, ncol=ncol(mgd), nrow=nrow(mgd))
  mw=matrix(NA, ncol=ncol(mgd), nrow=nrow(mgd))
  for(i in 1:nc){
    for(j in 1:nc){
      if(i==j){mw[clusters==i, clusters==j]=mgd[clusters==i, clusters==j]
        mw[clusters==j, clusters==i]=mgd[clusters==i, clusters==j]}
      if(i!=j){
        mb[clusters==i, clusters==j]=mgd[clusters==i, clusters==j]
        mb[clusters==j, clusters==i]=mgd[clusters==i, clusters==j]}
    }
  }
  diag(mb)=NA; mb[lower.tri(mb)]=NA
  diag(mw)=NA; mw[lower.tri(mw)]=NA
  bd=mean(mb, na.rm = T)  
  wd=mean(mw, na.rm = T)  
  # diag(m)=NA; m[lower.tri(m)]=NA
  # vm=na.omit(as.vector(m))
  # res2[, paste("K_", sprintf("%02d",nc), sep="")]=vm
  res[nc-1, "diff"]=diff(range(mw, na.rm=T))/diff(range(mgd, na.rm=T))
  res[nc-1, "mw"]=mean(mw, na.rm=T)
  res[nc-1, "mb"]=mean(mb, na.rm=T)
  res[nc-1, "diffm"]=mean(mw, na.rm=T)-mean(mb, na.rm=T)
}

res[, "der"]=c(0, diff(res$diffm))

p1=ggplot(res, aes(x=nclust, y=mw))+geom_point(col="blue")+geom_line()+
  geom_point(data=res, aes(x=nclust, y=mb), col="red")+geom_line(aes(x=nclust, y=mb))+
  geom_point(data=res, aes(x=nclust, y=diffm), col="green")+geom_line(aes(x=nclust, y=diffm))+
  geom_point(data=res, aes(x=nclust, y=der), col="grey70")+geom_line(aes(x=nclust, y=der))
p2=p1+scale_x_continuous(limits = c(2,10), breaks=1:10, labels=1:10)
ggarrange(p1, p2)
```

```
## Warning: Removed 40 rows containing missing values (`geom_point()`).
```

```
## Warning: Removed 40 rows containing missing values (`geom_line()`).
```

```
## Warning: Removed 40 rows containing missing values (`geom_point()`).
```

```
## Warning: Removed 40 rows containing missing values (`geom_line()`).
```

```
## Warning: Removed 40 rows containing missing values (`geom_point()`).
```

```
## Warning: Removed 40 rows containing missing values (`geom_line()`).
```

```
## Warning: Removed 40 rows containing missing values (`geom_point()`).
```

```
## Warning: Removed 40 rows containing missing values (`geom_line()`).
```

![](main_files/figure-html/unnamed-chunk-18-1.png)<!-- -->

## Compare six clusters my way vs. DF


```r
clim=read.table("./data/worldclim_swedish_acc.txt", sep="\t", h=T, stringsAsFactor=F )
#clim_exp=read.table("./data/worldclim_swedish_exp.txt", sep="\t", h=T)

acc<-clim%>%mutate(id=paste(lines),lon=long)%>%dplyr::select("id","region","lon","lat")%>%left_join(y = clusts, by="id")

##Add Daniel's way of clustering

acc$K_DF=paste(cutree(k.clust.df, k = 6)[paste(acc$id)])

table(acc$K_06, acc$region)
```

```
##    
##     C Sweden N Sweden S Sweden
##   1        0        0       33
##   2        0        0       22
##   3       11        2       56
##   4        7        2        0
##   5        0       51        0
##   6        0        0       16
```

```r
table(acc$K_07, acc$region)
```

```
##    
##     C Sweden N Sweden S Sweden
##   1        0        0       33
##   2        0        0       22
##   3       11        2       31
##   4        7        2        0
##   5        0       51        0
##   6        0        0       25
##   7        0        0       16
```

```r
table(acc$K_DF, acc$region)
```

```
##    
##     C Sweden N Sweden S Sweden
##   1       11        2       91
##   2        0        0       25
##   3        7        2        5
##   4        0       38        0
##   5        0       13        0
##   6        0        0        6
```


```r
table(acc$K_06, acc$K_DF)
```

```
##    
##      1  2  3  4  5  6
##   1 31  2  0  0  0  0
##   2  0 22  0  0  0  0
##   3 68  1  0  0  0  0
##   4  0  0  9  0  0  0
##   5  0  0  0 38 13  0
##   6  5  0  5  0  0  6
```

Compared to 6, 7 clusters finds another cluster of southern accessions that was in a mixed group. 

Save this table. 


```r
saveRDS(acc, "./res/acc_clusters.rds")
write.table(acc, "./res/acc_clusters.txt", sep="\t", row.names=F, col.names=T, quote=F)
```

# Make maps


```r
library(mapview)
```

```
## The legacy packages maptools, rgdal, and rgeos, underpinning the sp package,
## which was just loaded, will retire in October 2023.
## Please refer to R-spatial evolution reports for details, especially
## https://r-spatial.org/r/2023/05/15/evolution4.html.
## It may be desirable to make the sf package available;
## package maintainers should consider adding sf to Suggests:.
## The sp package is now running under evolution status 2
##      (status 2 uses the sf package in place of rgdal)
```

```r
library(sp)
library(leafpop)

loc=acc

coordinates(loc) <- ~lon+lat
proj4string(loc) <- CRS("+proj=longlat +datum=WGS84")
#head(loc2)

mylabel <- glue::glue("{loc$id}/{loc$region}/{loc$K_06}") %>%
  lapply(htmltools::HTML)
```


## map of my clustering with K=6


```r
m=mapview(loc, map.types="Esri.WorldImagery", label=mylabel, zcol="K_06", size=10, popup = popupTable(loc, c("id", "region", "K_06", "K_DF")))

m
```

```{=html}
<div class="leaflet html-widget html-fill-item-overflow-hidden html-fill-item" id="htmlwidget-88536f9d7d792214d776" style="width:672px;height:480px;"></div>
<script type="application/json" data-for="htmlwidget-88536f9d7d792214d776">{"x":{"options":{"minZoom":1,"maxZoom":52,"crs":{"crsClass":"L.CRS.EPSG3857","code":null,"proj4def":null,"projectedBounds":null,"options":{}},"preferCanvas":false,"bounceAtZoomLimits":false,"maxBounds":[[[-90,-370]],[[90,370]]]},"calls":[{"method":"addProviderTiles","args":["Esri.WorldImagery","Esri.WorldImagery","Esri.WorldImagery",{"errorTileUrl":"","noWrap":false,"detectRetina":false,"pane":"tilePane"}]},{"method":"createMapPane","args":["point",440]},{"method":"addCircleMarkers","args":[[55.3833,55.3833,55.3833,55.7167,55.7167,55.7167,56.7,59.4333,59.4333,59.5667,59.5667,59.7833,62.8,63.0833,65.25,55.3838,56.3333,56.3333,56.3333,63.0167,55.76,55.76,62.877,62.877,62.877,62.877,62.9,62.9,56.06,56.06,56.06,55.7509,62.6437,62.806,56.1,56.1,56.1,56.1,56.1,55.66,56.0328,56.09,62.801,62.9513,62.9513,62.9308,56.1481,55.7097,58.9,55.6525,55.6514,55.6494,55.6481,55.6561,55.6,55.7,55.7931,55.7989,55.7936,55.7989,55.7967,55.8097,55.8,55.7,55.8364,55.8378,55.8411,55.8428,55.8544,55.8369,55.8364,55.8383,55.9336,55.9403,55.9392,55.9428,55.9497,55.9319,55.9281,55.6528,62.6422,62.6425,62.6425,62.8714,62.8717,62.8717,62.8719,62.6322,62.6322,62.8892,55.7683,55.7686,55.7689,55.7692,55.7694,55.7706,55.7708,55.7714,55.7708,55.7719,55.7717,55.7714,62.8836,62.8839,63.0169,63.0172,62.806,62.806,62.96,62.9611,62.9619,62.9619,62.9622,62.9614,55.7,55.95,62.9169,55.5796,55.5796,55.5796,55.5796,56.06,62.877,63.0165,56.3,56.0648,55.58,56.1509,56.0328,62.7989,56.68,56.3,55.8,55.9473,56.1494,57.7,56.4,56.4,56.4,55.76,56.1,56.1,56.0328,55.71,55.71,60.25,58.9,59,56.06,56.1,56.06,57.75,62.8622,62.8622,62.8698,62.8794,57.7133,57.3089,57.2608,62.8762,62.9147,57.6781,55.7488,55.8106,55.8106,62.806,62.806,57.3263,57.3263,55.4234,55.7491,55.7491,55.7491,56.047,56.0573,57.2746,57.2746,57.7215,55.9745,62.8815,62.9513,62.8959,56.1633,56.1633,55.5678,57.2545,57.2545,57.2545,57.8009,57.8009,57.6511,55.5796,55.4242,62.801,63.324,56.0648,55.58,55.705,56.07,55.6942],[14.05,14.05,14.05,14.1333,14.1333,14.1333,16.5167,17.0167,17.0167,16.8667,16.8667,17.5833,18.2,18.3667,15.6,14.0612,15.9667,15.9667,15.9667,17.4914,14.12,14.12,18.177,18.177,18.177,18.177,18.4,18.4,14.29,14.29,14.29,13.3712,17.7339,18.1896,13.74,13.74,13.74,13.74,13.74,13.4,14.775,13.9,18.079,18.2763,18.2763,18.3448,15.8155,13.2145,11.2,13.2197,13.2233,13.2147,13.2264,13.2178,13.2,13.2,13.1186,13.1206,13.1233,13.1219,13.1044,13.1342,13.1367,13.2,13.3075,13.3092,13.3047,13.3058,13.2875,13.3181,13.2906,13.2906,13.5519,13.5511,13.5539,13.5558,13.5533,13.5508,13.5481,13.2244,17.7406,17.7356,17.7372,18.3447,18.3444,18.3419,18.3422,17.69,17.6906,18.4522,14.1386,14.1383,14.1375,14.1369,14.1347,14.1342,14.1342,14.1333,14.1331,14.1211,14.1206,14.1208,18.1842,18.1836,18.3283,18.3283,18.1896,18.1896,18.2844,18.3589,18.35,18.35,18.35,18.3608,13.2,13.85,18.4728,14.3336,14.3336,14.3336,14.3336,13.97,18.177,18.3174,16,13.9707,14.334,15.7735,14.775,17.9103,16.5,16,13.1,13.821,15.7884,15.8,12.9,12.9,12.9,14.12,13.74,13.74,14.775,13.2,13.2,18.37,11.2,18,14.29,13.74,13.97,16.6333,18.336,18.336,18.381,18.4473,15.0689,18.1512,16.3675,18.1746,18.4045,14.9986,13.3742,14.2091,14.2091,18.1896,18.1896,15.8979,15.8979,13.9905,13.399,13.399,13.399,13.9519,14.302,16.1494,16.1494,18.3837,14.3997,18.4055,18.2763,18.3659,14.6806,14.6806,14.3398,18.2109,18.2109,18.2109,18.5162,18.5162,14.8043,14.3336,13.8484,18.079,18.484,13.9707,14.334,13.196,13.74,13.4504],6,null,"loc - K_06",{"crs":{"crsClass":"L.CRS.EPSG3857","code":null,"proj4def":null,"projectedBounds":null,"options":{}},"pane":"point","stroke":true,"color":"#333333","weight":1,"opacity":[0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9],"fill":true,"fillColor":["#008298","#008298","#008298","#4B0055","#274983","#4B0055","#008298","#00B28A","#00B28A","#00B28A","#00B28A","#00B28A","#008298","#7ED357","#00B28A","#008298","#008298","#4B0055","#4B0055","#7ED357","#008298","#008298","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#008298","#008298","#008298","#274983","#7ED357","#7ED357","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#274983","#4B0055","#008298","#7ED357","#7ED357","#7ED357","#7ED357","#008298","#008298","#00B28A","#008298","#008298","#4B0055","#008298","#008298","#008298","#274983","#008298","#008298","#008298","#008298","#008298","#008298","#008298","#274983","#274983","#008298","#008298","#008298","#274983","#274983","#274983","#008298","#274983","#274983","#274983","#274983","#274983","#274983","#274983","#4B0055","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#008298","#7ED357","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#008298","#4B0055","#4B0055","#4B0055","#008298","#008298","#4B0055","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#7ED357","#008298","#008298","#7ED357","#FDE333","#4B0055","#FDE333","#4B0055","#008298","#7ED357","#7ED357","#008298","#008298","#FDE333","#008298","#008298","#7ED357","#008298","#008298","#008298","#008298","#008298","#008298","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#008298","#008298","#274983","#00B28A","#008298","#008298","#008298","#4B0055","#4B0055","#FDE333","#7ED357","#7ED357","#7ED357","#00B28A","#008298","#FDE333","#008298","#7ED357","#7ED357","#008298","#274983","#FDE333","#FDE333","#7ED357","#7ED357","#008298","#008298","#4B0055","#274983","#274983","#274983","#008298","#008298","#008298","#008298","#FDE333","#008298","#7ED357","#7ED357","#7ED357","#008298","#008298","#FDE333","#FDE333","#FDE333","#FDE333","#FDE333","#FDE333","#008298","#FDE333","#4B0055","#7ED357","#7ED357","#008298","#FDE333","#008298","#008298","#274983"],"fillOpacity":[0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6]},null,null,["<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>1&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>991&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>2&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>992&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>3&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>997&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>4&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1061&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>5&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1062&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>6&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1063&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>7&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1158&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>8&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1254&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>9&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1257&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>10&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1317&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>11&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1318&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>12&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1363&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>13&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1435&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>14&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1552&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>15&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1585&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>16&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5829&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>17&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5830&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>18&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5831&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>19&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5832&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>20&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5856&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>21&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5865&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>22&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5867&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>23&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6009&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>24&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6011&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>25&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6012&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>26&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6013&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>27&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6016&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>28&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6017&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>29&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6019&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>30&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6020&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>31&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6021&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>32&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6023&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>33&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6025&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>34&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6030&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>35&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6034&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>36&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6035&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>37&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6036&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>38&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6038&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>39&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6039&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>40&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6040&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>41&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6041&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>42&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6042&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>43&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6046&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>44&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6064&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>45&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6069&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>46&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6071&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>47&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6073&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>48&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6085&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>49&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6086&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>50&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6090&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>51&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6092&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>52&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6094&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>53&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6097&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>54&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6098&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>55&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6102&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>56&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6104&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>57&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6106&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>58&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6108&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>59&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6109&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>60&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6111&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>61&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6112&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>62&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6114&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>63&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6115&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>64&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6118&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>65&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6122&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>66&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6124&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>67&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6126&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>68&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6127&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>69&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6129&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>70&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6131&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>71&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6133&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>72&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6134&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>73&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6136&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>74&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6138&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>75&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6140&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>76&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6142&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>77&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6145&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>78&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6148&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>79&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6149&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>80&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6151&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>81&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6154&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>82&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6163&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>83&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6166&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>84&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6169&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>85&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6171&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>86&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6173&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>87&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6174&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>88&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6177&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>89&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6180&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>90&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6184&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>91&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6188&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>92&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6189&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>93&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6191&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>94&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6192&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>95&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6193&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>96&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6194&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>97&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6195&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>98&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6197&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>99&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6198&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>100&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6201&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>101&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6202&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>102&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6203&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>103&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6209&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>104&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6210&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>105&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6217&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>106&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6218&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>107&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6220&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>108&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6221&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>109&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6231&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>110&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6235&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>111&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6237&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>112&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6238&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>113&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6240&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>114&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6241&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>115&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6242&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>116&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6243&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>117&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6244&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>118&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6252&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>119&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6258&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>120&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6276&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>121&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6284&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>122&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6413&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>123&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6913&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>124&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6918&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>125&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6964&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>126&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6973&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>127&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>7516&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>128&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>7519&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>129&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8222&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>130&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8227&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>131&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8230&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>132&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8231&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>133&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8237&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>134&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8241&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>135&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8242&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>136&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8249&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>137&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8256&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>138&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8258&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>139&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8259&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>140&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8283&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>141&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8306&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>142&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8307&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>143&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8326&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>144&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8334&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>145&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8335&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>146&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8351&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>147&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8386&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>148&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8387&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>149&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8422&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>150&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8423&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>151&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8426&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>152&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9058&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>153&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9321&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>154&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9323&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>155&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9332&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>156&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9336&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>157&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9339&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>158&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9343&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>159&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9353&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>160&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9356&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>161&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9363&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>162&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9369&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>163&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9380&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>164&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9382&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>165&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9383&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>166&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9386&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>167&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9388&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>168&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9391&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>169&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9392&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>170&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9399&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>171&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9404&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>172&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9405&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>173&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9407&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>174&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9408&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>175&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9409&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>176&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9412&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>177&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9413&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>178&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9416&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>179&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9421&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>180&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9427&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>181&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9433&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>182&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9434&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>183&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9436&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>184&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9437&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>185&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9442&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>186&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9450&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>187&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9451&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>188&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9452&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>189&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9453&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>190&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9454&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>191&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9470&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>192&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9476&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>193&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9481&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>194&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6043&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>195&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6900&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>196&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6974&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>197&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>7517&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>198&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8240&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>199&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8247&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>200&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8369&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>"],{"maxWidth":800,"minWidth":50,"autoPan":true,"keepInView":false,"closeButton":true,"closeOnClick":true,"className":""},["991/S Sweden/3","992/S Sweden/3","997/S Sweden/3","1061/S Sweden/1","1062/S Sweden/2","1063/S Sweden/1","1158/S Sweden/3","1254/C Sweden/4","1257/C Sweden/4","1317/C Sweden/4","1318/C Sweden/4","1363/C Sweden/4","1435/N Sweden/3","1552/N Sweden/5","1585/N Sweden/4","5829/S Sweden/3","5830/S Sweden/3","5831/S Sweden/1","5832/S Sweden/1","5856/N Sweden/5","5865/S Sweden/3","5867/S Sweden/3","6009/N Sweden/5","6011/N Sweden/5","6012/N Sweden/5","6013/N Sweden/5","6016/N Sweden/5","6017/N Sweden/5","6019/S Sweden/3","6020/S Sweden/3","6021/S Sweden/3","6023/S Sweden/2","6025/N Sweden/5","6030/N Sweden/5","6034/S Sweden/1","6035/S Sweden/1","6036/S Sweden/1","6038/S Sweden/1","6039/S Sweden/1","6040/S Sweden/2","6041/S Sweden/1","6042/S Sweden/3","6046/N Sweden/5","6064/N Sweden/5","6069/N Sweden/5","6071/N Sweden/5","6073/S Sweden/3","6085/S Sweden/3","6086/C Sweden/4","6090/S Sweden/3","6092/S Sweden/3","6094/S Sweden/1","6097/S Sweden/3","6098/S Sweden/3","6102/S Sweden/3","6104/S Sweden/2","6106/S Sweden/3","6108/S Sweden/3","6109/S Sweden/3","6111/S Sweden/3","6112/S Sweden/3","6114/S Sweden/3","6115/S Sweden/3","6118/S Sweden/2","6122/S Sweden/2","6124/S Sweden/3","6126/S Sweden/3","6127/S Sweden/3","6129/S Sweden/2","6131/S Sweden/2","6133/S Sweden/2","6134/S Sweden/3","6136/S Sweden/2","6138/S Sweden/2","6140/S Sweden/2","6142/S Sweden/2","6145/S Sweden/2","6148/S Sweden/2","6149/S Sweden/2","6151/S Sweden/1","6154/N Sweden/5","6163/N Sweden/5","6166/N Sweden/5","6169/N Sweden/5","6171/N Sweden/5","6173/N Sweden/5","6174/N Sweden/5","6177/N Sweden/5","6180/N Sweden/3","6184/N Sweden/5","6188/S Sweden/1","6189/S Sweden/1","6191/S Sweden/1","6192/S Sweden/1","6193/S Sweden/1","6194/S Sweden/3","6195/S Sweden/1","6197/S Sweden/1","6198/S Sweden/1","6201/S Sweden/3","6202/S Sweden/3","6203/S Sweden/1","6209/N Sweden/5","6210/N Sweden/5","6217/N Sweden/5","6218/N Sweden/5","6220/N Sweden/5","6221/N Sweden/5","6231/N Sweden/5","6235/N Sweden/5","6237/N Sweden/5","6238/N Sweden/5","6240/N Sweden/5","6241/N Sweden/5","6242/S Sweden/3","6243/S Sweden/3","6244/N Sweden/5","6252/S Sweden/6","6258/S Sweden/1","6276/S Sweden/6","6284/S Sweden/1","6413/S Sweden/3","6913/N Sweden/5","6918/N Sweden/5","6964/C Sweden/3","6973/S Sweden/3","7516/S Sweden/6","7519/S Sweden/3","8222/S Sweden/3","8227/N Sweden/5","8230/S Sweden/3","8231/S Sweden/3","8237/S Sweden/3","8241/S Sweden/3","8242/S Sweden/3","8249/S Sweden/3","8256/S Sweden/1","8258/S Sweden/1","8259/S Sweden/1","8283/S Sweden/1","8306/S Sweden/1","8307/S Sweden/1","8326/S Sweden/3","8334/S Sweden/3","8335/S Sweden/2","8351/C Sweden/4","8386/C Sweden/3","8387/C Sweden/3","8422/S Sweden/3","8423/S Sweden/1","8426/S Sweden/1","9058/S Sweden/6","9321/N Sweden/5","9323/N Sweden/5","9332/N Sweden/5","9336/N Sweden/4","9339/C Sweden/3","9343/S Sweden/6","9353/C Sweden/3","9356/N Sweden/5","9363/N Sweden/5","9369/C Sweden/3","9380/S Sweden/2","9382/S Sweden/6","9383/S Sweden/6","9386/N Sweden/5","9388/N Sweden/5","9391/C Sweden/3","9392/C Sweden/3","9399/S Sweden/1","9404/S Sweden/2","9405/S Sweden/2","9407/S Sweden/2","9408/S Sweden/3","9409/S Sweden/3","9412/C Sweden/3","9413/C Sweden/3","9416/S Sweden/6","9421/S Sweden/3","9427/N Sweden/5","9433/N Sweden/5","9434/N Sweden/5","9436/S Sweden/3","9437/S Sweden/3","9442/S Sweden/6","9450/S Sweden/6","9451/S Sweden/6","9452/S Sweden/6","9453/S Sweden/6","9454/S Sweden/6","9470/C Sweden/3","9476/S Sweden/6","9481/S Sweden/1","6043/N Sweden/5","6900/N Sweden/5","6974/S Sweden/3","7517/S Sweden/6","8240/S Sweden/3","8247/S Sweden/3","8369/S Sweden/2"],{"interactive":false,"permanent":false,"direction":"auto","opacity":1,"offset":[0,0],"textsize":"10px","textOnly":false,"className":"","sticky":true},null]},{"method":"addScaleBar","args":[{"maxWidth":100,"metric":true,"imperial":true,"updateWhenIdle":true,"position":"bottomleft"}]},{"method":"addHomeButton","args":[11.2,55.3833,18.5162,65.25,true,"loc - K_06","Zoom to loc - K_06","<strong> loc - K_06 <\/strong>","bottomright"]},{"method":"addLayersControl","args":["Esri.WorldImagery","loc - K_06",{"collapsed":true,"autoZIndex":true,"position":"topleft"}]},{"method":"addLegend","args":[{"colors":["#4B0055","#274983","#008298","#00B28A","#7ED357","#FDE333"],"labels":["1","2","3","4","5","6"],"na_color":null,"na_label":"NA","opacity":1,"position":"topright","type":"factor","title":"loc - K_06","extra":null,"layerId":null,"className":"info legend","group":"loc - K_06"}]}],"limits":{"lat":[55.3833,65.25],"lng":[11.2,18.5162]},"fitBounds":[55.3833,11.2,65.25,18.5162,[]]},"evals":[],"jsHooks":{"render":[{"code":"function(el, x, data) {\n  return (\n      function(el, x, data) {\n      // get the leaflet map\n      var map = this; //HTMLWidgets.find('#' + el.id);\n      // we need a new div element because we have to handle\n      // the mouseover output separately\n      // debugger;\n      function addElement () {\n      // generate new div Element\n      var newDiv = $(document.createElement('div'));\n      // append at end of leaflet htmlwidget container\n      $(el).append(newDiv);\n      //provide ID and style\n      newDiv.addClass('lnlt');\n      newDiv.css({\n      'position': 'relative',\n      'bottomleft':  '0px',\n      'background-color': 'rgba(255, 255, 255, 0.7)',\n      'box-shadow': '0 0 2px #bbb',\n      'background-clip': 'padding-box',\n      'margin': '0',\n      'padding-left': '5px',\n      'color': '#333',\n      'font': '9px/1.5 \"Helvetica Neue\", Arial, Helvetica, sans-serif',\n      'z-index': '700',\n      });\n      return newDiv;\n      }\n\n\n      // check for already existing lnlt class to not duplicate\n      var lnlt = $(el).find('.lnlt');\n\n      if(!lnlt.length) {\n      lnlt = addElement();\n\n      // grab the special div we generated in the beginning\n      // and put the mousmove output there\n\n      map.on('mousemove', function (e) {\n      if (e.originalEvent.ctrlKey) {\n      if (document.querySelector('.lnlt') === null) lnlt = addElement();\n      lnlt.text(\n                           ' lon: ' + (e.latlng.lng).toFixed(5) +\n                           ' | lat: ' + (e.latlng.lat).toFixed(5) +\n                           ' | zoom: ' + map.getZoom() +\n                           ' | x: ' + L.CRS.EPSG3857.project(e.latlng).x.toFixed(0) +\n                           ' | y: ' + L.CRS.EPSG3857.project(e.latlng).y.toFixed(0) +\n                           ' | epsg: 3857 ' +\n                           ' | proj4: +proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs ');\n      } else {\n      if (document.querySelector('.lnlt') === null) lnlt = addElement();\n      lnlt.text(\n                      ' lon: ' + (e.latlng.lng).toFixed(5) +\n                      ' | lat: ' + (e.latlng.lat).toFixed(5) +\n                      ' | zoom: ' + map.getZoom() + ' ');\n      }\n      });\n\n      // remove the lnlt div when mouse leaves map\n      map.on('mouseout', function (e) {\n      var strip = document.querySelector('.lnlt');\n      if( strip !==null) strip.remove();\n      });\n\n      };\n\n      //$(el).keypress(67, function(e) {\n      map.on('preclick', function(e) {\n      if (e.originalEvent.ctrlKey) {\n      if (document.querySelector('.lnlt') === null) lnlt = addElement();\n      lnlt.text(\n                      ' lon: ' + (e.latlng.lng).toFixed(5) +\n                      ' | lat: ' + (e.latlng.lat).toFixed(5) +\n                      ' | zoom: ' + map.getZoom() + ' ');\n      var txt = document.querySelector('.lnlt').textContent;\n      console.log(txt);\n      //txt.innerText.focus();\n      //txt.select();\n      setClipboardText('\"' + txt + '\"');\n      }\n      });\n\n      }\n      ).call(this.getMap(), el, x, data);\n}","data":null},{"code":"function(el, x, data) {\n  return (function(el,x,data){\n           var map = this;\n\n           map.on('keypress', function(e) {\n               console.log(e.originalEvent.code);\n               var key = e.originalEvent.code;\n               if (key === 'KeyE') {\n                   var bb = this.getBounds();\n                   var txt = JSON.stringify(bb);\n                   console.log(txt);\n\n                   setClipboardText('\\'' + txt + '\\'');\n               }\n           })\n        }).call(this.getMap(), el, x, data);\n}","data":null}]}}</script>
```

## map of DF's clustering with K=6



```r
m=mapview(loc, map.types="Esri.WorldImagery", label=mylabel, zcol="K_DF", size=10, popup = popupTable(loc, c("id", "region", "K_06", "K_DF")))

m
```

```{=html}
<div class="leaflet html-widget html-fill-item-overflow-hidden html-fill-item" id="htmlwidget-09e6d94f71b8559aad02" style="width:672px;height:480px;"></div>
<script type="application/json" data-for="htmlwidget-09e6d94f71b8559aad02">{"x":{"options":{"minZoom":1,"maxZoom":52,"crs":{"crsClass":"L.CRS.EPSG3857","code":null,"proj4def":null,"projectedBounds":null,"options":{}},"preferCanvas":false,"bounceAtZoomLimits":false,"maxBounds":[[[-90,-370]],[[90,370]]]},"calls":[{"method":"addProviderTiles","args":["Esri.WorldImagery","Esri.WorldImagery","Esri.WorldImagery",{"errorTileUrl":"","noWrap":false,"detectRetina":false,"pane":"tilePane"}]},{"method":"createMapPane","args":["point",440]},{"method":"addCircleMarkers","args":[[55.3833,55.3833,55.3833,55.7167,55.7167,55.7167,56.7,59.4333,59.4333,59.5667,59.5667,59.7833,62.8,63.0833,65.25,55.3838,56.3333,56.3333,56.3333,63.0167,55.76,55.76,62.877,62.877,62.877,62.877,62.9,62.9,56.06,56.06,56.06,55.7509,62.6437,62.806,56.1,56.1,56.1,56.1,56.1,55.66,56.0328,56.09,62.801,62.9513,62.9513,62.9308,56.1481,55.7097,58.9,55.6525,55.6514,55.6494,55.6481,55.6561,55.6,55.7,55.7931,55.7989,55.7936,55.7989,55.7967,55.8097,55.8,55.7,55.8364,55.8378,55.8411,55.8428,55.8544,55.8369,55.8364,55.8383,55.9336,55.9403,55.9392,55.9428,55.9497,55.9319,55.9281,55.6528,62.6422,62.6425,62.6425,62.8714,62.8717,62.8717,62.8719,62.6322,62.6322,62.8892,55.7683,55.7686,55.7689,55.7692,55.7694,55.7706,55.7708,55.7714,55.7708,55.7719,55.7717,55.7714,62.8836,62.8839,63.0169,63.0172,62.806,62.806,62.96,62.9611,62.9619,62.9619,62.9622,62.9614,55.7,55.95,62.9169,55.5796,55.5796,55.5796,55.5796,56.06,62.877,63.0165,56.3,56.0648,55.58,56.1509,56.0328,62.7989,56.68,56.3,55.8,55.9473,56.1494,57.7,56.4,56.4,56.4,55.76,56.1,56.1,56.0328,55.71,55.71,60.25,58.9,59,56.06,56.1,56.06,57.75,62.8622,62.8622,62.8698,62.8794,57.7133,57.3089,57.2608,62.8762,62.9147,57.6781,55.7488,55.8106,55.8106,62.806,62.806,57.3263,57.3263,55.4234,55.7491,55.7491,55.7491,56.047,56.0573,57.2746,57.2746,57.7215,55.9745,62.8815,62.9513,62.8959,56.1633,56.1633,55.5678,57.2545,57.2545,57.2545,57.8009,57.8009,57.6511,55.5796,55.4242,62.801,63.324,56.0648,55.58,55.705,56.07,55.6942],[14.05,14.05,14.05,14.1333,14.1333,14.1333,16.5167,17.0167,17.0167,16.8667,16.8667,17.5833,18.2,18.3667,15.6,14.0612,15.9667,15.9667,15.9667,17.4914,14.12,14.12,18.177,18.177,18.177,18.177,18.4,18.4,14.29,14.29,14.29,13.3712,17.7339,18.1896,13.74,13.74,13.74,13.74,13.74,13.4,14.775,13.9,18.079,18.2763,18.2763,18.3448,15.8155,13.2145,11.2,13.2197,13.2233,13.2147,13.2264,13.2178,13.2,13.2,13.1186,13.1206,13.1233,13.1219,13.1044,13.1342,13.1367,13.2,13.3075,13.3092,13.3047,13.3058,13.2875,13.3181,13.2906,13.2906,13.5519,13.5511,13.5539,13.5558,13.5533,13.5508,13.5481,13.2244,17.7406,17.7356,17.7372,18.3447,18.3444,18.3419,18.3422,17.69,17.6906,18.4522,14.1386,14.1383,14.1375,14.1369,14.1347,14.1342,14.1342,14.1333,14.1331,14.1211,14.1206,14.1208,18.1842,18.1836,18.3283,18.3283,18.1896,18.1896,18.2844,18.3589,18.35,18.35,18.35,18.3608,13.2,13.85,18.4728,14.3336,14.3336,14.3336,14.3336,13.97,18.177,18.3174,16,13.9707,14.334,15.7735,14.775,17.9103,16.5,16,13.1,13.821,15.7884,15.8,12.9,12.9,12.9,14.12,13.74,13.74,14.775,13.2,13.2,18.37,11.2,18,14.29,13.74,13.97,16.6333,18.336,18.336,18.381,18.4473,15.0689,18.1512,16.3675,18.1746,18.4045,14.9986,13.3742,14.2091,14.2091,18.1896,18.1896,15.8979,15.8979,13.9905,13.399,13.399,13.399,13.9519,14.302,16.1494,16.1494,18.3837,14.3997,18.4055,18.2763,18.3659,14.6806,14.6806,14.3398,18.2109,18.2109,18.2109,18.5162,18.5162,14.8043,14.3336,13.8484,18.079,18.484,13.9707,14.334,13.196,13.74,13.4504],6,null,"loc - K_DF",{"crs":{"crsClass":"L.CRS.EPSG3857","code":null,"proj4def":null,"projectedBounds":null,"options":{}},"pane":"point","stroke":true,"color":"#333333","weight":1,"opacity":[0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9],"fill":true,"fillColor":["#4B0055","#4B0055","#4B0055","#4B0055","#274983","#4B0055","#4B0055","#008298","#008298","#008298","#008298","#008298","#4B0055","#00B28A","#008298","#4B0055","#4B0055","#4B0055","#4B0055","#00B28A","#4B0055","#4B0055","#00B28A","#00B28A","#00B28A","#00B28A","#00B28A","#00B28A","#4B0055","#4B0055","#4B0055","#274983","#7ED357","#7ED357","#274983","#274983","#4B0055","#4B0055","#4B0055","#274983","#4B0055","#4B0055","#7ED357","#00B28A","#00B28A","#00B28A","#4B0055","#4B0055","#008298","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#274983","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#274983","#274983","#4B0055","#4B0055","#4B0055","#274983","#274983","#274983","#4B0055","#274983","#274983","#274983","#274983","#274983","#274983","#274983","#4B0055","#7ED357","#7ED357","#7ED357","#00B28A","#00B28A","#00B28A","#00B28A","#7ED357","#4B0055","#7ED357","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#00B28A","#00B28A","#00B28A","#00B28A","#7ED357","#00B28A","#00B28A","#00B28A","#00B28A","#00B28A","#00B28A","#00B28A","#4B0055","#4B0055","#00B28A","#FDE333","#4B0055","#FDE333","#4B0055","#4B0055","#00B28A","#00B28A","#4B0055","#4B0055","#FDE333","#4B0055","#4B0055","#7ED357","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#274983","#008298","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#008298","#00B28A","#00B28A","#00B28A","#008298","#4B0055","#008298","#4B0055","#00B28A","#00B28A","#4B0055","#274983","#008298","#008298","#7ED357","#00B28A","#4B0055","#4B0055","#4B0055","#274983","#274983","#274983","#4B0055","#4B0055","#4B0055","#4B0055","#008298","#4B0055","#00B28A","#00B28A","#00B28A","#4B0055","#4B0055","#FDE333","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#FDE333","#4B0055","#7ED357","#7ED357","#4B0055","#FDE333","#4B0055","#274983","#274983"],"fillOpacity":[0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6]},null,null,["<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>1&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>991&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>2&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>992&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>3&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>997&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>4&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1061&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>5&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1062&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>6&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1063&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>7&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1158&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>8&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1254&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>9&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1257&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>10&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1317&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>11&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1318&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>12&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1363&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>13&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1435&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>14&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1552&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>15&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1585&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>16&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5829&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>17&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5830&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>18&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5831&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>19&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5832&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>20&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5856&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>21&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5865&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>22&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5867&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>23&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6009&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>24&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6011&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>25&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6012&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>26&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6013&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>27&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6016&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>28&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6017&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>29&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6019&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>30&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6020&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>31&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6021&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>32&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6023&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>33&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6025&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>34&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6030&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>35&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6034&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>36&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6035&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>37&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6036&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>38&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6038&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>39&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6039&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>40&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6040&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>41&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6041&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>42&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6042&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>43&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6046&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>44&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6064&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>45&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6069&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>46&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6071&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>47&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6073&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>48&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6085&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>49&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6086&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>50&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6090&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>51&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6092&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>52&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6094&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>53&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6097&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>54&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6098&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>55&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6102&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>56&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6104&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>57&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6106&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>58&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6108&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>59&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6109&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>60&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6111&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>61&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6112&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>62&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6114&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>63&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6115&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>64&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6118&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>65&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6122&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>66&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6124&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>67&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6126&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>68&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6127&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>69&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6129&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>70&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6131&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>71&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6133&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>72&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6134&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>73&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6136&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>74&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6138&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>75&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6140&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>76&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6142&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>77&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6145&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>78&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6148&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>79&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6149&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>80&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6151&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>81&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6154&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>82&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6163&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>83&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6166&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>84&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6169&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>85&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6171&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>86&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6173&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>87&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6174&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>88&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6177&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>89&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6180&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>90&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6184&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>91&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6188&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>92&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6189&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>93&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6191&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>94&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6192&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>95&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6193&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>96&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6194&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>97&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6195&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>98&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6197&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>99&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6198&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>100&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6201&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>101&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6202&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>102&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6203&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>103&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6209&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>104&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6210&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>105&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6217&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>106&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6218&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>107&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6220&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>108&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6221&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>109&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6231&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>110&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6235&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>111&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6237&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>112&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6238&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>113&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6240&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>114&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6241&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>115&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6242&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>116&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6243&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>117&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6244&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>118&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6252&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>119&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6258&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>120&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6276&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>121&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6284&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>122&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6413&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>123&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6913&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>124&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6918&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>125&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6964&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>126&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6973&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>127&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>7516&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>128&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>7519&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>129&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8222&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>130&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8227&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>131&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8230&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>132&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8231&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>133&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8237&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>134&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8241&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>135&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8242&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>136&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8249&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>137&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8256&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>138&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8258&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>139&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8259&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>140&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8283&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>141&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8306&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>142&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8307&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>143&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8326&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>144&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8334&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>145&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8335&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>146&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8351&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>147&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8386&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>148&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8387&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>149&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8422&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>150&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8423&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>151&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8426&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>152&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9058&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>153&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9321&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>154&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9323&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>155&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9332&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>156&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9336&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>157&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9339&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>158&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9343&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>159&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9353&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>160&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9356&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>161&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9363&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>162&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9369&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>163&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9380&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>164&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9382&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>165&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9383&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>166&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9386&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>167&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9388&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>168&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9391&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>169&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9392&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>170&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9399&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>171&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9404&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>172&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9405&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>173&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9407&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>174&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9408&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>175&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9409&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>176&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9412&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>177&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9413&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>178&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9416&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>179&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9421&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>180&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9427&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>181&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9433&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>182&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9434&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>183&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9436&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>184&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9437&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>185&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9442&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>186&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9450&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>187&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9451&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>188&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9452&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>189&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9453&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>190&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9454&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>191&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9470&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>192&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9476&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>193&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9481&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>194&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6043&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>195&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6900&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>196&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6974&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>197&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>7517&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>198&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8240&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>199&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8247&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>200&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8369&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>"],{"maxWidth":800,"minWidth":50,"autoPan":true,"keepInView":false,"closeButton":true,"closeOnClick":true,"className":""},["991/S Sweden/3","992/S Sweden/3","997/S Sweden/3","1061/S Sweden/1","1062/S Sweden/2","1063/S Sweden/1","1158/S Sweden/3","1254/C Sweden/4","1257/C Sweden/4","1317/C Sweden/4","1318/C Sweden/4","1363/C Sweden/4","1435/N Sweden/3","1552/N Sweden/5","1585/N Sweden/4","5829/S Sweden/3","5830/S Sweden/3","5831/S Sweden/1","5832/S Sweden/1","5856/N Sweden/5","5865/S Sweden/3","5867/S Sweden/3","6009/N Sweden/5","6011/N Sweden/5","6012/N Sweden/5","6013/N Sweden/5","6016/N Sweden/5","6017/N Sweden/5","6019/S Sweden/3","6020/S Sweden/3","6021/S Sweden/3","6023/S Sweden/2","6025/N Sweden/5","6030/N Sweden/5","6034/S Sweden/1","6035/S Sweden/1","6036/S Sweden/1","6038/S Sweden/1","6039/S Sweden/1","6040/S Sweden/2","6041/S Sweden/1","6042/S Sweden/3","6046/N Sweden/5","6064/N Sweden/5","6069/N Sweden/5","6071/N Sweden/5","6073/S Sweden/3","6085/S Sweden/3","6086/C Sweden/4","6090/S Sweden/3","6092/S Sweden/3","6094/S Sweden/1","6097/S Sweden/3","6098/S Sweden/3","6102/S Sweden/3","6104/S Sweden/2","6106/S Sweden/3","6108/S Sweden/3","6109/S Sweden/3","6111/S Sweden/3","6112/S Sweden/3","6114/S Sweden/3","6115/S Sweden/3","6118/S Sweden/2","6122/S Sweden/2","6124/S Sweden/3","6126/S Sweden/3","6127/S Sweden/3","6129/S Sweden/2","6131/S Sweden/2","6133/S Sweden/2","6134/S Sweden/3","6136/S Sweden/2","6138/S Sweden/2","6140/S Sweden/2","6142/S Sweden/2","6145/S Sweden/2","6148/S Sweden/2","6149/S Sweden/2","6151/S Sweden/1","6154/N Sweden/5","6163/N Sweden/5","6166/N Sweden/5","6169/N Sweden/5","6171/N Sweden/5","6173/N Sweden/5","6174/N Sweden/5","6177/N Sweden/5","6180/N Sweden/3","6184/N Sweden/5","6188/S Sweden/1","6189/S Sweden/1","6191/S Sweden/1","6192/S Sweden/1","6193/S Sweden/1","6194/S Sweden/3","6195/S Sweden/1","6197/S Sweden/1","6198/S Sweden/1","6201/S Sweden/3","6202/S Sweden/3","6203/S Sweden/1","6209/N Sweden/5","6210/N Sweden/5","6217/N Sweden/5","6218/N Sweden/5","6220/N Sweden/5","6221/N Sweden/5","6231/N Sweden/5","6235/N Sweden/5","6237/N Sweden/5","6238/N Sweden/5","6240/N Sweden/5","6241/N Sweden/5","6242/S Sweden/3","6243/S Sweden/3","6244/N Sweden/5","6252/S Sweden/6","6258/S Sweden/1","6276/S Sweden/6","6284/S Sweden/1","6413/S Sweden/3","6913/N Sweden/5","6918/N Sweden/5","6964/C Sweden/3","6973/S Sweden/3","7516/S Sweden/6","7519/S Sweden/3","8222/S Sweden/3","8227/N Sweden/5","8230/S Sweden/3","8231/S Sweden/3","8237/S Sweden/3","8241/S Sweden/3","8242/S Sweden/3","8249/S Sweden/3","8256/S Sweden/1","8258/S Sweden/1","8259/S Sweden/1","8283/S Sweden/1","8306/S Sweden/1","8307/S Sweden/1","8326/S Sweden/3","8334/S Sweden/3","8335/S Sweden/2","8351/C Sweden/4","8386/C Sweden/3","8387/C Sweden/3","8422/S Sweden/3","8423/S Sweden/1","8426/S Sweden/1","9058/S Sweden/6","9321/N Sweden/5","9323/N Sweden/5","9332/N Sweden/5","9336/N Sweden/4","9339/C Sweden/3","9343/S Sweden/6","9353/C Sweden/3","9356/N Sweden/5","9363/N Sweden/5","9369/C Sweden/3","9380/S Sweden/2","9382/S Sweden/6","9383/S Sweden/6","9386/N Sweden/5","9388/N Sweden/5","9391/C Sweden/3","9392/C Sweden/3","9399/S Sweden/1","9404/S Sweden/2","9405/S Sweden/2","9407/S Sweden/2","9408/S Sweden/3","9409/S Sweden/3","9412/C Sweden/3","9413/C Sweden/3","9416/S Sweden/6","9421/S Sweden/3","9427/N Sweden/5","9433/N Sweden/5","9434/N Sweden/5","9436/S Sweden/3","9437/S Sweden/3","9442/S Sweden/6","9450/S Sweden/6","9451/S Sweden/6","9452/S Sweden/6","9453/S Sweden/6","9454/S Sweden/6","9470/C Sweden/3","9476/S Sweden/6","9481/S Sweden/1","6043/N Sweden/5","6900/N Sweden/5","6974/S Sweden/3","7517/S Sweden/6","8240/S Sweden/3","8247/S Sweden/3","8369/S Sweden/2"],{"interactive":false,"permanent":false,"direction":"auto","opacity":1,"offset":[0,0],"textsize":"10px","textOnly":false,"className":"","sticky":true},null]},{"method":"addScaleBar","args":[{"maxWidth":100,"metric":true,"imperial":true,"updateWhenIdle":true,"position":"bottomleft"}]},{"method":"addHomeButton","args":[11.2,55.3833,18.5162,65.25,true,"loc - K_DF","Zoom to loc - K_DF","<strong> loc - K_DF <\/strong>","bottomright"]},{"method":"addLayersControl","args":["Esri.WorldImagery","loc - K_DF",{"collapsed":true,"autoZIndex":true,"position":"topleft"}]},{"method":"addLegend","args":[{"colors":["#4B0055","#274983","#008298","#00B28A","#7ED357","#FDE333"],"labels":["1","2","3","4","5","6"],"na_color":null,"na_label":"NA","opacity":1,"position":"topright","type":"factor","title":"loc - K_DF","extra":null,"layerId":null,"className":"info legend","group":"loc - K_DF"}]}],"limits":{"lat":[55.3833,65.25],"lng":[11.2,18.5162]},"fitBounds":[55.3833,11.2,65.25,18.5162,[]]},"evals":[],"jsHooks":{"render":[{"code":"function(el, x, data) {\n  return (\n      function(el, x, data) {\n      // get the leaflet map\n      var map = this; //HTMLWidgets.find('#' + el.id);\n      // we need a new div element because we have to handle\n      // the mouseover output separately\n      // debugger;\n      function addElement () {\n      // generate new div Element\n      var newDiv = $(document.createElement('div'));\n      // append at end of leaflet htmlwidget container\n      $(el).append(newDiv);\n      //provide ID and style\n      newDiv.addClass('lnlt');\n      newDiv.css({\n      'position': 'relative',\n      'bottomleft':  '0px',\n      'background-color': 'rgba(255, 255, 255, 0.7)',\n      'box-shadow': '0 0 2px #bbb',\n      'background-clip': 'padding-box',\n      'margin': '0',\n      'padding-left': '5px',\n      'color': '#333',\n      'font': '9px/1.5 \"Helvetica Neue\", Arial, Helvetica, sans-serif',\n      'z-index': '700',\n      });\n      return newDiv;\n      }\n\n\n      // check for already existing lnlt class to not duplicate\n      var lnlt = $(el).find('.lnlt');\n\n      if(!lnlt.length) {\n      lnlt = addElement();\n\n      // grab the special div we generated in the beginning\n      // and put the mousmove output there\n\n      map.on('mousemove', function (e) {\n      if (e.originalEvent.ctrlKey) {\n      if (document.querySelector('.lnlt') === null) lnlt = addElement();\n      lnlt.text(\n                           ' lon: ' + (e.latlng.lng).toFixed(5) +\n                           ' | lat: ' + (e.latlng.lat).toFixed(5) +\n                           ' | zoom: ' + map.getZoom() +\n                           ' | x: ' + L.CRS.EPSG3857.project(e.latlng).x.toFixed(0) +\n                           ' | y: ' + L.CRS.EPSG3857.project(e.latlng).y.toFixed(0) +\n                           ' | epsg: 3857 ' +\n                           ' | proj4: +proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs ');\n      } else {\n      if (document.querySelector('.lnlt') === null) lnlt = addElement();\n      lnlt.text(\n                      ' lon: ' + (e.latlng.lng).toFixed(5) +\n                      ' | lat: ' + (e.latlng.lat).toFixed(5) +\n                      ' | zoom: ' + map.getZoom() + ' ');\n      }\n      });\n\n      // remove the lnlt div when mouse leaves map\n      map.on('mouseout', function (e) {\n      var strip = document.querySelector('.lnlt');\n      if( strip !==null) strip.remove();\n      });\n\n      };\n\n      //$(el).keypress(67, function(e) {\n      map.on('preclick', function(e) {\n      if (e.originalEvent.ctrlKey) {\n      if (document.querySelector('.lnlt') === null) lnlt = addElement();\n      lnlt.text(\n                      ' lon: ' + (e.latlng.lng).toFixed(5) +\n                      ' | lat: ' + (e.latlng.lat).toFixed(5) +\n                      ' | zoom: ' + map.getZoom() + ' ');\n      var txt = document.querySelector('.lnlt').textContent;\n      console.log(txt);\n      //txt.innerText.focus();\n      //txt.select();\n      setClipboardText('\"' + txt + '\"');\n      }\n      });\n\n      }\n      ).call(this.getMap(), el, x, data);\n}","data":null},{"code":"function(el, x, data) {\n  return (function(el,x,data){\n           var map = this;\n\n           map.on('keypress', function(e) {\n               console.log(e.originalEvent.code);\n               var key = e.originalEvent.code;\n               if (key === 'KeyE') {\n                   var bb = this.getBounds();\n                   var txt = JSON.stringify(bb);\n                   console.log(txt);\n\n                   setClipboardText('\\'' + txt + '\\'');\n               }\n           })\n        }).call(this.getMap(), el, x, data);\n}","data":null}]}}</script>
```

## map of my clustering with K=7


```r
m=mapview(loc, map.types="Esri.WorldImagery", label=mylabel, zcol="K_07", size=10 , popup = popupTable(loc, c("id", "region", "K_06", "K_07", "K_DF")))

m
```

```{=html}
<div class="leaflet html-widget html-fill-item-overflow-hidden html-fill-item" id="htmlwidget-2d20242b4b4ea05189e0" style="width:672px;height:480px;"></div>
<script type="application/json" data-for="htmlwidget-2d20242b4b4ea05189e0">{"x":{"options":{"minZoom":1,"maxZoom":52,"crs":{"crsClass":"L.CRS.EPSG3857","code":null,"proj4def":null,"projectedBounds":null,"options":{}},"preferCanvas":false,"bounceAtZoomLimits":false,"maxBounds":[[[-90,-370]],[[90,370]]]},"calls":[{"method":"addProviderTiles","args":["Esri.WorldImagery","Esri.WorldImagery","Esri.WorldImagery",{"errorTileUrl":"","noWrap":false,"detectRetina":false,"pane":"tilePane"}]},{"method":"createMapPane","args":["point",440]},{"method":"addCircleMarkers","args":[[55.3833,55.3833,55.3833,55.7167,55.7167,55.7167,56.7,59.4333,59.4333,59.5667,59.5667,59.7833,62.8,63.0833,65.25,55.3838,56.3333,56.3333,56.3333,63.0167,55.76,55.76,62.877,62.877,62.877,62.877,62.9,62.9,56.06,56.06,56.06,55.7509,62.6437,62.806,56.1,56.1,56.1,56.1,56.1,55.66,56.0328,56.09,62.801,62.9513,62.9513,62.9308,56.1481,55.7097,58.9,55.6525,55.6514,55.6494,55.6481,55.6561,55.6,55.7,55.7931,55.7989,55.7936,55.7989,55.7967,55.8097,55.8,55.7,55.8364,55.8378,55.8411,55.8428,55.8544,55.8369,55.8364,55.8383,55.9336,55.9403,55.9392,55.9428,55.9497,55.9319,55.9281,55.6528,62.6422,62.6425,62.6425,62.8714,62.8717,62.8717,62.8719,62.6322,62.6322,62.8892,55.7683,55.7686,55.7689,55.7692,55.7694,55.7706,55.7708,55.7714,55.7708,55.7719,55.7717,55.7714,62.8836,62.8839,63.0169,63.0172,62.806,62.806,62.96,62.9611,62.9619,62.9619,62.9622,62.9614,55.7,55.95,62.9169,55.5796,55.5796,55.5796,55.5796,56.06,62.877,63.0165,56.3,56.0648,55.58,56.1509,56.0328,62.7989,56.68,56.3,55.8,55.9473,56.1494,57.7,56.4,56.4,56.4,55.76,56.1,56.1,56.0328,55.71,55.71,60.25,58.9,59,56.06,56.1,56.06,57.75,62.8622,62.8622,62.8698,62.8794,57.7133,57.3089,57.2608,62.8762,62.9147,57.6781,55.7488,55.8106,55.8106,62.806,62.806,57.3263,57.3263,55.4234,55.7491,55.7491,55.7491,56.047,56.0573,57.2746,57.2746,57.7215,55.9745,62.8815,62.9513,62.8959,56.1633,56.1633,55.5678,57.2545,57.2545,57.2545,57.8009,57.8009,57.6511,55.5796,55.4242,62.801,63.324,56.0648,55.58,55.705,56.07,55.6942],[14.05,14.05,14.05,14.1333,14.1333,14.1333,16.5167,17.0167,17.0167,16.8667,16.8667,17.5833,18.2,18.3667,15.6,14.0612,15.9667,15.9667,15.9667,17.4914,14.12,14.12,18.177,18.177,18.177,18.177,18.4,18.4,14.29,14.29,14.29,13.3712,17.7339,18.1896,13.74,13.74,13.74,13.74,13.74,13.4,14.775,13.9,18.079,18.2763,18.2763,18.3448,15.8155,13.2145,11.2,13.2197,13.2233,13.2147,13.2264,13.2178,13.2,13.2,13.1186,13.1206,13.1233,13.1219,13.1044,13.1342,13.1367,13.2,13.3075,13.3092,13.3047,13.3058,13.2875,13.3181,13.2906,13.2906,13.5519,13.5511,13.5539,13.5558,13.5533,13.5508,13.5481,13.2244,17.7406,17.7356,17.7372,18.3447,18.3444,18.3419,18.3422,17.69,17.6906,18.4522,14.1386,14.1383,14.1375,14.1369,14.1347,14.1342,14.1342,14.1333,14.1331,14.1211,14.1206,14.1208,18.1842,18.1836,18.3283,18.3283,18.1896,18.1896,18.2844,18.3589,18.35,18.35,18.35,18.3608,13.2,13.85,18.4728,14.3336,14.3336,14.3336,14.3336,13.97,18.177,18.3174,16,13.9707,14.334,15.7735,14.775,17.9103,16.5,16,13.1,13.821,15.7884,15.8,12.9,12.9,12.9,14.12,13.74,13.74,14.775,13.2,13.2,18.37,11.2,18,14.29,13.74,13.97,16.6333,18.336,18.336,18.381,18.4473,15.0689,18.1512,16.3675,18.1746,18.4045,14.9986,13.3742,14.2091,14.2091,18.1896,18.1896,15.8979,15.8979,13.9905,13.399,13.399,13.399,13.9519,14.302,16.1494,16.1494,18.3837,14.3997,18.4055,18.2763,18.3659,14.6806,14.6806,14.3398,18.2109,18.2109,18.2109,18.5162,18.5162,14.8043,14.3336,13.8484,18.079,18.484,13.9707,14.334,13.196,13.74,13.4504],6,null,"loc - K_07",{"crs":{"crsClass":"L.CRS.EPSG3857","code":null,"proj4def":null,"projectedBounds":null,"options":{}},"pane":"point","stroke":true,"color":"#333333","weight":1,"opacity":[0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9,0.9],"fill":true,"fillColor":["#96D84B","#96D84B","#96D84B","#4B0055","#353E7C","#4B0055","#007094","#009B95","#009B95","#009B95","#009B95","#009B95","#007094","#00BE7D","#009B95","#007094","#007094","#4B0055","#4B0055","#00BE7D","#007094","#007094","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#007094","#007094","#007094","#353E7C","#00BE7D","#00BE7D","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#353E7C","#4B0055","#007094","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#007094","#96D84B","#009B95","#96D84B","#96D84B","#4B0055","#96D84B","#96D84B","#96D84B","#353E7C","#96D84B","#007094","#96D84B","#96D84B","#96D84B","#96D84B","#96D84B","#353E7C","#353E7C","#96D84B","#96D84B","#96D84B","#353E7C","#353E7C","#353E7C","#96D84B","#353E7C","#353E7C","#353E7C","#353E7C","#353E7C","#353E7C","#353E7C","#4B0055","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#007094","#00BE7D","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#007094","#4B0055","#4B0055","#4B0055","#007094","#007094","#4B0055","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#00BE7D","#96D84B","#007094","#00BE7D","#FDE333","#4B0055","#FDE333","#4B0055","#007094","#00BE7D","#00BE7D","#007094","#96D84B","#FDE333","#007094","#007094","#00BE7D","#007094","#007094","#96D84B","#007094","#007094","#007094","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#4B0055","#007094","#007094","#353E7C","#009B95","#007094","#007094","#96D84B","#4B0055","#4B0055","#FDE333","#00BE7D","#00BE7D","#00BE7D","#009B95","#007094","#FDE333","#007094","#00BE7D","#00BE7D","#007094","#353E7C","#FDE333","#FDE333","#00BE7D","#00BE7D","#007094","#007094","#4B0055","#353E7C","#353E7C","#353E7C","#007094","#007094","#007094","#007094","#FDE333","#007094","#00BE7D","#00BE7D","#00BE7D","#007094","#007094","#FDE333","#FDE333","#FDE333","#FDE333","#FDE333","#FDE333","#007094","#FDE333","#4B0055","#00BE7D","#00BE7D","#007094","#FDE333","#96D84B","#96D84B","#353E7C"],"fillOpacity":[0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6]},null,null,["<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>1&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>991&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>2&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>992&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>3&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>997&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>4&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1061&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>5&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1062&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>6&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1063&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>7&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1158&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>8&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1254&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>9&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1257&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>10&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1317&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>11&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1318&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>12&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1363&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>13&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1435&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>14&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1552&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>15&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>1585&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>16&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5829&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>17&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5830&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>18&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5831&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>19&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5832&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>20&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5856&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>21&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5865&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>22&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>5867&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>23&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6009&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>24&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6011&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>25&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6012&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>26&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6013&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>27&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6016&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>28&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6017&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>29&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6019&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>30&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6020&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>31&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6021&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>32&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6023&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>33&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6025&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>34&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6030&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>35&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6034&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>36&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6035&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>37&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6036&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>38&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6038&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>39&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6039&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>40&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6040&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>41&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6041&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>42&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6042&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>43&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6046&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>44&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6064&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>45&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6069&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>46&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6071&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>47&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6073&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>48&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6085&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>49&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6086&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>50&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6090&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>51&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6092&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>52&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6094&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>53&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6097&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>54&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6098&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>55&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6102&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>56&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6104&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>57&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6106&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>58&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6108&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>59&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6109&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>60&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6111&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>61&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6112&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>62&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6114&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>63&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6115&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>64&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6118&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>65&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6122&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>66&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6124&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>67&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6126&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>68&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6127&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>69&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6129&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>70&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6131&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>71&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6133&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>72&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6134&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>73&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6136&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>74&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6138&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>75&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6140&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>76&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6142&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>77&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6145&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>78&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6148&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>79&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6149&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>80&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6151&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>81&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6154&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>82&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6163&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>83&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6166&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>84&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6169&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>85&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6171&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>86&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6173&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>87&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6174&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>88&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6177&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>89&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6180&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>90&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6184&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>91&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6188&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>92&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6189&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>93&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6191&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>94&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6192&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>95&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6193&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>96&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6194&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>97&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6195&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>98&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6197&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>99&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6198&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>100&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6201&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>101&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6202&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>102&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6203&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>103&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6209&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>104&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6210&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>105&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6217&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>106&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6218&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>107&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6220&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>108&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6221&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>109&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6231&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>110&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6235&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>111&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6237&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>112&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6238&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>113&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6240&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>114&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6241&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>115&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6242&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>116&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6243&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>117&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6244&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>118&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6252&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>119&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6258&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>120&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6276&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>121&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6284&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>122&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6413&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>123&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6913&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>124&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6918&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>125&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6964&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>126&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6973&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>127&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>7516&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>128&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>7519&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>129&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8222&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>130&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8227&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>131&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8230&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>132&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8231&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>133&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8237&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>134&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8241&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>135&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8242&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>136&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8249&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>137&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8256&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>138&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8258&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>139&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8259&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>140&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8283&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>141&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8306&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>142&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8307&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>143&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8326&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>144&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8334&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>145&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8335&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>146&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8351&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>147&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8386&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>148&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8387&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>149&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8422&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>150&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8423&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>151&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8426&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>152&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9058&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>153&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9321&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>154&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9323&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>155&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9332&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>156&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9336&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>4&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>157&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9339&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>158&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9343&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>159&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9353&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>160&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9356&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>161&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9363&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>162&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9369&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>163&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9380&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>164&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9382&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>165&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9383&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>166&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9386&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>167&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9388&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>168&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9391&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>169&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9392&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>170&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9399&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>171&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9404&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>172&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9405&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>173&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9407&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>174&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9408&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>175&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9409&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>176&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9412&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>177&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9413&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>178&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9416&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>3&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>179&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9421&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>180&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9427&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>181&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9433&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>182&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9434&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>4&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>183&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9436&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>184&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9437&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>185&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9442&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>186&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9450&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>187&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9451&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>188&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9452&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>189&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9453&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>190&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9454&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>191&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9470&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>C Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>192&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9476&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>193&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>9481&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>1&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>194&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6043&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>195&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6900&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>N Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>5&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>5&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>196&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>6974&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>197&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>7517&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>7&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>6&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>198&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8240&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>1&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>199&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8247&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>3&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>6&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>","<div class='scrollableContainer'><table class= id='popup'><tr class='coord'><td><\/td><th><b>Feature ID&emsp;<\/b><\/th><td>200&emsp;<\/td><\/tr><tr><td>1<\/td><th>id&emsp;<\/th><td>8369&emsp;<\/td><\/tr><tr><td>2<\/td><th>region&emsp;<\/th><td>S Sweden&emsp;<\/td><\/tr><tr><td>3<\/td><th>K_06&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>4<\/td><th>K_07&emsp;<\/th><td>2&emsp;<\/td><\/tr><tr><td>5<\/td><th>K_DF&emsp;<\/th><td>2&emsp;<\/td><\/tr><\/table><\/div>"],{"maxWidth":800,"minWidth":50,"autoPan":true,"keepInView":false,"closeButton":true,"closeOnClick":true,"className":""},["991/S Sweden/3","992/S Sweden/3","997/S Sweden/3","1061/S Sweden/1","1062/S Sweden/2","1063/S Sweden/1","1158/S Sweden/3","1254/C Sweden/4","1257/C Sweden/4","1317/C Sweden/4","1318/C Sweden/4","1363/C Sweden/4","1435/N Sweden/3","1552/N Sweden/5","1585/N Sweden/4","5829/S Sweden/3","5830/S Sweden/3","5831/S Sweden/1","5832/S Sweden/1","5856/N Sweden/5","5865/S Sweden/3","5867/S Sweden/3","6009/N Sweden/5","6011/N Sweden/5","6012/N Sweden/5","6013/N Sweden/5","6016/N Sweden/5","6017/N Sweden/5","6019/S Sweden/3","6020/S Sweden/3","6021/S Sweden/3","6023/S Sweden/2","6025/N Sweden/5","6030/N Sweden/5","6034/S Sweden/1","6035/S Sweden/1","6036/S Sweden/1","6038/S Sweden/1","6039/S Sweden/1","6040/S Sweden/2","6041/S Sweden/1","6042/S Sweden/3","6046/N Sweden/5","6064/N Sweden/5","6069/N Sweden/5","6071/N Sweden/5","6073/S Sweden/3","6085/S Sweden/3","6086/C Sweden/4","6090/S Sweden/3","6092/S Sweden/3","6094/S Sweden/1","6097/S Sweden/3","6098/S Sweden/3","6102/S Sweden/3","6104/S Sweden/2","6106/S Sweden/3","6108/S Sweden/3","6109/S Sweden/3","6111/S Sweden/3","6112/S Sweden/3","6114/S Sweden/3","6115/S Sweden/3","6118/S Sweden/2","6122/S Sweden/2","6124/S Sweden/3","6126/S Sweden/3","6127/S Sweden/3","6129/S Sweden/2","6131/S Sweden/2","6133/S Sweden/2","6134/S Sweden/3","6136/S Sweden/2","6138/S Sweden/2","6140/S Sweden/2","6142/S Sweden/2","6145/S Sweden/2","6148/S Sweden/2","6149/S Sweden/2","6151/S Sweden/1","6154/N Sweden/5","6163/N Sweden/5","6166/N Sweden/5","6169/N Sweden/5","6171/N Sweden/5","6173/N Sweden/5","6174/N Sweden/5","6177/N Sweden/5","6180/N Sweden/3","6184/N Sweden/5","6188/S Sweden/1","6189/S Sweden/1","6191/S Sweden/1","6192/S Sweden/1","6193/S Sweden/1","6194/S Sweden/3","6195/S Sweden/1","6197/S Sweden/1","6198/S Sweden/1","6201/S Sweden/3","6202/S Sweden/3","6203/S Sweden/1","6209/N Sweden/5","6210/N Sweden/5","6217/N Sweden/5","6218/N Sweden/5","6220/N Sweden/5","6221/N Sweden/5","6231/N Sweden/5","6235/N Sweden/5","6237/N Sweden/5","6238/N Sweden/5","6240/N Sweden/5","6241/N Sweden/5","6242/S Sweden/3","6243/S Sweden/3","6244/N Sweden/5","6252/S Sweden/6","6258/S Sweden/1","6276/S Sweden/6","6284/S Sweden/1","6413/S Sweden/3","6913/N Sweden/5","6918/N Sweden/5","6964/C Sweden/3","6973/S Sweden/3","7516/S Sweden/6","7519/S Sweden/3","8222/S Sweden/3","8227/N Sweden/5","8230/S Sweden/3","8231/S Sweden/3","8237/S Sweden/3","8241/S Sweden/3","8242/S Sweden/3","8249/S Sweden/3","8256/S Sweden/1","8258/S Sweden/1","8259/S Sweden/1","8283/S Sweden/1","8306/S Sweden/1","8307/S Sweden/1","8326/S Sweden/3","8334/S Sweden/3","8335/S Sweden/2","8351/C Sweden/4","8386/C Sweden/3","8387/C Sweden/3","8422/S Sweden/3","8423/S Sweden/1","8426/S Sweden/1","9058/S Sweden/6","9321/N Sweden/5","9323/N Sweden/5","9332/N Sweden/5","9336/N Sweden/4","9339/C Sweden/3","9343/S Sweden/6","9353/C Sweden/3","9356/N Sweden/5","9363/N Sweden/5","9369/C Sweden/3","9380/S Sweden/2","9382/S Sweden/6","9383/S Sweden/6","9386/N Sweden/5","9388/N Sweden/5","9391/C Sweden/3","9392/C Sweden/3","9399/S Sweden/1","9404/S Sweden/2","9405/S Sweden/2","9407/S Sweden/2","9408/S Sweden/3","9409/S Sweden/3","9412/C Sweden/3","9413/C Sweden/3","9416/S Sweden/6","9421/S Sweden/3","9427/N Sweden/5","9433/N Sweden/5","9434/N Sweden/5","9436/S Sweden/3","9437/S Sweden/3","9442/S Sweden/6","9450/S Sweden/6","9451/S Sweden/6","9452/S Sweden/6","9453/S Sweden/6","9454/S Sweden/6","9470/C Sweden/3","9476/S Sweden/6","9481/S Sweden/1","6043/N Sweden/5","6900/N Sweden/5","6974/S Sweden/3","7517/S Sweden/6","8240/S Sweden/3","8247/S Sweden/3","8369/S Sweden/2"],{"interactive":false,"permanent":false,"direction":"auto","opacity":1,"offset":[0,0],"textsize":"10px","textOnly":false,"className":"","sticky":true},null]},{"method":"addScaleBar","args":[{"maxWidth":100,"metric":true,"imperial":true,"updateWhenIdle":true,"position":"bottomleft"}]},{"method":"addHomeButton","args":[11.2,55.3833,18.5162,65.25,true,"loc - K_07","Zoom to loc - K_07","<strong> loc - K_07 <\/strong>","bottomright"]},{"method":"addLayersControl","args":["Esri.WorldImagery","loc - K_07",{"collapsed":true,"autoZIndex":true,"position":"topleft"}]},{"method":"addLegend","args":[{"colors":["#4B0055","#353E7C","#007094","#009B95","#00BE7D","#96D84B","#FDE333"],"labels":["1","2","3","4","5","6","7"],"na_color":null,"na_label":"NA","opacity":1,"position":"topright","type":"factor","title":"loc - K_07","extra":null,"layerId":null,"className":"info legend","group":"loc - K_07"}]}],"limits":{"lat":[55.3833,65.25],"lng":[11.2,18.5162]},"fitBounds":[55.3833,11.2,65.25,18.5162,[]]},"evals":[],"jsHooks":{"render":[{"code":"function(el, x, data) {\n  return (\n      function(el, x, data) {\n      // get the leaflet map\n      var map = this; //HTMLWidgets.find('#' + el.id);\n      // we need a new div element because we have to handle\n      // the mouseover output separately\n      // debugger;\n      function addElement () {\n      // generate new div Element\n      var newDiv = $(document.createElement('div'));\n      // append at end of leaflet htmlwidget container\n      $(el).append(newDiv);\n      //provide ID and style\n      newDiv.addClass('lnlt');\n      newDiv.css({\n      'position': 'relative',\n      'bottomleft':  '0px',\n      'background-color': 'rgba(255, 255, 255, 0.7)',\n      'box-shadow': '0 0 2px #bbb',\n      'background-clip': 'padding-box',\n      'margin': '0',\n      'padding-left': '5px',\n      'color': '#333',\n      'font': '9px/1.5 \"Helvetica Neue\", Arial, Helvetica, sans-serif',\n      'z-index': '700',\n      });\n      return newDiv;\n      }\n\n\n      // check for already existing lnlt class to not duplicate\n      var lnlt = $(el).find('.lnlt');\n\n      if(!lnlt.length) {\n      lnlt = addElement();\n\n      // grab the special div we generated in the beginning\n      // and put the mousmove output there\n\n      map.on('mousemove', function (e) {\n      if (e.originalEvent.ctrlKey) {\n      if (document.querySelector('.lnlt') === null) lnlt = addElement();\n      lnlt.text(\n                           ' lon: ' + (e.latlng.lng).toFixed(5) +\n                           ' | lat: ' + (e.latlng.lat).toFixed(5) +\n                           ' | zoom: ' + map.getZoom() +\n                           ' | x: ' + L.CRS.EPSG3857.project(e.latlng).x.toFixed(0) +\n                           ' | y: ' + L.CRS.EPSG3857.project(e.latlng).y.toFixed(0) +\n                           ' | epsg: 3857 ' +\n                           ' | proj4: +proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs ');\n      } else {\n      if (document.querySelector('.lnlt') === null) lnlt = addElement();\n      lnlt.text(\n                      ' lon: ' + (e.latlng.lng).toFixed(5) +\n                      ' | lat: ' + (e.latlng.lat).toFixed(5) +\n                      ' | zoom: ' + map.getZoom() + ' ');\n      }\n      });\n\n      // remove the lnlt div when mouse leaves map\n      map.on('mouseout', function (e) {\n      var strip = document.querySelector('.lnlt');\n      if( strip !==null) strip.remove();\n      });\n\n      };\n\n      //$(el).keypress(67, function(e) {\n      map.on('preclick', function(e) {\n      if (e.originalEvent.ctrlKey) {\n      if (document.querySelector('.lnlt') === null) lnlt = addElement();\n      lnlt.text(\n                      ' lon: ' + (e.latlng.lng).toFixed(5) +\n                      ' | lat: ' + (e.latlng.lat).toFixed(5) +\n                      ' | zoom: ' + map.getZoom() + ' ');\n      var txt = document.querySelector('.lnlt').textContent;\n      console.log(txt);\n      //txt.innerText.focus();\n      //txt.select();\n      setClipboardText('\"' + txt + '\"');\n      }\n      });\n\n      }\n      ).call(this.getMap(), el, x, data);\n}","data":null},{"code":"function(el, x, data) {\n  return (function(el,x,data){\n           var map = this;\n\n           map.on('keypress', function(e) {\n               console.log(e.originalEvent.code);\n               var key = e.originalEvent.code;\n               if (key === 'KeyE') {\n                   var bb = this.getBounds();\n                   var txt = JSON.stringify(bb);\n                   console.log(txt);\n\n                   setClipboardText('\\'' + txt + '\\'');\n               }\n           })\n        }).call(this.getMap(), el, x, data);\n}","data":null}]}}</script>
```



# Check-out how the groups are doing in the selection experiments



```r
library(ggbeeswarm)

fit=read.table("../data.for.paper/fitnesses.csv", h=T, sep=",", stringsAsFactors = F)%>%mutate(id=paste(id))%>%left_join(acc, by="id")->fit

p1=ggplot(fit, aes(x=group, y=NA.))+ geom_beeswarm(aes(col=group), corral = "gutter", size=3, alpha=0.3)+theme_pubclean()
p2=ggplot(fit, aes(x=K_DF, y=NA.))+ geom_beeswarm(aes(col=K_DF),corral = "gutter", size=3, alpha=0.3)+theme_pubclean()

ggarrange(p1, p2)
```

![](main_files/figure-html/unnamed-chunk-28-1.png)<!-- -->


```r
fit%>%dplyr::select(K_06, K_DF, NA., NB, SR, ST)%>%pivot_longer(cols=c(NA., NB, SR, ST), values_to = "fitness", names_to = "site")->df

df%>%ggplot(aes(x=K_06, y=fitness))+ geom_beeswarm(aes(col=K_06), corral = "gutter", size=5, alpha=0.8)+theme_pubclean()+facet_wrap(~site, nrow=2)
```

![](main_files/figure-html/unnamed-chunk-29-1.png)<!-- -->



```r
df%>%ggplot(aes(x=K_DF, y=fitness))+ geom_beeswarm(aes(col=K_DF),corral = "gutter", size=5, alpha=0.8)+theme_pubclean()+facet_wrap(~site, nrow=2)
```

![](main_files/figure-html/unnamed-chunk-30-1.png)<!-- -->



```r
fit%>%dplyr::select(K_07, K_DF, NA., NB, SR, ST)%>%pivot_longer(cols=c(NA., NB, SR, ST), values_to = "fitness", names_to = "site")->df

df%>%ggplot(aes(x=K_07, y=fitness))+ geom_beeswarm(aes(col=K_07), corral = "gutter", size=5, alpha=0.8)+theme_pubclean()+facet_wrap(~site, nrow=2)
```

![](main_files/figure-html/unnamed-chunk-31-1.png)<!-- -->




```r
fit%>%dplyr::select(K_07, K_DF,NA11, NA12, NM11, NM12, SR11, SR12, SU11, SU12)%>%pivot_longer(cols=c(NA11, NA12, NM11, NM12, SR11, SR12, SU11, SU12), values_to = "fitness", names_to = "site")->df

df%>%ggplot(aes(x=K_07, y=fitness))+ geom_beeswarm(aes(col=K_07), corral = "gutter", size=5, alpha=0.8)+geom_violin(fill=NA, draw_quantiles =0.5 )+theme_pubclean()+facet_wrap(~site, nrow=4)->p1
  ggsave(p1, filename = "figures/fitCG_K07.pdf", device="pdf", unit="in", width=8,height=12) 
```



```r
fit%>%dplyr::select(K_06, K_DF,NA11, NA12, NM11, NM12, SR11, SR12, SU11, SU12)%>%pivot_longer(cols=c(NA11, NA12, NM11, NM12, SR11, SR12, SU11, SU12), values_to = "fitness", names_to = "site")->df

df%>%ggplot(aes(x=K_06, y=fitness))+ geom_beeswarm(aes(col=K_06), corral = "gutter", size=5, alpha=0.8)+geom_violin(fill=NA, draw_quantiles =0.5 )+theme_pubclean()+facet_wrap(~site, nrow=4)->p1
  ggsave(p1, filename = "figures/fitCG_K06.pdf", device="pdf", unit="in", width=8,height=12) 
```



```r
fit%>%dplyr::select(group, NA11, NA12, NM11, NM12, SR11, SR12, SU11, SU12)%>%pivot_longer(cols=c(NA11, NA12, NM11, NM12, SR11, SR12, SU11, SU12), values_to = "fitness", names_to = "site")->df

df%>%ggplot(aes(x=group, y=fitness))+ geom_beeswarm(aes(col=group), corral = "gutter", size=5, alpha=0.8)+geom_violin(fill=NA, draw_quantiles =0.5 )+theme_pubclean()+facet_wrap(~site, nrow=4)->p1
  ggsave(p1, filename = "figures/fitCG_group.pdf", device="pdf", unit="in", width=8,height=12) 
```


```r
table(fit$group, fit$region)
```

```
##     
##      C Sweden N Sweden S Sweden
##   B         0        0        5
##   C         7        1        5
##   N1        0       37        0
##   N2        0       13        0
##   S1       11        2       90
##   S2        0        0       25
```


```r
library(ggalt)
```

```
## Registered S3 methods overwritten by 'ggalt':
##   method                  from   
##   grid.draw.absoluteGrob  ggplot2
##   grobHeight.absoluteGrob ggplot2
##   grobWidth.absoluteGrob  ggplot2
##   grobX.absoluteGrob      ggplot2
##   grobY.absoluteGrob      ggplot2
```

```r
library("RColorBrewer")    
pcadat=fit%>%dplyr::select(NA11, NA12, NM11, NM12, SR11, SR12, SU11, SU12, NA., NB, SR, ST)
row.names(pcadat)=fit$id
pca=princomp(pcadat)

df=data.frame(pca$scores[,1:3])
df$id=row.names(df)
df%>%left_join(fit, by="id")->df

ggplot(df, aes(x=Comp.1, y=Comp.2, col=K_07))+geom_encircle(aes(group=K_07, fill=K_07, alpha=0.2))+geom_point(size=5, alpha=0.7)+theme_pubclean()+scale_colour_brewer(palette = "Paired")->p1
ggplot(df, aes(y=Comp.2, x=Comp.3, col=K_07))+geom_encircle(aes(group=K_07, fill=K_07, alpha=0.2))+geom_point(size=5, alpha=0.7)+theme_pubclean()+scale_colour_brewer(palette = "Paired")->p2

ggsave(plot=ggarrange(p1, p2, common.legend = T), filename="figures/pcs.pdf", device="pdf", unit="in", width=16, height=8)
```


## Save Clustering for MN



```r
acc=readRDS("./res/acc_clusters.rds")
write.table(acc[, c(1:13, 54)], "./res/acc_clustering.txt", col.names=T, row.names=F, quote=F, sep="\t")
```


# For K_7, define names and colors and plot. 
## Name clusters

```r
acc=readRDS("./res/acc_clusters.rds")
acc=acc%>%dplyr::select(id, region, lon, lat,K_07)

table(acc$region, acc$K_07)
```

```
##           
##             1  2  3  4  5  6  7
##   C Sweden  0  0 11  7  0  0  0
##   N Sweden  0  0  2  2 51  0  0
##   S Sweden 33 22 31  0  0 25 16
```
I'll sort the groups by geographic center of mass.


```r
acc%>%group_by(K_07)%>%summarise(lat=mean(lat), lon=mean(lon))%>%arrange(lat)->clusters
kable(clusters)
```

<table>
 <thead>
  <tr>
   <th style="text-align:left;"> K_07 </th>
   <th style="text-align:right;"> lat </th>
   <th style="text-align:right;"> lon </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 6 </td>
   <td style="text-align:right;"> 55.74478 </td>
   <td style="text-align:right;"> 13.39591 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2 </td>
   <td style="text-align:right;"> 55.81171 </td>
   <td style="text-align:right;"> 13.43612 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:right;"> 55.91487 </td>
   <td style="text-align:right;"> 13.99508 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 7 </td>
   <td style="text-align:right;"> 56.57709 </td>
   <td style="text-align:right;"> 16.20376 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 3 </td>
   <td style="text-align:right;"> 56.76793 </td>
   <td style="text-align:right;"> 14.98931 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:right;"> 60.56252 </td>
   <td style="text-align:right;"> 16.55193 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:right;"> 62.88372 </td>
   <td style="text-align:right;"> 18.21475 </td>
  </tr>
</tbody>
</table>



```r
clusters%>%mutate(names=c("S1","S2", "S3", "B", "CS", "CN", "N" ))->clusters
clusters
```

```
## # A tibble: 7 × 4
##   K_07    lat   lon names
##   <chr> <dbl> <dbl> <chr>
## 1 6      55.7  13.4 S1   
## 2 2      55.8  13.4 S2   
## 3 1      55.9  14.0 S3   
## 4 7      56.6  16.2 B    
## 5 3      56.8  15.0 CS   
## 6 4      60.6  16.6 CN   
## 7 5      62.9  18.2 N
```

```r
acc%>%left_join(clusters%>%dplyr::select(-lat, -lon), by="K_07")->acc
acc
```

```
##       id   region     lon     lat K_07 names
## 1    991 S Sweden 14.0500 55.3833    6    S1
## 2    992 S Sweden 14.0500 55.3833    6    S1
## 3    997 S Sweden 14.0500 55.3833    6    S1
## 4   1061 S Sweden 14.1333 55.7167    1    S3
## 5   1062 S Sweden 14.1333 55.7167    2    S2
## 6   1063 S Sweden 14.1333 55.7167    1    S3
## 7   1158 S Sweden 16.5167 56.7000    3    CS
## 8   1254 C Sweden 17.0167 59.4333    4    CN
## 9   1257 C Sweden 17.0167 59.4333    4    CN
## 10  1317 C Sweden 16.8667 59.5667    4    CN
## 11  1318 C Sweden 16.8667 59.5667    4    CN
## 12  1363 C Sweden 17.5833 59.7833    4    CN
## 13  1435 N Sweden 18.2000 62.8000    3    CS
## 14  1552 N Sweden 18.3667 63.0833    5     N
## 15  1585 N Sweden 15.6000 65.2500    4    CN
## 16  5829 S Sweden 14.0612 55.3838    3    CS
## 17  5830 S Sweden 15.9667 56.3333    3    CS
## 18  5831 S Sweden 15.9667 56.3333    1    S3
## 19  5832 S Sweden 15.9667 56.3333    1    S3
## 20  5856 N Sweden 17.4914 63.0167    5     N
## 21  5865 S Sweden 14.1200 55.7600    3    CS
## 22  5867 S Sweden 14.1200 55.7600    3    CS
## 23  6009 N Sweden 18.1770 62.8770    5     N
## 24  6011 N Sweden 18.1770 62.8770    5     N
## 25  6012 N Sweden 18.1770 62.8770    5     N
## 26  6013 N Sweden 18.1770 62.8770    5     N
## 27  6016 N Sweden 18.4000 62.9000    5     N
## 28  6017 N Sweden 18.4000 62.9000    5     N
## 29  6019 S Sweden 14.2900 56.0600    3    CS
## 30  6020 S Sweden 14.2900 56.0600    3    CS
## 31  6021 S Sweden 14.2900 56.0600    3    CS
## 32  6023 S Sweden 13.3712 55.7509    2    S2
## 33  6025 N Sweden 17.7339 62.6437    5     N
## 34  6030 N Sweden 18.1896 62.8060    5     N
## 35  6034 S Sweden 13.7400 56.1000    1    S3
## 36  6035 S Sweden 13.7400 56.1000    1    S3
## 37  6036 S Sweden 13.7400 56.1000    1    S3
## 38  6038 S Sweden 13.7400 56.1000    1    S3
## 39  6039 S Sweden 13.7400 56.1000    1    S3
## 40  6040 S Sweden 13.4000 55.6600    2    S2
## 41  6041 S Sweden 14.7750 56.0328    1    S3
## 42  6042 S Sweden 13.9000 56.0900    3    CS
## 43  6046 N Sweden 18.0790 62.8010    5     N
## 44  6064 N Sweden 18.2763 62.9513    5     N
## 45  6069 N Sweden 18.2763 62.9513    5     N
## 46  6071 N Sweden 18.3448 62.9308    5     N
## 47  6073 S Sweden 15.8155 56.1481    3    CS
## 48  6085 S Sweden 13.2145 55.7097    6    S1
## 49  6086 C Sweden 11.2000 58.9000    4    CN
## 50  6090 S Sweden 13.2197 55.6525    6    S1
## 51  6092 S Sweden 13.2233 55.6514    6    S1
## 52  6094 S Sweden 13.2147 55.6494    1    S3
## 53  6097 S Sweden 13.2264 55.6481    6    S1
## 54  6098 S Sweden 13.2178 55.6561    6    S1
## 55  6102 S Sweden 13.2000 55.6000    6    S1
## 56  6104 S Sweden 13.2000 55.7000    2    S2
## 57  6106 S Sweden 13.1186 55.7931    6    S1
## 58  6108 S Sweden 13.1206 55.7989    3    CS
## 59  6109 S Sweden 13.1233 55.7936    6    S1
## 60  6111 S Sweden 13.1219 55.7989    6    S1
## 61  6112 S Sweden 13.1044 55.7967    6    S1
## 62  6114 S Sweden 13.1342 55.8097    6    S1
## 63  6115 S Sweden 13.1367 55.8000    6    S1
## 64  6118 S Sweden 13.2000 55.7000    2    S2
## 65  6122 S Sweden 13.3075 55.8364    2    S2
## 66  6124 S Sweden 13.3092 55.8378    6    S1
## 67  6126 S Sweden 13.3047 55.8411    6    S1
## 68  6127 S Sweden 13.3058 55.8428    6    S1
## 69  6129 S Sweden 13.2875 55.8544    2    S2
## 70  6131 S Sweden 13.3181 55.8369    2    S2
## 71  6133 S Sweden 13.2906 55.8364    2    S2
## 72  6134 S Sweden 13.2906 55.8383    6    S1
## 73  6136 S Sweden 13.5519 55.9336    2    S2
## 74  6138 S Sweden 13.5511 55.9403    2    S2
## 75  6140 S Sweden 13.5539 55.9392    2    S2
## 76  6142 S Sweden 13.5558 55.9428    2    S2
## 77  6145 S Sweden 13.5533 55.9497    2    S2
## 78  6148 S Sweden 13.5508 55.9319    2    S2
## 79  6149 S Sweden 13.5481 55.9281    2    S2
## 80  6151 S Sweden 13.2244 55.6528    1    S3
## 81  6154 N Sweden 17.7406 62.6422    5     N
## 82  6163 N Sweden 17.7356 62.6425    5     N
## 83  6166 N Sweden 17.7372 62.6425    5     N
## 84  6169 N Sweden 18.3447 62.8714    5     N
## 85  6171 N Sweden 18.3444 62.8717    5     N
## 86  6173 N Sweden 18.3419 62.8717    5     N
## 87  6174 N Sweden 18.3422 62.8719    5     N
## 88  6177 N Sweden 17.6900 62.6322    5     N
## 89  6180 N Sweden 17.6906 62.6322    3    CS
## 90  6184 N Sweden 18.4522 62.8892    5     N
## 91  6188 S Sweden 14.1386 55.7683    1    S3
## 92  6189 S Sweden 14.1383 55.7686    1    S3
## 93  6191 S Sweden 14.1375 55.7689    1    S3
## 94  6192 S Sweden 14.1369 55.7692    1    S3
## 95  6193 S Sweden 14.1347 55.7694    1    S3
## 96  6194 S Sweden 14.1342 55.7706    3    CS
## 97  6195 S Sweden 14.1342 55.7708    1    S3
## 98  6197 S Sweden 14.1333 55.7714    1    S3
## 99  6198 S Sweden 14.1331 55.7708    1    S3
## 100 6201 S Sweden 14.1211 55.7719    3    CS
## 101 6202 S Sweden 14.1206 55.7717    3    CS
## 102 6203 S Sweden 14.1208 55.7714    1    S3
## 103 6209 N Sweden 18.1842 62.8836    5     N
## 104 6210 N Sweden 18.1836 62.8839    5     N
## 105 6217 N Sweden 18.3283 63.0169    5     N
## 106 6218 N Sweden 18.3283 63.0172    5     N
## 107 6220 N Sweden 18.1896 62.8060    5     N
## 108 6221 N Sweden 18.1896 62.8060    5     N
## 109 6231 N Sweden 18.2844 62.9600    5     N
## 110 6235 N Sweden 18.3589 62.9611    5     N
## 111 6237 N Sweden 18.3500 62.9619    5     N
## 112 6238 N Sweden 18.3500 62.9619    5     N
## 113 6240 N Sweden 18.3500 62.9622    5     N
## 114 6241 N Sweden 18.3608 62.9614    5     N
## 115 6242 S Sweden 13.2000 55.7000    6    S1
## 116 6243 S Sweden 13.8500 55.9500    3    CS
## 117 6244 N Sweden 18.4728 62.9169    5     N
## 118 6252 S Sweden 14.3336 55.5796    7     B
## 119 6258 S Sweden 14.3336 55.5796    1    S3
## 120 6276 S Sweden 14.3336 55.5796    7     B
## 121 6284 S Sweden 14.3336 55.5796    1    S3
## 122 6413 S Sweden 13.9700 56.0600    3    CS
## 123 6913 N Sweden 18.1770 62.8770    5     N
## 124 6918 N Sweden 18.3174 63.0165    5     N
## 125 6964 C Sweden 16.0000 56.3000    3    CS
## 126 6973 S Sweden 13.9707 56.0648    6    S1
## 127 7516 S Sweden 14.3340 55.5800    7     B
## 128 7519 S Sweden 15.7735 56.1509    3    CS
## 129 8222 S Sweden 14.7750 56.0328    3    CS
## 130 8227 N Sweden 17.9103 62.7989    5     N
## 131 8230 S Sweden 16.5000 56.6800    3    CS
## 132 8231 S Sweden 16.0000 56.3000    3    CS
## 133 8237 S Sweden 13.1000 55.8000    6    S1
## 134 8241 S Sweden 13.8210 55.9473    3    CS
## 135 8242 S Sweden 15.7884 56.1494    3    CS
## 136 8249 S Sweden 15.8000 57.7000    3    CS
## 137 8256 S Sweden 12.9000 56.4000    1    S3
## 138 8258 S Sweden 12.9000 56.4000    1    S3
## 139 8259 S Sweden 12.9000 56.4000    1    S3
## 140 8283 S Sweden 14.1200 55.7600    1    S3
## 141 8306 S Sweden 13.7400 56.1000    1    S3
## 142 8307 S Sweden 13.7400 56.1000    1    S3
## 143 8326 S Sweden 14.7750 56.0328    3    CS
## 144 8334 S Sweden 13.2000 55.7100    3    CS
## 145 8335 S Sweden 13.2000 55.7100    2    S2
## 146 8351 C Sweden 18.3700 60.2500    4    CN
## 147 8386 C Sweden 11.2000 58.9000    3    CS
## 148 8387 C Sweden 18.0000 59.0000    3    CS
## 149 8422 S Sweden 14.2900 56.0600    6    S1
## 150 8423 S Sweden 13.7400 56.1000    1    S3
## 151 8426 S Sweden 13.9700 56.0600    1    S3
## 152 9058 S Sweden 16.6333 57.7500    7     B
## 153 9321 N Sweden 18.3360 62.8622    5     N
## 154 9323 N Sweden 18.3360 62.8622    5     N
## 155 9332 N Sweden 18.3810 62.8698    5     N
## 156 9336 N Sweden 18.4473 62.8794    4    CN
## 157 9339 C Sweden 15.0689 57.7133    3    CS
## 158 9343 S Sweden 18.1512 57.3089    7     B
## 159 9353 C Sweden 16.3675 57.2608    3    CS
## 160 9356 N Sweden 18.1746 62.8762    5     N
## 161 9363 N Sweden 18.4045 62.9147    5     N
## 162 9369 C Sweden 14.9986 57.6781    3    CS
## 163 9380 S Sweden 13.3742 55.7488    2    S2
## 164 9382 S Sweden 14.2091 55.8106    7     B
## 165 9383 S Sweden 14.2091 55.8106    7     B
## 166 9386 N Sweden 18.1896 62.8060    5     N
## 167 9388 N Sweden 18.1896 62.8060    5     N
## 168 9391 C Sweden 15.8979 57.3263    3    CS
## 169 9392 C Sweden 15.8979 57.3263    3    CS
## 170 9399 S Sweden 13.9905 55.4234    1    S3
## 171 9404 S Sweden 13.3990 55.7491    2    S2
## 172 9405 S Sweden 13.3990 55.7491    2    S2
## 173 9407 S Sweden 13.3990 55.7491    2    S2
## 174 9408 S Sweden 13.9519 56.0470    3    CS
## 175 9409 S Sweden 14.3020 56.0573    3    CS
## 176 9412 C Sweden 16.1494 57.2746    3    CS
## 177 9413 C Sweden 16.1494 57.2746    3    CS
## 178 9416 S Sweden 18.3837 57.7215    7     B
## 179 9421 S Sweden 14.3997 55.9745    3    CS
## 180 9427 N Sweden 18.4055 62.8815    5     N
## 181 9433 N Sweden 18.2763 62.9513    5     N
## 182 9434 N Sweden 18.3659 62.8959    5     N
## 183 9436 S Sweden 14.6806 56.1633    3    CS
## 184 9437 S Sweden 14.6806 56.1633    3    CS
## 185 9442 S Sweden 14.3398 55.5678    7     B
## 186 9450 S Sweden 18.2109 57.2545    7     B
## 187 9451 S Sweden 18.2109 57.2545    7     B
## 188 9452 S Sweden 18.2109 57.2545    7     B
## 189 9453 S Sweden 18.5162 57.8009    7     B
## 190 9454 S Sweden 18.5162 57.8009    7     B
## 191 9470 C Sweden 14.8043 57.6511    3    CS
## 192 9476 S Sweden 14.3336 55.5796    7     B
## 193 9481 S Sweden 13.8484 55.4242    1    S3
## 194 6043 N Sweden 18.0790 62.8010    5     N
## 195 6900 N Sweden 18.4840 63.3240    5     N
## 196 6974 S Sweden 13.9707 56.0648    3    CS
## 197 7517 S Sweden 14.3340 55.5800    7     B
## 198 8240 S Sweden 13.1960 55.7050    6    S1
## 199 8247 S Sweden 13.7400 56.0700    6    S1
## 200 8369 S Sweden 13.4504 55.6942    2    S2
```
## Assign colors to clusters


DF's colors


```r
colors_groups = as.character(c("#e2282f","#9bc5db","#1f6ba7","#fd9194","#a4d986","#1f9436"))
                               names(colors_groups) = c("B","S1","S2","C","N1","N2")
```

New colors


```r
colors=data.frame(cluster=clusters$names, color1=c("#596d8f", "#8f8fae", "#c1b5cd","#eedded", "#e9abca", "#e47695", "#d43d51"), color2=c("#de425b","#eb7a95","#f0acc5","#1f9436","#dd92d4","#a94ed3","#1e1cde"), 
color3=c("#6d0a1b", "#a05252", "#d19390", "#1f9436","#ffd7d4","#6996b3","#004c6d"), colordf=c("#1f6ba7","#8f8fae","#9bc5db","#e2282f","#fd9194","#a4d986","#1f9436"), 
color4=c("#004c6d", "#4d7c9b", "#87b0cc","#e2282f", "#c3e7ff", "#a4d986","#1f9436")
)%>%mutate(cluster=factor(cluster, levels=paste(cluster)))

colors%>%pivot_longer(cols=starts_with("color"), values_to = "color", names_to = "set")->colsets


colsets%>%ggplot(aes(x=cluster, y=set))+geom_point(size=20, pch=16, aes(col=color))+ scale_colour_identity()+theme_pubclean()->p1 

ggsave(plot=p1,filename="./figures/color_choices.pdf",unit="in", width=8, height=8)
```

I choose set "color_4". 

## Make new maps and plots. 


```r
colset=colors$color4
names(colset)=colors$cluster
```



```r
fit=read.table("../data.for.paper/fitnesses.csv", h=T, sep=",", stringsAsFactors = F)%>%dplyr::select(!group)%>%mutate(id=paste(id))%>%left_join(acc, by="id")%>%mutate(group=factor(names, levels=paste(names(colset))))%>%dplyr::select(-names)
```


```r
fit%>%dplyr::select(id, group, NA11, NA12, NM11, NM12, SR11, SR12, SU11, SU12)%>%pivot_longer(cols=c(NA11, NA12, NM11, NM12, SR11, SR12, SU11, SU12), values_to = "fitness", names_to = "site")->df

df%>%ggplot(aes(x=group, y=fitness))+ geom_beeswarm(aes(col=group),  size=2, alpha=0.8)+geom_violin(fill=NA, draw_quantiles =0.5 )+theme_pubclean()+facet_wrap(~site, nrow=4)+scale_colour_manual(values = colset)->p1
  ggsave(p1, filename = "figures/fitCG_New_groups.pdf", device="pdf", unit="in", width=8,height=12) 
```



```r
fit%>%dplyr::select(id, group, NA., NB, SR, ST)%>%pivot_longer(cols=c(NA., NB, SR, ST), values_to = "fitness", names_to = "site")->df

df%>%ggplot(aes(x=group, y=fitness))+ geom_beeswarm(aes(col=group), corral = "gutter", size=2, alpha=1)+geom_violin(fill=NA, draw_quantiles = 0.5)+theme_pubclean()+facet_wrap(~site, nrow=4)+scale_colour_manual(values = colset)->p1
  ggsave(p1, filename = "figures/SelectionExp_New_groups.pdf", device="pdf", unit="in", width=8,height=12) 
  
df%>%filter(group!="B")%>%ggplot(aes(x=group, y=fitness))+ geom_beeswarm(aes(col=group), corral = "gutter", size=2, alpha=1)+geom_violin(fill=NA, draw_quantiles = 0.5)+theme_pubclean()+facet_wrap(~site, nrow=4)+scale_colour_manual(values = colset)->p1

ggsave(p1, filename = "figures/SelectionExp_New_groups_nobeach.pdf", device="pdf", unit="in", width=8,height=12) 
  

df%>%ggplot(aes(x=group, y=log(fitness+0.01)))+ geom_beeswarm(aes(col=group), corral = "gutter", size=2, alpha=1)+geom_violin(fill=NA, draw_quantiles = 0.5)+theme_pubclean()+facet_wrap(~site, nrow=4)+scale_colour_manual(values = colset)->p1
  ggsave(p1, filename = "figures/SelectionExp_New_groups_logfit.pdf", device="pdf", unit="in", width=8,height=12) 
```


















# Estimating fitness heritability within experiments

## DF's way: 

Daniel did this with a lm and this little function: 
### preparing the data


```r
d11=readRDS(file="./data/d11_for_analysis.rds")
d12=readRDS(file="./data/d12_for_analysis.rds")


##restrict to fitness
traits=c("fitness")

d11$year=2011
d12$year=2012

d11=d11[, c("year", "exp", "block", "id", "fitness")]
d12=d12[, c("year", "exp", "block", "id", "fitness")]

d=rbind(d11, d12)
d$region[d$exp%in%c("ULL", "RAT")]="S"
d$region[d$exp%in%c("RAM", "ADA")]="N"
d$year=as.factor(d$year)
d=d[, c("year", "region", "exp","block", "id", "fitness")]
d=na.omit(d)

#add short names of sites
exp.index <- as.data.frame(cbind(c("ULL", "RAT", "RAM", "ADA"), c("SU", "SR","NM", "NA")))
colnames(exp.index) <- c("exp", "exp.s")
d <- merge(d, exp.index, all.x=TRUE)

## will also look at background effects captured as genetic or K groups in subsection 5
kg <- readRDS("./res/acc_clusters.rds")
```

```r
lm.pve <- function(up.lm){
  ssq <- anova(up.lm)[[2]]
  pve <- ssq/sum(ssq) *100
  names(pve) <- rownames(anova(up.lm))
  return(pve)
}
```

### Estimating PVE

This is how it goes: 


```r
combos <- expand.grid(unique(d$year), unique(d$exp))

check.pvals <- function(d,y,e){
  up.d <- d[d$year==y & d$exp==e,]
  up.lm <- lm(fitness~id+block, data=up.d)
  return(anova(up.lm))
  }

exp.anovas <- apply(combos, 1, function(x){check.pvals(d=d, y=x[1], e=x[2])}) # all genotypes significant, block sig in all but RAM_2012
exp.pvals <- do.call(rbind, lapply(exp.anovas, function(x){x[5][1:2,1]}))
colnames(exp.pvals) <- c("genotype.pval", "block.pval")

get.pve <- function(d, y, e){
  up.d <- d[d$year==y & d$exp==e,]
  up.lm <- lm(fitness~id+block, data=up.d)
  pve <- lm.pve(up.lm)
  return(pve)
}

exp.pves <- apply(combos, 1, function(x){get.pve(d=d, y=x[1], e=x[2])})

exp.pves <- as.data.frame(t(exp.pves))
exp.pves <- cbind(combos, exp.pves)
colnames(exp.pves) <- c("year", "site", "genotype", "block", "residuals")
exp.pves$site <- gsub("ULL", "SU", exp.pves$site)
exp.pves$site <- gsub("RAT", "SR", exp.pves$site)
exp.pves$site <- gsub("RAM", "NM", exp.pves$site)
exp.pves$site <- gsub("ADA", "NA", exp.pves$site)
exp.pves <- exp.pves[order(exp.pves$site),]

xtab.pve <- xtable(exp.pves)
print(xtab.pve, file="./res/pve.table.txt")

table1 <- cbind(exp.pves, exp.pvals)
xtab.table1 <- xtable(table1)
digits(xtab.table1) <- c(0,0,0,2,2,2,-1,-1)
print(xtab.table1, file="./res/table1.txt",include.rownames=FALSE)
kable(table1)
```

<table>
 <thead>
  <tr>
   <th style="text-align:left;"> year </th>
   <th style="text-align:left;"> site </th>
   <th style="text-align:right;"> genotype </th>
   <th style="text-align:right;"> block </th>
   <th style="text-align:right;"> residuals </th>
   <th style="text-align:right;"> genotype.pval </th>
   <th style="text-align:right;"> block.pval </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 2011 </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> 10.29647 </td>
   <td style="text-align:right;"> 4.8350994 </td>
   <td style="text-align:right;"> 84.86844 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2012 </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> 14.42850 </td>
   <td style="text-align:right;"> 13.4083299 </td>
   <td style="text-align:right;"> 72.16317 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2011 </td>
   <td style="text-align:left;"> NM </td>
   <td style="text-align:right;"> 18.19622 </td>
   <td style="text-align:right;"> 0.4740708 </td>
   <td style="text-align:right;"> 81.32971 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0000256 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2012 </td>
   <td style="text-align:left;"> NM </td>
   <td style="text-align:right;"> 16.46388 </td>
   <td style="text-align:right;"> 0.0028119 </td>
   <td style="text-align:right;"> 83.53331 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.9591024 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2011 </td>
   <td style="text-align:left;"> SR </td>
   <td style="text-align:right;"> 16.80541 </td>
   <td style="text-align:right;"> 0.8136187 </td>
   <td style="text-align:right;"> 82.38097 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0000001 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2012 </td>
   <td style="text-align:left;"> SR </td>
   <td style="text-align:right;"> 13.65054 </td>
   <td style="text-align:right;"> 2.7090905 </td>
   <td style="text-align:right;"> 83.64037 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2011 </td>
   <td style="text-align:left;"> SU </td>
   <td style="text-align:right;"> 16.54386 </td>
   <td style="text-align:right;"> 3.0097519 </td>
   <td style="text-align:right;"> 80.44638 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 2012 </td>
   <td style="text-align:left;"> SU </td>
   <td style="text-align:right;"> 25.53496 </td>
   <td style="text-align:right;"> 4.4047287 </td>
   <td style="text-align:right;"> 70.06031 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>


## Decomposing overall variance

### With id effect

Only code from DF. 


```r
### do as normal lm first to understand specification, then move to glm so can use tweedie
lm1 <- lm(fitness~id*year*exp, data=d)

lm3 <- lm(fitness~id*year*exp + exp:year/block, data=d)

#plot(lm3)
```



```r
anova(lm1,lm3) ### yes, lm3 is much better.  The block helps.
```

```
## Analysis of Variance Table
## 
## Model 1: fitness ~ id * year * exp
## Model 2: fitness ~ id * year * exp + exp:year/block
##   Res.Df     RSS Df Sum of Sq      F    Pr(>F)    
## 1  23808 0.84223                                  
## 2  23792 0.81493 16  0.027295 49.806 < 2.2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```



```r
anova(lm3)
```

```
## Analysis of Variance Table
## 
## Response: fitness
##                   Df  Sum Sq   Mean Sq  F value    Pr(>F)    
## id               199 0.05316 0.0002672   7.7997 < 2.2e-16 ***
## year               1 0.00068 0.0006794  19.8350 8.481e-06 ***
## exp                3 0.01622 0.0054059 157.8252 < 2.2e-16 ***
## id:year          199 0.02394 0.0001203   3.5118 < 2.2e-16 ***
## id:exp           597 0.04704 0.0000788   2.3005 < 2.2e-16 ***
## year:exp           3 0.08364 0.0278815 813.9999 < 2.2e-16 ***
## id:year:exp      595 0.03653 0.0000614   1.7923 < 2.2e-16 ***
## year:exp:block    16 0.02730 0.0017060  49.8056 < 2.2e-16 ***
## Residuals      23792 0.81493 0.0000343                       
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```



```r
### test model fits by removing some components

lm4 <- lm(fitness~id + year*exp + exp:year/block, data=d)
lm5 <- lm(fitness~id, data=d)
lm6 <- lm(fitness~year*exp + exp:year/block, data=d)
pander(anova(lm3, lm4))
```


----------------------------------------------------------
 Res.Df    RSS      Df     Sum of Sq     F       Pr(>F)   
-------- -------- ------- ----------- ------- ------------
 23792    0.8149    NA        NA        NA         NA     

 25183    0.9209   -1391    -0.106     2.224   5.746e-119 
----------------------------------------------------------

Table: Analysis of Variance Table



```r
pander(anova(lm3, lm5))
```


------------------------------------------------------
 Res.Df    RSS      Df     Sum of Sq     F     Pr(>F) 
-------- -------- ------- ----------- ------- --------
 23792    0.8149    NA        NA        NA       NA   

 25206     1.05    -1414    -0.2353    4.859     0    
------------------------------------------------------

Table: Analysis of Variance Table



```r
pander(anova(lm3, lm6))
```


----------------------------------------------------------
 Res.Df    RSS      Df     Sum of Sq     F       Pr(>F)   
-------- -------- ------- ----------- ------- ------------
 23792    0.8149    NA        NA        NA         NA     

 25382    0.9746   -1590    -0.1597    2.932   4.441e-261 
----------------------------------------------------------

Table: Analysis of Variance Table



```r
lm.pve <- function(up.lm){
  ssq <- anova(up.lm)[[2]]
  pve <- ssq/sum(ssq) *100
  names(pve) <- rownames(anova(up.lm))
  return(pve)
}

pve3 <- lm.pve(lm3)  
pve1 <- lm.pve(lm1)  

#(summary(lm3))
pander(anova(lm3))
```


---------------------------------------------------------------------------
       &nbsp;          Df      Sum Sq      Mean Sq    F value     Pr(>F)   
-------------------- ------- ----------- ----------- --------- ------------
       **id**          199     0.05316    0.0002672     7.8     5.165e-200 

      **year**          1     0.0006794   0.0006794    19.84    8.481e-06  

      **exp**           3      0.01622    0.005406     157.8    2.704e-101 

    **id:year**        199     0.02394    0.0001203    3.512    1.159e-55  

     **id:exp**        597     0.04704    7.88e-05      2.3     2.025e-60  

    **year:exp**        3      0.08364     0.02788      814         0      

  **id:year:exp**      595     0.03653    6.139e-05    1.792    2.976e-28  

 **year:exp:block**    16      0.0273     0.001706     49.81    1.589e-156 

   **Residuals**      23792    0.8149     3.425e-05     NA          NA     
---------------------------------------------------------------------------

Table: Analysis of Variance Table




```r
# make anova table with PVE
lm3.tab <- as.data.frame(anova(lm3))
lm3.tab$PVE <- pve3
rownames(lm3.tab)[8] <- "year:exp/block"
xtab.lm3 <- xtable(lm3.tab)
print(xtab.lm3, file="./res/fixed.effects.anova.table.txt")
kable(xtab.lm3)
```

<table>
 <thead>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:right;"> Df </th>
   <th style="text-align:right;"> Sum Sq </th>
   <th style="text-align:right;"> Mean Sq </th>
   <th style="text-align:right;"> F value </th>
   <th style="text-align:right;"> Pr(&gt;F) </th>
   <th style="text-align:right;"> PVE </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> id </td>
   <td style="text-align:right;"> 199 </td>
   <td style="text-align:right;"> 0.0531644 </td>
   <td style="text-align:right;"> 0.0002672 </td>
   <td style="text-align:right;"> 7.799684 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 4.8180570 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> year </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0006794 </td>
   <td style="text-align:right;"> 0.0006794 </td>
   <td style="text-align:right;"> 19.835010 </td>
   <td style="text-align:right;"> 8.5e-06 </td>
   <td style="text-align:right;"> 0.0615707 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> exp </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0162177 </td>
   <td style="text-align:right;"> 0.0054059 </td>
   <td style="text-align:right;"> 157.825182 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 1.4697363 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> id:year </td>
   <td style="text-align:right;"> 199 </td>
   <td style="text-align:right;"> 0.0239374 </td>
   <td style="text-align:right;"> 0.0001203 </td>
   <td style="text-align:right;"> 3.511824 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 2.1693401 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> id:exp </td>
   <td style="text-align:right;"> 597 </td>
   <td style="text-align:right;"> 0.0470419 </td>
   <td style="text-align:right;"> 0.0000788 </td>
   <td style="text-align:right;"> 2.300482 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 4.2631940 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> year:exp </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0836444 </td>
   <td style="text-align:right;"> 0.0278815 </td>
   <td style="text-align:right;"> 813.999915 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 7.5803189 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> id:year:exp </td>
   <td style="text-align:right;"> 595 </td>
   <td style="text-align:right;"> 0.0365275 </td>
   <td style="text-align:right;"> 0.0000614 </td>
   <td style="text-align:right;"> 1.792307 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 3.3103290 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> year:exp/block </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0272954 </td>
   <td style="text-align:right;"> 0.0017060 </td>
   <td style="text-align:right;"> 49.805592 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 2.4736597 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Residuals </td>
   <td style="text-align:right;"> 23792 </td>
   <td style="text-align:right;"> 0.8149335 </td>
   <td style="text-align:right;"> 0.0000343 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 73.8537943 </td>
  </tr>
</tbody>
</table>



```r
table2 <- xtab.lm3
digits(table2) <- c(0,0,-1,-1,2,-1,2)
print(table2, file="./res/table2.txt")

kable(table2)
```

<table>
 <thead>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:right;"> Df </th>
   <th style="text-align:right;"> Sum Sq </th>
   <th style="text-align:right;"> Mean Sq </th>
   <th style="text-align:right;"> F value </th>
   <th style="text-align:right;"> Pr(&gt;F) </th>
   <th style="text-align:right;"> PVE </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> id </td>
   <td style="text-align:right;"> 199 </td>
   <td style="text-align:right;"> 0.0531644 </td>
   <td style="text-align:right;"> 0.0002672 </td>
   <td style="text-align:right;"> 7.799684 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 4.8180570 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> year </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0006794 </td>
   <td style="text-align:right;"> 0.0006794 </td>
   <td style="text-align:right;"> 19.835010 </td>
   <td style="text-align:right;"> 8.5e-06 </td>
   <td style="text-align:right;"> 0.0615707 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> exp </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0162177 </td>
   <td style="text-align:right;"> 0.0054059 </td>
   <td style="text-align:right;"> 157.825182 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 1.4697363 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> id:year </td>
   <td style="text-align:right;"> 199 </td>
   <td style="text-align:right;"> 0.0239374 </td>
   <td style="text-align:right;"> 0.0001203 </td>
   <td style="text-align:right;"> 3.511824 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 2.1693401 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> id:exp </td>
   <td style="text-align:right;"> 597 </td>
   <td style="text-align:right;"> 0.0470419 </td>
   <td style="text-align:right;"> 0.0000788 </td>
   <td style="text-align:right;"> 2.300482 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 4.2631940 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> year:exp </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0836444 </td>
   <td style="text-align:right;"> 0.0278815 </td>
   <td style="text-align:right;"> 813.999915 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 7.5803189 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> id:year:exp </td>
   <td style="text-align:right;"> 595 </td>
   <td style="text-align:right;"> 0.0365275 </td>
   <td style="text-align:right;"> 0.0000614 </td>
   <td style="text-align:right;"> 1.792307 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 3.3103290 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> year:exp/block </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0272954 </td>
   <td style="text-align:right;"> 0.0017060 </td>
   <td style="text-align:right;"> 49.805592 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 2.4736597 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Residuals </td>
   <td style="text-align:right;"> 23792 </td>
   <td style="text-align:right;"> 0.8149335 </td>
   <td style="text-align:right;"> 0.0000343 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 73.8537943 </td>
  </tr>
</tbody>
</table>

### With a K cluster effect


```r
dk <-merge(d, kg%>%dplyr::select(-starts_with("K_"), K_02:K_07), all.x=TRUE, by="id")

#check models and get pve for all 7 ks
ks <- colnames(dk)[grep("K_", colnames(dk))]
ks <- ks[order(ks)]

k.anovas <- lapply(as.list(ks), function(up.k){
  up.lm <- lm(fitness~as.factor(get(up.k))*year*exp + exp:year/block, data=dk)
  return(anova(up.lm))
})

k.pve <- sapply(ks, function(up.k){
  up.lm <- lm(fitness~as.factor(get(up.k))*year*exp + exp:year/block, data=dk)
  return(lm.pve(up.lm))
})
k.pve <- as.data.frame(k.pve)
k.pve$terms <- gsub("as.factor(get(up.k))", "k.group", rownames(k.pve), fixed=TRUE)

k.pve.l <- gather(k.pve, kgroup, pve,K_02:K_07)

kterms <- k.pve[grep("k.group", k.pve$terms),"terms"]
k.pve.plot <- ggplot(k.pve.l[k.pve.l$terms%in%kterms,], aes(x=kgroup, y=pve, group=terms, color=terms)) + geom_line()
ggsave(k.pve.plot, file="./figures/pve.by.kgroup.number.pdf", width=6, height=4)
k.pve.plot
```

![](main_files/figure-html/unnamed-chunk-49-1.png)<!-- -->

## output model with K_07


```r
lm.k7 <-  lm(fitness~as.factor(K_07)*year*exp + exp:year/block, data=dk)

lm.k7.tab <- as.data.frame(anova(lm.k7))
lm.k7.tab$PVE <- lm.pve(lm.k7)
rownames(lm.k7.tab)[8] <- "year:exp/block"
xtab.lm.k7 <- xtable(lm.k7.tab)
#print(xtab.lm.k6, file="./res/fixed.effects.k6.anova.table.txt")

table4_k7 <- xtab.lm.k7

#print(table4, file="./res/table4.txt")
digits(table4_k7) <- c(0,0,-1,-1,2,-1,2)
kable(table4_k7)
```

<table>
 <thead>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:right;"> Df </th>
   <th style="text-align:right;"> Sum Sq </th>
   <th style="text-align:right;"> Mean Sq </th>
   <th style="text-align:right;"> F value </th>
   <th style="text-align:right;"> Pr(&gt;F) </th>
   <th style="text-align:right;"> PVE </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> as.factor(K_07) </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0050217 </td>
   <td style="text-align:right;"> 0.0008369 </td>
   <td style="text-align:right;"> 22.199290 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 0.4550919 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> year </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0007289 </td>
   <td style="text-align:right;"> 0.0007289 </td>
   <td style="text-align:right;"> 19.332485 </td>
   <td style="text-align:right;"> 1.1e-05 </td>
   <td style="text-align:right;"> 0.0660536 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> exp </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0157777 </td>
   <td style="text-align:right;"> 0.0052592 </td>
   <td style="text-align:right;"> 139.496819 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 1.4298625 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> as.factor(K_07):year </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0058207 </td>
   <td style="text-align:right;"> 0.0009701 </td>
   <td style="text-align:right;"> 25.731362 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 0.5275004 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> as.factor(K_07):exp </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0061372 </td>
   <td style="text-align:right;"> 0.0003410 </td>
   <td style="text-align:right;"> 9.043543 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 0.5561857 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> year:exp </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0852982 </td>
   <td style="text-align:right;"> 0.0284327 </td>
   <td style="text-align:right;"> 754.154648 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 7.7301941 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> as.factor(K_07):year:exp </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0025721 </td>
   <td style="text-align:right;"> 0.0001429 </td>
   <td style="text-align:right;"> 3.790151 </td>
   <td style="text-align:right;"> 1.0e-07 </td>
   <td style="text-align:right;"> 0.2330976 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> year:exp/block </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0269566 </td>
   <td style="text-align:right;"> 0.0016848 </td>
   <td style="text-align:right;"> 44.687680 </td>
   <td style="text-align:right;"> 0.0e+00 </td>
   <td style="text-align:right;"> 2.4429610 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Residuals </td>
   <td style="text-align:right;"> 25334 </td>
   <td style="text-align:right;"> 0.9551286 </td>
   <td style="text-align:right;"> 0.0000377 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 86.5590533 </td>
  </tr>
</tbody>
</table>




















```r
knitr::opts_chunk$set(echo = TRUE)
library(knitr)
library(rmdformats)
library(tidyverse)
library(ggplot2)
library(ggpubr)
library(RColorBrewer)
library(lme4)
library(statmod) 
library(merTools)
library(MuMIn) # for calculating R2 of mixed models
```

```
## 
## Attaching package: 'MuMIn'
```

```
## The following object is masked from 'package:arm':
## 
##     coefplot
```

```r
library(doParallel)
```

```
## Loading required package: foreach
```

```
## 
## Attaching package: 'foreach'
```

```
## The following objects are masked from 'package:purrr':
## 
##     accumulate, when
```

```
## Loading required package: iterators
```

```
## Loading required package: parallel
```

```r
select=dplyr::select


#set the vector of colors

bbgroups=c("S1","S2","S3","CS","B","CN","N")
k07col=c("#08306B","#2171B5","#6BAED6","#C6DBEF","#E2282F","#74C476","#006D2C")
names(k07col)=bbgroups
```


# prep data


```r
d11=readRDS(file="./data/d11_for_analysis.rds")
d12=readRDS(file="./data/d12_for_analysis.rds")

##restrict to fitness
#traits=c("fecundity")

d11$year=2011
d12$year=2012

d11=d11[, c("year", "exp", "block", "id", "fecundity", "fitness", "ows", "sss", "herbivory")]
colnames(d11)[!colnames(d11)%in%colnames(d12)]->missingtrait
d12[,missingtrait]=NA
d12=d12[, c("year", "exp", "block", "id", "fecundity", "fitness", "ows", "sss", "herbivory")]

d=rbind(d11, d12)
d$region[d$exp%in%c("ULL", "RAT")]="S"
d$region[d$exp%in%c("RAM", "ADA")]="N"
d$year=as.factor(d$year)
#d=d[, c("year", "region", "exp","block", "id", "fecundity")]
#d=na.omit(d)

# hist(d$fecundity)
# hist(sqrt(d$fecundity))
# with(d, boxplot(fecundity~exp+year))
# with(d, boxplot(sqrt(fecundity)~exp+year))

d$exp <- gsub("ULL", "SU", d$exp)
d$exp <- gsub("RAT", "SR", d$exp)
d$exp <- gsub("ADA", "NA", d$exp)
d$exp <- gsub("RAM", "NM", d$exp)


pdf(file="./figures/raw.fecundity.boxplot.by.year.site.pdf", width=10, height=6)
with(d, boxplot(sqrt(fecundity)~year+exp, col=c(rep(brewer.pal(9, "Paired")[4],4), rep(brewer.pal(9, "Paired")[3],4))))
dev.off()
```

```
## quartz_off_screen 
##                 2
```

```r
with(d, boxplot(sqrt(fecundity)~year+exp, col=c(rep(brewer.pal(9, "Paired")[4],4), rep(brewer.pal(9, "Paired")[3],4))))
```

![](main_files/figure-html/data prep blups-1.png)<!-- -->

# Fitness blups

## Mixte effect model to get BLUPS

(FROM DF)


```r
mm1 <- lmer(fitness ~ 1 + (1 | block), data=d)
mm2 <- lmer(fitness ~ 1 + (1 | id), data=d)
mm3 <- lmer(fitness~exp + (1|exp:block), data=d) # so this is correctly specified (without year effect, at least)
mm4 <- lmer(fitness~exp + year + (1|exp:year:block), data=d)  ## I think this is also correctly specified!
#ranef(mm4)
#fixef(mm4)
mm5 <- lmer(fitness~exp*year + (1|exp:year:block), data=d)
anova(mm4, mm5)
```

```
## refitting model(s) with ML (instead of REML)
```

```
## Data: d
## Models:
## mm4: fitness ~ exp + year + (1 | exp:year:block)
## mm5: fitness ~ exp * year + (1 | exp:year:block)
##     npar     AIC     BIC logLik deviance  Chisq Df Pr(>Chisq)    
## mm4    7 -186087 -186030  93051  -186101                         
## mm5   10 -186116 -186034  93068  -186136 34.667  3  1.432e-07 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```



```r
#ranef(mm5)
#fixef(mm5)

mm6 <- lmer(fitness~exp*year + (1|exp:year:block) + (1|id), data=d)
anova(mm5,mm6)
```

```
## refitting model(s) with ML (instead of REML)
```

```
## Data: d
## Models:
## mm5: fitness ~ exp * year + (1 | exp:year:block)
## mm6: fitness ~ exp * year + (1 | exp:year:block) + (1 | id)
##     npar     AIC     BIC logLik deviance  Chisq Df Pr(>Chisq)    
## mm5   10 -186116 -186034  93068  -186136                         
## mm6   11 -186957 -186867  93489  -186979 842.83  1  < 2.2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```



```r
mm7 <- lmer(fitness~exp*year + (1|exp:year:block) + (1|exp:year:id), data=d)
anova(mm6,mm7)
```

```
## refitting model(s) with ML (instead of REML)
```

```
## Data: d
## Models:
## mm6: fitness ~ exp * year + (1 | exp:year:block) + (1 | id)
## mm7: fitness ~ exp * year + (1 | exp:year:block) + (1 | exp:year:id)
##     npar     AIC     BIC logLik deviance  Chisq Df Pr(>Chisq)
## mm6   11 -186957 -186867  93489  -186979                     
## mm7   11 -187384 -187295  93703  -187406 427.58  0
```



```r
mm8 <- lmer(fitness~exp*year  + (1|exp:year:id), data=d)
```



```r
registerDoParallel(cores=7)

c2 <- predictInterval(mm7)

int.mm7 <- REsim(mm7)
plotREsim(int.mm7)
```

![](main_files/figure-html/mixed model specifications 5-1.png)<!-- -->



```r
anova(mm7)
```

```
## Analysis of Variance Table
##          npar     Sum Sq    Mean Sq F value
## exp         3 0.00046361 0.00015454  4.5173
## year        1 0.00002556 0.00002556  0.7473
## exp:year    3 0.00168311 0.00056104 16.3999
```



```r
summary(mm7)
```

```
## Linear mixed model fit by REML ['lmerMod']
## Formula: fitness ~ exp * year + (1 | exp:year:block) + (1 | exp:year:id)
##    Data: d
## 
## REML criterion at convergence: -187304.2
## 
## Scaled residuals: 
##     Min      1Q  Median      3Q     Max 
## -2.4655 -0.5730 -0.1502  0.4045 20.9310 
## 
## Random effects:
##  Groups         Name        Variance  Std.Dev.
##  exp:year:id    (Intercept) 4.062e-06 0.002016
##  exp:year:block (Intercept) 1.596e-06 0.001264
##  Residual                   3.421e-05 0.005849
## Number of obs: 25406, groups:  exp:year:id, 1598; exp:year:block, 24
## 
## Fixed effects:
##                  Estimate Std. Error t value
## (Intercept)     0.0056671  0.0007496   7.560
## expNM           0.0001016  0.0010600   0.096
## expSR           0.0030166  0.0010605   2.844
## expSU          -0.0011184  0.0010602  -1.055
## year2012        0.0011693  0.0010615   1.102
## expNM:year2012  0.0020343  0.0015014   1.355
## expSR:year2012 -0.0078419  0.0015019  -5.221
## expSU:year2012 -0.0007130  0.0015020  -0.475
## 
## Correlation of Fixed Effects:
##             (Intr) expNM  expSR  expSU  yr2012 eNM:20 eSR:20
## expNM       -0.707                                          
## expSR       -0.707  0.500                                   
## expSU       -0.707  0.500  0.500                            
## year2012    -0.706  0.499  0.499  0.499                     
## expNM:y2012  0.499 -0.706 -0.353 -0.353 -0.707              
## expSR:y2012  0.499 -0.353 -0.706 -0.353 -0.707  0.500       
## expSU:y2012  0.499 -0.353 -0.353 -0.706 -0.707  0.500  0.500
```



```r
# random effect variances
var.mm7 <- VarCorr(mm7)
print(var.mm7,comp=c("Variance","Std.Dev."),digits=4)
```

```
##  Groups         Name        Variance  Std.Dev.
##  exp:year:id    (Intercept) 4.062e-06 0.002016
##  exp:year:block (Intercept) 1.596e-06 0.001264
##  Residual                   3.421e-05 0.005849
```



```r
re.var <- as.data.frame(var.mm7, row.names = NULL,optional = FALSE)

kable(rbind(data.frame(model="mm5", r.squaredGLMM(mm5)),
      data.frame(model="mm6", r.squaredGLMM(mm6)), 
      data.frame(model="mm7", r.squaredGLMM(mm7)), 
      data.frame(model="mm8", r.squaredGLMM(mm8))))
```

```
## Warning: 'r.squaredGLMM' now calculates a revised statistic. See the help page.
```

<table>
 <thead>
  <tr>
   <th style="text-align:left;"> model </th>
   <th style="text-align:right;"> R2m </th>
   <th style="text-align:right;"> R2c </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> mm5 </td>
   <td style="text-align:right;"> 0.0908289 </td>
   <td style="text-align:right;"> 0.1265360 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mm6 </td>
   <td style="text-align:right;"> 0.0909569 </td>
   <td style="text-align:right;"> 0.1686370 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mm7 </td>
   <td style="text-align:right;"> 0.0923543 </td>
   <td style="text-align:right;"> 0.2211835 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mm8 </td>
   <td style="text-align:right;"> 0.0940274 </td>
   <td style="text-align:right;"> 0.1858004 </td>
  </tr>
</tbody>
</table>

```r
## R2m is from marginal(fixed effects)
## R2c is from entire model
```

In all models, adding the random effects helps capture more variance. 

## Compute the blups form model 7


```r
blups <- ranef(mm7)[[1]]
blups <- as.data.frame(blups)
desc <- do.call(rbind,strsplit(rownames(blups),":"))
colnames(desc) <- c("exp","year","id")
blups <- cbind(blups, desc)
colnames(blups)[1] <- "fitness"

hist(blups$fitness)
```

![](main_files/figure-html/unnamed-chunk-51-1.png)<!-- -->



```r
with(blups, boxplot(fitness~year+exp, col=c(rep(brewer.pal(9, "Paired")[4],4), rep(brewer.pal(9, "Paired")[3],4))))
```

![](main_files/figure-html/unnamed-chunk-52-1.png)<!-- -->



```r
## write BLUPS to file
write.csv(blups, file="./res/fitness.blups.csv", quote=FALSE,row.names=FALSE)

## save model to file
save(mm7, file="./res/mm7.final.mixed.model.Rdat")
```


# Fecundity blups


```r
mm5 <- lmer(fecundity~exp*year + (1|exp:year:block), data=d)
mm6 <- lmer(fecundity~exp*year + (1|exp:year:block) + (1|id), data=d)
mm7 <- lmer(fecundity~exp*year + (1|exp:year:block) + (1|exp:year:id), data=d)
mm8 <- lmer(fecundity~exp*year  + (1|exp:year:id), data=d)
int.mm7 <- REsim(mm7)
plotREsim(int.mm7)
```

![](main_files/figure-html/unnamed-chunk-54-1.png)<!-- -->

```r
# random effect variances
var.mm7 <- VarCorr(mm7)
print(var.mm7,comp=c("Variance","Std.Dev."),digits=4)
```

```
##  Groups         Name        Variance  Std.Dev.
##  exp:year:id    (Intercept) 3.603e-06 0.001898
##  exp:year:block (Intercept) 1.310e-06 0.001145
##  Residual                   3.143e-05 0.005606
```



```r
kable(rbind(data.frame(model="mm5", r.squaredGLMM(mm5)),
      data.frame(model="mm6", r.squaredGLMM(mm6)), 
      data.frame(model="mm7", r.squaredGLMM(mm7)), 
      data.frame(model="mm8", r.squaredGLMM(mm8))))
```

<table>
 <thead>
  <tr>
   <th style="text-align:left;"> model </th>
   <th style="text-align:right;"> R2m </th>
   <th style="text-align:right;"> R2c </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> mm5 </td>
   <td style="text-align:right;"> 0.1706685 </td>
   <td style="text-align:right;"> 0.2003213 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mm6 </td>
   <td style="text-align:right;"> 0.1716650 </td>
   <td style="text-align:right;"> 0.2427294 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mm7 </td>
   <td style="text-align:right;"> 0.1707131 </td>
   <td style="text-align:right;"> 0.2828407 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mm8 </td>
   <td style="text-align:right;"> 0.1718416 </td>
   <td style="text-align:right;"> 0.2532949 </td>
  </tr>
</tbody>
</table>

```r
## R2m is from marginal(fixed effects)
## R2c is from entire model
```

```r
data.frame(fitted=fitted(mm7), residuals=residuals(mm7), site=mm7@frame$exp, year=mm7@frame$year)%>%ggplot(aes(x=fitted, y=residuals, color=site, shape=year))+geom_point()+theme_pubclean()
```

![](main_files/figure-html/unnamed-chunk-55-1.png)<!-- -->
## Compute the blups form model 7


```r
blups <- ranef(mm7)[[1]]
blups <- as.data.frame(blups)
desc <- do.call(rbind,strsplit(rownames(blups),":"))
colnames(desc) <- c("exp","year","id")
blups <- cbind(blups, desc)
colnames(blups)[1] <- "fecundity"

hist(blups$fecundity)
```

![](main_files/figure-html/unnamed-chunk-56-1.png)<!-- -->

```r
write.csv(blups, file="./res/fecundity.blups.csv", quote=FALSE,row.names=FALSE)
```

# OWS blups

## 3. Fit mixed models

```r
### was having convergence issues.  Setting to the other optimizer and increasing iterations seems to fix.
mm1 <- glmer(ows ~ 1 + (1 | block), data=d,family=binomial("logit"),control=glmerControl(optimizer="bobyqa", optCtrl=list(maxfun=2e5))) 
```

```
## boundary (singular) fit: see help('isSingular')
```

```r
mm2 <- glmer(ows ~ 1 + (1 | id), data=d, family=binomial("logit"),control=glmerControl(optimizer="bobyqa", optCtrl=list(maxfun=2e5)))

mm3 <- glmer(ows~exp + (1|exp:block), data=d, family=binomial("logit"),control=glmerControl(optimizer="bobyqa", optCtrl=list(maxfun=2e5))) # so this is correctly specified (without year effect, at least)

mm4 <- glmer(ows~exp + year + (1|exp:year:block), data=d, family=binomial("logit"),control=glmerControl(optimizer="bobyqa", optCtrl=list(maxfun=2e5)))  ## I think this is also correctly specified!

mm5 <- glmer(ows~exp*year + (1|exp:year:block), data=d, family=binomial("logit"), control=glmerControl(optimizer="bobyqa", optCtrl=list(maxfun=2e5)))

mm6 <- glmer(ows~exp*year + (1|exp:year:block) + (1|id), data=d,family=binomial("logit"),control=glmerControl(optimizer="bobyqa", optCtrl=list(maxfun=2e5)))

mm7 <- glmer(ows~exp*year + (1|exp:year:block) + (1|exp:year:id), data=d, family=binomial("logit"),control=glmerControl(optimizer="bobyqa", optCtrl=list(maxfun=2e5)))

mm8 <- glmer(ows~exp*year  + (1|exp:year:id), data=d, family=binomial("logit"), control=glmerControl(optimizer="bobyqa", optCtrl=list(maxfun=2e5)))

# get fit information for all these models
mods <- c("mm1", "mm2", "mm3","mm4", "mm5", "mm6", "mm7", "mm8")
mod.sum <- matrix(NA, ncol=5, nrow=length(mods))
for(up in 1:length(mods)){
  up.m <- mods[up]
  up.lm <- get(up.m)
  up.fit <- summary(up.lm)$AICtab
  mod.sum[up,] <- up.fit
}
rownames(mod.sum) <- mods
colnames(mod.sum) <- names(up.fit)
print(mod.sum)
```

```
##           AIC       BIC    logLik  deviance df.resid
## mm1 13679.937 13696.671 -6837.968 13675.937    31796
## mm2 12147.549 12164.283 -6071.774 12143.549    31796
## mm3 13004.185 13046.021 -6497.092 12994.185    31793
## mm4 11741.876 11792.078 -5864.938 11729.876    31792
## mm5 11732.294 11807.599 -5857.147 11714.294    31789
## mm6  9891.401  9975.073 -4935.701  9871.401    31788
## mm7  9506.934  9590.605 -4743.467  9486.934    31788
## mm8  9620.423  9695.728 -4801.212  9602.423    31789
```
This results leads to choose model 7, like for fitness and fecundity.



```r
#c2 <- predictInterval(mm7)
int.mm7 <- REsim(mm7)
plotREsim(REsim(mm7))
```

![](main_files/figure-html/unnamed-chunk-58-1.png)<!-- -->


```r
anova(mm7)
```

```
## Analysis of Variance Table
##          npar  Sum Sq Mean Sq  F value
## exp         3  71.474  23.825  23.8245
## year        1 168.261 168.261 168.2613
## exp:year    3  18.767   6.256   6.2556
```



```r
summary(mm7)
```

```
## Generalized linear mixed model fit by maximum likelihood (Laplace
##   Approximation) [glmerMod]
##  Family: binomial  ( logit )
## Formula: ows ~ exp * year + (1 | exp:year:block) + (1 | exp:year:id)
##    Data: d
## Control: glmerControl(optimizer = "bobyqa", optCtrl = list(maxfun = 2e+05))
## 
##      AIC      BIC   logLik deviance df.resid 
##   9506.9   9590.6  -4743.5   9486.9    31788 
## 
## Scaled residuals: 
##     Min      1Q  Median      3Q     Max 
## -8.0837  0.0635  0.0806  0.1727  3.2640 
## 
## Random effects:
##  Groups         Name        Variance Std.Dev.
##  exp:year:id    (Intercept) 2.99338  1.730   
##  exp:year:block (Intercept) 0.05665  0.238   
## Number of obs: 31798, groups:  exp:year:id, 1598; exp:year:block, 24
## 
## Fixed effects:
##                Estimate Std. Error z value Pr(>|z|)    
## (Intercept)      3.7370     0.2185  17.100  < 2e-16 ***
## expNM           -1.2514     0.2919  -4.287 1.81e-05 ***
## expSR           -1.2050     0.2910  -4.141 3.46e-05 ***
## expSU            1.2713     0.3137   4.053 5.05e-05 ***
## year2012         1.5780     0.3247   4.860 1.17e-06 ***
## expNM:year2012   1.3488     0.4550   2.965  0.00303 ** 
## expSR:year2012   1.3979     0.4573   3.057  0.00224 ** 
## expSU:year2012  -0.3616     0.4951  -0.730  0.46521    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Correlation of Fixed Effects:
##             (Intr) expNM  expSR  expSU  yr2012 eNM:20 eSR:20
## expNM       -0.726                                          
## expSR       -0.736  0.540                                   
## expSU       -0.654  0.491  0.492                            
## year2012    -0.625  0.472  0.473  0.444                     
## expNM:y2012  0.467 -0.641 -0.347 -0.314 -0.708              
## expSR:y2012  0.469 -0.344 -0.636 -0.313 -0.705  0.509       
## expSU:y2012  0.419 -0.313 -0.314 -0.632 -0.653  0.466  0.465
```



```r
# random effect variances
var.mm7 <- VarCorr(mm7)
print(var.mm7,comp=c("Variance","Std.Dev."),digits=4)
```

```
##  Groups         Name        Variance Std.Dev.
##  exp:year:id    (Intercept) 2.99338  1.730   
##  exp:year:block (Intercept) 0.05665  0.238
```

```r
re.var <- as.data.frame(var.mm7, row.names = NULL,optional = FALSE)
```



```r
kable(rbind(data.frame(model="mm5", r.squaredGLMM(mm5)),
      data.frame(model="mm6", r.squaredGLMM(mm6)), 
      data.frame(model="mm7", r.squaredGLMM(mm7)), 
      data.frame(model="mm8", r.squaredGLMM(mm8))))
```

```
## Warning: the null model is correct only if all variables used by the original
## model remain unchanged.

## Warning: the null model is correct only if all variables used by the original
## model remain unchanged.

## Warning: the null model is correct only if all variables used by the original
## model remain unchanged.

## Warning: the null model is correct only if all variables used by the original
## model remain unchanged.
```

<table>
 <thead>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:left;"> model </th>
   <th style="text-align:right;"> R2m </th>
   <th style="text-align:right;"> R2c </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> theoretical </td>
   <td style="text-align:left;"> mm5 </td>
   <td style="text-align:right;"> 0.2869613 </td>
   <td style="text-align:right;"> 0.3017865 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> delta </td>
   <td style="text-align:left;"> mm5 </td>
   <td style="text-align:right;"> 0.0628541 </td>
   <td style="text-align:right;"> 0.0661013 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> theoretical1 </td>
   <td style="text-align:left;"> mm6 </td>
   <td style="text-align:right;"> 0.2754504 </td>
   <td style="text-align:right;"> 0.5053494 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> delta1 </td>
   <td style="text-align:left;"> mm6 </td>
   <td style="text-align:right;"> 0.0778748 </td>
   <td style="text-align:right;"> 0.1428713 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> theoretical2 </td>
   <td style="text-align:left;"> mm7 </td>
   <td style="text-align:right;"> 0.2185461 </td>
   <td style="text-align:right;"> 0.5944918 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> delta2 </td>
   <td style="text-align:left;"> mm7 </td>
   <td style="text-align:right;"> 0.0673980 </td>
   <td style="text-align:right;"> 0.1833368 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> theoretical3 </td>
   <td style="text-align:left;"> mm8 </td>
   <td style="text-align:right;"> 0.2161315 </td>
   <td style="text-align:right;"> 0.5827572 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> delta3 </td>
   <td style="text-align:left;"> mm8 </td>
   <td style="text-align:right;"> 0.0731617 </td>
   <td style="text-align:right;"> 0.1972666 </td>
  </tr>
</tbody>
</table>
Delta is a method for computing r2 for glmm. Theoritical is the method specific to binomial models. 


## compute OWS blups and OWS probability


```r
blups <- ranef(mm7)[[1]]
blups <- as.data.frame(blups)
desc <- do.call(rbind,strsplit(rownames(blups),":"))
colnames(desc) <- c("exp","year","id")
blups <- cbind(blups, desc)
colnames(blups)[1] <- "ows.b"
blups$exp <- factor(blups$exp, levels=c("NA","NM","SU","SR"))
blups$region[blups$exp%in%c("SU", "SR")]="S"
blups$region[blups$exp%in%c("NM", "NA")]="N"

hist(blups$ows.b)
```

![](main_files/figure-html/unnamed-chunk-63-1.png)<!-- -->



```r
### BLUPS are the log odds probability of survival.  Make probability

logit2prob <- function(logit){
  odds <- exp(logit)
  prob <- odds / (1 + odds)
  return(prob)
}

blups$ows.p <- logit2prob(blups$ows.b)
with(blups, boxplot(ows.p~year+exp, col=c(rep(brewer.pal(9, "Paired")[4],4), rep(brewer.pal(9, "Paired")[3],4)), ylab="BLUP (logit ows)"))
```

![](main_files/figure-html/unnamed-chunk-64-1.png)<!-- -->
Overwinter survival was in 2011, but much less in 2012, a year in which most lines survived winter. 


```r
ows.blup.plot <- ggplot(data=blups, aes(x=exp, y=ows.b, fill=region)) +
  geom_boxplot() +
  scale_fill_manual(values=c("dodgerblue1","springgreen2")) +
  facet_wrap(~year) +
  ylab("overwinter survival BLUPs (logit)") +
  xlab("experiment") +
  labs(fill = "experiment \nregion")

ows.blup.plot
```

![](main_files/figure-html/unnamed-chunk-65-1.png)<!-- -->

```r
ows.prob.plot <- ggplot(data=blups, aes(x=exp, y=ows.p, fill=region)) +
  geom_boxplot() +
  scale_fill_manual(values=c("dodgerblue1","springgreen2")) +
  facet_wrap(~year) +
  ylab("overwinter survival BLUPs (probability)") +
  xlab("experiment") +
  labs(fill = "experiment \nregion")

ows.prob.plot
```

![](main_files/figure-html/unnamed-chunk-66-1.png)<!-- -->


```r
## write BLUPS to file
write.csv(blups, file="./res/ows.blups.csv", quote=FALSE,row.names=FALSE)
```


# SLUGS

## Fit glm and compute average fitted per accession

* 0 = no plant
* 1 = no damage
* 2 = light damage
* 3 = heavy damage  
Scored visually in the field in Rathckegården, Fall 2011 by DLF


```r
data <- read.csv("./data/rat.snail.2011.csv")
data <- data[,1:5]
data$rep <-c(rep("A",nrow(data)/3),rep("B", nrow(data)/3),rep("C",nrow(data)/3))

glm.data.t2 <- unique(data[,c(4,6)])
glm.data.t2$no.plant<-99
glm.data.t2$s1 <- 99
glm.data.t2$s2<- 99
glm.data.t2$s3<- 99

for (i in 1:nrow(glm.data.t2)){
  glm.data.t2$no.plant[i]<-nrow(data[data$line.no==glm.data.t2[i,1] & data$rep==glm.data.t2[i,2] & data$slug=="0",])
  glm.data.t2$s1[i]<-nrow(data[data$line.no==glm.data.t2[i,1] & data$rep==glm.data.t2[i,2] & data$slug=="1",])
  glm.data.t2$s2[i]<-nrow(data[data$line.no==glm.data.t2[i,1] & data$rep==glm.data.t2[i,2] & data$slug=="2",])
  glm.data.t2$s3[i]<-nrow(data[data$line.no==glm.data.t2[i,1] & data$rep==glm.data.t2[i,2] & data$slug=="3",])
}

total.plant <- apply(glm.data.t2,1,function(x) sum(as.numeric(x[4:6])))
glm.data.t2$total.plant <- total.plant
glm.data.t2$ps3 <- glm.data.t2$s3/glm.data.t2$total.plant
glm.data.t2$ps23 <- (glm.data.t2$s2+glm.data.t2$s3)/glm.data.t2$total.plant
glm.data.t2$s12 <- glm.data.t2$total.plant-glm.data.t2$s3
glm.data.t2$s23 <- glm.data.t2$total.plant-glm.data.t2$s1

glm.data.t2$rep <- as.factor(glm.data.t2$rep)
glm.data.t2$line.no <- as.factor(glm.data.t2$line.no)

to.remove <- c("201","202","203","error", "Eden-1","Ull-1","empty")
glm.data.t2 <- glm.data.t2[glm.data.t2$line.no%in%to.remove==FALSE,,drop=TRUE]
glm.data.t2$line.no <- as.character(glm.data.t2$line.no)
glm.data.t2$line.no <- as.factor(glm.data.t2$line.no)
```

Test two sets of models.  First is first is proportion s3 (heavy damage) vs s2 s1


```r
glm1 <- glm(cbind(s3,s12) ~ rep, data=glm.data.t2, family=binomial(link="logit")) # rep sig
glm2 <- glm(cbind(s3,s12) ~ line.no, data=glm.data.t2, family=binomial(link="logit")) # line.no sig
glm3 <- glm(cbind(s3,s12) ~ rep + line.no, data=glm.data.t2, family=binomial(link="logit")) # both sig 
```

Second model is proportion s1 (not touched) vs s2 s3


```r
glm4 <- glm(cbind(s1,s23) ~ rep, data=glm.data.t2, family=binomial(link="logit")) # rep sig
glm5 <- glm(cbind(s1,s23) ~ line.no, data=glm.data.t2, family=binomial(link="logit")) # line.no sig
glm6 <- glm(cbind(s1,s23) ~ rep + line.no, data=glm.data.t2, family=binomial(link="logit")) # both sig together
```
Again, both block and line are significant together.  Include both in model.



```r
glm.data.t2$fitted.s3 <- glm3$fitted.values
s3.means <- aggregate(fitted.s3~line.no, data=glm.data.t2, mean)
hist(s3.means$fitted)
```

![](main_files/figure-html/unnamed-chunk-71-1.png)<!-- -->



```r
glm.data.t2$fitted.s1 <- glm6$fitted.values
s1.means <- aggregate(fitted.s1~line.no, data=glm.data.t2, mean)
hist(s1.means$fitted)
```

![](main_files/figure-html/unnamed-chunk-72-1.png)<!-- -->



```r
id=read.table("./data/acc_list.txt", sep="\t", h=T, na.strings=".", encoding="utf-8")

s1.means <- merge(s1.means, id, all.x=TRUE, by.x="line.no", by.y="tubes")
slug.no.damage.GWAS <- s1.means[,c(3,2)]
s3.means <- merge(s3.means, id, all.x=TRUE, by.x="line.no", by.y="tubes")
slug.severe.damage.GWAS <- s3.means[,c(3,2)]

slug.phenos <- merge(slug.no.damage.GWAS, slug.severe.damage.GWAS)
colnames(slug.phenos) <- c("id", "X2011_RAT_noslug", "X2011_RAT_severeslug")

save(slug.phenos, file="./res/slug.phenos.fitted.Rdat")
write.csv(slug.phenos, file="./res/slug.phenos.fitted.csv", quote=FALSE, row.names=FALSE)
```

# Make a accession level phenotype table


```r
fitness=read_csv("./res/fitness.blups.csv", show_col_types = F, na="na")
fecundity=read_csv("./res/fecundity.blups.csv", show_col_types = F,  na="na")
OWS=read_csv("./res/ows.blups.csv", show_col_types = F,  na="na")
slugs=read_csv("./res/slug.phenos.fitted.csv", show_col_types = F,  na="na")%>%mutate(region="S", year=2011, exp="SR")%>%mutate(noslugdamage=X2011_RAT_noslug, severeslugdamage=X2011_RAT_severeslug)%>%dplyr::select(region, year, exp, id, noslugdamage, severeslugdamage)

fitness%>%full_join(fecundity, by = join_by(exp, year, id))%>%full_join(OWS, by = join_by(exp, year, id))%>%full_join(slugs,by = join_by(region, exp, year, id))%>%dplyr::select(region, exp, year, id, fitness, fecundity, ows.b, ows.p, noslugdamage, severeslugdamage)->acc_phen

head(acc_phen)
```

```
## # A tibble: 6 × 10
##   region exp    year    id   fitness fecundity  ows.b ows.p noslugdamage
##   <chr>  <chr> <dbl> <dbl>     <dbl>     <dbl>  <dbl> <dbl>        <dbl>
## 1 N      NA     2011  1061  0.00183   0.00290   0.770 0.683           NA
## 2 N      NA     2011  1062  0.000392  0.000839  0.748 0.679           NA
## 3 N      NA     2011  1063  0.000574 -0.000299  0.835 0.697           NA
## 4 N      NA     2011  1158 -0.00132  -0.00142  -0.485 0.381           NA
## 5 N      NA     2011  1254  0.00108   0.00192   0.810 0.692           NA
## 6 N      NA     2011  1257 -0.000299 -0.000256 -1.06  0.257           NA
## # ℹ 1 more variable: severeslugdamage <dbl>
```

Now save the table and make it downloadable 


```r
write_csv(acc_phen, "res/accession_phenotypes.csv")

acc_phen%>%DT::datatable(extensions = 'Buttons',
            options = list(dom = 'Blfrtip',
                           buttons = c('copy', 'csv', 'excel', 'pdf', 'print'),
                           lengthMenu = list(c(10,25,50,-1),
                                             c(10,25,50,"All"))))
```

```{=html}
<div class="datatables html-widget html-fill-item-overflow-hidden html-fill-item" id="htmlwidget-2b1e20fd721166ff3e21" style="width:100%;height:auto;"></div>
<script type="application/json" data-for="htmlwidget-2b1e20fd721166ff3e21">{"x":{"filter":"none","vertical":false,"extensions":["Buttons"],"data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110","111","112","113","114","115","116","117","118","119","120","121","122","123","124","125","126","127","128","129","130","131","132","133","134","135","136","137","138","139","140","141","142","143","144","145","146","147","148","149","150","151","152","153","154","155","156","157","158","159","160","161","162","163","164","165","166","167","168","169","170","171","172","173","174","175","176","177","178","179","180","181","182","183","184","185","186","187","188","189","190","191","192","193","194","195","196","197","198","199","200","201","202","203","204","205","206","207","208","209","210","211","212","213","214","215","216","217","218","219","220","221","222","223","224","225","226","227","228","229","230","231","232","233","234","235","236","237","238","239","240","241","242","243","244","245","246","247","248","249","250","251","252","253","254","255","256","257","258","259","260","261","262","263","264","265","266","267","268","269","270","271","272","273","274","275","276","277","278","279","280","281","282","283","284","285","286","287","288","289","290","291","292","293","294","295","296","297","298","299","300","301","302","303","304","305","306","307","308","309","310","311","312","313","314","315","316","317","318","319","320","321","322","323","324","325","326","327","328","329","330","331","332","333","334","335","336","337","338","339","340","341","342","343","344","345","346","347","348","349","350","351","352","353","354","355","356","357","358","359","360","361","362","363","364","365","366","367","368","369","370","371","372","373","374","375","376","377","378","379","380","381","382","383","384","385","386","387","388","389","390","391","392","393","394","395","396","397","398","399","400","401","402","403","404","405","406","407","408","409","410","411","412","413","414","415","416","417","418","419","420","421","422","423","424","425","426","427","428","429","430","431","432","433","434","435","436","437","438","439","440","441","442","443","444","445","446","447","448","449","450","451","452","453","454","455","456","457","458","459","460","461","462","463","464","465","466","467","468","469","470","471","472","473","474","475","476","477","478","479","480","481","482","483","484","485","486","487","488","489","490","491","492","493","494","495","496","497","498","499","500","501","502","503","504","505","506","507","508","509","510","511","512","513","514","515","516","517","518","519","520","521","522","523","524","525","526","527","528","529","530","531","532","533","534","535","536","537","538","539","540","541","542","543","544","545","546","547","548","549","550","551","552","553","554","555","556","557","558","559","560","561","562","563","564","565","566","567","568","569","570","571","572","573","574","575","576","577","578","579","580","581","582","583","584","585","586","587","588","589","590","591","592","593","594","595","596","597","598","599","600","601","602","603","604","605","606","607","608","609","610","611","612","613","614","615","616","617","618","619","620","621","622","623","624","625","626","627","628","629","630","631","632","633","634","635","636","637","638","639","640","641","642","643","644","645","646","647","648","649","650","651","652","653","654","655","656","657","658","659","660","661","662","663","664","665","666","667","668","669","670","671","672","673","674","675","676","677","678","679","680","681","682","683","684","685","686","687","688","689","690","691","692","693","694","695","696","697","698","699","700","701","702","703","704","705","706","707","708","709","710","711","712","713","714","715","716","717","718","719","720","721","722","723","724","725","726","727","728","729","730","731","732","733","734","735","736","737","738","739","740","741","742","743","744","745","746","747","748","749","750","751","752","753","754","755","756","757","758","759","760","761","762","763","764","765","766","767","768","769","770","771","772","773","774","775","776","777","778","779","780","781","782","783","784","785","786","787","788","789","790","791","792","793","794","795","796","797","798","799","800","801","802","803","804","805","806","807","808","809","810","811","812","813","814","815","816","817","818","819","820","821","822","823","824","825","826","827","828","829","830","831","832","833","834","835","836","837","838","839","840","841","842","843","844","845","846","847","848","849","850","851","852","853","854","855","856","857","858","859","860","861","862","863","864","865","866","867","868","869","870","871","872","873","874","875","876","877","878","879","880","881","882","883","884","885","886","887","888","889","890","891","892","893","894","895","896","897","898","899","900","901","902","903","904","905","906","907","908","909","910","911","912","913","914","915","916","917","918","919","920","921","922","923","924","925","926","927","928","929","930","931","932","933","934","935","936","937","938","939","940","941","942","943","944","945","946","947","948","949","950","951","952","953","954","955","956","957","958","959","960","961","962","963","964","965","966","967","968","969","970","971","972","973","974","975","976","977","978","979","980","981","982","983","984","985","986","987","988","989","990","991","992","993","994","995","996","997","998","999","1000","1001","1002","1003","1004","1005","1006","1007","1008","1009","1010","1011","1012","1013","1014","1015","1016","1017","1018","1019","1020","1021","1022","1023","1024","1025","1026","1027","1028","1029","1030","1031","1032","1033","1034","1035","1036","1037","1038","1039","1040","1041","1042","1043","1044","1045","1046","1047","1048","1049","1050","1051","1052","1053","1054","1055","1056","1057","1058","1059","1060","1061","1062","1063","1064","1065","1066","1067","1068","1069","1070","1071","1072","1073","1074","1075","1076","1077","1078","1079","1080","1081","1082","1083","1084","1085","1086","1087","1088","1089","1090","1091","1092","1093","1094","1095","1096","1097","1098","1099","1100","1101","1102","1103","1104","1105","1106","1107","1108","1109","1110","1111","1112","1113","1114","1115","1116","1117","1118","1119","1120","1121","1122","1123","1124","1125","1126","1127","1128","1129","1130","1131","1132","1133","1134","1135","1136","1137","1138","1139","1140","1141","1142","1143","1144","1145","1146","1147","1148","1149","1150","1151","1152","1153","1154","1155","1156","1157","1158","1159","1160","1161","1162","1163","1164","1165","1166","1167","1168","1169","1170","1171","1172","1173","1174","1175","1176","1177","1178","1179","1180","1181","1182","1183","1184","1185","1186","1187","1188","1189","1190","1191","1192","1193","1194","1195","1196","1197","1198","1199","1200","1201","1202","1203","1204","1205","1206","1207","1208","1209","1210","1211","1212","1213","1214","1215","1216","1217","1218","1219","1220","1221","1222","1223","1224","1225","1226","1227","1228","1229","1230","1231","1232","1233","1234","1235","1236","1237","1238","1239","1240","1241","1242","1243","1244","1245","1246","1247","1248","1249","1250","1251","1252","1253","1254","1255","1256","1257","1258","1259","1260","1261","1262","1263","1264","1265","1266","1267","1268","1269","1270","1271","1272","1273","1274","1275","1276","1277","1278","1279","1280","1281","1282","1283","1284","1285","1286","1287","1288","1289","1290","1291","1292","1293","1294","1295","1296","1297","1298","1299","1300","1301","1302","1303","1304","1305","1306","1307","1308","1309","1310","1311","1312","1313","1314","1315","1316","1317","1318","1319","1320","1321","1322","1323","1324","1325","1326","1327","1328","1329","1330","1331","1332","1333","1334","1335","1336","1337","1338","1339","1340","1341","1342","1343","1344","1345","1346","1347","1348","1349","1350","1351","1352","1353","1354","1355","1356","1357","1358","1359","1360","1361","1362","1363","1364","1365","1366","1367","1368","1369","1370","1371","1372","1373","1374","1375","1376","1377","1378","1379","1380","1381","1382","1383","1384","1385","1386","1387","1388","1389","1390","1391","1392","1393","1394","1395","1396","1397","1398","1399","1400","1401","1402","1403","1404","1405","1406","1407","1408","1409","1410","1411","1412","1413","1414","1415","1416","1417","1418","1419","1420","1421","1422","1423","1424","1425","1426","1427","1428","1429","1430","1431","1432","1433","1434","1435","1436","1437","1438","1439","1440","1441","1442","1443","1444","1445","1446","1447","1448","1449","1450","1451","1452","1453","1454","1455","1456","1457","1458","1459","1460","1461","1462","1463","1464","1465","1466","1467","1468","1469","1470","1471","1472","1473","1474","1475","1476","1477","1478","1479","1480","1481","1482","1483","1484","1485","1486","1487","1488","1489","1490","1491","1492","1493","1494","1495","1496","1497","1498","1499","1500","1501","1502","1503","1504","1505","1506","1507","1508","1509","1510","1511","1512","1513","1514","1515","1516","1517","1518","1519","1520","1521","1522","1523","1524","1525","1526","1527","1528","1529","1530","1531","1532","1533","1534","1535","1536","1537","1538","1539","1540","1541","1542","1543","1544","1545","1546","1547","1548","1549","1550","1551","1552","1553","1554","1555","1556","1557","1558","1559","1560","1561","1562","1563","1564","1565","1566","1567","1568","1569","1570","1571","1572","1573","1574","1575","1576","1577","1578","1579","1580","1581","1582","1583","1584","1585","1586","1587","1588","1589","1590","1591","1592","1593","1594","1595","1596","1597","1598"],["N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S","S"],["NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","NM","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SR","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU","SU"],[2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2011,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012,2012],[1061,1062,1063,1158,1254,1257,1317,1318,1363,1435,1552,1585,5829,5830,5831,5832,5856,5865,5867,6009,6011,6012,6013,6016,6017,6019,6020,6021,6023,6025,6030,6034,6035,6036,6038,6039,6040,6041,6042,6043,6046,6064,6069,6071,6073,6085,6086,6090,6092,6094,6097,6098,6102,6104,6106,6108,6111,6112,6114,6115,6118,6122,6124,6126,6127,6129,6131,6133,6134,6136,6138,6140,6142,6145,6148,6149,6151,6154,6163,6166,6169,6171,6173,6174,6177,6180,6184,6188,6189,6191,6192,6193,6194,6195,6197,6198,6201,6202,6203,6209,6210,6217,6218,6220,6221,6231,6235,6237,6238,6240,6241,6242,6243,6244,6252,6258,6276,6284,6413,6900,6913,6918,6964,6973,6974,7516,7517,7519,8222,8227,8230,8231,8237,8240,8241,8242,8247,8249,8256,8258,8259,8283,8306,8307,8326,8334,8335,8351,8369,8386,8387,8422,8423,8426,9058,9321,9323,9332,9336,9339,9343,9353,9356,9363,9369,9380,9382,9383,9386,9388,9391,9392,9399,9404,9405,9407,9408,9409,9412,9413,9416,9421,9427,9433,9434,9436,9437,9442,9450,9451,9452,9453,9454,9470,9476,9481,991,992,997,1061,1062,1063,1158,1254,1257,1317,1318,1363,1435,1552,1585,5829,5830,5831,5832,5856,5865,5867,6009,6011,6012,6013,6016,6017,6019,6020,6021,6023,6025,6030,6034,6035,6036,6038,6039,6040,6041,6042,6043,6046,6064,6069,6071,6073,6085,6086,6090,6092,6094,6097,6098,6102,6104,6106,6108,6109,6111,6112,6114,6115,6118,6122,6124,6126,6127,6129,6131,6133,6134,6136,6138,6140,6142,6145,6148,6149,6151,6154,6163,6166,6169,6171,6173,6174,6177,6180,6184,6188,6189,6191,6192,6193,6194,6195,6197,6198,6201,6202,6203,6209,6210,6217,6218,6220,6221,6231,6235,6237,6238,6240,6241,6242,6243,6244,6252,6258,6276,6284,6413,6900,6913,6918,6964,6973,6974,7516,7517,7519,8222,8227,8230,8231,8237,8240,8241,8242,8247,8249,8256,8258,8259,8283,8306,8307,8326,8334,8335,8351,8369,8386,8387,8422,8423,8426,9058,9321,9323,9332,9336,9339,9343,9353,9356,9363,9369,9380,9382,9383,9386,9388,9391,9392,9399,9404,9405,9407,9408,9409,9412,9413,9416,9421,9427,9433,9434,9436,9437,9442,9450,9451,9452,9453,9454,9470,9476,9481,991,992,997,1061,1062,1063,1158,1254,1257,1317,1318,1363,1435,1552,1585,5829,5830,5831,5832,5856,5865,5867,6009,6011,6012,6013,6016,6017,6019,6020,6021,6023,6025,6030,6034,6035,6036,6038,6039,6040,6041,6042,6043,6046,6064,6069,6071,6073,6085,6086,6090,6092,6094,6097,6098,6102,6104,6106,6108,6109,6111,6112,6114,6115,6118,6122,6124,6126,6127,6129,6131,6133,6134,6136,6138,6140,6142,6145,6148,6149,6151,6154,6163,6166,6169,6171,6173,6174,6177,6180,6184,6188,6189,6191,6192,6193,6194,6195,6197,6198,6201,6202,6203,6209,6210,6217,6218,6220,6221,6231,6235,6237,6238,6240,6241,6242,6243,6244,6252,6258,6276,6284,6413,6900,6913,6918,6964,6973,6974,7516,7517,7519,8222,8227,8230,8231,8237,8240,8241,8242,8247,8249,8256,8258,8259,8283,8306,8307,8326,8334,8335,8351,8369,8386,8387,8422,8423,8426,9058,9321,9323,9332,9336,9339,9343,9353,9356,9363,9369,9380,9382,9383,9386,9388,9391,9392,9399,9404,9405,9407,9408,9409,9412,9413,9416,9421,9427,9433,9434,9436,9437,9442,9450,9451,9452,9453,9454,9470,9476,9481,991,992,997,1061,1062,1063,1158,1254,1257,1317,1318,1363,1435,1552,1585,5829,5830,5831,5832,5856,5865,5867,6009,6011,6012,6013,6016,6017,6019,6020,6021,6023,6025,6030,6034,6035,6036,6038,6039,6040,6041,6042,6043,6046,6064,6069,6071,6073,6085,6086,6090,6092,6094,6097,6098,6102,6104,6106,6108,6109,6111,6112,6114,6115,6118,6122,6124,6126,6127,6129,6131,6133,6134,6136,6138,6140,6142,6145,6148,6149,6151,6154,6163,6166,6169,6171,6173,6174,6177,6180,6184,6188,6189,6191,6192,6193,6194,6195,6197,6198,6201,6202,6203,6209,6210,6217,6218,6220,6221,6231,6235,6237,6238,6240,6241,6242,6243,6244,6252,6258,6276,6284,6413,6900,6913,6918,6964,6973,6974,7516,7517,7519,8222,8227,8230,8231,8237,8240,8241,8242,8247,8249,8256,8258,8259,8283,8306,8307,8326,8334,8335,8351,8369,8386,8387,8422,8423,8426,9058,9321,9323,9332,9336,9339,9343,9353,9356,9363,9369,9380,9382,9383,9386,9388,9391,9392,9399,9404,9405,9407,9408,9409,9412,9413,9416,9421,9427,9433,9434,9436,9437,9442,9450,9451,9452,9453,9454,9470,9476,9481,991,992,997,1061,1062,1063,1158,1254,1257,1317,1318,1363,1435,1552,1585,5829,5830,5831,5832,5856,5865,5867,6009,6011,6012,6013,6016,6017,6019,6020,6021,6023,6025,6030,6034,6035,6036,6038,6039,6040,6041,6042,6043,6046,6064,6069,6071,6073,6085,6086,6090,6092,6094,6097,6098,6102,6104,6106,6108,6109,6111,6112,6114,6115,6118,6122,6124,6126,6127,6129,6131,6133,6134,6136,6138,6140,6142,6145,6148,6149,6151,6154,6163,6166,6169,6171,6173,6174,6177,6180,6184,6188,6189,6191,6192,6193,6194,6195,6197,6198,6201,6202,6203,6209,6210,6217,6218,6220,6221,6231,6235,6237,6238,6240,6241,6242,6243,6244,6252,6258,6276,6284,6413,6900,6913,6918,6964,6973,6974,7516,7517,7519,8222,8227,8230,8231,8237,8240,8241,8242,8247,8249,8256,8258,8259,8283,8306,8307,8326,8334,8335,8351,8369,8386,8387,8422,8423,8426,9058,9321,9323,9332,9336,9339,9343,9353,9356,9363,9369,9380,9382,9383,9386,9388,9391,9392,9399,9404,9405,9407,9408,9409,9412,9413,9416,9421,9427,9433,9434,9436,9437,9442,9450,9451,9452,9453,9454,9470,9476,9481,991,992,997,1061,1062,1063,1158,1254,1257,1317,1318,1363,1435,1552,1585,5829,5830,5831,5832,5856,5865,5867,6009,6011,6012,6013,6016,6017,6019,6020,6021,6023,6025,6030,6034,6035,6036,6038,6039,6040,6041,6042,6043,6046,6064,6069,6071,6073,6085,6086,6090,6092,6094,6097,6098,6102,6104,6106,6108,6109,6111,6112,6114,6115,6118,6122,6124,6126,6127,6129,6131,6133,6134,6136,6138,6140,6142,6145,6148,6149,6151,6154,6163,6166,6169,6171,6173,6174,6177,6180,6184,6188,6189,6191,6192,6193,6194,6195,6197,6198,6201,6202,6203,6209,6210,6217,6218,6220,6221,6231,6235,6237,6238,6240,6241,6242,6243,6244,6252,6258,6276,6284,6413,6900,6913,6918,6964,6973,6974,7516,7517,7519,8222,8227,8230,8231,8237,8240,8241,8242,8247,8249,8256,8258,8259,8283,8306,8307,8326,8334,8335,8351,8369,8386,8387,8422,8423,8426,9058,9321,9323,9332,9336,9339,9343,9353,9356,9363,9369,9380,9382,9383,9386,9388,9391,9392,9399,9404,9405,9407,9408,9409,9412,9413,9416,9421,9427,9433,9434,9436,9437,9442,9450,9451,9452,9453,9454,9470,9476,9481,991,992,997,1061,1062,1063,1158,1254,1257,1317,1318,1363,1435,1552,1585,5829,5830,5831,5832,5856,5865,5867,6009,6011,6012,6013,6016,6017,6019,6020,6021,6023,6025,6030,6034,6035,6036,6038,6039,6040,6041,6042,6043,6046,6064,6069,6071,6073,6085,6086,6090,6092,6094,6097,6098,6102,6104,6106,6108,6109,6111,6112,6114,6115,6118,6122,6124,6126,6127,6129,6131,6133,6134,6136,6138,6140,6142,6145,6148,6149,6151,6154,6163,6166,6169,6171,6173,6174,6177,6180,6184,6188,6189,6191,6192,6193,6194,6195,6197,6198,6201,6202,6203,6209,6210,6217,6218,6220,6221,6231,6235,6237,6238,6240,6241,6242,6243,6244,6252,6258,6276,6284,6413,6900,6913,6918,6964,6973,6974,7516,7517,7519,8222,8227,8230,8231,8237,8240,8241,8242,8247,8249,8256,8258,8259,8283,8306,8307,8326,8334,8335,8351,8369,8386,8387,8422,8423,8426,9058,9321,9323,9332,9339,9343,9353,9356,9363,9369,9380,9382,9383,9386,9388,9391,9392,9399,9404,9405,9407,9408,9409,9412,9413,9416,9421,9427,9433,9434,9436,9437,9442,9450,9451,9452,9453,9454,9470,9476,9481,991,992,997,1061,1062,1063,1158,1254,1257,1317,1318,1363,1435,1552,1585,5829,5830,5831,5832,5856,5865,5867,6009,6011,6012,6013,6016,6017,6019,6020,6021,6023,6025,6030,6034,6035,6036,6038,6039,6040,6041,6042,6043,6046,6064,6069,6071,6073,6085,6086,6090,6092,6094,6097,6098,6102,6104,6106,6108,6109,6111,6112,6114,6115,6118,6122,6124,6126,6127,6129,6131,6133,6134,6136,6138,6140,6142,6145,6148,6149,6151,6154,6163,6166,6169,6171,6173,6174,6177,6180,6184,6188,6189,6191,6192,6193,6194,6195,6197,6198,6201,6202,6203,6209,6210,6217,6218,6220,6221,6231,6235,6237,6238,6240,6241,6242,6243,6244,6252,6258,6276,6284,6413,6900,6913,6918,6964,6973,6974,7516,7517,7519,8222,8227,8230,8231,8237,8240,8241,8242,8247,8249,8256,8258,8259,8283,8306,8307,8326,8334,8335,8351,8369,8386,8387,8422,8423,8426,9058,9321,9323,9332,9336,9339,9343,9353,9356,9363,9369,9380,9382,9383,9386,9388,9391,9392,9399,9404,9405,9407,9408,9409,9412,9413,9416,9421,9427,9433,9434,9436,9437,9442,9450,9451,9452,9453,9454,9470,9476,9481,991,992,997],[0.00183325298360158,0.00039212257121856,0.00057423345387314,-0.00132458428750101,0.00108114108185058,-0.00029876488128072,0.00349238230982765,0.00331133846265616,0.00192186464374482,-0.00372483493011358,0.00037630376987094,0.00022305040457697,-0.00384008279600818,0.0019484631163832,0.00051950935076707,-5.5750691614878e-05,-0.00130561898494963,0.00236588888283248,0.00291672136490557,-0.00135219007713743,0.00075657974174398,-0.00038855672670189,0.00239972066973392,0.00065935204368301,-0.00071249348519101,-0.00145234609718596,-0.00029220931505881,0.00142320065501383,-0.00016289593667946,0.00169370750158486,0.00072207776298626,-0.00075280579960039,-0.00057326644030257,0.00064954290202643,0.00525859829900291,-0.00134266695284048,0.00038250847845203,-0.00188017792929249,-0.00340718953614956,0.00080093994567767,0.00076863186843618,8.14427929498157e-05,0.00140886809842849,0.0013129828680629,-0.00111670997118427,0.00336364900033951,0.00042296301449202,-8.41067947486288e-05,0.00060161042722263,-0.0007103000381373,0.0012567032451354,0.00090391830575701,0.0011733347675236,0.00035174421889943,-0.00093895899155156,-0.00240689811958829,-0.00067509309110605,-7.75743089738819e-06,-0.0024718097133144,0.00095263138075874,0.00114648946398867,-0.00016671181918291,-3.27149879593755e-05,0.00067033377622975,-0.00042319649641669,0.00017173783693045,-0.00102970783952897,-0.00171495490060564,-0.00068736986960571,-0.00063976329217626,-0.00152946976184184,-0.00026488125326805,-0.00128493255225669,0.0006400307948661,-0.00198728779669906,0.00111387500347928,-0.00014008871387077,-0.00030335820593216,-0.00108776798830898,-4.35266105785138e-06,0.00030063749526478,0.00119765808479742,-0.00116782655626067,-0.00106550150089418,0.00061118901224642,-0.00395174628286502,0.000907576260602,-0.00284446377963829,-0.00218438361630843,-0.00185004775872117,0.0003357115177959,-0.00231291722472534,0.00096321603940908,-0.00213372877959439,-0.00177632594207483,0.00267479660285868,-0.00154711332077436,0.00066702213721764,0.00227109045073777,-0.00242940666768648,0.00138307336759136,0.00028244815068325,-3.87421273174718e-05,0.00074769095058835,-2.18068980172604e-05,-0.00155365605694686,-0.00135799680731527,0.00218224136713888,-0.00259118207867421,-0.00121837556812891,0.00080786219288876,-0.0010950969104054,-0.00121254501675171,-0.00204141387320076,-0.00023471581797967,0.00217307142625014,-0.00060394316507261,0.00224967093996061,-0.00163705157591253,-0.00196144422744733,-0.00178609031127226,0.00291591504050994,-0.0029945744337599,-0.00322570845250351,0.00302245976557581,-0.0011013127158074,-0.00234942743423033,0.00113355345231259,0.00179818822873112,-0.00107061240423426,-0.0005748167467094901,0.00164441307170486,0.00077545888455567,0.0004630071798459,-3.28895843208408e-05,0.00190077603674607,0.00195834316096309,3.14870313614811e-05,-0.00139953870685853,-0.00113656143688763,-0.00180378218060764,8.17731936132596e-05,-0.00065920324788089,0.00066024038947927,0.00129360464536604,-0.00103513135876514,-0.0008523728018989,0.0019477871060941,-0.00082097321537147,-0.00020462637759349,-0.00174764105888494,0.00186971341830827,-2.60636154888945e-05,-0.00356271632363583,0.00010493532950743,-0.00098709302573986,0.00286982048257114,-0.00067999609332858,0.00262764623197684,0.00205537324032303,-0.00096822215188708,-0.00030646595410816,8.23043113703557e-05,-0.0005482418361483199,0.0009849248836875601,-0.00014629217898206,0.00239358502856312,0.001494242348901,0.00035218998835966,-0.0009379384153444501,0.00068967270840081,0.00122401691998004,0.00029054389987475,0.00119769415117206,-0.00058851987482334,0.00160365219309293,-0.00020966475418482,0.00028795649696917,-0.00081156878784034,-0.00082982209813305,-0.00213686627552485,-0.00071617696219933,-0.00092576078678084,-0.00073637852848595,-0.00018826844402165,0.0010573591724576,-0.00059076175737089,-0.00032768546105301,-0.00184623045314335,0.00166077264277967,-0.00088536623146407,0.0001921880772588,0.0050861752297928,0.00265672853581943,-0.00185400309159877,5.40361067196376e-05,0.00079970066421447,0.00125767585754639,0.00069310591645555,0.00144242237433752,-0.0006242007588573,0.0002219644414566,-0.00027762594980193,0.00057191305791065,0.00027573167562105,-0.00055211976944033,0.0008702495248816701,0.00290284392616671,-0.00228477291137878,-0.00028969844976862,-0.00087235343489232,-0.00383800080691416,0.00180297262455876,0.00055871848668805,0.00105945902895719,-0.0014425443578986,0.00089160145102167,-0.00067055859730708,-0.00067740189549659,-0.00061880710818256,-6.90181891190863e-05,8.99448420405813e-05,-0.00052194893283537,-0.00017848903500538,-0.00143393369269976,-0.00165925349237741,0.00115441305224415,0.00111531811484967,-0.00027533856709592,-0.00015207161432641,-0.00084332462931417,-0.00187441686787199,0.00020020574785358,0.00037631054107704,0.00056945593731888,-0.00131165022996934,0.0016915355244429,0.00065429179492874,-0.00047336218101618,0.00027143669536872,0.00081609510671757,-0.00082838000041901,0.00030674598990519,-0.00010210724992557,0.00054231135729676,0.00180470665447167,0.00104247776124592,0.00058887422828386,-0.00085040952606101,0.00117289971569763,0.00023503826216626,-0.00214966618876113,5.16012150040439e-07,4.9911291655633e-05,0.00101688570505818,0.00013221027273866,0.00122413049938479,-0.00024037521130516,-0.00122442407648133,-3.56350645895445e-05,6.11226167224274e-05,0.00094813072127601,-0.0004224981397647,-0.00087354457777017,-0.00144584108958373,0.00017193826960554,0.00157260076933391,-0.00065574824870161,0.00019817009089665,0.00058221290845942,-0.00016709698207964,0.00200056015340834,-8.82142612296581e-05,0.00042819645224858,0.00035457495603527,-5.4351095292248e-06,0.00041407306796207,0.00049750302094076,-0.00053830617048045,-1.21914497160916e-05,-0.00078478932409999,-0.00069046579894581,0.0001995738037461,0.00178388135632061,-0.00125271464967898,-0.00371904778638551,-0.00021617858048091,-0.00061602018178404,0.00111076856652124,-0.00028575889752038,0.00048765935809987,-0.00073131785455223,0.00112315027889096,0.00115945585494265,0.00178202837831036,0.00185199473258458,0.00158983248186252,0.00072591586218191,0.00022222970464008,-0.00042309329138268,0.0007886325420183,0.00019121101601839,-0.0016275562679687,4.68618694866033e-05,-0.00027368387785857,-0.00017277968406323,0.00101313901193987,-0.00092624655993413,-0.00105197055771172,-0.00114410607500757,-0.00140328963442005,-0.00054641162940411,-0.00126284921192638,-0.00135818663282079,-0.00112779108907145,0.00010432378632386,-0.00075288899320556,7.836063904432829e-05,-0.00132528365244697,-0.00066706755487169,-0.00093460496026387,-0.00105758261082931,8.169836324251369e-05,-0.00146912833639153,-0.00126280967100316,-0.00133340036827773,-0.00182165891423887,-0.00125125511466051,0.00013125597816652,3.34511997388729e-05,0.00037571866259021,0.00169322312375992,0.00014739443463027,-0.00046435374863943,-0.00036129025037056,-7.27821345541714e-05,0.00119567667758766,0.00083778271735668,0.00224947941622994,0.0003642120002073,0.0010261215122209,0.00320013820909471,-0.00313054038417057,0.00183458876507983,0.0001504589040606,-0.00010466700255156,-0.00117420515763014,0.0009876226529199599,-0.00094177629751612,0.00143114755040909,-0.00063730285560474,-0.00170275985860614,-0.00210087147246935,-0.0031771740801467,-0.00132786178320218,-0.00032914984649407,0.00015664180389123,-0.00183019883359404,0.0007326291954529,0.00049737717087534,-0.00014511619417125,0.00010020048007712,0.00042090720691557,-0.00118543278241506,0.00042986626743854,-0.00045533133169888,-0.00084124623464391,-0.00028899231612102,1.86957381719662e-05,0.00403319354975424,0.00150735725348264,0.00230572986830379,0.00096675846189222,0.00123819406446102,-0.00122800478783611,-0.00070015443459787,0.00162589111025105,-0.00067446285250954,0.00116589410955017,0.00105398955451834,0.00111273291026634,-0.00062855796861765,0.00028438681494764,0.00018886781549335,-0.00030946346973141,-2.64491104739946e-05,0.00199456957573796,-0.00221714879965537,0.00035947997635905,0.00068168812259221,0.00090260707039847,0.00026893754349538,-0.001136547935192,0.0014301207759387,-0.00177693108309187,0.00018794051968114,0.0029557033758644,0.00178551107570076,-0.00024805996168835,8.431412700829721e-06,0.0019610319246708,-0.00088813984211462,4.378935925014e-05,0.00191325427478127,0.0012425488280903,0.00058731641358083,0.00463777589518909,0.00079545806014036,-0.00360607222287792,-0.00047420167107941,9.900969795611191e-05,-0.00393569046938669,-0.00119289995111062,0.00199038643652476,0.00175603098139062,-0.00093142221228741,0.00249727437559133,-3.41779886779457e-05,2.2587707482472e-05,-0.00128277028755256,-0.00044178402878529,-0.0008310176624431299,-0.00092065298619874,0.00115873147804237,-0.00032028932816283,0.0002786389036169,-0.0015298140659094,0.0028748284951686,0.00339712769403873,0.00419001368708054,-0.00100508851284235,-0.00207067305248789,1.34372456763463e-05,0.00286379170472229,-0.00392596827330108,-0.00011217957614507,-0.00316633199604474,-0.00411753677401919,-0.00065886623857415,0.00139418322477683,0.00286469806145152,0.00183764362764054,0.00090790484535398,0.00069321624994927,0.00473332925328625,0.00031653311870922,0.00156237112378823,0.00308779169367582,-0.00118934919781385,-0.00049892630715654,0.00390178034038735,-0.00087115636635669,-0.00105435167253985,0.00039782987899538,-0.00177561697334426,-0.0013765811944622,0.00262968286714312,-0.00174101341726745,-0.00072245144075977,0.0006323021553009301,-3.35853283829369e-06,0.00113912866341951,-0.00139897896578316,0.0026263416379282,-0.00041189518248714,-0.0006379570660576,9.72463756976271e-05,0.00124271314233269,0.00140412181327043,-0.00256522742941023,-0.00071921827965545,-0.00105160144760043,-0.0023999767327987,0.00012021906778922,-0.00129129829324016,-0.00267186458811233,-0.00082000751738656,0.00209784958419786,0.00076666218826048,0.00122471297874678,0.00160151300324522,-0.00068817348454299,0.00020256535302913,0.0006463126526858,0.0027161910031485,-0.00424250252853316,0.00243094602623183,-0.00400529225919795,-0.00405826371054408,-0.00425047346832873,-0.00222370076459412,-0.00282411010686971,-0.00225031701642142,-0.00377811726561433,-0.00372630703328293,0.00278980448251129,0.00308992807566624,-0.00087698740690409,0.00238155768026126,-0.00151163887758483,0.00411393960389199,-0.00031472443605184,0.00117939653096795,0.00051711283471667,0.0015245506350021,-0.00104037759767282,5.25042560217451e-05,0.00216314090153831,-0.00055359056251467,0.00208390772996432,0.00294051353133327,0.00194387328441459,-0.00337695950722866,0.00100235048639689,-0.00129955087321794,-0.00134413473965315,-0.00131181852152532,0.00078690157912816,-0.00266733408707335,-0.00172791994448887,0.00166137717186593,0.00214325268467568,-0.00347286072080699,-0.00399237128376838,4.76318556189502e-06,-0.00074686910037877,-0.00312236886858262,-4.6848663679141e-05,-0.00134601767219721,0.00160529410468855,-0.00019690600746828,-0.00044885374557939,0.00173681739492359,-0.00337221590158772,-0.00114935622844971,0.00119113183093347,-0.00221099068577457,0.00537738201170849,-0.0041811895724814,-0.00180878337016294,-0.00392628709936053,0.00241292244485628,-0.0002953320288202,0.00187575427191966,0.00316927531228535,-0.00320295508697707,0.00149458235082237,0.00176623988405195,0.00067379822197946,-0.00324862354705679,-0.00412967374517606,0.00800781590106101,-0.00269162980294347,-0.00417126376577938,-0.00141215840981638,0.00123096123862751,0.0009624467906983501,-0.00104686159875757,0.00226082697281109,-0.00210527871362883,0.0017860042786237,-2.70292346207414e-05,0.00360631024066491,0.00029586490766402,0.00307040095151733,-0.00093842130108046,-0.00024204038963663,0.00031153650647058,0.00130564053963089,0.00164730911789651,-0.00129814741257383,-0.00270465750758965,-0.00142768754270508,0.00312093503499589,0.00134285842818581,0.0016536902190398,-0.00301028311927387,0.00036475398306367,-0.00233885991699919,-0.0040426373161349,-0.00407453314350257,-0.00186736580001729,0.00155150334282195,0.00134615244536855,0.00274657235772316,0.00065258606853959,0.00273911500637308,-0.00174405431383425,0.00119398322374592,0.00194567630546181,0.00033054061619071,0.00119860412333618,0.00105473715423481,0.00336911389230378,-0.00166628955373345,0.00117549820426782,0.00127188324540017,-0.00142679320606939,0.00302054922898941,0.00043246850439816,6.08004200087986e-05,0.00155698148656034,-0.00074335847180829,-5.19730173526717e-05,0.00087741083551781,0.0010768654794426,0.00019217153037129,0.00112880514752203,-0.00289589321528997,-0.00038014877462818,-0.00149903093116671,-0.00389798935177186,8.94439207605578e-05,-0.00137952530512333,0.00056250360324586,-0.00132998264476723,0.00287594032907797,0.00097464175019667,-0.00039402779676082,0.00042228681590827,0.00072138525526071,0.00157561906148212,-0.00130095196061883,-0.00180466036624554,-0.00198432827784789,-0.00200743802103644,0.00208159892780095,0.00316322212039729,-0.00028667877725867,0.00024173976149574,-0.00209247003223276,0.00059759978113795,0.00244215902192346,0.00303760200705065,0.00246175866731435,-0.00139895469050236,0.00503336709206645,-5.16033038739478e-05,-0.00090637765032618,0.00215783278089215,-0.00073147279147384,0.00010656985458795,0.00023881245651929,-0.002145008204511,-0.00144520543816982,9.36109313964353e-05,0.00379253470410164,0.00123932446034817,-0.00087107002653617,0.0002452969169282,-0.00028569636577087,-0.00191155366005564,-0.00064366601856709,0.00049051920914964,0.0022559295368698,0.00011246533576096,-0.00055548360393919,0.00058351837305988,0.00032112956419532,0.00185713270332908,-0.0002234216895604,0.00113792470621011,-0.00084117912346721,0.00204345798352104,-0.00278023143964945,-0.00224916082948313,0.00141257177343931,-0.00207396870980806,-0.00063805535069557,0.00217718346723902,0.0004520798209204,0.00098297613046344,0.00083833011436705,0.0004990044376616401,-0.00253046158272812,0.00110870591963727,0.00102666850372502,-0.00158692206336948,-0.00034761630899522,-0.00086793325177248,-0.00170224651019,0.00013588146128211,8.785810283631609e-05,-0.00070798307729989,-0.0002255652872995,-0.00369324453886108,0.00027496187364309,0.00307142791489258,0.00026061591919495,-4.77758653332758e-05,0.00079337226888815,0.00053583615500695,-0.00104140572530905,-0.00058502520375515,0.00256595226776212,0.00499493322127965,-3.107372670977e-05,0.00390758760314354,0.00182760412802302,-0.00062810171396553,-0.00158231315765177,-0.00049991814830618,0.00224014585459465,0.00048506998813793,-0.00109215753421794,0.00024356319831385,4.90635081926597e-05,0.00090347064819409,-0.00289321368364223,-0.00069589618150323,-2.11748392429571e-05,0.00037670763063293,-0.00118580929868652,-0.00121348444254199,-0.00212883539808433,-0.00207877792052126,-0.00252260390649781,0.00141366723581441,-0.00092967329332046,-0.0032106039006164,-0.00117776520857706,-0.00049722197663213,-0.00139053507568706,-0.00057258156028781,-0.00185584863967167,-0.00241584231617678,-0.00328192043201069,-0.00208592882351391,0.00141705270954056,-0.00057317084092918,0.00226416558140396,0.00113706318683401,0.0011950388885494,-0.00023710672216064,0.00121165497925628,-1.17955447635949e-05,0.00167978069830528,0.00077450526815919,0.00160947883320346,0.00186420254374385,0.00333348387737726,0.00237648691319843,-0.00421156809162383,0.00093094074645925,0.00217489757177933,-0.00192096095749097,0.00017166394905879,0.00125236781031214,-0.00102129171762673,0.00096548592404423,-0.0009287404487583,0.00094825771852089,-0.00177802881047867,-0.00273485044831324,-0.0020773087920253,8.49747024043459e-05,0.00343863990668543,-0.00076078021240294,0.00017990444291875,0.0014868161457621,0.00062525670391588,-0.00106462252715611,-0.00020144062444657,-0.00194764041880772,0.00102589758063815,-0.00193766791989513,0.00162403556605847,0.00063325243366118,-0.00074647182641885,-0.00057742565587262,0.00171280896163519,-0.00048817819273597,-0.00024462227288546,-0.0006171216729323799,-0.00106520754455438,0.0015087456740105,-0.00080370402109793,-2.76342816206525e-05,0.0003738423966591,-0.0005466632697659,0.00043219054318764,-0.00016459976220935,0.00107065695351919,-0.00105146224853574,-0.00147673004476575,0.00088905607061539,0.00077711277076368,-0.00367283772218923,-0.00087333548865251,0.00309107525006922,0.00031597034575954,0.00035965216124099,-0.00146774760199942,0.00205267379543119,-0.00367336106403286,0.00072244362903908,-0.00019121544290713,-0.00043704087313162,1.15607965010542e-06,-0.00107165389092854,-0.00192653832745075,0.00193306803327944,0.00415236089049723,0.00108967675262804,0.0004842518400256,0.00077864231651114,0.00345586172118772,0.00402671902731028,-0.00431886969429515,0.00102555040867456,-0.00241242326956175,-0.00580800147487322,-0.00212196888605736,0.00340645673185623,-0.00052099997147207,-0.00076826950300854,-0.0006110607490865401,0.00055954352159014,0.00139785367976436,0.00258348378754073,-0.00019552770141185,0.00142892242121239,-0.00036220998302431,-0.00036935296295024,0.00078280688231644,-0.00077641503383127,-0.00122754674054394,-8.6641540520788e-05,-0.00137415753001844,0.00325744276550452,-0.0035994901248261,-0.00239042175238356,-0.00158932838844275,0.0004666744916934,-0.00144192381438292,-0.00240473561602817,-0.00211437572218959,-0.00463780941799503,-0.00020054991271818,-0.00148663470123895,-0.00211368228226866,0.00069323682459013,-0.00031161798734735,0.00211434614967729,-0.00193124853335256,0.00447990997472899,0.00368257304875823,0.00035599391404239,-0.00357134037674201,0.00291879248183048,0.00444398115024319,-0.00109530287403493,-0.00155443744204819,-0.00155285112793377,-0.00106427753077401,0.0042323973953146,0.00078243319722222,-0.00111589389606603,0.00297197878725023,0.00446484175009903,-0.00194140309102344,-0.00220996706726379,-0.00140709538001357,-0.00160980387077478,-0.00082950982381555,0.00172619881117821,-0.00199827665499909,-0.00084371588419279,0.00344694619409926,-0.00208697940488114,0.00270329900489489,-0.00285308956845375,0.00109833602105666,-0.0008225049436053501,0.00119971937879249,-0.00164492961034369,0.00469894707447135,8.672139786166281e-06,0.00084557426980686,-0.00136933170092341,-0.00114359619755924,0.00115173082741992,0.00010724724236681,0.00332977794412272,0.00200918143147696,-0.00533671606770369,-0.00100305300860505,-0.00150645669377767,-0.00178303257613706,-0.00208571172578153,-0.00226254607484457,-0.00147100994134797,-0.00059273027768839,-0.00272502058301509,-0.00431550845811649,-0.0008779350011908199,-0.00131502073577728,-1.9939144901667e-05,-0.00051841177326983,0.00183176696944852,-0.00040387292742548,0.00076762778186152,-0.0041669031843557,-0.00047249478553022,0.00227089365011041,-0.00327281039173624,5.22684113711975e-05,0.00156904950960065,-0.00189780119235388,-0.00124529653290223,0.0007467224292284,-0.00177097380506567,-0.00317384387706791,-0.0011218397730692,0.0010191849503094,0.00194550031253441,-0.00053278753261853,0.00244957226474699,-0.00314190562451888,-0.00332871746560826,-0.00148759264024594,-0.0005893402345269,-0.00287544940101032,-0.00473742819519607,0.00085839992985234,-0.00294649001983964,-0.00137789494363334,0.0022338336364336,0.00264288560974737,-0.00165034446387209,0.00235290089798719,0.00364402514081784,0.0022537950047711,-0.00394711529445771,0.00443012889074987,-0.00056611318677191,8.19947677528822e-05,0.00359058623323498,0.00133599532311569,0.00061092529770417,4.84072733831219e-05,-0.00177374155001608,-0.00209242627250258,-0.00020336981071535,0.0015708015971266,-0.0027478965056648,-0.00035167309617776,0.00356358851004491,-0.00266675826851581,-0.0026627493210849,0.00204063731205423,0.0013014468779786,0.00164335643604916,-0.00549044330359295,-0.00021532709441417,0.00194782839143541,-0.00229727263207613,-0.00076780586759815,-0.0016668095420466,-0.0008882611538590099,0.00052762083261613,0.003276565276493,0.00318381402815359,-0.001352056030005,-0.00020279439803564,-0.00294116347941755,0.00466197825307315,0.00343501495065551,-6.60341772825113e-06,0.00088386746380259,-0.0009916016999618801,-0.00064873378911297,0.00442446546415165,-0.00082518953701855,0.00010792741319523,0.005328315481937,-0.00159650383504797,0.00230117924121042,0.00239121327879999,0.00259982539030571,8.98253690956109e-05,-0.00169928591827212,0.0015655034368992,-0.00175160311761945,-0.0008501664248953,0.00127815900761691,0.00125768832273815,0.00144583221748321,0.00161333376222787,0.00140357402848169,0.00283197963353238,0.00358810714773948,0.00214253193788525,-0.000192836140656,-0.00100945777159659,-0.00083829854084568,-0.00056108011081208,0.00486957777402059,0.00039232471457804,-0.00020465262557947,0.00023685254882292,0.00012985995183871,0.00047457410711882,0.0001578273116103,6.51940614289018e-05,-0.00070562368112916,1.96815612783976e-05,-0.00067272349254821,0.00157425701183763,-0.00028392513780377,-0.00074653055142519,-0.00105017002567323,0.00017162521797972,0.00081541074838757,0.00119937401947782,-0.00075393221134162,0.00025403646888276,-0.00019287636777156,-0.00074945127524038,-0.00020621625433056,-0.00050770867165237,-0.00014098182441728,-0.00082076196098317,-0.00088716673281756,-9.23880754870977e-05,0.00021158054909795,0.00022223801871849,0.00024364995495568,-2.86186721026637e-05,-0.00054216848993389,-1.60352598314097e-05,-0.00062202752917873,7.26075451510755e-05,0.00068487817508885,-5.5233399911153e-05,0.00011953040436033,6.57926024901413e-05,0.00049239321761683,-0.00021624159212675,-0.00028966644093722,-0.0009068239106034401,-0.00034326971403058,-5.05272192340093e-05,-0.00027955838004159,0.00012342028836083,-0.000234423557133,0.00041019516325202,5.80400510633528e-05,-0.00022192116198714,0.0006847402078461,4.90566989683506e-05,-7.087681473521919e-05,0.00016706511829828,0.00026505185352434,0.00245976611892,0.00024706612383638,0.00026723533510513,0.00015565078190412,0.0005854120218237501,9.7017749490219e-05,0.0006600540690214701,4.80164415626205e-06,-0.00083467304122486,0.00010187059728929,-0.00036170519020734,0.00025561809336116,0.00073900070802862,-0.00059512991541971,0.00013854389003046,0.00093880668482183,0.0007721194615830399,0.00087363368864322,0.00066602571857062,0.00074454568030631,-0.00029846601616542,5.24943205895227e-05,0.00064957855619933,-0.00058613272377711,-8.040413170329319e-05,0.00321493009330678,-0.00041222921772986,-0.00029878562016516,-0.00064106395647327,-0.00055291393866575,0.00017922223186168,-0.00040108998589124,-0.00057682520910303,0.00024460949345155,-7.69389483555479e-05,0.00046036348111634,-7.67939249879397e-06,0.00033924023913994,-0.00053134306019174,-0.0002060602913605,0.00052267068765402,0.00020602087808422,6.468881291559441e-05,4.85446954387136e-05,1.10598841793038e-05,-0.00017243427511121,-0.00040178782020599,-9.94755975287518e-05,-0.00065546860226561,-0.00063773663694074,-0.00011622162155728,-0.0004224349180112,-6.1260845948006e-05,-0.00046971569224148,-0.00056631441272206,-0.00051468701479978,-0.00034127764748327,9.86033730191288e-05,4.27266397084016e-05,-0.0006743948586678999,-0.00056879816730845,-0.0005353719993312,-0.00041861777711511,-3.65794220400448e-05,-0.00024931672981753,-0.00085976203912382,-0.00080531338342045,-0.00023796871541391,1.54464204474262e-05,0.00077796407390965,-1.14873875847055e-05,-0.00057539149511465,-0.00077788227593459,-0.00021757155826584,-0.00039945679553857,-0.00070650747129282,-3.05826738079161e-05,0.0006762701736835901,-0.00023844558976435,0.00044228734216051,0.00059991434786253,0.00018577267660407,0.00075205613385644,1.01471861734478e-06,0.00091908056893218,-0.00014284470571326,0.00010815510517247,-0.00040288955862267,-0.00083110020977857,0.00068232600266324,0.00048921063200107,-0.00049130056739229,0.00040969751062571,-0.00050385358695661,0.00174951852320462,0.00108926216309248,0.0006561529952448,4.43942437728556e-05,-0.00016663765139076,0.00074909965880074,-0.00031668202668641,-0.00040746058929791,-0.00027523220007067,-0.00065616864134336,-0.00051845012133371,-0.00016668809484246,3.43749153202282e-05,-0.00058094435745141,-0.0007395136347124,-0.0006333508407099,-4.27250716806558e-05,0.00053855172198035,-0.00012846134083866,0.00041410058100249,-0.00076366590029337,-0.00060968038172993,-5.7659525826221e-05,0.0007849084251277,0.00076774450032042,0.00034474893122335,-0.00068057856928044,0.00124696562194705,-0.0001706736872273,0.00020524085750017,-0.00081784537222305,2.03931981899074e-05,-0.00027732137199965,-2.47933291746398e-05,-0.00042842749520684,-0.0004896489595299,-0.00069942915188255,0.00043995432681673,-3.89224412728916e-05,-0.00073040679475432,0.0002378703071788,2.81620977354078e-05,0.00011433763726755,0.00078339278498279,0.00086224869973745,2.03799454346268e-05,-0.00080856532944107,0.00047394625618808,8.3947910433391e-06,0.00111695958695436,0.0001569735143255,2.11679219579561e-05,-0.00035854484279987,-0.00114475922632806,-0.00077267416509742,0.00066065801387662,0.0017237561486767,-0.00049493710782931,0.0008583325570815,3.50939842347977e-05,-0.00057354030985599,-0.00049631874135228,-0.00038666006514682,-0.00275693082076331,0.00041410202316883,-0.00139378165091312,-0.00039408189311986,-0.00092484613301904,0.00043637108704128,-0.00070905178987449,1.37543917457725e-05,0.00057666805256632,-0.00040912282971966,-0.000353969921073,-0.00207474231426503,-0.00172579021353541,-0.00062698047270926,0.00041475064103786,-0.00072168264163616,0.00211035389927887,-0.00111279465568419,-0.00080292879660996,-0.00195449847057958,-0.00053988656086692,-0.00039239795501541,-9.364520562042091e-05,0.00076817111337782,0.00114758096966753,-0.0001593978596689,9.62289494492253e-05,-0.00047767054813097,-0.00109505596488255,0.0003826520878305,-0.0005785707540587501,-0.00076414955457915,-0.00057747657449023,0.00049532940161833,-0.00041185090864054,-0.00066561413300176,0.00313426024865328,1.35596648734097e-05,-0.00024734652597119,7.576717333721309e-05,0.00024134535289419,0.0005943742888892399,0.0002072564482337,0.00184362908403383,3.70783437912796e-05,-3.41716006265627e-05,0.00273721370135221,-0.00065237378413649,0.00169123732789468,0.00190894913155275,-0.00103612541891875,-0.00032217602009649,0.0001304375154794,-0.00064499842865867,-0.00058973664019238,7.087266604778531e-05,0.00155713485488964,-0.00021448194284257,0.00070638551313747,0.00043822497357158,3.87571213019263e-05,0.0038401621609418,-2.27785331418358e-05,-0.00051674033572873,0.00179045557917882,0.00027485067310863,-0.00071376220337527,-0.00164078684112216,3.02814486749151e-05,0.00035314633031308,-0.00025826977624006,-0.00060158949868619,4.89934543561455e-05,-0.00022903447271204,-0.00289338308802132,-1.9463539957526e-05,0.00213450930416036,-0.00082949641996896,0.00042465281965801,-0.00248772868343951,-0.00107431264215308,-0.0004448122631855,-0.00119989257996215,-0.00033489634596709,-0.0002071898616458,-0.00114073178006311,-0.00095435498143919,0.00040823239887325,0.00029010955633368,0.00101298761003042,-0.00056460605549771,-0.00184985251727369,0.00047268285889248,0.00096842761073754,-0.00079939135283605,0.00125374729684208,0.00035652813166371,-0.00137414354650518,-0.00107586764069775,-0.00125032282746909,0.00053242023444978,-8.448531536242149e-05,-0.00200725399847684,-0.00118194742259569,0.0002453603701049,-0.00045160265147124,-0.00024892165162729,-0.00124405756762331,-0.0009881565442991301,0.0008645531879417,0.00010426342396254,-0.00221096698005515,-0.00221116239741909,0.0004218110105787,-0.00133108038870273,-0.00250397174984957,0.00128850429493813,-0.00054681620807897,-0.00150386803344802,-0.00152483960881121,-0.0005989656192654,0.00178021921440574,0.00050231421698952,-0.00048247613344931,0.00014909402236798,0.00249415259467938,0.0006870364620104,0.00086631345704983,0.00033165154028571,-0.00102120534331624,0.00298468563310166,9.21068070650013e-06,-0.00125766526691835,-0.00031527529539926,-0.00048496710524226,0.00128344314487776,0.00025996488553228,0.0023567850256199,0.00275310866867185,0.00028333601362044,0.00136866311491407,-0.00065991321044643,-0.00250150879182613,-0.00139986224728591,0.0016405908072449,-0.00031761409489173,-0.0004830896776424,-0.00048487621942378,-0.00175611934807132,-8.47526784490792e-05,0.00027937111835996,0.00030190742181404,0.00053932299063997,0.00275986967419048,0.00337523376861581,-0.0002253868679097,-0.00018031752872154,-0.00015505989753245,0.0012950185756239,0.00328263931807745,0.00090121276460739,0.00018952168181809,0.00175718612669965,-0.00036924245716901,0.00166629472136395,-0.00037472697697396,-0.00202692759439119,6.06268028734785e-06,-0.00079555585911126,0.00020331945654182,-0.00065414359677366,-0.0008968518269395,-0.00081634357679695,-0.00058912155054793,-0.00062775695916771,-0.00136793785837761,-0.00092843744251598,2.33494912952084e-05,-0.00032449484726534,-0.00109515332831873,0.00418910498896321,5.33024922523017e-05,-0.00095192862279146,0.00148037073999085,0.00514046476022204,0.00361119581351917,0.00093252709494167,0.00281382803270064,0.00118034256399542,0.00064621968889493,0.00053230492725025,-0.00031858704734913,0.00071249841207182,-0.00120848176161426,-0.00077717930303473,-0.00120023980829962,-0.00170574621189186,7.589275156428419e-05,-0.00139757086660605,-0.00270888952311614,-0.00016801699723062,0.00101334877712308,0.00176632030937666,-0.00121758770447762,-0.00067733187135858,-0.00103725385398822,-0.00161465358780985,-0.00047741983518023,-0.00028355633654452,-0.00089831905775716,-0.00217153710551282,-0.00223953161992795,-0.00033639082023869,0.00018417123265852,0.00067456544339932,0.00119598082034096,-0.00075445755957692,-0.00047880116543286,-0.0001550905241696,-7.43490362402208e-05,0.00273374624390478,0.00166821723010315,0.00052983874960145,0.00034542729719285,-0.00095084734259969,0.0012831907509997,0.00017529460289434,-0.00046395178709029,-0.0007657001436450201,-0.00012724247786729,-0.00115767982340615,-0.00085589585603853,0.00181080737524018,-0.00034337928565899,0.00273115077721072,0.00131709749074317,0.00133973342800081,0.00036273518781864,-0.00090792712163804,0.00110867317944226,0.00060955671979031,0.00064002800899945,0.00254527956458219,0.00148227737739135,0.00071525266265164,0.0017642964182339,0.00098991662238108,2.78367020258253e-05,0.00113438091094342,0.00030825013116103,0.00089820679860778,0.00160496424811884,-0.00248222214255552,0.00011753112113724,0.0007470066977135101,-0.00130007163638793,0.00245063946132235,0.00102501334370079,0.00144167468942976,0.00163113524046525,0.00050392174888837,-0.00075202702791299,0.00192868295893733,0.00066662703544902,0.00063445850900199,-0.00060513226692105,-0.00146972573503514,-0.00021709479831668,-0.00031110682336218,-0.00175324507786668,-0.00112091980462614,-0.00021411750010634,-0.00073621847057741,-0.00276401720677232,-0.0003753575684498,-0.00074079280396963,0.00050672700016093,0.00138450191384438,-0.00022854108496531,0.00011754244216783,0.00107701559779984,0.00036784797390114,-0.00132133994824424,-0.00063519417230866,0.0017167824460013,0.00073931534501208,0.00038099685199209,-0.00138887051410972,-0.00117016086006121,0.00038853757192645,-0.00174238405204744,-0.00059312792670352,0.00074338447574337,-0.0007642781867897599,-0.00037110257870261,-0.00050721026295601,-0.00167457692575008,-0.00092396131467504,-0.00130707835204375,0.00067263844725021,-0.00013948169896265,-0.00148424228840196,-0.00064130242575021,-0.00165434315509918,-0.0008278581322412,0.00142881607059125,-0.00064496821359269,-0.00204641755179099,-0.0002355503979131,-0.00046996376451218,-8.39378549069127e-05,-0.00265221697875307,-0.00078100446079896,-0.00082879790911206,-0.00260991274393287,0.00049534993238922,-0.0013045569506945,-0.00022164480223876,-0.00040493679034127,0.00145628272990956,0.00139564408833275,1.56548267687143e-05,-0.00016111412966165,0.00084186198758731,0.00162834737372085,0.00059908199529009,-0.00019880148776976,0.00091115556820523,0.00118114687414109,0.00161442795763126,-0.00241265272305599,-0.00034111938877476,0.00159874278282829,-0.000364646618267,0.00100164083566061,-0.00157159704049105,0.0003436690152988,0.00216481532391548,0.00088315682395228,0.00036998946545154,-0.0015277497938293,-0.00162391912523935,-0.00152055966564736,-0.0001734758549908,0.00167455393463768,-0.00077650898215878,-0.0012057744044161,-0.00136086568222806,-0.00085196412533195,-0.00114472090021464,-0.00021282045371458,-0.00095478799556434,0.00021988019797898,0.00161249078741212,4.82718501537816e-05,-0.0008103423410573001,-0.0013234671190974,-0.00034232016022685,-0.00027719287547586,0.00070144476875156,0.00082208815040934,0.00309286210255358,-0.0017216683032208,0.00128887129736941,0.00174153148085421,0.00052598042886192,-0.00087525887798692,-0.00147555235163444,-9.72176470241344e-05,0.0008889242559113599,-0.00045227855863912,-3.5136346422482e-05,-0.00165733919038716,0.00010058542945628,0.00043018166290504,-0.00197078342821703,-0.00051155923039028,0.00015362883547765,-0.00061871420751814,-0.00118582314140037,0.00028364894983505,0.00214078103032467,-0.00161086898924532,0.00257779698023988,0.00229396386522396,0.00357986038649774,0.00307248538006907],[0.00290482840275484,0.00083904688151336,-0.00029898414423613,-0.00141568135998631,0.00191941919302301,-0.00025625256431034,0.00230262851122016,0.00255140508632378,0.0022594209994861,-0.00085586627366572,-0.00087586262139816,-8.447017281152491e-05,-0.00046376146944036,0.00189741341046938,-0.00025003541874507,-0.00015774788129765,-0.00234040236576701,0.00265740996724879,0.00173361990176478,-0.00160105957000398,0.00043411434908711,-0.00092147906632922,0.00119207236835306,-0.00030607338087182,-0.00102777195343993,-0.00158492002121636,-0.00017359939893414,0.00081196085006134,-0.00138594988072003,0.00128215358939449,-0.00021583654253633,-0.00059050562009975,-0.0004183656055072,0.00012816952108903,0.00554951358457781,-0.00067042948204932,1.14449443934834e-06,-0.00063926738765893,-0.00092601549621775,0.00066632687382142,-2.69502434881047e-06,0.00145735575190147,0.00052015116836103,0.00091118146001865,-0.00115287014396841,0.00232927525105953,0.00022170924872091,-0.00031422248325861,0.00055811211186698,0.00017986495446105,9.70089303224681e-05,0.00090050486654631,0.00097045882529059,0.00101260925123611,-0.00155844344865329,-0.00144458174775351,-0.00184938746658717,0.00024230664578322,-0.00272060159013232,0.00137144770906614,0.00144160900934321,-0.00075907700432381,-0.00020726836841646,8.50334303124783e-05,null,0.00010155407320608,-0.0011476629314265,-0.00045117637377,-0.00017047489740043,-0.0003451336792122,-0.001796892068333,-5.40582494406161e-05,-0.00128160419218495,0.00017900925282775,-0.00256961156551698,0.00264453524974371,-0.00055679967854911,0.00084476338829949,-0.0018141283349713,-0.0003444947454732,0.00070772799315175,0.00069480410383557,-0.0009963750629929499,-0.00175368446305915,-5.68964986507746e-05,null,0.00093644458771555,-0.0018656875865394,-0.00229686225378903,-0.00192871554931555,0.00066190031690141,-0.00110249296699303,0.00071698947437041,-0.00158223321151737,-0.0019440896499021,0.00286141633279882,-0.00138540696921146,0.00051624415579112,0.00312371317818562,-0.0020534532717897,0.00101788812763951,0.0006799999218625,-0.00026518859174396,9.582144465934019e-05,0.0004873632253836,-0.00165399641051085,-0.00185728869534027,0.0009568694913217,-0.00331010221473326,-0.00190726649645815,0.00012660365434455,-0.00103575356772359,-0.00109635724894351,-0.00232954202589047,-0.00088840635111532,0.0009590120927187601,-0.00084625968185255,0.00168409187153115,-0.00075905545137431,-0.00230744321901073,-0.00185679410101731,0.00287593603354728,-0.0030721085584536,-0.00162500827254089,0.00218636153663963,-0.00184583781474471,-0.00284357811447232,0.00110150556041272,0.00093735976423215,-0.00096913900580895,-0.00065683878906335,0.00076990162181559,0.00067235470289303,0.00075432273154295,0.00024441923996667,0.0016955249185946,0.00125006545242972,-0.00032674917873769,-0.00020449460190173,-0.00157300035709406,-0.00266267448547709,0.00038190614342704,-0.00124029215738239,8.99131661982074e-05,0.0012471504516583,-0.00053967808991074,-0.00115153178609092,0.00169967792943208,-0.00072512283659676,-3.49202256842856e-05,0.00084633933581318,0.00228753525634877,0.00037118912973779,-0.00054357332301545,0.00051612478941968,-0.00065121710283214,0.00256561418441351,-0.00119966176085087,0.00217764190152233,0.00145352362517236,-0.00081043882125976,-7.49948657313597e-05,0.00036874928521052,-0.00029583472733026,0.00189640403456077,0.00104028567942436,0.00416282159618424,0.00251416363209636,-0.00028455859584427,-0.00117421863082563,0.00123750814579377,0.00195393535551511,-0.00061879565411592,0.00312527297154265,-0.00036123195349189,0.0022579902314099,0.00012828896815588,-5.90655527748847e-05,-0.00171240501975494,-0.00035324000796132,-0.00160935565063308,-0.00158719152832523,-0.00182988837251682,-0.0014321413559218,-0.0006142209283709,0.00141582051191961,-0.00060332192099476,-0.00104520113032065,-0.00141615983741425,0.00157691447086191,-0.00121939450042661,-0.00033457055717701,0.00532260545905206,0.00221003345094328,-0.00243230009157438,-0.00054264899591669,0.00011751343367859,0.00104159106120113,-0.00056281592814852,0.00161154608447886,-0.00072202695103677,0.0002324161389047,-0.00039130152222619,0.00046774291344879,-4.73741738780817e-05,-0.00041043629726197,0.00082285549247682,0.00254161361734479,-0.00176275326021029,4.93349910278267e-05,-0.00102097219945971,-0.00306327343397762,0.00175627720201722,0.00023150595344703,0.00140795690309861,-0.00174071471226173,0.00055955814474065,-0.0002990232692661,-0.00079507638404804,8.362051789559401e-05,0.00035433413641239,-0.00023766846667294,-0.00083347890014548,-0.00028897311941566,-0.00153190154142211,-0.00172127451189943,0.0008185558433793,0.00113239918884026,-0.00038894050242441,-0.00047633448427962,-0.00115019081620714,-0.00168736094206941,0.00010191288265698,0.00028368697008766,0.00114692889914714,-0.00147378875453619,0.00175409440057027,0.0003256923088837,-0.00035995704912403,-5.16068256935994e-05,0.00046251166526711,-0.00074048852204685,-3.34246823505048e-05,-0.00044288378627884,0.00021662148074337,0.00145941243278759,0.00127028395405913,0.0005319848221827,-0.00098252456598321,0.00113571900584028,0.00012963592563671,-0.00228895402968827,-0.00033313815131358,-0.0002699174690777,0.0015838476501771,0.00025711917910537,0.0014287951986765,-0.00029991915309862,-0.00092051033996663,0.00017321148440558,0.0002893461465903,0.00087391560343452,-4.30107080758746e-05,-0.00053292046549136,-0.00025180409577424,-0.00017312003152099,0.00206435115586571,-0.00072866418288462,-0.00012381019060239,0.00025465942590155,0.00021906022306212,0.001652423720185,-0.00040532045702463,8.63444383907388e-05,3.0324938662682e-05,-0.00034128399445077,0.00125821561296486,0.00051772460169728,-0.0005985703601630599,-0.00013368305426506,-0.00042888849633248,-0.00083283191593862,0.00010126059454295,0.00173657133371935,-0.00141238448845413,-0.00320728324464683,-0.00026607260336373,-0.00018849055289525,0.00102383708186992,0.00016160173742152,0.00016147802969481,0.00110269577157892,0.00147713595468921,0.0008235254652967701,0.00220255525373666,0.00180687746557174,0.00189426950145048,0.00133711323122611,0.00015296740437302,-0.00054145185477992,0.00044179070612972,9.262858584088339e-05,-0.00194879902828109,-5.63675539116543e-05,-0.00058881664934569,0.0005226238102637399,0.00094101668850184,-0.0008456741690995,-0.00119057439767294,-0.0007214030762851,-0.001266988509586,-0.00066874014686444,-0.00108985385224482,-0.00150664840446573,-0.00143052911966981,1.91275885106838e-05,-0.00080887014424171,4.24329088328041e-06,-0.00147268618730103,-0.00042074730903317,-0.00083567995790988,-0.00073281568809842,7.693650906075371e-06,-0.00053001235153669,-0.00118794412339372,-0.00166556045722516,-0.00225193861413447,-0.00095083646261131,0.00013849138879522,-1.13110450963081e-05,0.00057211028257344,0.00134954674285554,-0.0001973390393343,-0.00064984549994275,-0.00047765920510363,4.79722272332144e-05,0.00128093034223479,0.00076001519592669,0.0018912848032116,0.00033300233506848,0.00069212606072789,0.00281113524735754,-0.00316815152592891,0.00181973760892036,8.392096896639019e-05,-0.00022900462668038,-0.00092253587179058,0.00102522260450417,-0.00125963577881287,0.00186723949332267,-0.00041300986986319,-0.00130420753916357,-0.00240605978154884,-0.00115375146773719,-0.00116949680814347,-0.00065164631088236,-0.00018145084899975,-0.00183805500169429,0.00040289288282608,0.00058055394197854,-0.00021046445135375,0.00023448715219581,9.56945808078787e-05,-0.001070874838537,8.79911369149237e-05,-0.0008118207566054,-0.00116552427834284,-0.00032001961296098,0.00074030603889286,0.00365555969051836,0.00116637814438665,0.00244456034491448,0.00155107760320819,0.00088512856770017,-0.00130001933652153,-0.00078373533452322,0.00128319184656334,-0.00050798590965039,0.00116049278077367,0.00135219024280608,0.00105726314912953,-0.00069603883304944,0.00042444827305417,-0.00015641454093462,-0.00064110343259226,0.00053138099384227,0.00164652007733618,-0.00256337950877893,0.00027518554681725,0.00062793084152592,0.0008269264219176,-6.18387511307179e-05,-0.0011127921385774,0.00142435466182565,-0.00207938468574933,0.0001971858171178,0.002593705974605,0.00171869051839776,-0.00056356458555806,0.00131513097007961,0.00274854909905696,-0.00162743072262465,-0.00017276705427531,0.00120243176846108,-0.00027700728986545,-7.74386038171176e-05,0.00352226993287907,-8.99092024701793e-05,0.00040141913411851,0.00039596528113833,-0.00099287836842007,4.71696383900875e-05,-0.00147088437402817,0.00042221830941082,0.002139783240118,-0.00220995381566794,0.00086770585221212,-0.00061465754420036,-0.0002887281637982,-0.00011783270099947,0.00035305464789974,-0.00117353931088285,-0.00094197454622919,-0.00109498831142653,-0.00184488233974412,0.0010043706798336,-0.00011927611202316,0.00222570167357982,0.00189688107820412,0.00185900284121563,-0.00197452410635728,-0.00168605242601343,0.00057971017889159,0.00244066116929181,-0.00049520365015009,-0.00054134253744525,0.00103044691712686,null,-0.00056992185647375,0.00207382033791385,0.00154705735425443,0.00282020129387897,0.00233007902756881,-4.05743306208789e-05,0.00426779433458793,6.40194212557584e-05,0.0013288306241242,0.00199802409673619,-0.0010342816735067,-0.00112297033689211,0.00400964295734256,-0.0002272116318289,-0.00025547613701724,0.0005737158619356,0.00059176728336278,-0.00085335093091542,0.00084963153514625,-0.00192928368843362,-0.00074241815576055,-8.969835123003379e-05,0.00112053527036606,0.00071494341402315,-0.00160037973883055,0.00131648377151565,-0.00071200291731125,-0.00132943651756012,-0.00110301545082229,0.0012670862786122,0.0002825415434346,-0.00219943720450969,-0.00143696912567206,-0.00151736022665144,-0.00099391066009958,0.00039649305232087,-0.00203979293449782,6.58066750616072e-05,-0.00045432419068851,0.0010364874176757,-0.00139046961322112,-0.0009925290322536199,0.00149372069074905,-0.00191878796117978,-0.00049816305183794,0.00030252417232747,0.00203274469856886,null,0.00119900030866159,-0.00098640110387046,null,-0.0009989997524726001,-0.00153158146770187,0.00211807568829905,-0.00195292381552108,3.48796409263904e-05,-0.00130965116851927,0.00241093998590944,0.00061663486044467,-0.00239769976397641,0.00091737561139047,-0.00123102658054008,0.00285258708305848,0.0001504125010853,0.00010669719608148,-0.0008239348567068,0.00118741762488392,-0.00158131646573011,-0.00058837116221884,0.00169646479858388,-0.00200409325813575,-0.00034578551492449,0.00141526197105943,2.30174358079119e-05,-0.0005448939714950499,-0.00083154768159002,-0.00247447449276506,-0.00194006818666689,-0.00352950320372021,-0.00039805042164495,0.00022295974878228,-0.00344271355837645,0.00026097990156017,0.00200119446176311,-0.00496412640312971,null,-0.00052677752683577,-0.00150529847127486,-0.00393744806373642,-0.0009470417869608399,-0.00220937906999709,1.18594763575294e-05,-0.00053350444875055,-0.00168238161873419,0.0005028121000713801,-0.00179304301949108,-0.0016705665302955,-0.00087243444168264,-0.0013312072790973,0.0036055809247872,null,-0.00186449548688316,-0.00010567187709139,0.00274216257559961,-7.80233740541311e-05,0.00172608401458963,0.00211177276045697,-0.00102779772294599,0.0006629067872747,0.00067749961905915,-0.00020325628278282,-0.00027919806349303,null,0.008719952819492109,-0.00170094343399494,null,-0.00202103592868636,-0.00128087025544836,0.00043260579249388,-0.00126989288934658,0.0035617952327958,-9.18728601572726e-05,0.0011839081496123,-0.0004386443859675,0.00114762348347019,0.00010534771604273,0.00270094336991506,-0.00105532933414571,0.00079974940732062,-0.00068357408874542,-3.95203301565548e-05,8.53909423283471e-05,0.00235354846413679,0.0005530190266233199,-0.00156118311593537,0.00114569121083644,-0.0004505884572565,0.00074680402079679,-0.00270677132151383,0.0006928082962817,-0.00147366999541625,null,null,-0.00225395699960621,-0.00077276009397769,0.00055283242299565,0.00132125510285778,0.00108156557244546,0.00233316658577383,-0.00275244714940234,-0.00105731349559989,0.00331043715258013,-0.0001337934331564,-0.0006269041940334,0.00117792099707545,0.00268292629191691,-0.00373881496683535,0.00016481594922729,0.00074118318767875,-0.00218117003476113,0.00181248976033351,0.00049375281328575,0.00015862389551504,0.00118545264148494,-0.00022337308554517,3.27210535605557e-05,0.0005157438696359,0.00124162526960663,-0.00015955129322662,0.00129540658358974,-0.00256137575035001,-0.00036407366344036,-0.00182621114930949,-0.00360348896703801,0.00074586293913188,-0.00170843978010192,0.00069024326081163,-0.00167559563618857,0.00248527085050678,0.0011314247510573,-0.00075389764832426,6.396199682031969e-05,0.00034508842903647,0.00173928116527471,-0.00163100668618084,-0.00131557470116045,-0.00170789524203328,-0.002361143094222,0.00226320176519099,0.00276838362964741,-0.00063145256049063,-0.00011070239261833,-0.00241103830588613,0.00123020481503584,0.00204302543436681,0.00261961287928222,0.00207709976248735,-0.00099876729265194,0.00461120718376157,-0.00040336766071366,-0.00115657993120536,0.00177990079874539,-0.00106534934765474,-0.0002439107238011,0.00036950485929523,-0.00176457475340193,-0.0006804471156029701,-0.00025668158805453,0.00338856367552672,0.00085617293019432,-0.00077537897666029,-0.0001071968583633,-0.0006499073438954,-0.00159839270002226,-0.00098325922926844,0.00136690685546267,0.00245814367911087,0.00107686991556201,-7.92057880470108e-05,0.00019511395987566,-3.57948493908173e-05,0.00200184210972285,-0.0002025231592189,0.00075684673879388,-0.0011779060385499,0.00166486942738228,-0.00075657264959232,-0.00222111549951985,0.00104313859147559,-0.00206191941042134,-0.00097772998299548,0.00276613365998806,9.658522736912921e-05,0.00158541459195156,0.00156272164774122,0.00012807122915644,-0.00225287139456723,0.0007436824165445,0.00064713075013858,-0.00152096547089232,-0.00023336494808299,-0.0003488850688521,-0.00170838486262373,0.00019026965890727,0.0001393481063785,-0.00065119918721452,-0.00057122594499277,-0.00376212636389668,0.00041128824200486,0.00267792153488639,0.00036518242743373,9.338976597140019e-06,0.00090795597409192,0.0001759393762571,-0.0010456814005858,-0.0005875359614724,0.00219451461802436,0.00457351307405804,-0.00038312229187191,0.00413936806434266,0.00197572696330245,-0.00052379563796189,-0.00062653497812811,-0.0008415974175044,0.00527728371807949,0.00062449404421715,-0.00055818710059438,-0.00012765230385147,-0.00030058254233658,0.00106208230741125,-0.00290718076618295,-0.00101759796244163,6.45585560667363e-05,7.4674803960876e-06,-0.00151753496607078,-0.00156071021723477,-0.00244687594022913,-0.00169699009908338,-0.00248982416511095,0.00102877193104468,-0.00093323936495922,-0.00230650567552762,-0.00150960761120401,-2.17260611109932e-05,-0.0009778214413594299,-0.00015622879493322,-0.00210246942567944,-0.00274772247714769,-0.00324479672962981,-0.00177091130939209,0.00159387465647151,-0.0009177146966571,0.00247102395344339,0.00077162816517003,0.00174831918833944,-2.81330446450574e-05,0.00083303395961011,0.00012272818704502,0.00178259570818913,0.0009241919485012,0.00125262525688026,0.00148821532933336,0.00353242457454687,0.00322070354265998,-0.00430134305905798,0.00100957188262033,0.00176512970471985,-0.00228942236347435,-0.00018319128544008,0.0008852594964519,-0.00040242978379452,0.00112629648408149,-0.00126836185328624,0.00056980547183724,-0.00142129846702592,-0.00248900267396782,-0.00244946923909453,-0.00028152638640461,0.00359540143781909,-0.0010986738705564,-0.00017164036161032,0.00111630553494014,0.00076965141730598,-0.00141520876796471,0.00036060428118309,-0.00226831044913503,0.0011888501813163,-0.0007115262177113,0.00125153363712428,0.00070365236112592,-0.00110016285225242,-0.00048547141893411,0.00192828829880131,0.0003391356266921,-0.000620415012222,-0.00053803338465305,-0.00138127815599348,0.00164610821791509,-0.00114097477210373,9.7962811980241e-05,0.00049489261720799,-0.00012542823371766,0.0005740918323356901,-6.26413039477326e-05,0.00070618561421795,-0.00140223067822623,-0.00102285459565809,0.00052722010994018,0.00043295090894484,-0.00397447026428614,-0.00077772476618688,0.00269728374938325,-5.49609874763675e-05,0.00086068211359601,-0.00185568764442153,0.00225203321732899,-0.00375865564445904,0.00036302553583422,-7.577071399461469e-05,-0.00035601769664585,6.97672613604926e-05,0.00010856691337395,-0.00029535378840069,0.001824855846101,0.00225012053054098,-0.00077675778687485,-0.00010761420114012,0.00015706237434895,0.00199994371602132,0.00267979735129404,-0.00192774157969844,0.00062675041670046,-0.00066468941415578,-0.00440058880616237,0.00015750777398846,0.00308389310153177,0.00135505451522314,-0.00111609567500687,-0.00030968305050834,-0.00044997521791295,0.00086004223924483,0.0016028805991572,-0.00096447049573134,6.86337653787003e-05,-0.00145106646832624,-0.00078440309840134,0.00089143808164357,0.0003457984811738,-0.00117990939151633,0.00182435037072364,-0.00139820566127694,0.00179613045392946,-0.00378772502284401,-0.0009225035862159,-0.00272948984491108,-0.00060188749004988,0.00204674212496926,-0.00124523443193752,-0.00065043549039618,-0.0007112341002638,-0.0010250767480735,-0.00179728019117467,-0.00266665675672753,-0.00071527183009413,-0.00222035445947591,0.00256595211368401,-0.0017278277583451,0.00405084736935477,0.00319399276617339,-0.00099884446858708,-0.00228420143545536,0.00283510065085585,0.00373600835353631,-0.00160684141934215,-0.00130011790865911,-0.00110748168573641,4.23135352834406e-05,0.00356536116540806,0.00106909615150916,0.00162131488386769,0.00148607965805746,0.00332337429892863,-0.00055953773187993,-0.00206816635894706,-0.0005532588745577,-0.00025373817476322,null,0.00123913960107359,0.00038414720760032,0.00039782202997423,0.00337801913323292,-0.00052845513580551,0.00238722513072704,-0.00028499516438845,0.00119884844814587,-0.00152818052302923,0.00257975210006982,0.00015469364114631,0.00327682414329188,-0.00099364836554931,-0.00023213915069943,-0.00218550528951168,-0.00234518904414802,0.00019794176705801,-0.00143970769261824,0.0014988644132081,0.00110342460955346,-0.00364614930990184,0.00010022949792031,-0.00019858469232374,-0.00103361785090123,-0.00067190058649032,-0.0015152709753233,-0.00069564785917668,-0.00074024276535423,-0.00066411506631092,-0.00156194565469749,-0.00109695612282656,-0.0007509369722156,-0.00148615042773626,-0.0010655630731005,0.00133002100330376,-0.00042197995457876,-0.00020369603718265,-0.00338316311434512,-0.00058863199621196,0.00032239527163705,-0.00278036793333485,-0.00076174531774746,0.00053148871234815,-0.00280056073955039,-0.00269772635490231,-0.0006003263427101099,-0.00138764814028404,-0.00012876019755807,-0.00230170554069463,-0.00085393223909888,0.00103526287198087,-0.0012902846600106,0.00365625634493661,-0.00272354626913576,-0.00468577879775308,-0.00113943894376072,-0.00090455980108446,-0.0036354801804683,-0.00325229389060269,0.00162965262141749,-0.00352028695061147,-0.0029679503763655,0.0015935347787962,0.0015714388298048,-0.00217865642769714,0.00191378331512625,0.00278428578920205,0.0013756218603162,-0.00271972711213399,0.00252464114689864,-0.00073452607777949,0.00078124679224109,0.00169491357716451,0.00224052023000805,-0.00049249329199854,-0.00028477631908108,-0.0013490948177455,-0.00226158735386559,0.0010051795700287,0.00129425569129595,-0.00297300071285564,0.0012388482566723,0.00222638458045154,-0.00127098443041407,-0.00230714571349006,0.00197681176347709,0.00024549209970185,0.00230127191708405,-0.00259305530095322,-0.00093337163645672,0.00086429067621182,-0.00160551137713154,-0.0023385503875185,null,-0.00180496773987427,0.00095496945232832,0.00189495043836178,0.00186194795404095,-0.00259040244561881,0.00218745842640603,-0.00219266065283089,0.00592915890263498,0.00394800403996985,-0.00060889094774058,-0.00062969969766108,-0.00066607337060329,-0.00013971789672334,0.00351673977368345,-0.00075675371126376,-1.04100259465328e-07,0.00398231189889363,-0.00127330404502987,0.00143970948246953,0.00435469850311301,0.00164269247988873,0.00037159501878288,-0.00048229659830418,0.00165274476748512,-0.00301741287133023,-0.00149353468151736,0.00238966083818648,0.00080559471226769,0.00090350021816881,0.00096984465848347,0.0009827906674595699,0.00272296544393698,0.00294273226193339,0.00074006716247574,-0.00090167055973631,-0.00123157558493751,-0.00163055945306687,0.0007044371355567,0.00598746688364434,0.00042701107402174,-0.00033190727695378,0.00031333776873352,6.89966162273113e-05,0.00031727691765961,9.9502428166795e-05,0.00012790792575025,-0.00052075149938198,0.00024217652947699,-0.00065441441970432,0.00160846476029168,-0.0005239939822553799,-0.00076707646028127,-0.00027892987009354,0.00031978535778711,0.00049755868464548,0.00106765671834948,-0.00067291131143518,0.0002044439915671,-0.00034639174057264,-0.0007076365778643,-0.00038777214060467,-0.00073415716763101,-0.00021657735285349,-0.0008387441138632,-0.00056698731376921,-0.0002802834869616,-9.007426235824511e-05,0.00029649724203268,0.00014240236515143,-0.00012007332857912,-0.00054614350231688,-0.00012553422641615,-0.0006762285760369,-2.94816387923114e-05,0.00082960394136314,2.23717719543088e-06,0.00021634135471619,-0.00011676479720049,0.00054673593374652,-0.00040672214652586,-0.00025504873442624,-0.00064820302471116,-0.00035501206099829,-0.0003483783420774,-9.583139818196599e-05,0.0005063232785645,-0.0005296059898767,0.0002348266597489,0.00040668745216698,-8.097620887568899e-05,0.00090570287770074,-0.00010070033681122,-2.79074467402149e-05,0.00023292074001377,0.00038402774850927,0.00278110319853854,0.00032524015163023,0.00021884085040835,0.00068696195125449,0.00058118593392928,0.00022859523677863,0.00080099877800414,-3.1383240943406e-05,-0.00064531946299342,-0.0001981921114335,null,5.62351844750534e-05,0.00057916158750023,-0.00064536721894626,-0.00016205101881323,0.0010452018291635,0.00062537245858963,0.00104812602025443,0.00059556272492544,0.00041877730526325,-7.981569309839689e-05,0.00042091462681179,0.0005998580379350499,-0.00065401083106134,-3.0468453258086e-05,0.00386753608313926,-0.00060842435112845,-0.00041339891818849,-0.00067247068843764,-0.00067562309177407,0.00012283925434485,-0.00051014489308752,-0.00061526107377508,0.00028259836813388,-0.00015657234738383,0.00046565360747071,0.00023843097069455,0.00026135192169287,-0.00012736193937983,-4.43163540745652e-05,0.00033275677105327,0.00017130809512283,7.68630537842796e-05,0.00011333933832321,-0.00015098603758984,-0.00035279234002935,-0.00062448063754065,-0.00018102850458937,-0.0007240286571596,-0.0006350623434938899,-0.00032878900221861,-0.00071488894374505,-4.84930340808261e-05,-0.00025723463451384,-0.00026887725834269,-0.00042107779349093,-0.00048188137272771,0.00015403131687654,-0.00016420484790466,-0.00081371320839844,-0.0005768292506547899,-0.0002325613653304,-0.00046293958727246,-0.00028825601552277,-0.00011930103686451,-0.00044738386085331,-0.00068312700642919,-0.00042065046159527,-8.57972188071005e-05,0.00077592578746723,0.00022231677421977,-0.00048004531068329,-0.00092929385613232,-0.00043373254479105,-0.00048854170993297,-0.00073551360069082,-0.00022093965917945,0.00048164834675592,3.2308006127945e-05,0.00078623983759863,0.00058184143811232,-1.60867695880788e-05,0.00055024052171634,0.00044386596152484,0.0006071585693414,-0.00014628089132958,0.00029907261455138,-8.46253517258734e-05,-0.0009916987363568499,0.0007473947616398,0.00033231277924706,-0.00064457276548031,0.00021577785820889,-0.00058608044206767,0.00172877362135731,0.00095376021765749,0.00083469904432316,-2.42263708127504e-05,0.00055691366864301,0.00058948747738516,-0.00028631230843899,-0.00031863290059179,-0.00024758541563088,-0.00039729772866314,-0.00055700802364589,-0.0003714024809075,0.0002532273845361,-0.00035536573300795,-0.00069564158702191,-0.00068927658571084,2.95052466318262e-05,0.00049130771205698,-0.00028467165737812,0.0003935997527333,-0.00076331566504371,-0.000780791049554,-0.0003554071451621,0.00047493352179733,0.00072874929182707,0.00062834540512128,-0.00043013158224585,0.00121751073257165,-0.00011084451555179,5.04478495272474e-05,-0.00027466425760988,-1.24901066058897e-05,-0.00013134213704904,-0.00019991642911602,-0.0006520648863163299,-0.0007154571049618,-0.00072750772290653,0.00028130997324691,-5.72996932938861e-05,-0.00078730834363631,7.21802149601887e-05,7.28608537292197e-05,-0.0001859059922204,0.00078197326118939,0.00084767310031539,0.00010208889917995,-0.00087182862164692,0.00028230469763395,8.814510067823831e-05,0.00098243972032511,-1.17046048307455e-05,0.00047064418078727,-0.00013616109231748,-0.00135734324355964,-0.0009932223831203,0.0004259408534365,0.00147889133161646,-0.00071483388967575,0.00126812320056958,-2.33030570220321e-05,-0.00064848395504911,-0.0005691630064714,-0.00040776725711564,-0.00237205223284056,0.00018397538148623,-0.00149862518597552,2.29723399322086e-06,-0.00113990493933722,0.00035143781149135,-0.00071599002880416,-6.12453258019689e-05,0.00086333475926157,-8.608914971109809e-06,-0.00037372674877604,-0.00154901943394491,-0.00193183560623817,-0.00084539130629565,0.00044430029521726,-0.00073970623189107,0.00186113850954604,-0.00108575148751794,-0.00087593530145524,-0.00174720466261289,-0.00039338039404766,-0.00046241698319181,-0.00017156473662229,0.00092450467175318,0.00090920013236277,-0.00021420135856909,0.00047078690093563,-0.00030147825166632,-0.00120020029634658,0.00031768138792009,-0.00059270364057694,-0.0009769498647917501,-0.00051363768960125,0.00044944452611166,-0.00063631317282818,-0.0007705333236881,0.00287352234236172,0.00026152045550557,-0.00031342212116972,0.00026459102229517,0.0004182381057314,0.00055118209195408,0.00020361719190332,0.00213729835630038,-3.72872783719689e-05,-0.00025925360101341,0.00281322521296352,-0.00036299857124886,0.00193383572424129,0.0016620002477094,-0.00059522177902498,-2.46099914365675e-05,0.00013558004465418,-0.00034130913271314,-0.00029323401082631,0.00014351075473358,0.00154011669698868,-6.770291314247619e-05,0.00085847216574996,0.00020782686458572,2.92775914588358e-05,0.00357148039681778,-0.00024798874526292,-0.00022930492645416,0.00176958045004167,0.00047735876012823,-0.00064262043075117,-0.00167253371126984,-0.00019552587324968,0.00012370565271373,-0.00034066486439452,-0.00081629029746117,0.00015904679616442,2.50526621771743e-05,-0.00258991153643841,-7.93438064504622e-05,0.00188502208189821,-0.00091139860236617,0.00063745773307397,-0.0025966401572443,-0.00115029568772917,-0.00052418225245203,-0.00142219597594709,0.00025715608079447,-0.00043668425582225,-0.00117014782748108,-0.0010556741530838,0.00017817181093003,0.00045138777446236,0.00096515349334261,-0.00049988250037703,-0.00158386198711218,0.00023862557570742,0.00073206287338771,-0.00089501118362644,0.00122848110919494,0.00012913955547816,-0.00149468699178286,-0.00096620276259082,-0.00108923648362439,0.00030096210034162,-8.5185774322436e-05,-0.00189819620723564,-0.00127343149547135,0.00019267972478876,-0.00067198709704908,-0.00032751346666947,-0.0013532527232528,-0.00125095013557732,0.00109092452914957,0.00012503059336325,-0.0022652715533449,-0.00225961682535891,0.00036363721819921,-0.00131904134664193,-0.00264035173391496,0.00147362891962787,-0.00064570927391431,-0.00139484290419826,-0.00163390370375631,-0.0006344070634507699,0.00183021344618504,0.00070202969787274,-0.0007025131447061,0.00032440943576211,0.0026954407152591,0.00045383835623061,0.0008914618384574399,0.00056360334894961,-0.00113108192290975,0.00300369203504486,7.860178432454609e-05,-0.00135120783895037,-0.00038319763248288,-0.00050672120184879,0.00104353338438142,0.00067052757638401,0.00225761823722389,0.00249666004664385,0.00022593166301005,0.0011309281055726,-0.00051033194497186,-0.0023439833176701,-0.00140922164744083,0.00202910665756428,-0.00028097697700123,-0.00034935015080337,-0.0007048862213448,-0.00176910368521362,-0.00014640792464074,5.07606691324555e-05,0.00051106892025486,0.00107444916563289,0.0024981899710323,0.00310687887634324,3.91582979689202e-05,-0.00018059261338448,-0.00021862654720455,0.00147089497236218,0.00331007367956019,0.00067255106747632,0.00046602785813217,0.00174257594040332,-0.00059055374210399,0.00171319175095911,-0.00046387449044734,-0.00191133484707655,-0.00021947209604756,-0.0005283058790531,0.00030415663278038,-0.0006957632775371,-0.00111222566681085,-0.00092085911636148,-0.00044599960854153,-0.00022005981852222,-0.00135843225776203,-0.00103208272004386,-0.00020583362839658,-0.00037926844199055,-0.00130829558486739,0.00422761821884231,-4.59952436763031e-06,-0.00116668265666924,0.00152221315713333,0.00485715005799708,0.00365002027486169,0.00069364906884378,0.0025787827677613,0.00096689390776374,0.00044599096513648,0.00032406770559739,-0.0003224538808767,0.00050618332924815,-0.00139145681516012,-0.00076833227318388,-0.00125405373965361,-0.00162441683938046,9.65190458423897e-05,-0.00139504433514108,-0.00161669272167196,-7.163156759509369e-05,0.00080244640138338,0.0019220947578002,-0.00128330896423797,-0.00057888980008489,-0.00100509227043818,-0.00148386663026234,-0.00042434771745597,-0.00047468381974034,-0.00076541058014202,-0.00195888696063643,-0.00178810103111243,-0.00053202159323975,0.00042667798399092,0.00046882875322092,0.0009822937157944801,-0.00077377923086267,-0.00047281695869406,-0.00035924001182274,4.54732422883697e-05,0.00249356186814351,0.00144349573284487,0.0003166709838091,0.00013990202993174,-0.00081819425344034,0.00150399664007535,-8.933039782411041e-05,-0.00065849134276272,-0.00021179935144191,-0.00032590839619551,-0.00114664737780317,-0.00086454142852776,0.00158401662030669,-0.00054445835469303,0.00248750838481009,0.00154012988641388,0.00177970885013997,0.00015695874972461,-0.0007057135057506,0.00089631732048872,0.00040481126675813,0.00079136647933495,0.00312839150619422,0.0012602542255691,0.00050889558368755,0.00183428322960026,0.0007793715344161099,8.09044885068975e-05,0.00092781930158306,0.00035372638263608,0.00068389969795099,0.00138116067347244,-0.00085813350624775,0.00019361231038531,0.00082477591598563,-0.00135136682391866,0.00221456351032074,0.00081393310949773,0.00122424130475167,0.00140272129470244,0.00059364826066668,-0.00093601138438032,0.00202458985233907,0.00081272240346037,0.0007290607398178,-0.00079762302187727,-0.00164890963529627,-0.00040923567082133,-0.0005071045158621,-0.0015601363966363,-0.00130592548885638,-0.00040052732283689,-0.00092044387206745,-0.00222630427995036,-0.00038123733739842,-0.00071497862602153,0.00029886096350052,0.00146008293215084,-0.00043162466349459,-7.97008972806345e-05,0.00086087357504922,0.00038832034568192,-0.00069077951923701,-0.00066993990707939,0.00178428606435936,0.00086043218119136,0.00040193544888306,-0.00109242031679936,-0.00067697648914152,0.00018716216490079,-0.00191126768639876,-0.0006205663834307,0.00076028842247625,-0.00094807574057756,-0.00026180241104012,-0.00070036211679628,-0.00166622019403893,-0.00072320303931339,-0.0010669567682933,0.0004574937843115,-7.37871490375195e-05,-0.00141441116747006,-0.00083250834400859,-0.00144658211603338,-0.00101635676783892,0.00153155895147842,-0.00030423067400454,-0.0004797264066736,-0.00043812223355652,-0.00048538425858075,-0.00010008073150399,-0.00179633703612084,-0.00082841648268193,-0.0010172829067371,-0.00258572048162717,0.00029234587656863,-0.00129873238704028,-0.00042440914481924,-0.00027502048143506,0.00123862660539668,0.00118337796605645,-0.00025055644414434,-0.00035410858173471,0.00063357440054729,0.00174891265235883,0.00038568058968164,-0.00039642894702123,0.00070181140956544,0.00096349370598422,0.00139356200249649,-0.00250149339987839,-0.00028379739069897,0.00164527184113402,-6.77681726675309e-05,0.00078070494778187,-0.00153435263295041,0.00041709249257194,0.00236403256248221,0.0009400119462198,0.00043518910141294,-0.00117554280239861,-0.00119984846147717,-0.00169900586765466,-1.01485023065154e-05,0.00145356966408432,-0.0006877697087851299,-0.00138878874693004,-0.00093401544985843,-0.00053129502544976,-0.00117788029725454,-0.00040502650038218,-0.00098121286447268,0.00055179219116223,0.00139245284057542,-0.00015294111916467,-0.00100495684506165,-0.00097017655101992,-0.00054268893855767,-0.00023494064659615,0.00049075300218695,0.00092369859418995,0.00284421223395623,-0.00189086780625264,0.00106965484032354,0.00152925407792124,0.00031783498786346,-0.0003868472834494,-0.0014054131080856,9.70264694680911e-05,0.0006755118008491,-0.00064698751534234,-2.89527533385361e-05,-0.00123835886827396,-0.00010138671066871,0.00021839353455975,-0.00152810920252346,-0.00040723929126584,0.00021115688471845,-0.00057176073966543,-0.0013691270151725,0.00028579846401959,0.00190920172203255,-0.00177484392152159,0.0027330808460757,0.00206353498327316,0.00332982446833223,0.00283018579212991],[0.769670215477636,0.748346431489357,0.835135694163459,-0.484909225991529,0.810476746572354,-1.05961200005363,0.859535873275983,-0.222468945626952,0.78264411177551,-5.15162917140942,0.8652374183707781,0.0707962690221462,-5.29287214582807,-0.43971691453269,0.815867853747014,0.809805864996421,-0.34414266969982,0.748346431489357,-0.275695631049514,0.704237964882126,0.8091349025383719,-0.9618895848371219,0.8939484551265789,0.847402809509468,-0.972490213539339,-0.363340065607789,0.7903274392765,0.835135694163459,0.888567096128607,0.789777922368647,-0.323719497103116,0.756575835491325,-0.427116651385262,0.755874130774668,0.719344671883322,-2.92720208784632,0.8474921794740859,-2.87124810260141,-4.31759648887748,0.0769823072126565,0.82858886935037,-0.484909225991533,0.87144593832288,0.848141458160848,-1.375330169848,0.8474921794740859,0.853863379423356,0.883141553239431,-0.314896257568557,0.80356521018814,0.8652374183707781,-0.450470091600046,0.882512583551996,-1.1663884327127,0.829248848948627,0.763156184182528,0.815867853747014,-0.560542504515453,-1.17978986556881,-1.03333073786053,-0.394317266385695,0.834479104911074,-0.314896257568558,0.704454243716132,-2.08930577602177,0.763156184182528,-1.19339257394426,-1.38944350300528,-0.700792616564469,0.727936952103059,0.816535248924361,0.797383361517469,-0.782548867962173,-1.10790990553798,-0.484909225991532,0.705184140788002,0.870810167551492,-1.31869557709462,-0.450470091600044,0.763156184182528,-0.474566943928114,-0.43971691453269,0.810476746572354,0.835135694163459,0.796704851150374,-5.87057119946243,0.809688902128752,-2.5671744847217,-1.04485018059585,-2.43540299478618,-0.559314425211741,-3.54556088654329,-0.416678034549981,-3.22245535565651,-2.43504820136439,0.783191750048905,0.791142174711344,-0.324107876594943,0.74161489687282,0.769820722899629,-0.363340065607789,0.719867350358868,0.815867853747014,0.840970091022315,0.689732504339428,0.727936952103059,-0.972490213539339,-0.240383804699016,-1.86527283002081,-1.00788378924304,-0.953765334040532,0.802890252775044,-1.65809932346104,-1.04183840317481,0.82858886935037,0.8939484551265789,0.829248848948627,0.776264107839269,-1.64618552196666,0.943064224606718,-1.50211001295919,0.87144593832288,-2.48433497836041,-3.99164875151658,1.16560338105398,-0.520748661337382,-0.5909297303265461,-0.363340065607788,0.888567096128607,0.770364426291416,0.705184140788002,-0.275695631049512,0.809805864996421,-0.0451992975894663,0.734812273873838,0.835135694163459,0.172463681991544,-0.394317266385697,-3.36539826604673,-0.33301430206815,-0.449702527740522,0.89996258705064,0.815867853747014,0.822538022271778,0.82858886935037,0.624119650366854,-0.31453050671974,0.783330458331076,-0.0574318031815697,-1.46072534308273,-3.6601744913889,0.713029504897951,0.756575835491325,-5.02377173805267,0.69776094303358,0.78401674512215,0.762458217142618,0.87144593832288,0.82858886935037,-0.333014302068151,-0.485810590471733,-0.53260163924037,0.705914067646935,-0.384853872807744,0.632971979280087,-0.615755594673462,0.720788867553028,0.755710748917137,0.840970091022315,-0.374930483846437,-1.17978986556881,-2.45807950899782,0.888567096128607,0.763854110300402,-0.497213859147532,0.822538022271778,0.749222251493062,-0.305788920150607,0.882512583551996,-1.03333073786053,-2.25509296902536,0.795898303328278,-0.295186339650299,0.8468427951618011,-0.384853872807744,0.74161489687282,0.80901746761952,-0.294867171823776,-1.72550813934708,0.789777922368647,0.75517239330511,-0.344583942976913,-0.416678034549979,0.905251814743398,0.763156184182528,-0.450470091600046,0.87144593832288,0.809688902128752,0.905251814743398,0.259900533685965,0.283386883226161,0.259900533685965,0.271588111539293,0.259536335702631,0.259900533685965,0.259900533685965,0.271588111539293,0.259900533685965,-1.43977231467661,0.259900533685965,0.259900533685965,-2.47082093024189,0.271588111539293,0.259900533685965,0.271588111539293,0.259900533685965,0.259900533685965,-1.50708217794222,0.283386883226161,-1.50708217794221,-1.50708217794222,0.270235409030693,0.259900533685965,0.283037555663606,0.283386883226161,0.304661462095417,0.259900533685965,0.270235409030693,0.283037555663606,0.270235409030693,0.271944591966582,-1.47314517413268,0.259900533685965,0.259900533685965,0.259900533685965,-1.40877815008595,0.259900533685965,0.259900533685965,0.436416264651451,0.259900533685965,0.293303033172183,-1.50708217794221,0.271588111539293,0.281712054169221,0.272300847716881,0.259900533685965,-1.50708217794221,0.283037555663606,0.259900533685965,0.247219353883035,-1.41226472434637,0.270235409030693,0.282062199039609,0.259900533685965,0.259900533685965,0.283037555663606,0.271944591966582,0.271588111539293,-2.33204874322158,0.271944591966582,0.284708479478074,0.259900533685965,0.271588111539293,-1.47314517413268,-4.08809514486333,0.283386883226161,-1.44072718824918,-1.50812807447477,0.270235409030693,0.259900533685965,-1.47314517413268,0.259900533685965,0.258154281925702,0.271588111539293,0.259900533685965,0.271588111539293,0.271588111539293,0.259900533685965,-1.50708217794221,0.259900533685965,-1.54379108681412,0.259900533685965,0.259900533685965,0.259900533685965,0.282062199039609,-2.99097054501234,-1.50708217794222,-1.44072718824918,0.259900533685965,-1.44072718824918,0.259900533685965,-2.42241173310994,0.283386883226161,0.259900533685965,0.259900533685965,0.259900533685965,0.259900533685965,0.249371107499743,0.271588111539293,-1.50708217794222,0.271588111539293,0.259900533685965,0.283386883226161,0.259900533685965,0.259900533685965,0.259900533685965,0.259900533685965,-1.50708217794221,0.259900533685965,-1.50708217794222,-1.47314517413268,0.270235409030693,-1.50708217794222,-1.44072718824918,0.259900533685965,-1.47314517413268,0.247963690760812,0.259536335702631,0.259900533685965,-2.27433019033081,0.270235409030693,0.271588111539293,0.271588111539293,0.259900533685965,-2.29815519539349,0.294943997314763,0.47766058410485,-1.40877815008595,0.271944591966582,0.259900533685965,0.259900533685965,0.259900533685965,0.271588111539293,0.452561715767499,0.259900533685965,-1.47314517413268,0.444780474473419,0.259900533685965,0.282062199039609,-1.50708217794221,0.259900533685965,0.273293212051193,0.272937581606526,0.271588111539293,-1.40786345432053,-1.47694966408386,0.30432498707061,0.259900533685965,0.46121752537371,-1.47694966408385,-1.50708217794222,0.271588111539293,0.282062199039609,-2.37665503957364,0.283386883226161,0.259900533685965,0.271944591966582,0.293303033172183,0.259900533685965,-1.50812807447477,0.283037555663606,-1.47314517413268,0.259900533685965,0.259900533685965,0.259900533685965,0.292959768705028,0.259900533685965,-1.47214759848643,0.259900533685965,0.259900533685965,0.259900533685965,-1.47314517413268,0.270235409030693,0.271588111539293,0.271588111539293,-1.50708217794221,0.259900533685965,0.292959768705028,-1.50708217794221,0.259900533685965,-1.44436784000824,0.258154281925702,0.271588111539293,0.271944591966582,0.271588111539293,0.283037555663606,0.259900533685965,0.324843305174181,0.283386883226161,0.271588111539293,-1.44072718824918,0.271944591966582,0.292959768705028,0.283386883226161,0.283386883226161,0.259900533685965,0.259900533685965,0.271944591966582,0.259900533685965,1.41436847660605,-0.874058289084316,-0.8089361665411851,-0.775702727650303,0.386619160046715,0.00236136401950911,1.40483167007934,0.00236136401950904,0.5630756490247411,-4.35388844778082,0.477688191644305,0.497286696709229,-3.83724060596022,-0.899074668609647,0.468972607635062,0.41813680483909,-1.13311683405174,1.36966805434848,0.565373148622294,-0.0729760742113455,1.47729590734999,-0.448152953898094,0.415418998633214,0.495031117700815,1.44744266696427,0.401925085283192,-0.195187877237989,-0.937384891955826,-0.182364670403062,0.0485018686489075,1.45808986834441,0.452325155335113,0.536781665891516,1.35762519615721,-0.0280114274828121,-4.31187109371789,0.5096023338411581,-3.81577383011739,-4.86163127472055,0.938805886262876,-0.453569942654445,1.48751830056726,0.578806554397464,1.36761355075818,-0.0922399506842,-0.128876461598255,0.522674078124705,1.29366779711134,0.578806554397463,-0.197666836815937,1.39515856573248,1.24063254054343,1.4255076460372,-0.400815543753292,0.222114857523198,-0.417637480139839,-2.07653870043022,1.35553674767982,-0.164321842632201,-0.689949835632559,0.536781665891517,-0.937384891955826,-0.716738555761293,-0.182364670403061,-0.0783922493591159,-1.04278314118589,-0.50575432825089,-0.0783922493591189,-0.892036768655624,0.553002160171908,1.29821032082331,0.369787112270764,0.466406150216507,0.389429383955475,0.536781665891517,-0.161363854262573,-0.343269190470815,-0.111800458882897,0.43938024919793,1.44559668043805,0.565373148622295,1.41243705082556,-0.392681182973657,-0.131735402254874,0.495031117700816,0.539149974847314,-4.49090895517145,0.492541425500481,-3.97268490605435,-4.37625203794715,-3.89829512039859,-1.561403052567,-3.79196399305189,-0.0410153537365612,-4.10512638345435,-3.27224701355646,0.364037161777427,1.47552367809905,1.43648025157418,1.41612145390717,-0.469330794470025,1.45808986834441,-0.128876461598255,1.43648025157418,1.44559668043805,1.44559668043805,-0.0921059870403535,-0.0948592576623032,0.5250792479087319,1.34514812563487,1.42360514310096,0.5630756490247441,1.36966805434848,-2.94707754830789,1.44374611695734,0.466406150216507,0.603812792967378,1.42360514310096,-0.489204578246965,-1.65603812469541,0.459936545855939,0.539149974847314,0.5630756490247421,-0.934238859842258,-4.82814125466483,1.80009686132774,-0.07568087832191669,-1.14548986304282,-0.382575769030536,1.36966805434848,1.32219290895117,1.39137680917778,1.44913395978583,1.36966805434848,-2.08102979954817,-1.08295495119857,1.34749777054911,1.72684351042739,1.34325671835447,-4.84297058488292,-0.6421737044640941,-3.4148778276806,-0.434391535950187,1.35970778117155,-0.544415188564315,-0.48639907157359,-2.93410055955192,1.26866974738633,0.451080325448045,0.121663430532535,-2.79421890447609,-4.91720452917387,0.536781665891516,-1.92651387662492,-4.91530224961773,-0.318516033950193,1.50596134834855,1.42740530385883,0.52026269815624,0.536781665891516,1.42740530385883,1.23330548813968,1.36363977744651,1.41050060339524,-0.526379873894336,1.44559668043805,-0.669161713196184,-0.453569942654445,1.4255076460372,1.47552367809905,1.41243705082556,-2.88357925557973,-3.43881544937694,1.39137680917778,1.47552367809905,0.00236136401950842,-0.0568507732419245,-0.0729760742113461,1.41243705082556,-1.33356395058707,-4.30949196918697,-4.86364753324324,1.33268250706071,1.48751830056726,-0.108993509559932,1.4255076460372,1.39137680917778,-0.0254333591924365,0.495031117700814,1.47729590734999,1.18817488219973,-0.0756808783219161,0.578806554397464,0.512046160647588,0.578806554397464,1.47552367809905,1.39137680917778,1.45627025047427,-0.111800458882898,0.5096023338411571,-1.57492465026398,0.250716918541055,0.240855177333347,-1.53517002691513,0.250083187592482,0.240855177333347,0.240855177333347,0.240855177333347,0.240855177333347,0.250083187592482,-1.5332958783851,0.240855177333347,0.240855177333347,0.262845372729413,0.240855177333347,0.240855177333347,0.253827723910362,0.240855177333347,0.240855177333347,0.221257985003517,0.250716918541055,0.263464837561449,0.240855177333347,0.240855177333347,0.253827723910362,-1.49570524916022,0.259160841699005,0.240855177333347,0.240855177333347,0.240855177333347,0.240855177333347,0.240855177333347,-1.60474766306723,0.250083187592482,0.253827723910362,0.240855177333347,0.253827723910362,0.250716918541055,0.250716918541055,0.41479870466536,0.230815192608385,0.230815192608385,0.240855177333347,0.240855177333347,0.250083187592482,-2.4435738988032,0.240855177333347,0.240855177333347,0.263464837561449,0.240855177333347,0.240855177333347,0.259784376905485,-1.497485580718,0.240855177333347,-2.49659458433183,0.240855177333347,-1.63587981970664,-1.56335187579121,0.272329772455566,0.250716918541055,0.250716918541055,-1.5332958783851,0.250716918541055,0.240855177333347,0.253827723910362,-4.61144732874382,0.240855177333347,0.240855177333347,-1.50625180971694,0.240855177333347,-2.5118011890814,0.240855177333347,-1.56530525570135,0.240855177333347,0.262845372729413,0.259784376905485,0.227572884848789,0.250083187592482,0.240855177333347,0.240855177333347,0.250716918541055,-1.53517002691513,-1.56335187579122,-1.53517002691513,-1.51512153817165,0.240855177333347,-1.56335187579121,0.253827723910362,0.240855177333347,0.250716918541055,-1.56335187579121,0.240855177333347,0.250716918541055,-1.5332958783851,-1.53517002691513,0.227572884848789,0.250716918541055,0.250716918541055,0.250716918541055,0.240855177333347,0.240855177333347,0.240855177333347,0.262845372729413,-3.43844858310002,0.240855177333347,0.240855177333347,0.250716918541055,0.250083187592482,0.240855177333347,0.253827723910362,0.227572884848789,0.240855177333347,0.253827723910362,0.240855177333347,0.244661909185106,0.240855177333347,0.250083187592482,0.240855177333347,0.230815192608385,-1.47000980078962,-2.24843620170693,0.240855177333347,-1.56335187579121,-1.50445047303649,-1.56335187579121,0.398012951074433,0.250083187592482,-1.11020803133699,-1.48019097409133,0.240855177333347,0.250716918541055,0.250716918541055,0.240855177333347,-2.45541943142094,0.41479870466536,0.21728682583991,0.237671069427648,-1.12690985670666,0.240855177333347,0.227572884848789,0.240855177333347,0.250083187592482,0.253827723910362,0.259784376905485,-1.56335187579121,0.268707106306475,0.275929349724952,0.250716918541055,0.240855177333347,0.428833511294831,0.227572884848789,0.21728682583991,0.240855177333347,-1.56335187579121,-2.45541943142094,0.286732371805136,0.250716918541055,0.271720011116813,0.240855177333347,0.240855177333347,0.240855177333347,0.250716918541055,0.250083187592482,-1.5332958783851,0.240855177333347,0.240855177333347,-1.53517002691513,0.240855177333347,-1.56335187579121,0.253827723910362,0.250083187592482,0.230815192608385,-2.39367841816385,0.262845372729413,0.250716918541055,0.227572884848789,-1.59269481648801,0.240855177333347,0.240855177333347,0.240855177333347,-1.56335187579121,0.240855177333347,0.250083187592482,0.240855177333347,0.262845372729413,0.253827723910362,0.240855177333347,0.227572884848789,0.240855177333347,0.240855177333347,0.240855177333347,0.237671069427648,-1.5332958783851,0.293142092005515,0.240855177333347,0.268707106306475,0.250716918541055,0.240855177333347,0.250716918541055,0.259784376905485,-0.151291339039698,-1.18509660449101,0.333070758274398,1.47377918636274,1.42855912482978,-0.571165534260348,0.556836974949068,0.556836974949069,0.503906975434145,-2.01978233618931,1.36243565032788,-0.867080986429832,-2.09866604742792,-1.58259919741842,-0.0778077458512421,-0.732499616297699,-0.07780774585124239,0.431081900856066,-0.151291339039698,0.613476750413961,0.556836974949068,0.535127049472068,1.42888687544222,0.431081900856065,0.613476750413961,-0.230749527225627,-1.43357112477661,-0.447385271794207,-1.36344841244088,0.021369164819674,0.556836974949068,-0.044833988001779,-1.99409812676422,-0.115960366515563,0.49623168203303,-2.668500899143,-2.04969295769679,-1.07760447742563,-2.98533359580719,1.84661501380463,0.556836974949068,0.613476750413961,0.512803763163353,1.51636135548805,-0.0448339880017794,-0.323879168083729,0.0542125108168412,0.137837982381871,1.3627297857874,-1.2570300482659,-0.155910952625004,1.42855912482978,-0.458050285147959,-1.14922913225927,-1.09441261762577,1.51636135548805,0.472847659762356,-0.855566225571103,-1.20253114681081,0.555765500083739,0.253824389247593,-0.904156721604037,-1.03124386220208,0.405754640871919,-1.65830324671465,-1.63616181965916,-0.617444071352746,-2.17289954638137,-1.00924823148665,0.395686036096991,-1.63645489173414,-0.458050285147959,-1.93823710292991,-0.981599505412189,-0.589143477861686,0.16327937545387,-1.73353205974351,0.613476750413961,1.32869021381234,0.556836974949068,0.585137370590012,1.47377918636274,0.526471083351976,0.556836974949068,1.38034222791035,1.49555528783652,-1.92994524803858,0.0213691648196729,-1.78536218825289,-1.47136096587308,-1.62836498201095,-1.24583261939112,-1.61126502399697,1.47377918636274,-1.76729840496232,-2.55582635895864,-1.03302875329319,0.293114929025303,0.367218927779661,1.45168800903991,0.0305458155532462,-0.350265217084868,0.496231682033028,-0.418779811247703,0.556836974949069,1.51636135548805,-1.05187707872049,0.05421251081684,0.0542125108168415,-0.571165534260348,0.39568603609699,0.463537015864653,-0.567376518655013,-2.21940487562612,0.535127049472067,1.48033591537678,0.556836974949067,1.51636135548805,-0.376767460379026,-1.53374428602869,0.425966199929201,-0.07780774585123949,0.613476750413961,-0.366973404511109,-2.46919846153157,-0.48466775729165,-0.192737345373037,1.8060448157638,-0.009521409977211411,0.0213691648196729,0.0213691648196731,1.51636135548805,-0.0778077458512422,-0.151291339039698,-2.0814338912432,1.35512135474206,1.47377918636274,-1.0767141028419,1.47377918636274,-0.9411164219250699,0.496231682033029,-0.418779811247703,-0.994549798460942,1.30941360847972,-0.786548924301552,-0.230749527225628,-1.03885778164048,-1.05364112333059,1.45809207635611,-1.56577978346149,-1.61503630731332,1.30153654317887,-0.07780774585124151,-0.904562912526542,-3.15116130021395,1.51636135548805,0.613476750413961,0.05421251081684,0.613476750413961,0.322677187681614,0.0542125108168401,0.613476750413961,1.42855912482978,1.45168800903991,1.51636135548805,-0.0612807916704315,-1.32103690024888,0.0213691648196737,-0.00952140997721293,1.49555528783652,1.51636135548805,-1.05021639942367,-0.707133962510725,0.613476750413961,-0.707133962510725,-0.729852506731452,0.463537015864652,-0.656477723444849,-0.0778077458512404,-0.376767460379026,0.496231682033029,0.0542125108168414,-0.743579901906364,-0.781330400734878,1.51636135548805,1.47377918636274,-0.376767460379026,1.48033591537678,-0.115960366515564,0.556836974949068,0.431081900856066,-0.192737345373037,1.38771122568129,1.45843123287679,0.613476750413961,0.556836974949065,-0.523222821635315,-0.492193001473317,-0.009521409977211371,0.369189054479503,0.220774178984751,0.220774178984751,0.231023204307164,0.220774178984751,0.220774178984751,0.211511865237484,0.220774178984751,0.199384926336953,0.210331115793532,0.220774178984751,0.210331115793532,0.229883454171144,-2.58268268187635,-1.62509870208897,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,-1.59236325562695,0.199685740931483,0.220774178984751,0.220774178984751,-1.62418316159159,-1.62509870208897,-1.65556180787647,0.211511865237484,0.199083947118427,0.220774178984751,0.24136491251708,0.229883454171144,0.210331115793532,0.222221573854533,0.220774178984751,0.220774178984751,0.220774178984751,-1.62509870208898,0.220774178984751,0.229883454171144,-1.62509870208897,0.377661305115874,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,0.241643738010258,0.229883454171144,0.220774178984751,0.231307262563723,0.231023204307164,-1.56106314337286,0.220774178984751,0.221063539889761,0.229883454171144,0.220774178984751,0.220774178984751,0.231023204307164,0.202090736388237,0.220774178984751,-1.62509870208898,0.220774178984751,0.220774178984751,0.220774178984751,0.231307262563723,0.220774178984751,0.220774178984751,-2.61263347518494,0.220774178984751,-1.59589144304681,0.210331115793532,0.220774178984751,0.24136491251708,0.220774178984751,0.220774178984751,0.231307262563723,0.231023204307164,0.231307262563723,0.220774178984751,0.220774178984751,0.239966671734216,-1.65556180787646,-1.66037445977851,-1.62509870208897,-1.65556180787646,0.210036071703487,0.220774178984751,0.220774178984751,0.220774178984751,0.210331115793532,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,0.211511865237484,0.210331115793532,-2.57775027697779,-1.65556180787647,0.220774178984751,0.220774178984751,0.220774178984751,-1.62509870208898,-1.6287903764623,0.220774178984751,0.231307262563723,-1.62509870208897,0.220774178984751,0.229883454171144,0.220774178984751,-1.62509870208897,0.220774178984751,0.220774178984751,-1.56444223440769,0.220774178984751,0.231307262563723,0.199685740931483,-1.62509870208896,-1.62509870208897,-4.88972058205916,-1.59589144304681,0.220774178984751,0.231023204307164,-1.62509870208897,0.385884207124413,0.24136491251708,0.400936070471081,0.220774178984751,0.211511865237484,0.220774178984751,0.231307262563723,0.229883454171144,-1.62509870208897,-1.96669052655938,0.220774178984751,0.220774178984751,0.377661305115874,-1.59236325562695,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,0.211511865237484,-1.6594132752183,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,0.394207578516108,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,0.229883454171144,0.220774178984751,-1.62509870208897,0.210036071703487,-1.59148803146202,0.220774178984751,0.211511865237484,0.239966671734216,0.229883454171144,0.220774178984751,0.220774178984751,0.220774178984751,0.210036071703487,-1.62051445919767,-1.6287903764623,0.220774178984751,0.199384926336953,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,0.210331115793532,0.220774178984751,-1.59148803146202,0.211511865237484,0.210331115793532,-1.65941327521831,0.229883454171144,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,-1.62509870208897,-1.59148803146201,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,-1.59589144304681,0.220774178984751,0.229883454171144,0.220774178984751,0.220774178984751,0.220774178984751,0.220774178984751,0.414148335120503,0.414148335120503,0.414148335120503,0.414148335120503,0.414148335120503,0.414148335120503,0.414148335120503,-1.92411110542286,0.414148335120503,-1.15156219281913,-1.11789983948364,0.0301371959274156,-1.92411110542287,0.414148335120503,-1.15156219281913,-1.15156219281913,0.414148335120503,0.414148335120503,0.400411668873347,0.414148335120503,0.414148335120503,0.414148335120503,0.414148335120503,-1.96672462236895,0.414148335120503,0.414148335120503,0.405579110250561,0.414148335120503,0.414148335120503,0.414148335120503,-1.18194173309944,0.414148335120503,-1.11789983948364,0.414148335120503,-1.11789983948364,0.414148335120503,0.414148335120503,-1.14923686903622,-2.43929200464027,-1.9435431275871,0.414148335120503,0.414148335120503,-1.11789983948364,0.384222335087343,-1.11789983948364,-1.11789983948364,0.414148335120503,-1.11789983948364,0.414148335120503,0.414148335120503,-1.11789983948364,-1.11789983948364,-1.11789983948364,0.414148335120503,0.414148335120503,0.414148335120503,0.414148335120503,0.414148335120503,0.414148335120503,-1.92411110542286,-1.15156219281913,0.414148335120503,-1.14923686903622,-1.92411110542286,0.414148335120503,-2.47872524608004,0.414148335120503,-1.11789983948364,0.414148335120503,-1.92411110542286,-1.11789983948364,0.399376226548253,0.414148335120503,0.414148335120503,0.414148335120503,-1.11789983948364,0.414148335120503,0.414148335120503,0.414148335120503,0.399376226548253,0.414148335120503,0.414148335120503,-1.11789983948364,0.399376226548253,0.405579110250561,0.414148335120503,-3.41513158317434,-1.11789983948364,0.414148335120503,0.414148335120503,0.414148335120503,0.34268485009721,-1.13755334923439,-1.11789983948364,0.414148335120503,0.414148335120503,0.414148335120503,0.414148335120503,-1.11789983948364,0.414148335120503,-1.16974824653208,0.414148335120503,0.414148335120503,-1.15156219281913,0.414148335120503,0.414148335120503,-1.15156219281913,0.414148335120503,0.400411668873347,0.414148335120503,0.414148335120503,-1.11789983948364,0.414148335120503,0.414148335120503,0.414148335120503,-1.11789983948364,-1.11789983948364,0.414148335120503,-1.15156219281913,0.414148335120503,0.601535340221708,0.414148335120503,0.405579110250561,0.384222335087343,0.414148335120503,-0.755417933911909,-1.11789983948364,-1.49091797256593,-1.11789983948364,0.414148335120503,-1.11789983948364,-1.11789983948364,0.414148335120503,0.414148335120503,0.601535340221708,0.414148335120503,0.414148335120503,0.601535340221708,0.414148335120503,0.414148335120503,0.405579110250561,0.414148335120503,-1.15156219281913,0.414148335120503,0.414148335120503,0.414148335120503,-1.13755334923438,0.414148335120503,-1.11789983948364,0.601535340221708,0.414148335120503,0.399376226548253,0.399376226548253,-1.92411110542286,-2.48385973742801,-1.11789983948364,0.399376226548253,-1.11789983948364,-1.11789983948364,0.414148335120503,-1.92411110542287,0.414148335120503,0.414148335120503,-2.00857978326911,-1.92411110542286,0.414148335120503,0.414148335120503,-1.11789983948364,0.405579110250561,0.414148335120503,-1.11789983948364,0.399376226548253,0.390586550687265,-1.92411110542286,0.399376226548253,0.414148335120503,0.405579110250561,0.414148335120503,0.399376226548253,0.414148335120503,-1.92411110542287,-1.11789983948364,0.0585995787940023,0.414148335120503,0.414148335120503,0.414148335120503,-1.92411110542286,-1.11789983948364,0.399376226548253,0.414148335120503,0.405579110250561,0.414148335120503,-1.11789983948364,0.414148335120503,0.414148335120503,0.414148335120503,0.414148335120503,0.414148335120503,0.399376226548253,0.100863000301938,0.112825481100808,0.100863000301938,0.106878219954305,0.112825481100808,0.106878219954305,0.106878219954305,0.106878219954305,0.107174967633301,0.106878219954305,-2.0386906001304,0.112825481100808,0.112825481100808,0.106878219954305,0.106878219954305,0.113118904459918,0.100562672069365,0.112825481100808,0.0880065754096125,0.094778070278322,0.100863000301938,0.106878219954305,-2.01057235876591,0.112825481100808,-2.03729134202014,0.106878219954305,-2.06804408100073,0.106878219954305,0.100863000301938,0.112825481100808,0.106878219954305,0.100562672069365,0.112825481100808,0.112825481100808,0.112825481100808,0.102021428359966,0.112825481100808,0.100863000301938,0.100863000301938,0.19538504121394,0.105730448041865,0.111690592011446,0.112825481100808,0.112825481100808,0.123412734837018,0.112825481100808,0.100863000301938,0.11758414392931,0.102021428359966,0.112825481100808,0.112825481100808,0.112825481100808,0.106878219954305,0.094778070278322,0.100562672069365,0.118996648071228,0.112825481100808,0.106878219954305,0.112825481100808,0.106878219954305,0.112825481100808,0.106581305310184,0.118706467345194,0.111690592011446,0.112825481100808,-4.57318080768205,-2.06804408100074,0.105730448041865,-2.06804408100074,0.112825481100808,0.106878219954305,0.100863000301938,0.112825481100808,0.106878219954305,0.106878219954305,0.106878219954305,0.112825481100808,0.106878219954305,0.106878219954305,0.112825481100808,0.106878219954305,0.112825481100808,0.0944742398415746,0.105730448041865,0.100863000301938,0.106878219954305,-3.16045168828705,0.112825481100808,0.112825481100808,0.106878219954305,0.100863000301938,0.100562672069365,0.100863000301938,0.112825481100808,0.112825481100808,-2.06804408100074,0.113118904459918,0.111690592011446,0.106581305310184,0.106878219954305,0.094778070278322,0.106878219954305,0.106878219954305,0.106878219954305,0.106878219954305,0.11758414392931,0.106878219954305,0.106878219954305,0.112825481100808,0.106878219954305,0.106878219954305,0.112825481100808,0.112825481100808,0.094778070278322,0.112825481100808,0.112825481100808,0.111690592011446,0.112825481100808,0.100863000301938,0.113118904459918,-5.48769231877291,0.102021428359966,0.107174967633301,0.107174967633301,0.112825481100808,0.20093071575826,0.106878219954305,0.20093071575826,0.106878219954305,0.112825481100808,0.11758414392931,0.106878219954305,0.100863000301938,0.100863000301938,0.196607518353043,0.106878219954305,0.100863000301938,0.185404779531442,0.106878219954305,0.112825481100808,0.106878219954305,0.112825481100808,0.105730448041865,-2.06804408100072,0.105730448041865,-2.06804408100074,-2.09875295529222,0.106878219954305,-2.06804408100074,0.195636680623863,0.112825481100808,0.100863000301938,0.106878219954305,-2.06804408100073,0.112825481100808,0.112825481100808,0.100863000301938,0.100863000301938,0.113118904459918,0.112825481100808,0.106878219954305,0.100863000301938,0.100863000301938,0.094778070278322,0.112825481100808,-2.06804408100074,0.0944742398415746,0.106878219954305,0.11758414392931,-2.03729134202014,0.106878219954305,0.112825481100808,0.106878219954305,0.106878219954305,0.112825481100808,0.094778070278322,0.112825481100808,0.09594998769111709,0.100863000301938,0.112825481100808,0.112825481100808,0.0886216059065633,0.112825481100808,0.102021428359966,0.112825481100808,0.112825481100808,0.112825481100808,0.112825481100808,-2.03729134202013,0.112825481100808,0.112825481100808,0.105730448041865,0.112825481100808,0.112825481100808,0.100863000301938,0.100863000301938,-2.0386906001304,0.106878219954305,0.106878219954305,0.100863000301938],[0.68344955040432,0.678818288173099,0.697439749012314,0.381093555040478,0.692211086800873,0.257383608972891,0.702563675850514,0.444611019986557,0.686249700588051,0.00575663349287449,0.703753738424038,0.517691678483666,0.0050021530386054,0.391808424920448,0.693358496830298,0.692068133766511,0.414803525353359,0.678818288173099,0.431509363818109,0.669126712881382,0.691925126645769,0.276500028061104,0.7097043225335929,0.700022038853064,0.274384428183367,0.410151271382826,0.687901633820449,0.697439749012314,0.708594383023022,0.687783644220074,0.419769546320818,0.6806098493994041,0.39481505836451,0.680457293442461,0.672462693028727,0.0508251321289682,0.700040805422262,0.0535933116269469,0.0131564875827999,0.519236077883057,0.69605647102018,0.381093555040477,0.705046478587945,0.700177125637402,0.201760042203613,0.700040805422262,0.701376945978365,0.7074728048618339,0.421920070055383,0.690736597169336,0.703753738424038,0.389249003375787,0.707342619433865,0.237508412322724,0.696196079462202,0.682038582968748,0.693358496830298,0.363421943936114,0.235089981069708,0.262438880904017,0.402678440157688,0.697301179201096,0.421920070055382,0.669174594432013,0.110140596595628,0.682038582968748,0.232652725784159,0.199496613285206,0.331636517968708,0.674352387757982,0.693500375067744,0.689414478729296,0.31377080682108,0.248260752081285,0.381093555040477,0.669336159056926,0.7049142490451999,0.211035397320836,0.389249003375788,0.682038582968748,0.383535880405796,0.391808424920448,0.692211086800873,0.697439749012314,0.6892691759584439,0.00281332424768131,0.692043207269492,0.07128112712478619,0.260215233018747,0.0805125742485119,0.363706103330403,0.0280433170448867,0.397311939919987,0.0383293778468321,0.08053884362126761,0.686367601130682,0.688076525011827,0.419674954366454,0.677348888737429,0.683482111223458,0.410151271382826,0.672577806043643,0.693358496830298,0.698669489039617,0.665907418368659,0.674352387757982,0.274384428183368,0.440191770266902,0.134089643630453,0.267394200783026,0.278128213710195,0.6905923945111,0.160017305179879,0.260795428573424,0.69605647102018,0.7097043225335929,0.696196079462202,0.68487438566211,0.161625153844183,0.719718200752252,0.182111033711843,0.705046478587945,0.07696367879601031,0.01813431091791,0.762349386965434,0.37267718829761,0.356421559442295,0.410151271382826,0.708594383023022,0.6835997211885581,0.669336159056926,0.431509363818109,0.692068133766511,0.48870209898267,0.675860403335836,0.697439749012314,0.543009368417536,0.402678440157687,0.0333945312344849,0.417507378293297,0.389431495073113,0.710941814191734,0.693358496830298,0.69477482604166,0.69605647102018,0.65115491793789,0.422009280523173,0.68639745969081,0.485645994439765,0.188356410948564,0.0250826948653888,0.671070223743604,0.6806098493994041,0.0065366541019379,0.667691157606044,0.686545168130429,0.681887201244476,0.705046478587945,0.69605647102018,0.417507378293297,0.380880980878525,0.369910301666041,0.669497690339657,0.404956737443506,0.653163043711417,0.350747389573466,0.672780707421773,0.680421767433921,0.698669489039617,0.407350182250347,0.235089981069708,0.07884971433881439,0.708594383023022,0.682189917357536,0.378195645288238,0.69477482604166,0.679009208058271,0.424142947074808,0.707342619433865,0.262438880904017,0.0949110603035323,0.689096405456299,0.426734642021718,0.69990442760598,0.404956737443506,0.677348888737429,0.691900093096953,0.426812722570411,0.151163040181138,0.687783644220074,0.6803046917215601,0.41469641401796,0.397311939919987,0.712027555351211,0.682038582968748,0.389249003375787,0.705046478587945,0.692043207269492,0.712027555351211,0.564611840622702,0.570376367155093,0.564611840622702,0.567482742413538,0.564522309434716,0.564611840622702,0.564611840622702,0.567482742413538,0.564611840622702,0.191580609414215,0.564611840622702,0.564611840622702,0.0779292257083416,0.567482742413538,0.564611840622702,0.567482742413538,0.564611840622702,0.564611840622702,0.181371616860513,0.570376367155093,0.181371616860513,0.181371616860513,0.567150696626243,0.564611840622702,0.570290763321904,0.570376367155093,0.575581652610185,0.564611840622702,0.567150696626243,0.570290763321904,0.567150696626243,0.567570237031104,0.186465033624837,0.564611840622702,0.564611840622702,0.564611840622702,0.196426845703614,0.564611840622702,0.564611840622702,0.60740476783567,0.564611840622702,0.5728045771522829,0.181371616860513,0.567482742413538,0.569965906755214,0.5676576722929521,0.564611840622702,0.181371616860513,0.570290763321904,0.564611840622702,0.561491971196529,0.195877095606315,0.567150696626243,0.570051726830485,0.564611840622702,0.564611840622702,0.570290763321904,0.567570237031104,0.567482742413538,0.0885032508741487,0.567570237031104,0.570700190406815,0.564611840622702,0.567482742413538,0.186465033624836,0.0164945177346529,0.570376367155093,0.191432764553814,0.181216378123444,0.567150696626243,0.564611840622702,0.186465033624837,0.564611840622702,0.56418251941172,0.567482742413538,0.564611840622702,0.567482742413538,0.567482742413538,0.564611840622702,0.181371616860513,0.564611840622702,0.175984837972673,0.564611840622702,0.564611840622702,0.564611840622702,0.570051726830485,0.0478354648054818,0.181371616860513,0.191432764553815,0.564611840622702,0.191432764553816,0.564611840622702,0.08147957749222449,0.570376367155093,0.564611840622702,0.564611840622702,0.564611840622702,0.564611840622702,0.562021702959248,0.567482742413538,0.181371616860513,0.567482742413538,0.564611840622702,0.570376367155093,0.564611840622702,0.564611840622702,0.564611840622702,0.564611840622702,0.181371616860513,0.564611840622702,0.181371616860513,0.186465033624837,0.567150696626243,0.181371616860513,0.191432764553815,0.564611840622702,0.186465033624837,0.561675232486367,0.564522309434716,0.564611840622702,0.0932713546669726,0.567150696626243,0.567482742413538,0.567482742413538,0.564611840622702,0.0912758622232986,0.5732060721903121,0.617195303812339,0.196426845703613,0.567570237031104,0.564611840622702,0.564611840622702,0.564611840622702,0.567482742413538,0.611248131915026,0.564611840622702,0.186465033624836,0.6093975307437,0.564611840622702,0.570051726830485,0.181371616860513,0.564611840622702,0.567901204397643,0.567813934340346,0.567482742413538,0.196571264430004,0.185888596676218,0.575499453906923,0.564611840622702,0.613302967227662,0.185888596676219,0.181371616860513,0.567482742413538,0.570051726830485,0.0849702767156702,0.570376367155093,0.564611840622702,0.567570237031104,0.5728045771522829,0.564611840622702,0.181216378123445,0.570290763321904,0.186465033624837,0.564611840622702,0.564611840622702,0.564611840622702,0.572720578412611,0.564611840622702,0.186616409019217,0.564611840622702,0.564611840622702,0.564611840622702,0.186465033624837,0.567150696626243,0.567482742413538,0.567482742413538,0.181371616860513,0.564611840622702,0.572720578412611,0.181371616860513,0.564611840622702,0.190869874643953,0.56418251941172,0.567482742413538,0.567570237031104,0.567482742413538,0.570290763321904,0.564611840622702,0.580504147121897,0.570376367155093,0.567482742413538,0.191432764553816,0.567570237031104,0.572720578412611,0.570376367155093,0.570376367155093,0.564611840622702,0.564611840622702,0.567570237031104,0.564611840622702,0.804454052862602,0.294410558698187,0.308117238478315,0.315246784803217,0.5954685666677429,0.500590340730564,0.8029494823330789,0.500590340730564,0.637163887020097,0.0126935254282871,0.617201826493743,0.621821483310238,0.0210982618832403,0.289240690371866,0.615140557588544,0.603037317776234,0.243586358990421,0.797326517460843,0.637694869354371,0.481764073692322,0.814163796012628,0.389800007447512,0.602386538486921,0.621290916856269,0.809604545899815,0.599150094541102,0.451357366219745,0.281428884759662,0.454534765241832,0.512123090693938,0.811240351266434,0.611191918034467,0.631063434113639,0.79537345876711,0.49299760098688,0.013231030220749,0.624713247611963,0.0215462057676994,0.00767843643207892,0.718858388911523,0.388512316216488,0.815705490858591,0.640792748309541,0.796994313150353,0.476956348361984,0.467825404980215,0.627772842314459,0.784767354863195,0.640792748309541,0.450743566134206,0.801414501945688,0.775674097914228,0.806200389792833,0.401116412517096,0.555301542594286,0.39708221837115,0.111398135296533,0.795033344654592,0.459011727173974,0.334044232654403,0.631063434113639,0.281428884759662,0.328111579084423,0.454534765241832,0.480411967900479,0.260613342033951,0.376189336271866,0.480411967900478,0.290689688780319,0.634831834219209,0.785533628889617,0.591407536250573,0.614532788388807,0.596145327557236,0.631063434113639,0.459746343263308,0.415015570831035,0.472078962107341,0.608111346632456,0.809319833620721,0.637694869354371,0.804150045987216,0.403072027451159,0.467113695356037,0.62129091685627,0.631614658075545,0.0110861684509614,0.620704944231808,0.018475074560758,0.0124162888317724,0.0198734871163928,0.1734454102884,0.0220539238534451,0.489747598791954,0.0162204924803736,0.0365356491851157,0.590017364576745,0.813895506455643,0.807909006399552,0.804729662578278,0.384774647808227,0.811240351266434,0.467825404980215,0.807909006399552,0.809319833620721,0.809319833620721,0.476989768240044,0.476302952306849,0.628334695264028,0.793335274069897,0.8059029670472569,0.637163887020098,0.797326517460843,0.0498748166755942,0.809034088885392,0.614532788388807,0.6465281279401,0.8059029670472569,0.380080966588177,0.160294548734905,0.612999122944649,0.631614658075545,0.637163887020097,0.282065533604951,0.00793786609723245,0.858160725556008,0.481088805863611,0.241313841909716,0.405505803416637,0.797326517460843,0.789546318061652,0.800811951930397,0.809865113770572,0.797326517460843,0.110954342998156,0.252947226997775,0.793720243221098,0.849008225183006,0.793024997437868,0.007821935317399321,0.34475533595785,0.0318337174222448,0.39307816236206,0.795712200395107,0.367161095934623,0.380742220492929,0.050493365817613,0.780514945560012,0.610896060281614,0.5303783951482131,0.0576373760564847,0.00726637718866379,0.631063434113639,0.127136943579009,0.00728011229863622,0.421037444259786,0.818461907461185,0.806496710088392,0.627209191876143,0.631063434113639,0.806496710088392,0.774396588314779,0.796350620137474,0.803844889924519,0.371361620014115,0.809319833620721,0.338684572641861,0.388512316216488,0.806200389792833,0.813895506455643,0.804150045987216,0.0529712942775569,0.0311041627779503,0.800811951930397,0.813895506455643,0.500590340730563,0.485791133417125,0.481764073692321,0.804150045987216,0.208570457049269,0.0132621279985642,0.00766308883448462,0.7912840067308889,0.815705490858591,0.472778565554932,0.806200389792833,0.800811951930397,0.493642002923782,0.621290916856269,0.814163796012628,0.766414484462129,0.481088805863611,0.640792748309541,0.6252860196589241,0.640792748309541,0.813895506455643,0.800811951930397,0.810961556378798,0.472078962107341,0.624713247611963,0.171515476750999,0.562352951100387,0.55992438350354,0.177238508039402,0.562196976081653,0.55992438350354,0.55992438350354,0.55992438350354,0.55992438350354,0.562196976081653,0.177511971123569,0.55992438350354,0.55992438350354,0.565335618356301,0.55992438350354,0.55992438350354,0.563118408880518,0.55992438350354,0.55992438350354,0.555089934989474,0.562352951100387,0.565487834062616,0.55992438350354,0.55992438350354,0.563118408880518,0.183066944506502,0.564429996928126,0.55992438350354,0.55992438350354,0.55992438350354,0.55992438350354,0.55992438350354,0.167319109032916,0.562196976081653,0.563118408880518,0.55992438350354,0.563118408880518,0.562352951100387,0.562352951100387,0.602237958107108,0.557448971709299,0.557448971709299,0.55992438350354,0.55992438350354,0.562196976081653,0.079909749367163,0.55992438350354,0.55992438350354,0.565487834062616,0.55992438350354,0.55992438350354,0.564583286131854,0.182800840019316,0.55992438350354,0.0760972574033152,0.55992438350354,0.163026476163607,0.173166200675144,0.5676647710589851,0.562352951100387,0.562352951100387,0.177511971123569,0.562352951100387,0.55992438350354,0.563118408880518,0.0098396444430826,0.55992438350354,0.55992438350354,0.181494939195883,0.55992438350354,0.075035002674121,0.55992438350354,0.172886694922772,0.55992438350354,0.565335618356301,0.564583286131854,0.556648947313706,0.562196976081653,0.55992438350354,0.55992438350354,0.562352951100387,0.177238508039402,0.173166200675143,0.177238508039402,0.180181021389406,0.55992438350354,0.173166200675144,0.563118408880518,0.55992438350354,0.562352951100387,0.173166200675144,0.55992438350354,0.562352951100387,0.177511971123569,0.177238508039402,0.556648947313706,0.562352951100387,0.562352951100387,0.562352951100387,0.55992438350354,0.55992438350354,0.55992438350354,0.565335618356301,0.0311152208167191,0.55992438350354,0.55992438350354,0.562352951100387,0.562196976081653,0.55992438350354,0.563118408880518,0.556648947313706,0.55992438350354,0.563118408880518,0.55992438350354,0.560862181692941,0.55992438350354,0.562196976081653,0.55992438350354,0.557448971709299,0.186941124300883,0.0954844403092209,0.55992438350354,0.173166200675144,0.181762689458305,0.173166200675144,0.598210156775812,0.562196976081653,0.247832107122979,0.185398575517747,0.55992438350354,0.562352951100387,0.562352951100387,0.55992438350354,0.07904313883347459,0.602237958107108,0.554108983958841,0.559139641370989,0.24473182665336,0.55992438350354,0.556648947313706,0.55992438350354,0.562196976081653,0.563118408880518,0.564583286131854,0.173166200675144,0.566775473957583,0.568547968566088,0.562352951100387,0.55992438350354,0.605595087326635,0.556648947313706,0.554108983958841,0.55992438350354,0.173166200675144,0.0790431388334747,0.571195975917561,0.562352951100387,0.567515116359519,0.55992438350354,0.55992438350354,0.55992438350354,0.562352951100387,0.562196976081653,0.177511971123568,0.55992438350354,0.55992438350354,0.177238508039402,0.55992438350354,0.173166200675144,0.563118408880518,0.562196976081653,0.557448971709299,0.0836560206642912,0.565335618356301,0.562352951100387,0.556648947313706,0.169005092862993,0.55992438350354,0.55992438350354,0.55992438350354,0.173166200675144,0.55992438350354,0.562196976081653,0.55992438350354,0.565335618356301,0.563118408880518,0.55992438350354,0.556648947313706,0.55992438350354,0.55992438350354,0.55992438350354,0.559139641370989,0.177511971123569,0.572765194468937,0.55992438350354,0.566775473957583,0.562352951100387,0.55992438350354,0.562352951100387,0.564583286131854,0.462249144614205,0.234137050918182,0.582506351507992,0.813631124265407,0.806676711443869,0.360967927423338,0.635720361917922,0.635720361917922,0.623377044689014,0.117141499653828,0.796155269558628,0.295862050800762,0.109226541945243,0.170427687202755,0.480557871175044,0.324646443682743,0.480557871175044,0.606131986752056,0.462249144614205,0.648733487333824,0.635720361917922,0.6306781189156661,0.8067278188160401,0.606131986752056,0.648733487333824,0.442567227976495,0.192542871382957,0.389982620652413,0.20368041649302,0.505342087921677,0.635720361917922,0.488793380126261,0.119823974815061,0.471042350094343,0.621573354691513,0.0648578317236551,0.11408340987274,0.253959614694445,0.0480928676390687,0.86372917675369,0.635720361917922,0.648733487333824,0.625463511658778,0.820002046630225,0.488793380126261,0.419730656864178,0.513549809296772,0.534405040169831,0.7962030012489471,0.221485576525716,0.461101027021361,0.806676711443869,0.38744845220931,0.240629913357876,0.250788264775059,0.820002046630225,0.616057537424256,0.298266519796748,0.231025245629359,0.635472193721323,0.563117588499727,0.288197039398448,0.262843026608258,0.600069485850973,0.159989897395271,0.162988001252404,0.350362980453236,0.102210653467672,0.267126998839648,0.597650744084672,0.162948023455149,0.38744845220931,0.12584165706811,0.272574522139311,0.356831404380166,0.540729396378378,0.150136347946078,0.648733487333824,0.790623898261629,0.635720361917922,0.6422486565716981,0.813631124265407,0.628659672781596,0.635720361917922,0.799045957927462,0.8169106271959981,0.126756640459751,0.505342087921676,0.143642274531335,0.18673584199002,0.164054465422582,0.223422365928477,0.166413055700886,0.813631124265407,0.145878620474516,0.07203603916649021,0.26249733865394,0.572758547532575,0.590786802799571,0.81025808428336,0.507635860179605,0.413318108108278,0.621573354691513,0.396808767423306,0.635720361917922,0.820002046630225,0.25886481312065,0.513549809296771,0.513549809296772,0.360967927423338,0.597650744084672,0.6138529184699389,0.361842399587821,0.09802140856907821,0.6306781189156661,0.814623313378526,0.635720361917922,0.820002046630225,0.406906782292434,0.177446512414717,0.604910023981556,0.480557871175044,0.648733487333824,0.409272555677806,0.0780458901100437,0.38115050978666,0.451964273008047,0.858883174710967,0.497619665488551,0.505342087921676,0.505342087921677,0.820002046630225,0.480557871175044,0.462249144614205,0.110914488254536,0.794965645867356,0.813631124265407,0.254128345691689,0.813631124265407,0.280674885729036,0.621573354691513,0.396808767423306,0.270014344524344,0.787415014814118,0.312910162204925,0.442567227976495,0.261370445838228,0.258526518409884,0.811240689377804,0.172818849463461,0.165890562162548,0.786093467166913,0.480557871175044,0.288113720762563,0.0410455441159908,0.820002046630225,0.648733487333824,0.513549809296771,0.648733487333824,0.579976564383611,0.513549809296771,0.648733487333824,0.806676711443869,0.81025808428336,0.820002046630225,0.484684594656128,0.210645832025783,0.505342087921677,0.497619665488551,0.8169106271959981,0.820002046630225,0.259183548353979,0.330232438708177,0.648733487333824,0.330232438708177,0.325227094634678,0.6138529184699389,0.341531285396258,0.480557871175044,0.406906782292434,0.621573354691513,0.513549809296772,0.322221814321636,0.314033225099313,0.820002046630225,0.813631124265407,0.406906782292434,0.814623313378526,0.471042350094343,0.635720361917922,0.606131986752056,0.451964273008047,0.800226601972623,0.811292618673659,0.648733487333824,0.635720361917921,0.37209893953232,0.379377088796354,0.497619665488551,0.591263010881099,0.554970448605801,0.554970448605801,0.557500287502027,0.554970448605801,0.554970448605801,0.552681709610662,0.554970448605801,0.549681751725667,0.552389781132262,0.554970448605801,0.552389781132262,0.557219099907132,0.07026128351724931,0.164502896665063,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.169051663167029,0.549756211767352,0.554970448605801,0.554970448605801,0.16462876875586,0.164502896665063,0.160358671451356,0.552681709610662,0.54960724870746,0.554970448605801,0.560049983035995,0.557219099907132,0.552389781132262,0.55532789515522,0.554970448605801,0.554970448605801,0.554970448605801,0.164502896665062,0.554970448605801,0.557219099907132,0.164502896665064,0.593308914560274,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.560118682813715,0.557219099907132,0.554970448605801,0.55757036174414,0.557500287502027,0.173494145793379,0.554970448605801,0.555041913318438,0.557219099907132,0.554970448605801,0.554970448605801,0.557500287502027,0.550351435117537,0.554970448605801,0.164502896665062,0.554970448605801,0.554970448605801,0.554970448605801,0.55757036174414,0.554970448605801,0.554970448605801,0.0683297638660651,0.554970448605801,0.168556625938426,0.552389781132262,0.554970448605801,0.560049983035995,0.554970448605801,0.554970448605801,0.55757036174414,0.557500287502027,0.55757036174414,0.554970448605801,0.554970448605801,0.559705436023663,0.160358671451356,0.159711736578029,0.164502896665063,0.160358671451357,0.552316828786951,0.554970448605801,0.554970448605801,0.554970448605801,0.552389781132262,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.552681709610662,0.552389781132262,0.07058417483010659,0.160358671451356,0.554970448605801,0.554970448605801,0.554970448605801,0.164502896665062,0.16399613491008,0.554970448605801,0.55757036174414,0.164502896665063,0.554970448605801,0.557219099907132,0.554970448605801,0.164502896665063,0.554970448605801,0.554970448605801,0.173010139121352,0.554970448605801,0.55757036174414,0.549756211767352,0.164502896665064,0.164502896665064,0.00746734364696684,0.168556625938426,0.554970448605801,0.557500287502027,0.164502896665063,0.595291514564923,0.560049983035995,0.598912540311361,0.554970448605801,0.552681709610662,0.554970448605801,0.55757036174414,0.557219099907132,0.164502896665064,0.122744801318978,0.554970448605801,0.554970448605801,0.593308914560274,0.169051663167029,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.552681709610662,0.159840773487928,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.597295176500422,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.557219099907132,0.554970448605801,0.164502896665063,0.552316828786951,0.169174644318878,0.554970448605801,0.552681709610662,0.559705436023663,0.557219099907132,0.554970448605801,0.554970448605801,0.554970448605801,0.552316828786951,0.165133932199835,0.16399613491008,0.554970448605801,0.549681751725667,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.552389781132262,0.554970448605801,0.169174644318878,0.552681709610662,0.552389781132262,0.159840773487927,0.557219099907132,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.164502896665063,0.169174644318879,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.168556625938426,0.554970448605801,0.557219099907132,0.554970448605801,0.554970448605801,0.554970448605801,0.554970448605801,0.602082153419305,0.602082153419305,0.602082153419305,0.602082153419305,0.602082153419305,0.602082153419305,0.602082153419305,0.127403825619233,0.602082153419305,0.240203857842548,0.246401049526779,0.507533728781025,0.127403825619233,0.602082153419305,0.240203857842547,0.240203857842548,0.602082153419305,0.602082153419305,0.598786563963433,0.602082153419305,0.602082153419305,0.602082153419305,0.602082153419305,0.122741129980059,0.602082153419305,0.602082153419305,0.600027360202233,0.602082153419305,0.602082153419305,0.602082153419305,0.23470324708675,0.602082153419305,0.246401049526779,0.602082153419305,0.246401049526779,0.602082153419305,0.602082153419305,0.240628499641447,0.080225138965256,0.125259122858115,0.602082153419305,0.602082153419305,0.246401049526778,0.594891073816842,0.246401049526779,0.246401049526779,0.602082153419305,0.24640104952678,0.602082153419305,0.602082153419305,0.246401049526779,0.246401049526779,0.246401049526779,0.602082153419305,0.602082153419305,0.602082153419305,0.602082153419305,0.602082153419305,0.602082153419305,0.127403825619233,0.240203857842548,0.602082153419305,0.240628499641447,0.127403825619233,0.602082153419305,0.0773631426253632,0.602082153419305,0.246401049526779,0.602082153419305,0.127403825619233,0.246401049526779,0.598537782616351,0.602082153419305,0.602082153419305,0.602082153419305,0.246401049526778,0.602082153419305,0.602082153419305,0.602082153419305,0.598537782616351,0.602082153419305,0.602082153419305,0.24640104952678,0.598537782616351,0.600027360202233,0.602082153419305,0.0318258975227613,0.246401049526779,0.602082153419305,0.602082153419305,0.602082153419305,0.584842557346838,0.242769852180185,0.246401049526779,0.602082153419305,0.602082153419305,0.602082153419305,0.602082153419305,0.246401049526779,0.602082153419305,0.236900492910488,0.602082153419305,0.602082153419305,0.240203857842548,0.602082153419305,0.602082153419305,0.240203857842548,0.602082153419305,0.598786563963433,0.602082153419305,0.602082153419305,0.246401049526778,0.602082153419305,0.602082153419305,0.602082153419305,0.246401049526779,0.246401049526779,0.602082153419305,0.240203857842548,0.602082153419305,0.6460074892675139,0.602082153419305,0.600027360202233,0.594891073816842,0.602082153419305,0.319641907962718,0.24640104952678,0.183783984738579,0.246401049526779,0.602082153419305,0.24640104952678,0.246401049526779,0.602082153419305,0.602082153419305,0.6460074892675139,0.602082153419305,0.602082153419305,0.6460074892675139,0.602082153419305,0.602082153419305,0.600027360202233,0.602082153419305,0.240203857842547,0.602082153419305,0.602082153419305,0.602082153419305,0.242769852180186,0.602082153419305,0.246401049526779,0.6460074892675139,0.602082153419305,0.598537782616351,0.598537782616351,0.127403825619233,0.0769974468288414,0.246401049526778,0.598537782616351,0.246401049526779,0.246401049526779,0.602082153419305,0.127403825619233,0.602082153419305,0.602082153419305,0.118305038847427,0.127403825619233,0.602082153419305,0.602082153419305,0.246401049526779,0.600027360202233,0.602082153419305,0.246401049526778,0.598537782616351,0.59642389145581,0.127403825619233,0.598537782616351,0.602082153419305,0.600027360202233,0.602082153419305,0.598537782616351,0.602082153419305,0.127403825619233,0.246401049526779,0.514645703935132,0.602082153419305,0.602082153419305,0.602082153419305,0.127403825619233,0.24640104952678,0.598537782616351,0.602082153419305,0.600027360202233,0.602082153419305,0.246401049526779,0.602082153419305,0.602082153419305,0.602082153419305,0.602082153419305,0.602082153419305,0.598537782616351,0.52519439442434,0.52817648702261,0.52519439442434,0.526694149321884,0.52817648702261,0.526694149321884,0.526694149321884,0.526694149321884,0.526768124199353,0.526694149321884,0.115200130711304,0.52817648702261,0.52817648702261,0.526694149321884,0.526694149321884,0.528249609304278,0.5251195024356839,0.52817648702261,0.521987454326244,0.523676796398986,0.52519439442434,0.526694149321884,0.118097353401481,0.52817648702261,0.115342832588321,0.526694149321884,0.112241785540268,0.526694149321884,0.52519439442434,0.52817648702261,0.526694149321884,0.5251195024356839,0.52817648702261,0.52817648702261,0.52817648702261,0.525483257654902,0.52817648702261,0.52519439442434,0.52519439442434,0.5486914577950029,0.526408015484955,0.527893656741323,0.52817648702261,0.52817648702261,0.530814083620245,0.52817648702261,0.52519439442434,0.529362213536082,0.525483257654902,0.52817648702261,0.52817648702261,0.52817648702261,0.526694149321884,0.523676796398986,0.5251195024356839,0.529714107142799,0.52817648702261,0.526694149321884,0.52817648702261,0.526694149321884,0.52817648702261,0.526620131649469,0.529641817547219,0.527893656741323,0.52817648702261,0.0102195478827126,0.112241785540267,0.526408015484955,0.112241785540268,0.52817648702261,0.526694149321884,0.52519439442434,0.52817648702261,0.526694149321884,0.526694149321884,0.526694149321884,0.52817648702261,0.526694149321884,0.526694149321884,0.52817648702261,0.526694149321884,0.52817648702261,0.523601008569698,0.526408015484955,0.52519439442434,0.526694149321884,0.0406814221261419,0.52817648702261,0.52817648702261,0.526694149321884,0.52519439442434,0.5251195024356839,0.52519439442434,0.52817648702261,0.52817648702261,0.112241785540267,0.528249609304278,0.527893656741323,0.526620131649469,0.526694149321884,0.523676796398986,0.526694149321884,0.526694149321884,0.526694149321884,0.526694149321884,0.529362213536082,0.526694149321884,0.526694149321884,0.52817648702261,0.526694149321884,0.526694149321884,0.52817648702261,0.52817648702261,0.523676796398986,0.52817648702261,0.52817648702261,0.527893656741323,0.52817648702261,0.52519439442434,0.528249609304278,0.00412033353306613,0.525483257654902,0.526768124199353,0.526768124199353,0.52817648702261,0.550064354186255,0.526694149321884,0.550064354186255,0.526694149321884,0.52817648702261,0.529362213536082,0.526694149321884,0.52519439442434,0.52519439442434,0.548994160704153,0.526694149321884,0.52519439442434,0.546218873120675,0.526694149321884,0.52817648702261,0.526694149321884,0.52817648702261,0.526408015484955,0.112241785540269,0.526408015484955,0.112241785540268,0.109218086435764,0.526694149321884,0.112241785540267,0.548753770282335,0.52817648702261,0.52519439442434,0.526694149321884,0.112241785540268,0.52817648702261,0.52817648702261,0.52519439442434,0.52519439442434,0.528249609304278,0.52817648702261,0.526694149321884,0.52519439442434,0.52519439442434,0.523676796398986,0.52817648702261,0.112241785540267,0.523601008569698,0.526694149321884,0.529362213536082,0.115342832588321,0.526694149321884,0.52817648702261,0.526694149321884,0.526694149321884,0.52817648702261,0.523676796398986,0.52817648702261,0.5239691106418251,0.52519439442434,0.52817648702261,0.52817648702261,0.522140912535081,0.52817648702261,0.525483257654902,0.52817648702261,0.52817648702261,0.52817648702261,0.52817648702261,0.115342832588321,0.52817648702261,0.52817648702261,0.526408015484955,0.52817648702261,0.52817648702261,0.52519439442434,0.52519439442434,0.115200130711304,0.526694149321884,0.526694149321884,0.52519439442434],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.608880765336187,0.292335494247225,0.655130701498964,0.56197736943078,0.563823652027445,0.608880765336186,0.649248675588127,0.649248675588127,0.756319593692878,0.308257166214284,0.53857197813972,0.9999996209977829,0.370690997686013,0.385012266246508,0.402200421868077,0.391041485329184,0.675980434065569,0.56077291007034,0.56197736943078,0.544265358688477,0.692953397587601,0.5938363401487879,0.67632852024371,0.705661600149167,0.541666666666667,0.335729228482337,0.51286247563049,0.5938363401487879,0.388941324548419,0.666666666666667,0.447536183266643,0.538935654294696,0.440201644839198,0.523685855141391,0.492994509544651,0.267191701113541,0.266218686552703,0.371008514997587,0.115647365160438,0.73032124998265,0.518414571142721,0.708333333333333,0.5938363401487891,0.666666666666667,0.649587886218852,0.183607287917597,0.541666666666667,0.596724601200823,0.296919116301908,0.258163834029851,0.357582616146207,0.458333333333333,0.285714285714286,0.287322718636596,0.624231935769596,0.916666666666667,0.412228309243185,0.412228309243185,0.213068517778675,0.493704734619944,0.428222290234777,0.360696751430622,0.114608969501442,0.481345018338756,0.166666666666667,0.409429124409459,0.418000809652408,0.222536179769889,0.190070785318066,0.45864923544856,0.467200948711358,0.402521001554642,0.258384024675873,0.167015128296002,0.23263204277438,0.279114395015734,0.336037547200318,0.65893951260274,0.649248675588127,0.467480586131665,0.780764415056652,0.78049139513246,0.584630627337549,0.6055893626128011,0.538571978139719,0.75,0.277777777777778,0.544265358688476,0.25762489154206,0.268214727060713,0.165732888262517,0.591723697628868,0.165924889700262,0.518414571142721,0.458333333333333,0.319770920457846,0.199591053960183,0.487143334707579,0.518684099370455,0.465518874848471,0.700641595964399,0.617205820858134,0.492994509544651,0.5109544918198901,0.692953397587602,0.75,0.684348799246108,0.848900700132878,0.75,0.790617913089131,0.474902778904957,0.630429840869319,0.397878159822551,0.474902778904957,0.72954248626323,0.657609234694143,0.824320880791526,0.666666666666666,0.602866754697619,0.36104170898623,0.477123314295441,0.675980434065569,0.458333333333333,0.447536183266643,0.222135283668078,0.503054157073728,0.453772028074424,0.584326885284663,0.708333333333333,0.780764415056651,0.649587886218853,0.625,0.824320880791526,0.287051097440058,0.262217129157952,0.336037547200318,0.692953397587602,0.279214519045099,0.625,0.428979424858098,0.518414571142721,0.388038950878107,0.349338732158819,0.465155628590495,0.418000809652408,0.649248675588127,0.489100499670736,0.167049978186262,0.864906957532714,0.237093278550641,0.394427252445195,0.401486900677258,0.483087296742832,0.465155628590495,0.402521001554643,0.666666666666667,0.666666666666667,0.583333333333334,0.666666666666668,0.388038950878105,0.583333333333333,0.75,0.546477699596486,0.584630627337549,0.5825544010142339,0.721904848227374,0.33614328529877,0.649587886218853,0.56197736943078,0.518758996077176,0.625,0.559723890515756,0.276713056965744,0.333333333333334,0.344689932719866,0.511464083772096,0.465518874848472,0.749774221687008,0.722320439837797,0.614497087733084,0.63006904679405,0.5,0.431443709118978,0.699967932078838,0.791666666666667,0.6055893626128021,0.571510752323423,0.476190476190476,0.754320202136266,0.431443709118978,0.492994509544651,0.413028185517171,0.528358497692565,0.619424428158,0.5,0.692953397587602,0.167214045759473,0.388038950878105,0.30139788995793,0.546232211054266,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.0533052186932602,0.410408049384548,1.31375351831084e-09,9.772841266108629e-10,0.13775942251751,0.105897756745007,0.0897570622764584,9.772841265842459e-10,0.07887944987884959,0.176464475462787,1.03198528609023e-09,2.47099536822761e-09,0.202041501373732,0.386335975491254,0.0487818178446041,0.185799246291785,1.03198528609599e-09,1.09753045502264e-09,0.178929537242627,0.0363369993756108,0.0449504921536294,1.00376306635152e-09,1.0134501136774e-09,0.0533052186932603,0.0833333333333333,0.227499763913113,0.0533052186932602,0.136824974290256,0.204996166188857,9.30901037573733e-10,1.03198528610093e-09,0.0935585970395959,0.45800305256764,0.109996140191607,0.0487818178446042,0.305192929960342,0.525294270630063,0.097711152397725,0.303769971179522,0.0248564969726006,9.772841265897909e-10,0.0416666666666668,0.0456857124868512,0.0833333333333333,9.64629438092032e-10,0.301051510570731,0.0416666666666667,0.108667555042163,0.149482748046065,0.178929537242627,0.09014740682550081,0.0833333333333334,0.190476190476191,0.41927968593557,1.17267157018165e-09,9.30901037577519e-10,0.136824974290256,0.227685391683789,0.362979226173055,0.0449979957728852,0.148075139250065,0.113615322266888,0.485375361431581,0.093785088373995,0.333333333333333,0.205400352529065,0.152020558398048,0.317896910600788,0.478887947949449,0.121100951185932,0.228914021943407,0.140212207586362,0.435179387147729,0.307726450518424,0.477961877488275,0.133084268819173,0.325347036672364,0.109475267813473,0.0449504921536295,0.036336999375611,9.64629438077485e-10,9.77284126597766e-10,0.09355859703959631,0.0449504921536295,1.03198528609337e-09,9.30901037579682e-10,0.222222222222222,0.0363369993756109,0.31451598674246,0.356496737311987,0.271546188554971,0.266398984213287,0.133084268819172,0.0449504921536297,0.0833333333333333,0.217115462290584,0.244622770532404,0.0633560868176392,0.10879559879066,0.152020558398047,9.55886665531336e-10,0.0972561353391817,1.03198528608784e-09,0.130124570004184,9.77284126628375e-10,9.30901037578412e-10,1.00376306634638e-09,0.109475267813472,0.0416666666666667,0.0587187273177456,0.08975706227645849,0.0468186351963798,0.248199886798312,0.0897570622764584,0.0456857124868511,0.0422964283548124,9.77284126579524e-10,9.30901037572284e-10,0.116750476811911,0.270900243040727,0.120580522531918,1.03198528609011e-09,0.0416666666666667,0.0487818178446042,0.333802513398171,0.097452543021079,0.0619382258212089,0.0796901268578916,0.0416666666666666,0.0432669497874883,9.646294380765081e-10,9.3090103755122e-10,0.0449504921536294,0.219660536855622,0.430546501249058,1.14066802977277e-09,0.0897570622764586,0.349225134679162,0.0416666666666666,0.0728269040116583,0.0449504921536294,0.0897570622764584,0.248942624624661,0.208981888700616,0.101666421400793,0.134417274721529,0.299150859530772,0.369203417356983,1.00376306635852e-09,0.317987919743251,0.323865073272332,0.122582904170512,0.0777385378778907,0.105897756745007,0.0935585970395959,0.0833333333333335,9.30901037587464e-10,0.0416666666666667,9.30901037585526e-10,0.134417274721529,0.0833333333333333,0.125,0.0972561353391817,1.01345011365867e-09,0.0363369993756107,1.03262836352737e-09,0.334590586541183,9.646294380790751e-10,0.0449504921536294,0.0432669497874884,0.0416666666666666,0.181956946357949,0.301051510570731,0.0416666666666668,0.267508281812414,0.168403426799563,0.0509890989598241,0.129878973819152,0.116750476811911,9.558866655312851e-10,0.145244422516125,0.0833333333333333,0.134417274721529,0.126210014419527,0.0416666666666669,0.0449504921536295,0.0738237836086769,1.0410876449309e-09,1.0975304550464e-09,0.0449504921536297,0.0971972512398071,0.05710172262387,0.123044502277522,0.0457373283964456,0.0416666666666666,9.77284126582034e-10,0.224042168181161,0.134417274721529,0.0449504921536295,0.155154589643893,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>region<\/th>\n      <th>exp<\/th>\n      <th>year<\/th>\n      <th>id<\/th>\n      <th>fitness<\/th>\n      <th>fecundity<\/th>\n      <th>ows.b<\/th>\n      <th>ows.p<\/th>\n      <th>noslugdamage<\/th>\n      <th>severeslugdamage<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"dom":"Blfrtip","buttons":["copy","csv","excel","pdf","print"],"lengthMenu":[[10,25,50,-1],["10","25","50","All"]],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6,7,8,9,10]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false},"selection":{"mode":"multiple","selected":null,"target":"row","selectable":null}},"evals":[],"jsHooks":[]}</script>
```



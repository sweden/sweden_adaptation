---
title: "Kcluster_structure"
author: "Benjamin Brachi"
date: "`r Sys.Date()`"
output: html_document
root.dir: "../"
---

```{r setupKclusters, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(bigmemory)
library(tidyverse)
library(ggplot2)
library(ggpubr)
library(knitr)
set.seed(123)
``````

# Intro

I ran structure with K=1:10 for SNPs with maf above 3%. 

The results suggests that K=8 is best. 

I'll map out the clusters and compare to phenotypes. 


# Read structure res and assign accessions to clusters. 

```{r}
st=read.delim("res/structure_M03.6.meanQ",sep="", h=F)
fam=read.table("data/snps/structure/sweden_200_MAF03.fam", stringsAsFactors = F)

clusts=data.frame(id=paste(fam[,1]), K8=paste(apply(st, 1,which.max)), stringsAsFactors = F)
```


## Compare six clusters to oseven clusters

```{r}
clim=read.table("./data/worldclim_swedish_acc.txt", sep="\t", h=T, stringsAsFactor=F, fileEncoding  = "ISO-8859-1" )
#clim_exp=read.table("./data/worldclim_swedish_exp.txt", sep="\t", h=T)

acc<-clim%>%mutate(id=paste(lines),lon=long)%>%dplyr::select("id","region","lon","lat")%>%left_join(y = clusts, by="id")

table(acc$K8, acc$region)
```

# Make maps

```{r}
library(mapview)
library(sf)
library(sp)
library(leafpop)

loc=acc

coordinates(loc) <- ~lon+lat
proj4string(loc) <- CRS("+proj=longlat +datum=WGS84")
#head(loc2)

mylabel <- glue::glue("{loc$id}/{loc$region}/{loc$K8}") %>%
  lapply(htmltools::HTML)
```


## map of my clustering with K=6

```{r}
m=mapview(loc, map.types="Esri.WorldImagery", label=mylabel, zcol="K8", size=10, popup = popupTable(loc, c("id", "region", "K8")))

m
```



# Check-out how the groups are doing in the selection experiments


```{r}
library(ggbeeswarm)

fit=read.table("../data.for.paper/fitnesses.csv", h=T, sep=",", stringsAsFactors = F, fileEncoding = "ISO-8859-1")%>%mutate(id=paste(id))%>%left_join(acc, by="id")->fit

```

```{r, fig.height=10}
fit%>%dplyr::select(K8, NA., NB, SR, ST)%>%pivot_longer(cols=c(NA., NB, SR, ST), values_to = "fitness", names_to = "site")->df

df%>%ggplot(aes(x=K8, y=fitness))+ geom_beeswarm(aes(col=K8), corral = "gutter", size=5, alpha=0.8)+theme_pubclean()+facet_wrap(~site, nrow=2)
```



```{r}
acc%>%group_by(K8)%>%summarise(lat=mean(lat), lon=mean(lon))%>%arrange(lat)->clusters
kable(clusters)
```
```{r}
clusters%>%mutate(group=c("B", "S1", "S2", "S3", "CNS", "CN", "N1", "N2"))->clusters
```


```{r}
acc%>%mutate(K8=factor(K8, levels=clusters$K8))%>%left_join(clusters, by="K8")->acc
```


```{r}
#bbgroups=c("S1","S2","S3","CS","B","CN","N")
bbgroups=c("B", "S1", "S2", "S3", "CNS", "CN", "N1", "N2")
colset=c("#E2282F","#08306B","#6BAAD3", "#2171B5","#6BAED6","#C6DBEF","#74C476","#006D2C")
names(colset)=bbgroups

fit=read.table("../data.for.paper/fitnesses.csv", h=T, sep=",", stringsAsFactors = F)%>%dplyr::select(!group)%>%mutate(id=paste(id))%>%left_join(acc, by="id")%>%mutate(group=factor(group, levels=paste(names(colset))))

```

```{r, fig.height=13}
fit%>%dplyr::select(id, group, NA11, NA12, NM11, NM12, SR11, SR12, SU11, SU12)%>%pivot_longer(cols=c(NA11, NA12, NM11, NM12, SR11, SR12, SU11, SU12), values_to = "fitness", names_to = "site")->df

df%>%ggplot(aes(x=group, y=fitness))+ geom_beeswarm(aes(col=group),  size=2, alpha=0.8)+geom_violin(fill=NA, draw_quantiles =0.5 )+theme_pubclean()+facet_wrap(~site, nrow=4)+scale_colour_manual(values = colset)->p1
  ggsave(p1, filename = "figures/fitCG_New_groups.pdf", device="pdf", unit="in", width=8,height=12) 
p1
```

```{r, fig.width=10}
fit%>%dplyr::select(id, group, NA., NB, SR, ST)%>%pivot_longer(cols=c(NA., NB, SR, ST), values_to = "fitness", names_to = "site")->df

df%>%ggplot(aes(x=group, y=fitness))+ geom_beeswarm(aes(col=group), corral = "gutter", size=2, alpha=1)+geom_violin(fill=NA, draw_quantiles = 0.5)+theme_pubclean()+facet_wrap(~site, nrow=4)+scale_colour_manual(values = colset)->p1
p1
```
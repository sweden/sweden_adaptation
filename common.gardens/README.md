---
author: Benjamin Brachi
date: October, 2017
title: Swedish project
output:
 html_document:
  keep_md: true
  fig_caption: yes
  toc: true
  toc_float: true
  number_sections: true
  highlight: tango
  theme: space
---



# Complete protocol of the experiments

## Location of the experiments

We performed a total of 8 experiments, in 4 locations in Sweden over 2 years. 




```r
library(ggmap)
library(grid)

clim=read.table("./data/worldclim_swedish_acc.txt", sep="\t", h=T, stringsAsFactor=T )
clim_exp=read.table("./data/worldclim_swedish_exp.txt", , sep="\t", h=T)
colnames(clim)[1]="id"


CG=clim_exp[clim_exp$experiments=="Common garden",c(2:4)]

map.bb=c(left=min(clim_exp$lon)-2, bottom=min(clim_exp$lat)-0.5,right=max(clim_exp$lon)+2, top=max(clim_exp$lat)+0.5)
mapcenterS=c(lon=mean(CG$lon[3:4]), lat=mean(CG$lat[3:4]))
mapcenterN=c(lon=mean(CG$lon[1:2]), lat=mean(CG$lat[1:2]))


apikey="AIzaSyADNSHBIYYrO6Wj8RpIDTm5qnp-lO12lDk"


mapImageData1 <- get_map(map.bb,color = "color",source = "stamen", maptype = "toner-lite", zoom=6, api_key=apikey)
mapImageS <- get_map(mapcenterS,color = "color", source="stamen", maptype = "toner-lite", zoom=9,api_key=apikey)
mapImageN <- get_map(mapcenterN,color = "color",source = "stamen", maptype = "toner-lite", zoom=9, api_key=apikey)

##mapImageData1 <- get_navermap(map.center, size=c(800, 800), color="color", baselayer="satellite")

##mapImageData1 <- get_map(c(lon=map.center$lon, lat=map.center$lat),color = "color",source = "osm",maptype = "terrain", zoom=6)

map=ggmap(mapImageData1, darken=0.1)+geom_point(aes(x=lon, y=lat), data=CG, cex=8, col="firebrick2", alpha=0.5)+geom_point(aes(x=long, y=lat), data=clim, cex=3, col="dodgerblue4", alpha=0.5)+labs(title="A")+theme(plot.title = element_text(hjust=0))
mapN=ggmap(mapImageN, darken=0.1)+geom_point(aes(x=lon, y=lat), data=CG, cex=8, col="firebrick2", alpha=0.5)+geom_point(aes(x=long, y=lat), data=clim, cex=3, col="dodgerblue4", alpha=0.5)+labs(title="B")+theme(plot.title = element_text(hjust=0))
mapS=ggmap(mapImageS, darken=0.1)+geom_point(aes(x=lon, y=lat), data=CG, cex=8, col="firebrick2", alpha=0.5)+geom_point(aes(x=long, y=lat), data=clim, cex=3, col="dodgerblue4", alpha=0.5)+labs(title="C")+theme(plot.title = element_text(hjust=0))


pdf("./figures/map_exp.pdf" , paper="special", width=5, height=5, pointsize=8)
vplayout <- function(x, y) viewport(layout.pos.row = x, layout.pos.col = y)
grid.newpage()
pushViewport(viewport(layout = grid.layout(2, 2)))
print(map, vp = vplayout(1:2, 1))
print(mapN, vp = vplayout(1, 2))
print(mapS, vp = vplayout(2, 2))
dev.off()
```
This doesn't work anymore, but I made the map before so I'll just use that for now. 

![Map of experiments and origin of accessions used in the experiments. Red dots: experiments; Blue dots: accessions](./figures/map.png)

## planting and setup

The list of the 203 accessions/genotypes used is presented in acc\_list.txt.
This list includes 200 re-sequenced Swedish accessions,  Edi-0,
Col-Fri and Col-Fri-FLC. Most Swedish accessions were planted in 8
replicates per block and Edi-0, Col-FRI and Col-FRI-FLC, 6043 Lov-1,
6974 Ull2-5, 7517 Var2-6, 8369 Rev-1, 8240 Kulturen-1, 8262 Bil-5,
8247 San-2, 6918 Fab-4 were planted in 16 replicates per block.

Each experiment is organized in a three complete randomized block
design. Plantings followed the calendar presented in table
\ref{calendar}.

\begin{table}
\center
\label{calendar}
\begin{tabular}{|l|l|l|l|l|}
\hline
Year & Experiment   & Block & planting date & field installation date \\
\hline
 2011 & Adal         & A     &    2011-08-08 &              2011-08-25 \\
      &              & B     &    2011-08-10 &              2011-08-25 \\
      &              & C     &    2011-08-12 &              2011-08-25 \\
      & Ramsta       & A     &    2011-08-07 &              2011-08-24 \\
      &              & B     &    2011-08-09 &              2011-08-24 \\
      &              & C     &    2011-08-11 &              2011-08-24 \\
      & Ullstorp     & A     &    2011-08-31 &              2011-09-17 \\
      &              & B     &    2011-09-02 &              2011-09-17 \\
      &              & C     &    2011-09-04 &              2011-09-17 \\
      & Ratckegarden & A     &    2011-09-01 &              2011-09-18 \\
      &              & B     &    2011-09-03 &              2011-09-18 \\
      &              & C     &    2011-09-05 &              2011-09-18 \\
\hline
2012 & Adal         & A     &    2012-08-08 &              2012-08-25 \\
      &              & B     &    2012-08-10 &              2012-08-25 \\
      &              & C     &    2012-08-12 &              2012-08-25 \\
      & Ramsta       & A     &    2012-08-07 &              2012-08-24 \\
      &              & B     &    2012-08-09 &              2012-08-24 \\
      &              & C     &    2012-08-11 &              2012-08-24 \\
      & Ullstorp     & A     &    2012-08-31 &              2012-09-17 \\
      &              & B     &    2012-09-02 &              2012-09-17 \\
      &              & C     &    2012-09-04 &              2012-09-17 \\
      & Ratckegarden & A     &    2012-09-01 &              2012-09-18 \\
      &              & B     &    2012-09-03 &              2012-09-18 \\
      &              & C     &    2012-09-05 &              2012-09-18 \\
\hline
\end{tabular}
\caption{Calendar followed for the planting of the common garden
experiments}
\end{table}

## Overview of the phenotypes collected and organisation.

To designate a particular round of experiment we'll use the year it
was sown (not the year it was harvested).

Table \ref{phenCG2011} and \ref{phenCG2012} provide a the list of the
phenotypes we have.

\begin{table}
\center
\caption{Table of the phenotypes we have collected (\textit{or will collect} indicated in italic) in the common garden experiments from 2011.}
\label{phenCG2011}
\begin{tabular}{|l|c|c|c|c|}
\hline
phenotype                     & ULL 2011   & RAT 2011   & RAM 2011   & ADA 2011   \\
\hline
 flowering time before winter  & X          & X          & X          & X          \\
 flowering time in the spring  &            &            &            &            \\
 herbivore damage in the fall  &            & X          &            &            \\
 rosette size                  & X          & X          &            &            \\
 overwinter survival           & X          & X          & X          & X          \\
 survival to seed set (Approx) & X          & X          & X          & X          \\
 fecundity estimate            & X          & X          & X          & X          \\
 microbial community           & \textit{X} & \textit{X} & \textit{X} & \textit{X} \\
\end{tabular}
\end{table}


\begin{table}
\center
\caption{Table of the phenotypes we have collected (\textit{or will
collect} indicated in italic) in the common garden experiments from
2012.}
\label{phenCG2012}
\begin{tabular}{|l|c|c|c|c|}
\hline
 phenotype                     & ULL 2012   & RAT 2012   & RAM 2012   & ADA 2012   \\
\hline
 flowering time before winter  & X          & X          & X          & X          \\
 flowering time in the spring  & X          & X          & X          & X          \\
 herbivore damage in the fall  &            &            &            &            \\
 rosette size                  & X          & X          & X          & X          \\
 overwinter survival           & X          & X          & X          & X          \\
 survival to seed set (Approx) & X          & X          & X          & X          \\
 fecundity estimate            & \textit{X} & \textit{X} & \textit{X} & \textit{X} \\
 microbial community           & \textit{X} & \textit{X} & \textit{X} & \textit{X} \\
\end{tabular}
\end{table}

To be able to combine everything in one file, the easiest is probably
to start from the initial randomizations, corrected for errors that
were made during planting. Then we can add all the phenotypes in
different columns. Some phenotypes will have many NAs, because only a
subset of the plants were measured (/i.e./ microbiota, fitness).

# Merging all phenotypes in one file and description

The script merging\_phen\_files_11202014.R was used in April 2014 to
merge all phenotype files into one. I copied that script and updated
the paths. This scripts uses the results from the treatment of each
trait or set of traits fom the folders in "../all\_phenotypes/"


```r
read_chunk("./scripts/merging_phen_files.R")
```

```r
##reading the randomization file.

acc=read.table("./data/acc_list.txt", sep="\t", h=T)


##reading orginal data files for each phenotype

##rosette data (for bothe years):
size=read.table("./data/rosette_size.txt", sep="\t", h=T)
size$experiment=toupper(size$experiment)
size$combi=paste(size$experiment, size$tray, size$row, size$col, sep="_")
color=read.table("./data/color_rosettes.txt", sep="\t", h=T)
color$exp=toupper(color$exp)
color$combi=paste(color$exp, color$tray, color$row, color$col, sep="_")

##I need to keep only one time point per year per experiment in this file.
size=size[(size$date=="20111119" & size$experiment=="ULL")==F,]
size=size[(size$date=="20121120" & size$experiment=="RAT")==F,]
color=color[(color$date=="20111119" & color$exp=="ULL")==F,]
color=color[(color$date=="20121120" & color$exp=="RAT")==F,]

##compare the area column from the color and size data
size$area_color=color$area[match(size$combi, color$combi)]
plot(size$area, size$area_color, col=as.factor(size$experiment))
```

![](main[exported]_files/figure-html/mergephen1-1.png)<!-- -->

```r
##Here the area in pixel number (in color) and the area in cm^2 (size) doesn't have a correlation of one. it might be related to the difference in height at which the pictures were taken (Svante vs. Ben), the number of pixels on the cameras (Mia's Camera vs Nikon D60 later)...

##the stockiness is not in the size data.
size$stockiness=(4*pi*size$area/(size$perimeter^2))

#################################################
#######   2011 experiments   ####################
#################################################

##read the survival data

surv=read.table("./data/survival_2011.txt", h=T, sep="\t")
surv$id=acc$lines[match(surv$line, acc$tubes)]
surv$name=acc$name[match(surv$line, acc$tubes)]

##clean up surv (typos).

surv$spring[surv$spring==11]=1
surv$spring[surv$spring==10]=NA
surv$spring[surv$spring=="?"]=NA
surv$spring[surv$spring=="-"]=NA
surv=droplevels(surv)

##subset size and color to keep only the 2011 data

size2011=size[size$year==2011,]
color2011=color[color$year==2011,]

##read the herbivory data scored in RAT

herb=read.table("./data/rat.snail.2011.csv", sep=",", h=T)[,1:5]

##read the last fecundity dataset for 2011

fecundity=read.table("./data/fecundity_2011.txt", sep="\t", h=T)
##swopping rows and cols for fecundity to be consistent with the other datasets
#x=colnames(fecundity)[c(1:3, 5, 4, 6:ncol(fecundity))]
#colnames(fecundity)=x

##now merge all this, based on surv.

surv$combi=paste(surv$exp, surv$tray, surv$row, surv$column, sep="_")
fecundity$combi=paste(fecundity$exp, fecundity$tray, fecundity$row, fecundity$col, sep="_")
herb$combi=paste("RAT", herb$tray, herb$row, herb$column, sep="_")

##now built the data table.

data2011=surv

##having the planting date in the data would be nice.

dates=data.frame(expand.grid(c("ADA", "RAM", "ULL", "RAT"), c("A", "B", "C")))
dates=dates[order(dates[,1]),]
colnames(dates)=c("exp", "block")
dates$planting=as.Date(c("2011-08-08","2011-08-10", "2011-08-12", "2011-08-07", "2011-08-09", "2011-08-11", "2011-08-31","2011-09-02", "2011-09-4", "2011-09-01", "2011-09-03", "2011-09-05"))
dates$comb=paste(dates$exp, dates$block, sep="_")
comb=paste(data2011$exp,data2011$block, sep="_")
data2011$planting_date=dates$planting[match(comb, dates$comb)]

##match in the phenotypes from size2011
data2011$rosette_date=size2011[match(data2011$combi, size2011$combi),"date"]
data2011$area=size2011[match(data2011$combi, size2011$combi),"area"]
data2011$perimeter=size2011[match(data2011$combi, size2011$combi),"perimeter"]
data2011$max_diameter=size2011[match(data2011$combi, size2011$combi),"max_diameter"]
data2011$sdR=size2011[match(data2011$combi, size2011$combi),"sdR"]
data2011$circle_area=size2011[match(data2011$combi, size2011$combi),"circle_area"]
data2011$stockiness=size2011[match(data2011$combi, size2011$combi),"stockiness"]
##match in the color phenotype
data2011$color=color2011[match(data2011$combi, color2011$combi),"color"]
##match in the herbivory scores
data2011$herbivory=herb[match(data2011$combi, herb$combi),"slug"]
##match in the fecundity estimates
data2011$fecundity=fecundity$fecundity[match(data2011$combi, fecundity$combi)]
##make the rosette_date a working date column.
x=data2011$rosette_date
y=as.Date(paste(substring(x, 1, 4),substring(x, 5, 6), substring(x, 7,8), sep="-"))
data2011$rosette_date=y
##reorder the columns a little
data2011=data2011[,c("exp", "block", "tray", "row", "column", "line", "id", "name", "planting_date", "errors", "fall", "spring", "sampled", "rosette_date", "area", "perimeter", "max_diameter", "sdR", "circle_area", "stockiness","color", "herbivory", "fecundity")]
##save it!
saveRDS(data2011, file="./data/data2011.rds")
write.table(data2011, "./data/data2011.txt", col.names=T, row.names=F, quote=F, sep="\t")


#################################################
#######   2012 experiments   ####################
#################################################

##read the survival data
surv=read.table("./data/survival_2012.txt", h=T, sep="\t")
surv$id=acc$lines[match(surv$line, acc$tubes)]
surv$name=acc$name[match(surv$line, acc$tubes)]

##put it in better shape
surv$combi=paste(surv$exp, surv$tray, surv$row, surv$column, sep="_")
surv$block[surv$tray<=27]="A"
surv$block[surv$tray>=28 & surv$tray<=54]="B"
surv$block[surv$tray>=55]="C"

##subset size to keep only data for the 2012 experiments
size2012=size[size$year==2012,]
color2012=color[color$year==2012,]

##read some flowering time data, from the North (FTN) and the South (FTS)
FTS=read.table("./data/FT_2012_South_dates.txt", sep="\t", h=T)
FTN=read.table("./data/FT_2012_North_dates.txt", sep="\t", h=T)

##read the 2012 fecundity estimates
fecundity=read.table("./data/fecundity_2012.txt", sep="\t", h=T)
##swopping rows and cols for fecundity.
x=colnames(fecundity)
#[c(1:3, 5, 4, 6:ncol(fecundity))]
#colnames(fecundity)=x
fecundity$combi=paste(fecundity$exp, fecundity$tray, fecundity$row, fecundity$col, sep="_")

##combine all data based on surv.

data2012=surv

##clean up the surv column

data2012$survival_03162013[data2012$survival_03162013==11]=1
data2012$survival_03162013[data2012$survival_03162013==","]=NA
data2012=droplevels(data2012)

##add the fall flowering and survival column from the phenotyping Rod did on the images.

for(e in c("ADA", "RAM", "ULL", "RAT")){
if(e=="ADA"){fbw=cbind(e, read.table(paste("./data/fbw_2012_", e, ".txt", sep=""), sep="\t", h=T))}else{fbw=rbind(fbw, cbind(e, read.table(paste("../all_phenotypes/fbw_2012/fbw_2012_", e, ".txt", sep=""), sep="\t", h=T)))}
}
fbw$combi=paste(fbw$e, fbw$tray, fbw$row, fbw$column, sep="_")


##clean it up

fbw[fbw$ft_fall=="9","ft_fall"]=0
fbw[fbw$ft_fall=="no photo","ft_fall"]=NA

##use this flowering before winter as a fall column. It's the same as what was done in 2011 but photos were phenotype the same evening for that.
data2012$fall=fbw[match(data2012$combi, fbw$combi),"ft_fall"]

##add planting date

dates=data.frame(expand.grid(c("ADA", "RAM", "ULL", "RAT"), c("A", "B", "C")))
dates=dates[order(dates[,1]),]
colnames(dates)=c("exp", "block")
dates$planting=as.Date(c("2012-08-08","2012-08-10", "2012-08-12", "2012-08-07", "2012-08-09", "2012-08-11", "2012-08-31","2012-09-02", "2012-09-4", "2012-09-01", "2012-09-03", "2012-09-05"))
dates$comb=paste(dates$exp, dates$block, sep="_")
comb=paste(data2012$exp,data2012$block, sep="_")
data2012$planting_date=dates$planting[match(comb, dates$comb)]

##add the rosette data
data2012$rosette_date=size2012[match(data2012$combi, size2012$combi),"date"]
data2012$area=size2012[match(data2012$combi, size2012$combi),"area"]
data2012$perimeter=size2012[match(data2012$combi, size2012$combi),"perimeter"]
data2012$max_diameter=size2012[match(data2012$combi, size2012$combi),"max_diameter"]
data2012$sdR=size2012[match(data2012$combi, size2012$combi),"sdR"]
data2012$circle_area=size2012[match(data2012$combi, size2012$combi),"circle_area"]
data2012$stockiness=size2012[match(data2012$combi, size2012$combi),"stockiness"]
##add the color data
data2012$color=color2012[match(data2012$combi, color2012$combi),"color"]
##add the fecundity data
data2012$fecundity=fecundity$fit[match(data2012$combi, fecundity$combi)]

##add flowering time in the spring data (not available for 2011)
colnames(FTS)[5]="col"
FT=rbind(FTN, FTS)

FT$combi=paste(FT$exp, FT$tray,FT$row, FT$col, sep="_")
data2012$flowering_date=FT[match(data2012$combi, FT$combi),"flowering_date"]
data2012$FT=FT[match(data2012$combi, FT$combi),"FT"]

##make the rosette_date a working date column.

x=data2012$rosette_date
y=as.Date(paste(substring(x, 1, 4),substring(x, 5, 6), substring(x, 7,8), sep="-"))
data2012$rosette_date=y

##reorder col

data2012=data2012[,c("exp", "block", "tray", "row", "column", "line", "id", "name", "planting_date", "errors", "fall", "survival_03162013", "epi", "PC_sampled", "rosette_date", "area", "perimeter", "max_diameter", "sdR", "circle_area", "stockiness", "color", "flowering_date", "FT", "fecundity")]

colnames(data2012)[match("survival_03162013", colnames(data2012))]="spring"

##save it:
saveRDS(data2012, file="./data/data2012.rds")
write.table(data2012, "./data/data2012.txt", col.names=T, row.names=F, quote=F, sep="\t")
```

This results in two files, one for each year. There are named
data\_2011.txt (or .R for the binary version) and data\_2012.txt (or
.R for the binary version).

The column names for each files are summarized below.

## columns in data2011

- exp: name of the experiment  
- block: experimental block within the experiment  
- tray: tray within the experiments  
row: coordinate 1 of position of the plants on a tray (varies from 1
to 11)  
- column: coordinate 2 of position of the plants on a tray (varies from 1
to 6)  
- line: number from 1 to 203 designating the accessions planted  
- id: accession id of the accession as refered to in the call\_method\_75
of the 250 KSNPs data.  
- name: actual name of the accession (might be messed up by encoding,
use ids!!)  
- planting\_date: the planting date. All plants of the same block within
the same experiment have the same date.  
- errors: errors during planting (only use lines with ".")  
- fall: score of survival,flowering, and pathogene infections the
survival code  
- spring: score of survival,flowering, and pathogene infections the
survival code  
- sampled: TRUE if the sample has been sample for microbial community
analysis in the spring.Otherwise FALSE.  
- rosette\_date: Date at which the photographe that was use to make
rosette measurements was taken.  
- area: rosette area on the photograph in cm2  
- perimeter: rosette perimiter (cm)  
- max\_diameter: maximum diameter of the plant.  
- sdR: standard deviation of the plants radius  
- circle\_area: area of a circle of diameter "max\_diameter"  
- stockiness: measure of plant stockiness: (4*pi*area)/(perimeter)^2  
- color: a measure of color variation from green to purple. lower values
are greener.  
- herbivory: herbivore damage score from 0 to 3, 0: no damage, 3:
extensive damage.  
- fecundity: area of occupied by the mature plant stems (number of
pixels of the image that are the plant, not the area of the bounding
box) as a proportion of the total number of pixel on the image.  


## columns for data2012 (only columns that are different):

- epi: sampled by Fernando and Manu for RNASeq.  
- PC\_sampled: TRUE if the sample has been sample for microbial community
analysis in the spring.Otherwise FALSE.  
- flowering\_date: the date at which the plant was scored as flowered (if
reading from the text file, in R,  as.Date() will turn it to a date
format.  
- FT: time from planting to the date the plant was scored as
  flowered.  



## Add some derived phenotype columons and clean up

This next chunck of script does some cleaning up, and compute derive
phenotypes such as ow, sss, fitness (composite of fecundity and sss).


```r
read_chunk("./scripts/deriv_phen.R")
```
### rational
In Both 2011 and 2012 we have scored survival in the spring and we
have harvested stems at the end of the plants life cycle.
We also have information about flowering time before winter for both
year.
I'll try to create to survival related columns in the data. The first
is going to be overwinter survival (ows). For ows our data are
relatively precise because we have survival/flowering scores before
and after winter. The second survival phenotype is going to be
survival until seed set (sss). This is will include the plants that
survived over-winter, plus the plants we don't have stem flowering
data and stem photos for. This is likely to be a little
unprecise. First we are missing some stems, because some were
missing, we disgarded some while taking the photos when they were in
such a state a photo wasn't meaningful. However that's probably quite
rare over the number of stems we photographed. Second, some dead
plant might have "flown" away from the experiments before we
harvested them. That again should only be a small
fraction. Nevertheless, we should keep those limitations in mind.

### Over-winter survival (ows)
To considered a plant has survived winter (ows=1) it needs to have been alive in
the last sensus in the fall and be alive in the spring. Following table \ref{survivalscores},that means it must have a value of 1, 2, 4, or 5 in the fall and
spring column. Whether it was sampled or not doesn't matter in 2011
because the only sampling we did was in the spring after the
survival/flowering sensus was made. To be considered having not
survived winter, plants must have been present in the last sensus in
the fall, but be dead or absent in the spring. ##A0002##
For the 2012, data, it's basically the same except we beed to
take into account the epi genetics sampling (epi column). This
sampling happened in Oct or November.##

### Survival until seed set (sss).
That I'll define as surviving until seed set (sss) as having survived
over winter and having data in the fecundity column. ##A0004##
They must not have been harvested for micorbiom

### Checking if it makes sens.
I want to know how many of the plants that are scored as having died,
have produced a stem that we have (Table \ref{counts}). ##A0005##
Basically there are only a few plants died.


### need to finish adding details from other org note file from
previous analysis


Table: Number of data points for each value of ows and sss in the 2011 experiments

ows   sss     Freq
----  ----  ------
0     0       1860
1     0       1588
NA    0       1011
0     1        166
1     1      11305
NA    1         33
0     NA         1
1     NA      3276
NA    NA      2144



Table: Number of data points for each value of ows and sss in the 2012 experiments

ows   sss     Freq
----  ----  ------
0     0        245
1     0       1174
NA    0          0
0     1         20
1     1      10263
NA    1          0
0     NA         0
1     NA      4437
NA    NA      5245

```
##    sss
## ows     1
##   0   166
##   1 11305
```

```
##    sss
## ows     1
##   0    20
##   1 10263
```

```
##       exp block tray row column  line   id name planting_date errors fall
## 27    ULL     A    1   5      3 empty <NA> <NA>    2011-08-31      .    1
## 1906  ULL     B   29  10      4 empty <NA> <NA>    2011-09-02      .    1
## 2671  ULL     B   41   6      1 empty <NA> <NA>    2011-09-02      .    1
## 2710  ULL     B   42   1      4 empty <NA> <NA>    2011-09-02      .    1
## 3519  ULL     B   54   4      3 empty <NA> <NA>    2011-09-02      .    1
## 5638  RAT     A    5   5      4 empty <NA> <NA>    2011-09-01      .    1
## 5922  RAT     A    9   8      6 empty <NA> <NA>    2011-09-01      .    1
## 7335  RAT     B   31   2      3 empty <NA> <NA>    2011-09-03      .    1
## 10946 RAM     A    4  10      2 empty <NA> <NA>    2011-08-07      .    2
## 12366 RAM     A   26   4      6 empty <NA> <NA>    2011-08-07      .    1
## 14744 RAM     C   62   5      2 empty <NA> <NA>    2011-08-11      .    1
## 15417 RAM     C   72   7      3 empty <NA> <NA>    2011-08-11      .    2
## 16322 ADA     A    5   4      2 empty <NA> <NA>    2011-08-08      .    1
## 19018 ADA     B   46   2      4 empty <NA> <NA>    2011-08-10      .    2
## 19826 ADA     C   58   5      2 empty <NA> <NA>    2011-08-12      .    1
## 20825 ADA     C   73   6      5 empty <NA> <NA>    2011-08-12      .    2
## 21075 ADA     C   77   4      3 empty <NA> <NA>    2011-08-12      .    1
## 21343 ADA     C   81   5      1 empty <NA> <NA>    2011-08-12      .    1
##       spring sampled rosette_date       area perimeter max_diameter
## 27         1   FALSE   2011-11-01  0.1644465   2.26089    0.6358726
## 1906       1   FALSE   2011-11-01 10.7207054  18.87364    4.4923238
## 2671       1   FALSE         <NA>         NA        NA           NA
## 2710       1   FALSE   2011-11-01  4.9950378  14.00717    3.0131634
## 3519       1   FALSE   2011-11-01  8.5278268  19.42155    4.0276701
## 5638       1   FALSE         <NA>         NA        NA           NA
## 5922       1   FALSE   2011-11-01  7.3085737  16.06244    3.9042611
## 7335       1   FALSE   2011-11-01  6.3106981  16.61579    3.6469942
## 10946      3   FALSE         <NA>         NA        NA           NA
## 12366      1   FALSE         <NA>         NA        NA           NA
## 14744      1   FALSE         <NA>         NA        NA           NA
## 15417      0   FALSE         <NA>         NA        NA           NA
## 16322      0   FALSE         <NA>         NA        NA           NA
## 19018      0   FALSE         <NA>         NA        NA           NA
## 19826      0   FALSE         <NA>         NA        NA           NA
## 20825      0   FALSE         <NA>         NA        NA           NA
## 21075      1   FALSE         <NA>         NA        NA           NA
## 21343      1    TRUE         <NA>         NA        NA           NA
##             sdR circle_area stockiness         color herbivory
## 27    0.1597015   0.3175632  0.4042737  0.0387945875        NA
## 1906  1.3388041  15.8500993  0.3782004  0.0227967715        NA
## 2671         NA          NA         NA  0.0001529256        NA
## 2710  0.8995149   7.1307505  0.3199249 -0.0070943860        NA
## 3519  1.1267250  12.7408282  0.2841061 -0.0044856454        NA
## 5638         NA          NA         NA -0.0208655204         1
## 5922  1.1088705  11.9720243  0.3559751 -0.0375922040         2
## 7335  1.0108851  10.4462406  0.2872404 -0.0109174673         2
## 10946        NA          NA         NA            NA        NA
## 12366        NA          NA         NA            NA        NA
## 14744        NA          NA         NA            NA        NA
## 15417        NA          NA         NA            NA        NA
## 16322        NA          NA         NA            NA        NA
## 19018        NA          NA         NA            NA        NA
## 19826        NA          NA         NA            NA        NA
## 20825        NA          NA         NA            NA        NA
## 21075        NA          NA         NA            NA        NA
## 21343        NA          NA         NA            NA        NA
##          fecundity ows sss
## 27    0.0001391575   1   1
## 1906  0.0019261142   1   1
## 2671  0.0086442599   1   1
## 2710  0.0036855498   1   1
## 3519  0.0121254426   1   1
## 5638  0.0212770067   1   1
## 5922  0.0102497566   1   1
## 7335  0.0116773049   1   1
## 10946           NA   0   0
## 12366           NA   1   0
## 14744 0.0003396863   1   1
## 15417           NA   0   0
## 16322           NA   0   0
## 19018           NA   0   0
## 19826           NA   0   0
## 20825           NA   0   0
## 21075 0.0033375336   1   1
## 21343           NA   1  NA
```

```
##       exp block tray row column  line   id name planting_date errors fall
## 27    ULL     A    1   5      3 empty <NA> <NA>    2011-08-31      .    1
## 1906  ULL     B   29  10      4 empty <NA> <NA>    2011-09-02      .    1
## 2671  ULL     B   41   6      1 empty <NA> <NA>    2011-09-02      .    1
## 2710  ULL     B   42   1      4 empty <NA> <NA>    2011-09-02      .    1
## 3519  ULL     B   54   4      3 empty <NA> <NA>    2011-09-02      .    1
## 5638  RAT     A    5   5      4 empty <NA> <NA>    2011-09-01      .    1
## 5922  RAT     A    9   8      6 empty <NA> <NA>    2011-09-01      .    1
## 7335  RAT     B   31   2      3 empty <NA> <NA>    2011-09-03      .    1
## 10946 RAM     A    4  10      2 empty <NA> <NA>    2011-08-07      .    2
## 12366 RAM     A   26   4      6 empty <NA> <NA>    2011-08-07      .    1
## 14744 RAM     C   62   5      2 empty <NA> <NA>    2011-08-11      .    1
## 15417 RAM     C   72   7      3 empty <NA> <NA>    2011-08-11      .    2
## 16322 ADA     A    5   4      2 empty <NA> <NA>    2011-08-08      .    1
## 19018 ADA     B   46   2      4 empty <NA> <NA>    2011-08-10      .    2
## 19826 ADA     C   58   5      2 empty <NA> <NA>    2011-08-12      .    1
## 20825 ADA     C   73   6      5 empty <NA> <NA>    2011-08-12      .    2
## 21075 ADA     C   77   4      3 empty <NA> <NA>    2011-08-12      .    1
## 21343 ADA     C   81   5      1 empty <NA> <NA>    2011-08-12      .    1
##       spring sampled rosette_date       area perimeter max_diameter
## 27         1   FALSE   2011-11-01  0.1644465   2.26089    0.6358726
## 1906       1   FALSE   2011-11-01 10.7207054  18.87364    4.4923238
## 2671       1   FALSE         <NA>         NA        NA           NA
## 2710       1   FALSE   2011-11-01  4.9950378  14.00717    3.0131634
## 3519       1   FALSE   2011-11-01  8.5278268  19.42155    4.0276701
## 5638       1   FALSE         <NA>         NA        NA           NA
## 5922       1   FALSE   2011-11-01  7.3085737  16.06244    3.9042611
## 7335       1   FALSE   2011-11-01  6.3106981  16.61579    3.6469942
## 10946      3   FALSE         <NA>         NA        NA           NA
## 12366      1   FALSE         <NA>         NA        NA           NA
## 14744      1   FALSE         <NA>         NA        NA           NA
## 15417      0   FALSE         <NA>         NA        NA           NA
## 16322      0   FALSE         <NA>         NA        NA           NA
## 19018      0   FALSE         <NA>         NA        NA           NA
## 19826      0   FALSE         <NA>         NA        NA           NA
## 20825      0   FALSE         <NA>         NA        NA           NA
## 21075      1   FALSE         <NA>         NA        NA           NA
## 21343      1    TRUE         <NA>         NA        NA           NA
##             sdR circle_area stockiness         color herbivory
## 27    0.1597015   0.3175632  0.4042737  0.0387945875        NA
## 1906  1.3388041  15.8500993  0.3782004  0.0227967715        NA
## 2671         NA          NA         NA  0.0001529256        NA
## 2710  0.8995149   7.1307505  0.3199249 -0.0070943860        NA
## 3519  1.1267250  12.7408282  0.2841061 -0.0044856454        NA
## 5638         NA          NA         NA -0.0208655204         1
## 5922  1.1088705  11.9720243  0.3559751 -0.0375922040         2
## 7335  1.0108851  10.4462406  0.2872404 -0.0109174673         2
## 10946        NA          NA         NA            NA        NA
## 12366        NA          NA         NA            NA        NA
## 14744        NA          NA         NA            NA        NA
## 15417        NA          NA         NA            NA        NA
## 16322        NA          NA         NA            NA        NA
## 19018        NA          NA         NA            NA        NA
## 19826        NA          NA         NA            NA        NA
## 20825        NA          NA         NA            NA        NA
## 21075        NA          NA         NA            NA        NA
## 21343        NA          NA         NA            NA        NA
##          fecundity ows sss
## 27    0.0001391575   1   1
## 1906  0.0019261142   1   1
## 2671  0.0086442599   1   1
## 2710  0.0036855498   1   1
## 3519  0.0121254426   1   1
## 5638  0.0212770067   1   1
## 5922  0.0102497566   1   1
## 7335  0.0116773049   1   1
## 10946           NA   0   0
## 12366           NA   1   0
## 14744 0.0003396863   1   1
## 15417           NA   0   0
## 16322           NA   0   0
## 19018           NA   0   0
## 19826           NA   0   0
## 20825           NA   0   0
## 21075 0.0033375336   1   1
## 21343           NA   1  NA
```

```
## 
##  ADA  RAM  RAT  ULL 
##    0    0 3424 4709
```

```
## 
##  ADA  RAM  RAT  ULL 
## 3958 4901 3851 3705
```

# Compute heritabilities and blups per accession


```r
read_chunk("./scripts/heritability.R")
```
This is slow, because of the bootstrapping. Set to not run. Also for
now the number of bootstraps is set to 100, but in the final version
this needs to be parallelized and increased to 1000.


```r
library(lme4)
library(HLMdiag)
library(boot)
library(gplots)

source("./scripts/heritability_functions.R")

##Variance explained by ID within

d11=readRDS(file="./data/d11_for_analysis.rds")
d12=readRDS(file="./data/d12_for_analysis.rds")

acclist=read.table("./data/acc_list.txt", h=T, sep="\t")
acclist=acclist[acclist$tubes<=200,]
###histograms of trait distributions.
##A0013##
##d11
pdf("./figures/dist_d11.pdf", paper="special", height=8, width=8)
par(mfrow=c(3, 4), mar=c(5, 5, 1, 1))
for (t in c("area", "perimeter", "max_diameter", "sdR", "circle_area", "stockiness", "color", "herbivory", "fecundity", "ows", "sss", "fitness")){
    hist(d11[,t], breaks=50, main=t, xlab="")
}
dev.off()

##d12
pdf("./figures/dist_d12.pdf", paper="special", height=8, width=8)
par(mfrow=c(3, 4), mar=c(5, 5, 1, 1))
for (t in c("area", "perimeter", "max_diameter", "sdR", "circle_area", "stockiness", "color", "FT", "fecundity", "ows", "sss", "fitness")){
    hist(d12[,t], breaks=50, main=t, xlab="")
}
dev.off()

##Heritability estimates by exp and by year.

###traits=c("area", "perimeter", "max_diameter", "sdR", "circle_area", "stockiness", "color", "herbivory", "FT", "fecundity", "ows", "sss", "fitness")

traits=c("area", "stockiness", "color", "FT", "fecundity", "ows", "sss", "fitness")

##set the family distribution for each trait.

fam=data.frame(traits, family=c("log", "log", "log", "log", "sqrt", "bn", "bn", "sqrt"))
exps=sort(c("ULL", "RAT", "ADA", "RAM"))

##loop over years, and experiments and fill a dataframe with the H2 and CI

res=data.frame(matrix(ncol=6))
colnames(res)=c("year", "experiment" ,"trait", "H2", "CI_low", "CI_high")
m=data.frame(id=unique(c(paste(d11$id), paste(d12$id))))
l=1
blups=data.frame(ID=acclist[,1])
for(year in c(2011, 2012)){
    for(e in exps){
        if(year==2011){
            sub=droplevels(d11[d11$exp==e,])
        }
        if(year==2012){
            sub=droplevels(d12[d12$exp==e,])
        }
        s=sub[,c("exp", "block","id", traits[traits%in%colnames(sub)]),]
        ##remove columns with just NAs
        s=s[,apply(s, 2, function(x){any(is.na(x)==F)})]
        ##remove herbivory for now because I can't make it fit.
        s=s[,colnames(s)!="herbivory"]
        ##fill in res
        res[l:(l+(ncol(s)-4)),1]=paste(year)
        res[l:(l+(ncol(s)-4)),2]=e
        res[l:(l+(ncol(s)-4)),3]=colnames(s)[4:ncol(s)]
        ##models and H2 estimates
        for(t in colnames(s)[4:ncol(s)]){
            f=fam[match(t, fam$traits), 2]
            if(f=="log"){
                model=formula(paste("log(", t, "+1)~block + (1|id)", sep=""))
                fit=lmer(model, data=s)}
            if(f=="g"){
                model=formula(paste(t, "~block + (1|id)", sep=""))
                fit=lmer(model, data=s)}
            if(f=="sqrt"){
                model=formula(paste("sqrt(", t, ")~block + (1|id)", sep=""))
                fit=lmer(model, data=s)}
            if(f=="bn"){
                model=formula(paste(t,"~block + (1|id)", sep=""))
                fit=glmer(model, data=s, family="binomial")}
            if(f=="nb"){
                model=formula(paste(t,"~block + (1|id)", sep=""))
                fit=glmer.nb(model, data=s)}
            H2=var_ID(fit);ci=get_CI(fit, var_ID, nsim=100)
            res[l,4:6]=c(H2, ci[1], ci[2]); l=l+1
            b=ranef(fit)$id; b=data.frame(id=row.names(b), t=b[,1])
            blups[,paste(year, "_",e, "_", t, sep="")]=b[match(blups[,1], b[,1]),2]  ##A0018##
        }
    }
}

write.table(res, "./res/H2_2011_2012.txt", sep="|",col.names=T, row.names=F, quote=F)
write.table(blups, "./res/blups.txt", sep="\t",col.names=T, row.names=F, quote=F)

res$H2=round(res$H2, 3)
res$CI_low=round(res$CI_low, 3)
res$CI_high=round(res$CI_high, 3)

##making a figure.

res=read.table("./res/H2_2011_2012.txt", sep="|",h=T)
res=res[order(res[,3]),]
res=res[order(res[,2]),]
res=res[order(res[,1]),]

cols=c("dodgerblue1", "dodgerblue4", "gold3", "firebrick4")[as.numeric(as.factor(res$experiment))]
pdf("./figures/H2.pdf", paper="special", width=12, height=5)
par(mar=c(8, 5, 1, 1))
plot(0, 0, xaxt="n", type="n", xlim=c(0.5, nrow(res)+0.5), ylim=c(0, 1), ylab="H2", xlab="")
for (i in 1:nrow(res)){
    rect(i-0.5,0,i+0.5, res[i, 4], col=cols[i])
    segments(i,res[i, 5],i, res[i, 6], col=1)
}
axis(1, labels=res[,3], at=1:nrow(res), las=2, cex.lab=0.6)
legend("topright", legend=levels(as.factor(res$experiment)), col=c("dodgerblue1", "dodgerblue4", "gold3", "firebrick4"), pch=15)
segments(which(res[,1]==2012)[1]-0.5, 0,which(res[,1]==2012)[1]-0.5, 1)
text((which(res[,1]==2012)[1]-0.5)/2, 0.9, "2011", cex=3)
text(which(res[,1]==2012)[1]-0.5+(nrow(res)-which(res[,1]==2012)[1]-0.5)/2 , 0.9, "2012", cex=3)
dev.off()
```
![Heritability of traits in the common garden experiments.](./figures/H2.pdf)

In the next chunk we compute heritability estimates just for the fitness trait (again, this is slow due to bootstrapping so not evaluated in the markdown). 


```r
##H2 estimate for just fitness

exps=c("ULL", "RAT", "ADA", "RAM")
res=data.frame(matrix(ncol=6))
colnames(res)=c("year", "experiment" ,"trait", "H2", "CI_low", "CI_high")
m=data.frame(id=unique(c(paste(d11$id), paste(d12$id))))
l=1
for(year in c(2011, 2012)){
    for(e in exps){
        if(year==2011){
            sub=droplevels(d11[d11$exp==e,])
        }
        if(year==2012){
            sub=droplevels(d12[d12$exp==e,])
        }
        s=sub[,c("exp", "block","id", "fitness")]##traits[traits%in%colnames(sub)]),]
        ##fill in res
        res[l:(l+(ncol(s)-4)),1]=paste(year)
        res[l:(l+(ncol(s)-4)),2]=e
        res[l:(l+(ncol(s)-4)),3]=colnames(s)[4:ncol(s)]
        ##models and H2 estimates
        model=formula(paste("log(fitness+1)~block + (1|id)", sep=""))
        fit=lmer(model, data=s)
        H2=var_ID(fit);ci=get_CI(fit, var_ID, nsim=1000)
        res[l,4:6]=c(H2, ci[1], ci[2]); l=l+1
    }
}

write.table(res, "./res/H2_fitness.txt", sep="|",col.names=T, row.names=F, quote=F)
res$H2=round(res$H2*100, 2)
res$CI_low=round(res$CI_low*100, 2)
res$CI_high=round(res$CI_high*100, 2)
write.table(res, "./res/H2_fitness_percent_rounded.txt", sep="|",col.names=T, row.names=F, quote=F)
```
The next table gives the fitness heritability estimates and 95\% confidence intervals (from 1000 bootstrapts)


Table: Broad sens Heritability estimates for fitness in each experiment

 year  experiment   trait         H2   CI_low   CI_high
-----  -----------  --------  ------  -------  --------
 2011  ULL          fitness    12.38     9.25     15.36
 2011  RAT          fitness    12.03     9.10     15.22
 2011  ADA          fitness     6.24     4.12      8.40
 2011  RAM          fitness    13.45    10.29     16.59
 2012  ULL          fitness    19.70    15.30     24.40
 2012  RAT          fitness     7.30     4.68      9.94
 2012  ADA          fitness    10.46     7.35     13.56
 2012  RAM          fitness    10.28     7.08     13.36

We then need to compute "fitness in the North" and "fitness in the
South" for comparison with allele frequencies changes.


```r
##calculate residual means per after regressing the block effect.

source("./scripts/custom_functions.R")

d11=readRDS(file="./data/d11_for_analysis.rds")
d12=readRDS(file="./data/d12_for_analysis.rds")

##restric to fitness
traits=c("fitness")

d11$year=2011
d12$year=2012

d11=d11[, c("year", "exp", "block", "id", "fitness")]
d12=d12[, c("year", "exp", "block", "id", "fitness")]

d=rbind(d11, d12)
d$region[d$exp%in%c("ULL", "RAT")]="S"
d$region[d$exp%in%c("RAM", "ADA")]="N"
d$year=as.factor(d$year)
d=d[, c("year", "region", "exp","block", "id", "fitness")]
d=na.omit(d)
##recode block

###within region, remove exp effect and block within ##A0022##

m=data.frame(id=unique(c(paste(d11$id), paste(d12$id))))
l=1
for (R in c("N", "S")){
    dat=d[d$region==R,]
    model=formula(paste("fitness~0+block%in%exp%in%year", sep=""))
    fit=glm(model, data=dat)
    r=fit$residuals+mean(fit$coefficients) ##here I the mean effect of the experimental units to keep a scale to compare the two regions
    b=by(r, dat$id[is.na(dat[,"fitness"])==F], betterMean, N=3)
    b=data.frame(id=names(b), as.numeric(b))
    m[,paste(R, "_fitness", sep="")]=b[match(m[,1], b[,1]),2]
    l=l+1
}

write.table(m, "./res/means_NvsS.txt", sep="\t", col.names=T, row.names=F, quote=F)
```

# Evidence for adaptation based on the common garden experiments

The idea here is to look for a relationship between the fitness
estimates in the North and South of Sweden and the latitude of origin
of the accessions.




```r
library(ggmap)
```

```
## Loading required package: ggplot2
```

```r
library(grid)

####local adaptation on the mean variation of fitness in the N and in the South.

##read in data
phen=read.table("./res/means_NvsS.txt", sep="\t", h=T, stringsAsFactors = T)
clim=read.table("./data/worldclim_swedish_acc.txt", sep="\t", h=T, stringsAsFactor=T )
clim_exp=read.table("./data/worldclim_swedish_exp.txt", , sep="\t", h=T)
colnames(clim)[1]="id"

##merge
phen=merge(phen, clim[, c(1, 3, 5,6)])

##plot and estimate correlations
bootCor=function(x, N=1000,method="spearman"){
    res=c()
    for(i in 1:N){
        s=sample(1:nrow(x), size=nrow(x), replace=T)
        res=c(res, cor.test(x[s,1], x[s,2], method=method)$estimate)
    }
    return(res)
}

rN=bootCor(phen[,c("lat", "N_fitness")])
rS=bootCor(phen[,c("lat", "S_fitness")])

##reorder factor levels for the region

phen$region = factor(phen$region,levels(phen$region)[c(2, 1, 3)])

pdf("./figures/fitness_N_S_lat.pdf", paper="special", height=6, width=4)
m=matrix(c(1,1, 2,2,3,3, 3, 3,4,4,4,5), ncol=4, byrow=T)
layout(m)
par(mar=c(3, 3, 1, 1), mgp=c(1.5, 0.5, 0))
plot(phen$lat, phen$N_fitness, cex=1, col="dodgerblue3", pch=16, xlab="latitude", ylab="fitness in the North", ylim=c(0, 0.012))
legend("topright", "A", bty="n", cex=1.5)
plot(phen$lat, phen$S_fitness, cex=1, col="firebrick3", pch=16, xlab="latitude", ylab="fitness in the South", ylim=c(0, 0.012))
legend("topright", "B", bty="n", cex=1.5)
hist(rN, xlim=range(c(rN, rS)), ylim=c(0, 100), col="dodgerblue3", breaks=50, main="", xlab="rho")
q=quantile(rN, c(0.025, 0.957))
b=mean(rN)
segments(q, 0,q, 110, col="dodgerblue3", lwd=2, lty=2)
segments(b, 0,b, 110, col="dodgerblue3", lwd=2, lty=1)
hist(rS, xlim=range(c(rN, rS)), col="firebrick3", add=T, breaks=50)
q=quantile(rS, c(0.025, 0.957))
b=mean(rS)
segments(b, 0,b, 110, col="firebrick3", lwd=2, lty=1)
segments(q, 0,q, 110, col="firebrick3", lwd=2, lty=2)
box()
legend("topright", "C", bty="n", cex=1.5)
mypal=colorRampPalette(c("firebrick3", "dodgerblue3"))
cols=mypal(200)
r=range(phen$lat, na.rm=T)
plot(phen$N_fitness, phen$S_fitness, col=cols[findInterval(phen$lat, seq(r[1],r[2],length=200))], pch=16, cex=1, xlab="fitness in the North", ylab="fitness in the South")
abline(0,1, lwd=1, col=1, lty=2)
x=cor.test(phen$N_fitness, phen$S_fitness, method="spearman")
legend("topright", "D", cex=1.5,bty="n")
legend("topleft",legend=bquote(paste(rho, "=", .(round(x$estimate, 3)), "; ", italic(p),"-value = ", .(format(x$p.value, scientific=T, digits=3)))), cex=0.8, bty="n")
par(mar=c(3, 0.2 ,1, 1))
plot(0, 0, xlim=c(0,2), ylim=c(0, 1), type="n", bty="n", axes=F, xlab="", ylab="")
for(i in 1:200){
    rect(0,(i-1)/200,0.3,i/200, col=cols[i], border=FALSE, lwd=0)
}
a=0
for(i in seq(r[1], r[2], length.out=6)){
    text(0.5, a, round(i, 2), cex=1, adj=-0.5)
    a=a+1/5
}
dev.off()
```

```
## png 
##   2
```

The mean fitness traits were computed to remove the year, experiment and block effects
within regions by fitting a linear model to the fitness data, and then
to compute means of the residuals per accessions. This data is saved in the file "./res/means_NvsS.txt".


This yields the following figure. 

![Fitness estimates from the common garden experiments display evidence for local adaptation across to successive years. A and B: Relationship between the latitude of origin of accessions and fitness in the North (blue) and in the South (red). C: Ditribution of the Spearman's rho rank correlation coefficients between latitude of origin and fitness in the North (blue) and in the South (red), over 1000 non-parametric bootstraps. The blue and red vertical lines delimite the 95% confidence interval for the correlation coefficients in the North and in the South, respectively. D. Genetic correlation between the fitness of accessions in the North (x axis) and in the South (y axis). The color gradient represents the latitude of origine of accessions.](./figures/fitness_N_S_lat.pdf)

_Conclusions:_  
- There is a negative relationship between fitness and latitude of origin in the South, but not in the North, indicating conditional neutrality.  
- Fitness in the North and in the South are positively correlated  
- Plants tend to grow bigger and produce more seeds in the North  


# Fitness estimates from the CG vs change in accession frequency in the NSE



We need to determine is accessions that became frequent in all
experimental evolution experiments are also fitter in the common
gardens.

Daniele provided a file called "./data/accs.per.plot.Rdata" which as
the allele frequency of accessions in the experimental plots after two
winters.

I'll compare those values with the fitness means per accession and per region that used I when looking
for adaptation above.


```r
load("./data/accs.per.plot.Rdata")
er=accs.per.plot

fit=read.table("./res/means_NvsS.txt", h=T, sep="\t")

sites=names(er[,1,1])
plots=names(er[1,,1])

acc=read.table("./data/acc_list.txt", h=T, sep="\t")
acc=droplevels(acc[acc$tubes<=200,])

##are there accessions that are sample more than 2 in each plot in more than one experiments

L=list()
for(s in sites){
    sub=er[s,,]
    rs=rowSums(sub)
    sub=sub[rs>0,]
    x=apply(sub, 2, function(x){sum(x>0)>=(length(x)-1)})
    #print(s)
    print(acc[match(names(x[x==T]), acc$lines),1:2])
    L[[s]]=names(x[x==T])
}
```

```
##     lines              name
## 6    1063 Br\xf6sarp-21-140
## 33   6025             Gro-3
## 118  6252              TV-4
## 127  7516         V\xe5r2-1
## 164  9382             Fri 2
## 192  9476         V\xe5rA 1
##     lines         name
## 8    1254   Tos-82-387
## 47   6073 \xd6M\xf61-7
## 52   6094        T1040
## 71   6133         T800
## 118  6252         TV-4
## 127  7516    V\xe5r2-1
## 143  8326        Lis-1
## 155  9332        Bar 1
## 158  9343        Dja 1
## 169  9392        Had 3
## 173  9407      HolA2 2
## 193  9481        Yst 1
##     lines      name
## 34   6030 Gr\xf6n-5
## 58   6108      T480
## 118  6252      TV-4
## 119  6258     TV-10
## 148  8387      St-0
## 170  9399     Ham 1
##     lines      name
## 40   6040     Kni-1
## 79   6149      T970
## 87   6174 T\xc5D 06
## 95   6193     TDr-7
## 118  6252      TV-4
## 119  6258     TV-10
## 127  7516 V\xe5r2-1
## 158  9343     Dja 1
## 164  9382     Fri 2
## 173  9407   HolA2 2
## 183  9436     Puk 1
## 185  9442     Sim 1
## 188  9452    Spro 3
## 190  9454     Ste 3
## 192  9476 V\xe5rA 1
```

```r
dom=unlist(L)
t1=table(dom)

fittest=names(t1[t1>=3])

for(s in sites){
    sub=er[s,,]
    print(100-100*sum(sub)/350)
}
```

```
## [1] 58.28571
## [1] 33.14286
## [1] 34
## [1] 65.71429
```

```r
pdf("./figures/fit_vs_overall_freq.pdf", paper="special", width=5, height=10, pointsize=8)
layout(matrix(1:8, ncol=2, byrow=T), width=c(2, 1))
par(mar=c(4, 4, 1, 1))
for(s in sites){
    fa=data.frame(id=colnames(er[s,,]), freq=colSums(er[s,,]))
    fa=fa[fa[,2]>0,]
    res=merge(fa, fit, by="id", all=F)
    col=rep("Dodgerblue", nrow(res))
    col[res$id%in%fittest]="firebrick"
    plot(res$N_fitness, res$S_fitness, cex=sqrt(res$freq), pch=16, col=col, xlab="fitness in the North", ylab="fitness in the South")
    abline(0, 1)
    subf=fit[fit$id%in%res$id==F,]
    points(subf$N_fitness, subf$S_fitness, pch=16, col="gold3")
    legend("topright", s, bty="n")
    bp=acc
    bp$neversampled=acc$lines%in%subf$id
    t1=table(bp$region, bp$neversampled)
    S=colSums(t1)
    t1[,1]=t1[,1]/S[1]
    t1[,2]=t1[,2]/S[2]
    barplot(t1, legend=T, names=c(paste("Sampled\n(N=", S[1], ")", sep=""), paste("Not Sampled\n(N=", S[2], ")", sep="")))
}
dev.off()
```

```
## png 
##   2
```

This scripts yields the following figure.

![Accession frequency and common garden fitness estimates. The plots
are organized in 4 lines, one for each of the natural selection
experiments. For each NSE, the plot on the left shows the relationship
between accession fitness in the North (x) and accession fitness in
the South (y). Each point is an accession. Gold dots correspond to
accessions that were not sampled in the NSE. Blue dots are accessions
that were sampled in the NSE and their sizes reflect the frequency of
the accessions in the NSE. Red dots are accessions that were sampled
more than twice in all NSE. The diagonal line has a slope of 1 and
intercept of 0. The barplots on the right break down the frequency of
acccessions from each region in Sweden that were sampled in the NSE,
and those that were not.](./figures/fit_vs_overall_freq.pdf)

What this shows is that the fittest plants in the common gardens are
not always the ones sampled in the NSE experiments, suggesting that
important fitness components are not captured in the commong gardens
(germination and establishement most likely). 

From the barplots, we can see that Northern accessions are more
frequent among sampled accessions than they are in the non-sampled
accessions. In the other experiments, including Barsta in the North,
it seems Northern accessions are at lower frequency in the sampled
accessions than in the non-sampled accessions.

# Genetic bases of adaptation

## GWA of common garden mean fitness estimates

Here we are going to investigates the genetics underlying the North and South fitness estimates. 



This first chunk prepare libraries, and loads data needed for mapping. 


```r
library(gplots)
library(plyr)
library(mixOmics)
library(lme4)
library(igraph)
library(Hmisc)
library(vegan)
library(readr)
library(mashr)
library(Rmosek)
library(REBayes)
require(GenomicRanges)
require(TxDb.Athaliana.BioMart.plantsmart22)
##require(VariantAnnotation)
require(snpStats)
require(dbscan)
require(reshape2)
library(mashr)
txdb=TxDb.Athaliana.BioMart.plantsmart22
genes=genes(txdb)
source("./scripts/GWA_functions.R")
library(gplots)
source("./scripts/modif_heatmap.R")
set.seed(123)

##read the fitness estimates

dat=read.table("./res/means_NvsS.txt", h=T, sep="\t")
row.names(dat)=paste(dat$id)
dat=dat[,-1]

pref="./GWA/snps/sweden_200.bimbam"

##make a phenotype file
acclist=scan(paste(pref, ".acclist", sep=""), sep=",")

##make a table of blups in the same order as the genotype
phen=dat[paste(acclist), ]
phenfile=paste(pref, "_means_NvsS.phen.txt", sep="")
write.table(phen, phenfile, sep="\t", row.names=F, col.names=F)
```

Then we run lmm using gemma for the two traits seperatly. 


```r
##run gemma using gemma-wrapper
system("mkdir ./GWA/output_gemma_loco/")
system(paste("gemma-wrapper --loco --json --cache-dir ./GWA/.gemma-cache -- -gk -g ", pref, ".geno.gz -a ", pref, ".map -p ", phenfile, " > ./GWA/output_gemma_loco/K.json", sep=""))

for(n in 1:ncol(phen)){
    pheno=colnames(phen)[n]
    system(paste("gemma-wrapper --loco --json --cache-dir ./GWA/.gemma-cache --input ./GWA/output_gemma_loco/K.json -- -lmm 4 -g ", pref, ".geno.gz -a ", pref, ".map -p ", phenfile, " -n ", n , " > ./GWA/output_gemma_loco/out_loco_", pheno, sep=""))
}
```

We then read the outputs, make mahattan plots, compute pseudo heritability and retrieve genome wide signigificant SNPs. 
We also retrieve the matrices of betas and their standard error for the next analysis. 


```
## [1] "Fig.  1: Manhattan plot for fitness in the North"
```

<img src="./GWA/manhattans/lmm_gwa_N_fitness.jpeg" width="4800" />

```
## [1] "Fig.  2: Manhattan plot for fitness in the South"
```

<img src="./GWA/manhattans/lmm_gwa_S_fitness.jpeg" width="4800" />



```r
ph2=data.frame(phen=colnames(phen), ph2=NA)
gwsignif=list()
for(n in 1:ncol(phen)){
    pheno=colnames(phen)[n]
    x=unlist(strsplit(scan(paste("./GWA/output_gemma_loco/out_loco_", pheno,sep=""), what="character")[1], ","))[7]
    f=substring(gsub("]]", "", x), 2, (nchar(x)-3))
    f2=gsub("assoc", "log", f)
    ph2$ph2[n]=ph2gemma(f2)
    p=read.delim(f, header=T, sep="\t")
    ##make a manhattan plot
    colnames(p)[3]="pos"
    colnames(p)=gsub("p_lrt", "pval", colnames(p))
    p$score=-log10(p$pval)
    p=p[p$af>=0.1,]
    p$fdr_pval=p.adjust(p$pval, "fdr")
    gwsignif[[pheno]]=p[p$fdr_pval<=0.05,]
    jpeg(paste("./GWA/manhattans/lmm_gwa_", pheno, ".jpeg", sep=""),res=600, unit="in", width=8, height=4, pointsize=12)
    manhattan(p)
    dev.off()
    if(n==1){
        beta=p[, c("rs","beta")]
        se=p[, c("rs", "se")]
    }else{
        beta=merge(p[, c("rs","beta")], beta, by="rs", all=T)
        se=merge(p[, c("rs","se")], se, by="rs", all=T)
    }
}


saveRDS(ph2, "./res/ph2_fitNvsS.rds")
saveRDS(gwsignif, "./res/list_signif_SNPs_fitNvsS.rds")

colnames(beta)=c("rs", colnames(phen))
colnames(se)=c("rs", colnames(phen))
row.names(beta)=beta$rs
row.names(se)=se$rs
beta=beta[, -1]
se=se[,-1]

saveRDS(beta, "./GWA/mashr/beta_fitNvsS.rds")
saveRDS(se, "./GWA/mashr/se_fitNvsS.rds")
```

At a 0.05 fdr corrected pvalue threshold, there are no significantly associated SNPs. 

To investigate which SNP effects might be significant we run mashr on betas and se. 



```r
beta=readRDS("./GWA/mashr/beta_fitNvsS.rds")
se=readRDS("./GWA/mashr/se_fitNvsS.rds")

##only keep a subset of the traits, as many are highly correlated
##remove traits that are not particularly interesting.


##try mashr
beta=as.matrix(na.omit(beta))
se=as.matrix(na.omit(se))
##
beta=beta[row.names(beta)%in%row.names(se),]
se=se[row.names(se)%in%row.names(beta),]
se=se[row.names(beta),]
data = mash_set_data(beta, se)
## identify a set of strong tests
out1by1=paste("./GWA/mashr/mashr_1by1_fit_NvsS.rds", sep="")
if(file.exists(out1by1)==F){
    m.1by1 = mash_1by1(mash_set_data(data$Bhat,data$Shat))
    saveRDS(m.1by1, out1by1)
}else{
    m.1by1=readRDS(out1by1)
}
strong.set = get_significant_results(m.1by1,0.1)
random.set=sample(1:nrow(data$Bhat), size=50000, replace=F)
data.temp = mash_set_data(data$Bhat[random.set,],data$Shat[random.set,])
Vhat = estimate_null_correlation(data.temp, z_thresh=2, apply_lower_bound=F)
rm(data.temp)
data.random = mash_set_data(data$Bhat[random.set,],data$Shat[random.set,],V=Vhat)
data.strong = mash_set_data(data$Bhat[strong.set,],data$Shat[strong.set,], V=Vhat)
U.pca = cov_pca(data.strong, ncol(beta))
U.ed = cov_ed(data.strong, U.pca)
U.c = cov_canonical(data.random)
m = mash(data.random, Ulist = c(U.ed,U.c), outputlevel = 1)
saveRDS(m, paste("./GWA/mashr/mashr_50000_random_snps_fit_NvsS.rds", sep=""))
m=readRDS(paste("./GWA/mashr/mashr_50000_random_snps_fit_NvsS.rds", sep=""))
m2 = mash(data.strong, g=get_fitted_g(m), fixg=TRUE)
saveRDS(m2, paste("./GWA/mashr/mashr_strongset_fit_NvsS.rds", sep=""))
m2=readRDS(paste("./GWA/mashr/mashr_strongset_fit_NvsS.rds", sep=""))
```

This generates posterior effect size estimates for SNPs it deems significant using adaptive shrinkage. 


```
## [1] "Fig.  3: Heatmap of posterior effect sizes of associated SNPs in fitness in the North and in the South."
```

![](./figures/heatmap_mashr_fit_NvsS.pdf)<!-- -->

This highlights the two main peaks we can see for the GWA of fitness in the South on chromosome 2 and 5. 

The figure shows that those peaks have effects going in the same direction for both traits, which is consitent with the positive correlation we observe between fitness in the North and the South. 

Then we run mlmm using gemma for the two traits simultaniously. We
also make a manhattan plot.



```r
n=1:2
sel=paste(n, collapse=" ")
pheno=paste(colnames(phen)[n], collapse="_")
system(paste("gemma-wrapper --loco --json --cache-dir ./GWA/.gemma-cache --input ./GWA/output_gemma_loco/K.json -- -lmm 4 -g ", pref, ".geno.gz -a ", pref, ".map -p ", phenfile, " -n ", sel , " > ./GWA/output_gemma_loco/out_loco_", pheno, sep=""))

##make a manhattan plot and a heatmap of SNP effects
n=1:2
pheno=paste(colnames(phen)[n], collapse="_")
x=unlist(strsplit(scan(paste("./GWA/output_gemma_loco/out_loco_", pheno,sep=""), what="character")[1], ","))[7]
f=substring(gsub("]]", "", x), 2, (nchar(x)-3))
p=read.delim(f, header=T, sep="\t")
##make a manhattan plot
colnames(p)[3]="pos"
colnames(p)=gsub("p_wald", "pval", colnames(p))
p$score=-log10(p$pval)
p=p[p$af>=0.1,]
p$fdr_pval=p.adjust(p$pval, "fdr")
mlmmsignif=p[p$fdr_pval<=0.1,]
jpeg(paste("./GWA/manhattans/lmm_gwa_", pheno, ".jpeg", sep=""),res=600, unit="in", width=8, height=4, pointsize=12)
manhattan(p)
dev.off()
   

##make the figure with a heatmap for significant SNPs
top=p[p$fdr_pval<=0.1,]

eff=as.matrix(top[,8:9])
row.names(eff)=paste(top$chr, "_", top$pos, sep="")
colnames(eff)=c("North", "South")


source("./scripts/modif_heatmap.R")
x=colorRampPalette(c("Dodgerblue", "gold", "Firebrick"))
r=x(200)

jpeg("./figures/mlmm_gwa_fitNvsS.jpeg", res=600, unit="in", width=8, height=6)
mat=matrix(c(2, rep(3, 3),0, rep(1, 3), rep(4, 2), 8, 7, rep(4, 2), 6, 5), ncol=4, byrow=T)
layout(mat, widths=c(1, 11, 5, 7), heights=c(10, 2, 6, 12))
manhattan(p, layout=F)
legend("topright", "A", cex=2, bty="n", xpd=TRUE)
par(mar=c(4, 4, 1, 1), mgp=c(2, 0.8, 0))
plot(-log10(qunif(seq(0, 1, length=nrow(p)))), -log10(quantile(p$pval, seq(0, 1, length=nrow(p)))), pch=16, col="Dodgerblue", cex=0.8, xlab="uniforme distribution of p-values", ylab="p-values from the mlmm GWA", cex.lab=1.5)
abline(0, 1)
legend("topleft", "B", cex=2, bty="n", xpd=TRUE)
heatmap.3(eff, col=r, label="C", margins=c(8,8))
dev.off()
```


```
## [1] "Fig.  4: Manhattan plot for fitness in the North and South mapped in a multivariate mixed model"
```

<img src="./GWA/manhattans/lmm_gwa_N_fitness_S_fitness.jpeg" width="4800" />

# Investigate allele frequency changes from NSE

DF produced a file in which the allele frequency changes between
initial (sown) and Spring sampling on year two (with associated SE).

_We'll add details about methods later. _

# Comparing GWA top hits and NSE results




Table: Contengency table with percentage of SNPs for which no more extrem allele frequency changes were observed in 10,000 permutations.

         FALSE    TRUE
------  ------  ------
FALSE    61.50   15.53
TRUE     15.24    7.73

The above chunk reads in the table of allele frequency changes between
North and South and outputs a table which gives the proportions of
SNPs for which no more extreme allele frequency changes were observe
in 10,000 permutations.

Basically 7% of the SNPs show changes in allele frequency in both
regions, and 15% in each of the two regions.

The question now is really the number of loci involved.





```r
top=readRDS("./res/mlmm_top_SNPs_fdr10.rds")
af$rs=paste(af$Chromosome, "_", af$Position, sep="")

ov=af[af$rs%in%top$rs,c("rs", "N.mean", "N.sd", "S.mean", "S.sd", "North", "South")]
kable(ov, caption="mlmm top SNPs in the SNE.")
```



Table: mlmm top SNPs in the SNE.

         rs                N.mean        N.sd       S.mean        S.sd   North   South
-------  -----------  -----------  ----------  -----------  ----------  ------  ------
123306   1_26532193     0.0698200   0.0460701    0.0045269   0.0655541      12    4101
222892   2_13093352     0.1102447   0.0074127    0.0947218   0.0164690       0       1
222893   2_13093353     0.1102447   0.0074127    0.0947218   0.0164690       0       1
222903   2_13094317     0.1127447   0.0074127    0.0892958   0.0276781       0       1
222906   2_13094519     0.1127447   0.0074127    0.0892958   0.0276781       0       1
558718   4_876420      -0.0284638   0.0157369   -0.0698668   0.0318081     785       1
646528   5_19695234    -0.0575617   0.0036116    0.0799455   0.1014970     135      19
646529   5_19695243    -0.0575617   0.0036116    0.0799455   0.1014970     135      19
646530   5_19695245    -0.0575617   0.0036116    0.0799455   0.1014970     135      19
646531   5_19695255    -0.0620666   0.0036116    0.0833666   0.1127061      92      13
646532   5_19695336    -0.0569807   0.0015538    0.0837059   0.1072266     166      11

The top SNPs in the two trait GWA, of which most two region are also
associated in single trait GWAs have evolved in the allele frequency
experiment.

The significance of allele frequency changes was done based on 10000
permutations. 


# Get the matrix

The SNP matrix is the same used in the microbiota paper.  The script
prep SNPs starts from the vcf files from Fernando, and produces
different formats, including plink and bimbam (for GWA).

Here we chose to use SNPs with MAF above 0.1, so I recomputed the SNP
matrices and used the prefix sweden\_200\_MAF10.

# Compute haplotype blocks

The SNPs are group into haplotype blocks (or LD blocks) using the
plink --block function. This has a bunch of settings which I'll detail
here:
-  'no-pheno-req' simply allows the function to run on all the
   genotypes in the SNP file, without removing those without
   phenotypes
- 'no-small-max-span' is a modifier that allows to limit the span of
  blocks with very few SNPs. By default, a two snp group can't span
  more than 20kb and a 3 snps block can span more than 30kb.
- --blocks-max-kb allows to limit the maximum size of a block. The
  default is 200kb, but I've set it to 50kb.
- --blocks-min-maf determines to MAF threshold for SNPs considered in
  making the blocks. Default is 0.05 but we've decided to work with
  SNPs at 0.1.
- --blocks-strong-lowci and --blocks-strong-highci sets the CI limits
  of Dprime for considering SNPs in strong LD. The default is 0.7 for the lowest
  CI and at least 0.98 for the highest. I've left the default values.
- --blocks-recomb-highci sets the lowest high CI of dprime value to
  considered historical recombination events.
- --blocks-inform-frac is the lowest percentage of informative SNPs in a
  block. I used th default is 0.95.

Better description of the parameters can be found at
  https://www.cog-genomics.org/plink/1.9/ld#blocks

The jobs can be started using "./scripts/compute\_LD\_blocks.sh"

# Start GWAs on blups

Here I need to run a GWA using mlmm on the Northern and Southern
fitness values. The fitness values for each region corrected for year
and block effects are have been calculated previously. I ran a two
trait GWA, which gave additive coefficients for each SNP for each
trait.

# The ideas on how to use that now are:

- Define the ancestral state of the SNPs and calculate the effects of
  derived alleles to see in which quadrant of the plot they fall.
- investigate the frequency of the SNPs in the founder populations for
  each region.
- Compare with allele frequency changes in the SR experiments
- Match SNPs in this study with regions that may be introgressed from
  neandertal Arabidopsis.
- Match with methylation patterns











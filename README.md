Author: Benjamin Brachi
Date: 06/12/2023 but on-going since 2012 at least. 

# Summary

This repository contains on-going analyses in the study of local adaptation in A. thaliana populations.

The current analyses and results are presented in the slides available here: https://sweden.pages.mia.inra.fr/sweden_adaptation


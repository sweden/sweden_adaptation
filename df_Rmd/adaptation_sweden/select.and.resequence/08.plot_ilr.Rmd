---
title: "08.plot_ilr.Rmd"
author: "Anna Iglokina, some DF edits"
date: "8APR21"
output:
  html_document:
    df_print: paged
---

# Introduction
Compositional data analysis of background group ratios in selected populations.


# Libraries and setup
```{r}

library('coda.base')
library(multcomp)
library("viridis")
library('ggplot2')
library(RColorBrewer)

#path.figures <- '/Users/anna/OneDrive/vienn/daniele/'
path.figures <- "./003.plots/08.plots/"
path.data <- "./001.data/08.data/"
#file.cnts <- '/Users/anna/OneDrive/vienn/daniele/SnRdata.05Feb21.txt'
file.cnts <- "./001.data/02.data/SnRdata.09Aug21.txt"
good.samples = 'good_sample'

# Initial population
s.groups <- c('B',   'C',  'N1',  'N2',  'S1',  'S2')
cnts.init = c(6,  14,  38,  13, 104,  25)
names(cnts.init) <- s.groups
freqs.init = cnts.init / sum(cnts.init)

```

## Functions

```{r}

getPvals <- function(freqs, groups) {
  psi <- coda.base::ilr_basis(ncol(freqs), type = "default")
  rownames(psi) <- colnames(cnts)
  b <- log(freqs) %*% psi
  
  f1 = apply(exp(b %*% t(psi)),1, function(x) {x/sum(x)})
  
  # PCA - to get new ilr basis of principal components 
  b.norm <-  apply(b, 2, function(y) y - mean(y))
  pca.res <- prcomp(b.norm)
  pca.loadings <- psi %*% pca.res$rotation
  
  psi <- psi %*% pca.res$rotation  # new Psi matrix
  
  b <- log(freqs) %*% psi  # New ilr coordinates
  b.init <- log(freqs.init) %*% psi
  
  b.plot <- rbind(b.init, b)
  
  # Standardization
  b.mean <- colMeans(b) 
  b.sd <- apply(b, 2, sd)
  b.init <- (b.init - b.mean) / b.sd
  b <- apply(b, 2, scale)
  
  # Create dataframe
  b.df <- data.frame(b)
  b.df$groups <- 1*groups
  
  # Linear model
  res.lm <- lm(groups ~ ., data = b.df)
  w <- res.lm$coefficients[-1]  # coefficients without intercept
  
  # w <- w / sqrt(sum(w ^ 2))  # not required
  
  loadings <- psi %*% w
  
  # define coefficients of linear function directly
  pvals = sapply(1:nrow(psi), function(i) {
     x <- paste0(paste(psi[i,], colnames(psi), sep = '*'), collapse = '+')
    x = paste(x, '==0')
    tmp <- summary(glht(res.lm, linfct = x))
    tmp$test$pvalues[[1]]
  })
  
  names(pvals) <- rownames(psi)
  return(list(pvals = pvals, w = w, psi = psi))
}

getCounts <- function(x.use){
  cnts = c()
  for(i in 1:nrow(uniq.combo)) {
    idx = (x.use$site == uniq.combo[i, 1]) & (x.use$plot == uniq.combo[i, 2])
    tmp = table(x.use[idx, 'Kname'])
    
    cnts = rbind(cnts, tmp[s.groups])
  }
  return(cnts)
}
```


# Get frequencies
```{r pvals all}

# Read counts
x = read.table(file.cnts, header = T)
x.good <- x[x$note == "good_sample",]

# Get groups
uniq.combo = unique(x[,3:4])
groups <- uniq.combo$site %in% c('Barsta', 'AdalEast')

# Get counts
cnts.all <- getCounts(x)
cnts.good <- getCounts(x.good)
cnts.remain = cnts.all - cnts.good

# Replace zeros
cnts = cnts.all
cnts[cnts == 0] = 0.5

# Get frequencies
freqs = cnts / rowSums(cnts)

pvals <- getPvals(freqs, groups)
print(pvals)

```

```{r pvals good}
# Replace zeros
cnts = cnts.good
cnts[cnts == 0] = 0.5

# Get frequencies
freqs = cnts / rowSums(cnts)

pvals <- getPvals(freqs, groups)
print(pvals)

```



# Change percentages of "non-good" samples
```{r change percentages}


pvals.all <- c()  
for(i in 0:100){

  cnts <- round(cnts.good + round(i/100 * cnts.remain))
  cnts[cnts == 0] = 0.5
  
  freqs <- cnts / rowSums(cnts)
  res <- getPvals(freqs, groups)
  pvals <- res$pvals
  pvals.all <- rbind(pvals.all, cbind(pvals, 1:length(pvals), rep(i, length(pvals))))
}

pvals.all = as.data.frame(pvals.all)
colnames(pvals.all) <- c('pvals', 'groups', 'percent')
pvals.all$groups <- s.groups[pvals.all$groups]

p <- ggplot(pvals.all, aes(x = percent, y=pvals, color = groups)) + 
      geom_line(show.legend = FALSE) + geom_point() +
    # scale_color_viridis(discrete=TRUE) +
    theme_bw()
p 

pdf(paste(path.figures, 'pval_changing.pdf', sep = ''), 
    width = 6, height = 3) 
print(p)
dev.off()

# output pvals for ms figure
save(pvals.all, file=paste(path.data, "pvals.all.Rdat", sep=''))

```
```{r sig pvals}
sig.pvals <- pvals.all[pvals.all$pvals<=0.05,]
min.sig.pvals <- aggregate(sig.pvals$percent, by=list(sig.pvals$groups), min)
colnames(min.sig.pvals) <- c("group", "per.included")

min.sig.pvals

```


# Plot
```{r cda plots}

# Produce CDA plot for "with bad samples"
# or with good

# ----- Get data -----
 cnts <- cnts.all
# cnts <- cnts.good
# cnts <- cnts.good + round(cnts.remain * 0.25)
cnts[cnts == 0] = 0.5
freqs <- cnts / rowSums(cnts)
psi <- coda.base::ilr_basis(ncol(freqs), type = "default")
rownames(psi) <- colnames(cnts)
b <- log(freqs) %*% psi

f1 = apply(exp(b %*% t(psi)),1, function(x) {x/sum(x)})

# PCA - to get new ilr basis of principal components 
b.norm <-  apply(b, 2, function(y) y - mean(y))
pca.res <- prcomp(b.norm)
pca.loadings <- psi %*% pca.res$rotation

psi <- psi %*% pca.res$rotation  # new Psi matrix

b <- log(freqs) %*% psi  # New ilr coordinates
b.init <- log(freqs.init) %*% psi


  
# Standardization
b.mean <- colMeans(b) 
b.sd <- apply(b, 2, sd)
b.init <- (b.init - b.mean) / b.sd
b <- apply(b, 2, scale)

b.plot <- rbind(b, b.init)

# ------------------------------
# Find the first projection

# Create dataframe
b.df <- data.frame(b)
b.df$groups <- 1*groups

# Linear model
res.lm <- lm(groups ~ ., data = b.df)
w <- res.lm$coefficients[-1]  # coefficients without intercept

w <- as.matrix(w / sqrt(sum(w ^ 2)))  # not required


# Projections of b.plot on w - is the first coordinate
b.proj = as.matrix(apply(b.plot, 1, function(x) sum(x*w)))
b.proj.vec = b.proj %*% t(w)
b.ort <- b.plot - b.proj.vec

b.ort.proj = as.matrix(apply(b.ort, 1, function(x) sum(x*w))) # <- check that all are small
if(sum(b.ort.proj > 10^(-10)) != 0) stop('Something is going wrong')

# ------------------------------
# Find the second projection - from PCA, 
# because the all differences between groups are in the first coordinate

pca.ort <- prcomp(b.ort)




b.plot <- data.frame(cbind(b.proj, pca.ort$x[,1]))
b.plot$group <- c(as.character(uniq.combo$site), 'init')
colnames(b.plot)

p <- ggplot(b.plot, aes(X1, X2, colour = group)) + 
  geom_point(size = 4) + theme_bw()


#pdf(paste(path.figures, 'separation_plot_all.pdf', sep = ''), 
#    width = 4, height = 3) 
#print(p)
#dev.off()

pdf(paste(path.figures, 'separation_plot_good.pdf', sep = ''), 
    width = 4, height = 3) 
print(p)
dev.off()


save(b.plot, file=paste(path.data, "b.plot.all.Rdat", sep=''))
#save(b.plot, file=paste(path.data, "b.plot.good.Rdat", sep=''))

```


# Plot Just PCA
```{r pca plots}

# Produce CDA plot for "with bad samples"

# ----- Get data -----
cnts <- cnts.all
# cnts <- cnts.good
cnts[cnts == 0] = 0.5
freqs <- cnts / rowSums(cnts)
psi <- coda.base::ilr_basis(ncol(freqs), type = "default")
rownames(psi) <- colnames(cnts)
b <- log(freqs) %*% psi

f1 = apply(exp(b %*% t(psi)),1, function(x) {x/sum(x)})

# PCA - to get new ilr basis of principal components 
b.norm <-  apply(b, 2, function(y) y - mean(y))
pca.res <- prcomp(b.norm)
pca.loadings <- psi %*% pca.res$rotation

psi <- psi %*% pca.res$rotation  # new Psi matrix

b <- log(freqs) %*% psi  # New ilr coordinates
b.init <- log(freqs.init) %*% psi


# Standardization
b.mean <- colMeans(b) 
b.sd <- apply(b, 2, sd)
b.init <- (b.init - b.mean) / b.sd
b <- apply(b, 2, scale)

b.plot <- rbind(b, b.init)

pca.2 <- prcomp(b.plot)




b.plot <- data.frame(pca.2$x[,1:2])
b.plot$group <- c(as.character(uniq.combo$site), 'init')


p1 <- ggplot(b.plot, aes(PC1, PC2, colour = group)) + 
  geom_point(size = 4) + theme_bw()

pdf(paste(path.figures, 'pca_plot_all.pdf', sep = ''), 
    width = 4, height = 3) 
print(p1)
dev.off()
```



# Plot combination
```{r combination plots}

groups.combo <- c()
plot.combo <- c()
frews.combo <- c()  
for(i in 0:100){

  cnts <- round(cnts.good + round(i/100 * cnts.remain))
  cnts[cnts == 0] = 0.5

  freqs <- cnts / rowSums(cnts)

  frews.combo <- rbind(frews.combo, freqs)
  groups.combo <- c(groups.combo, as.character(uniq.combo$site))
  plot.combo <- c(plot.combo, uniq.combo$plot)
}



# Produce CDA plot for "with bad samples"

# ----- Get data -----
cnts <- cnts.all
# cnts <- cnts.good
cnts[cnts == 0] = 0.5
freqs <- cnts / rowSums(cnts)
psi <- coda.base::ilr_basis(ncol(freqs), type = "default")
rownames(psi) <- colnames(cnts)
b <- log(freqs) %*% psi

f1 = apply(exp(b %*% t(psi)),1, function(x) {x/sum(x)})

# PCA - to get new ilr basis of principal components 
b.norm <-  apply(b, 2, function(y) y - mean(y))
pca.res <- prcomp(b.norm)
pca.loadings <- psi %*% pca.res$rotation

psi <- psi %*% pca.res$rotation  # new Psi matrix

b <- log(freqs) %*% psi  # New ilr coordinates
b.init <- log(rbind(freqs.init, frews.combo)) %*% psi



# Standardization
b.mean <- colMeans(b) 
b.sd <- apply(b, 2, sd)
b.init <- (b.init - b.mean) / b.sd
b <- apply(b, 2, scale)

b.plot <- rbind(b, b.init)

# ------------------------------
# Find the first projection

# Create dataframe
b.df <- data.frame(b)
b.df$groups <- 1*groups

# Linear model
res.lm <- lm(groups ~ ., data = b.df)
w <- res.lm$coefficients[-1]  # coefficients without intercept

w <- as.matrix(w / sqrt(sum(w ^ 2)))  # not required


# Projections of b.plot on w - is the first coordinate
b.proj = as.matrix(apply(b.plot, 1, function(x) sum(x*w)))
b.proj.vec = b.proj %*% t(w)
b.ort <- b.plot - b.proj.vec

b.ort.proj = as.matrix(apply(b.ort, 1, function(x) sum(x*w))) # <- check that all are small
if(sum(b.ort.proj > 10^(-10)) != 0) stop('Something is going wrong')

# ------------------------------
# Find the second projection - from PCA, 
# because the all differences between groups are in the first coordinate

pca.ort <- prcomp(b.ort)




b.plot <- data.frame(cbind(b.proj, pca.ort$x[,1]))
b.plot$group <- c(as.character(uniq.combo$site), 'init', groups.combo)
b.plot$plot <- c(as.character(uniq.combo$plot), '0',plot.combo)
colnames(b.plot)

ggplot(b.plot[1:34,], aes(X1, X2, colour = group, group = interaction(group, plot))) + 
  geom_line() + geom_point() + theme_bw()




```





